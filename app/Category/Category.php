<?php

namespace App\Category;

use Illuminate\Database\Eloquent\Model;

use App\Util;


class Category extends Model
{
    protected $table = 'categories';
    protected $primaryKey = 'c_id';

    public function format()
    {
        return [
            'id' => $this->c_id,
            'code' => $this->c_code,
            'name' => $this->c_name,
            'html' => $this->c_html,
            'keywords' => $this->c_keywords,
            'meta_title' => $this->c_meta_title,
            'meta_desc' => $this->c_meta_desc,
            'h1' => $this->c_h1,
            'h2' => $this->c_h2,
            'p' => $this->c_p,
            'filename' => $this->c_image_filename,
            'extension' => $this->c_image_ext,
            'alt' => $this->c_image_alt,
            'products' => $this->products()
            ->orderBy('p_order')
            ->where('active','1')
            // ->where(function($query) {
            //     $query->where('p_coming_soon','!=',1)
            //     ->orWhereNull('p_coming_soon');
            // })
            ->get()->map->format(),
            'created_at' => $this->created_at->diffForHumans(),

        ];
    }

    public function formatWithComingSoon()
    {
        return [
            'id' => $this->c_id,
            'code' => $this->c_code,
            'name' => $this->c_name,
            'html' => $this->c_html,
            'keywords' => $this->c_keywords,
            'meta_title' => $this->c_meta_title,
            'meta_desc' => $this->c_meta_desc,
            'h1' => $this->c_h1,
            'h2' => $this->c_h2,
            'p' => $this->c_p,
            'filename' => $this->c_image_filename,
            'extension' => $this->c_image_ext,
            'alt' => $this->c_image_alt,
            'products' => $this->products()
            ->orderBy('p_order')
            ->where('active','1')
            ->get()->map->format(),
            'created_at' => $this->created_at->diffForHumans(),
            'parent' => $this->parent()->get()->map->formatNoProducts()->first(),

        ];
    }

    public function formatNoComingSoon()
    {
        return [
            'id' => $this->c_id,
            'code' => $this->c_code,
            'name' => $this->c_name,
            'html' => $this->c_html,
            'keywords' => $this->c_keywords,
            'meta_title' => $this->c_meta_title,
            'meta_desc' => $this->c_meta_desc,
            'h1' => $this->c_h1,
            'h2' => $this->c_h2,
            'p' => $this->c_p,
            'filename' => $this->c_image_filename,
            'extension' => $this->c_image_ext,
            'alt' => $this->c_image_alt,
            'products' => $this->products()
            ->orderBy('p_order')
            ->where('active','1')
            ->where(function($query) {
                $query->where('p_coming_soon','!=',1)
                ->orWhereNull('p_coming_soon');
            })
            ->get()->map->format(),
            'created_at' => $this->created_at->diffForHumans(),
            'parent' => $this->parent()->get()->map->formatNoProducts()->first(),

        ];
    }

    public function formatGrandParent()
    {
        return [
            'id' => $this->c_id,
            'code' => $this->c_code,
            'name' => $this->c_name,
            'html' => $this->c_html,
            'keywords' => $this->c_keywords,
            'meta_title' => $this->c_meta_title,
            'meta_desc' => $this->c_meta_desc,
            'h1' => $this->c_h1,
            'h2' => $this->c_h2,
            'p' => $this->c_p,
            'filename' => $this->c_image_filename,
            'extension' => $this->c_image_ext,
            'alt' => $this->c_image_alt,
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
       
    
    public function formatNoProducts()
    {
        return [
            'id' => $this->c_id,
            'code' => $this->c_code,
            'name' => $this->c_name,
            'html' => $this->c_html,
            'keywords' => $this->c_keywords,
            'meta_title' => $this->c_meta_title,
            'meta_desc' => $this->c_meta_desc,
            'h1' => $this->c_h1,
            'h2' => $this->c_h2,
            'p' => $this->c_p,
            'filename' => $this->c_image_filename,
            'extension' => $this->c_image_ext,
            'order' => $this->c_order,
            'alt' => $this->c_image_alt,
            'created_at' => $this->created_at->diffForHumans(),
            'parent' => $this->parent()->get()->map->formatNoProducts()->first(),
        ];
    }


    public function formatNav()
    {
        return [
            'id' => $this->c_id,
            'code' => $this->c_code,
            'name' => $this->c_name,
            'html' => $this->c_html,
            'keywords' => $this->c_keywords,
            'meta_title' => $this->c_meta_title,
            'meta_desc' => $this->c_meta_desc,
            'h1' => $this->c_h1,
            'h2' => $this->c_h2,
            'p' => $this->c_p,
            'filename' => $this->c_image_filename,
            'extension' => $this->c_image_ext,
            'alt' => $this->c_image_alt,
            'created_at' => $this->created_at->diffForHumans(),
            
            'active' => $this->active,

            'children' => $this->children()->orderBy('c_order')->where('active','1')->get()->map->formatWithComingSoon(),

        ];
    }


    public function formatAllProductsPage()
    {
        return [
            'id' => $this->c_id,
            'code' => $this->c_code,
            'name' => $this->c_name,
            'html' => $this->c_html,
            'keywords' => $this->c_keywords,
            'meta_title' => $this->c_meta_title,
            'meta_desc' => $this->c_meta_desc,
            'h1' => $this->c_h1,
            'h2' => $this->c_h2,
            'p' => $this->c_p,
            'filename' => $this->c_image_filename,
            'extension' => $this->c_image_ext,
            'alt' => $this->c_image_alt,
            'created_at' => $this->created_at->diffForHumans(),
            
            'active' => $this->active,

            'children' => $this->children()->orderBy('c_order')->where('active','1')->get()->map->formatWithComingSoon(),

        ];
    }



    public function formatCategoryPage()
    {
        return [
            'id' => $this->c_id,
            'code' => $this->c_code,
            'name' => $this->c_name,
            'html' => $this->c_html,
            'keywords' => $this->c_keywords,
            'meta_title' => $this->c_meta_title,
            'meta_desc' => $this->c_meta_desc,
            'h1' => $this->c_h1,
            'h2' => $this->c_h2,
            'p' => $this->c_p,
            'filename' => $this->c_image_filename,
            'extension' => $this->c_image_ext,
            'alt' => $this->c_image_alt,
            'created_at' => $this->created_at->diffForHumans(),
            
            'active' => $this->active,
            'parent' => $this->parent()->get()->map->formatNoProducts()->first(),

            'products' => $this->products()
            ->orderBy('p_order')
            ->where('active','1')
            // ->where(function($query) {
            //     $query->where('p_coming_soon','!=',1)
            //     ->orWhereNull('p_coming_soon');
            // })
            ->get()->map->format(),
            'children' => $this->children()->orderBy('c_order')->where('active','1')->get()->map->formatWithComingSoon(),

        ];
    }



    public function formatAddProduct()
    {
        return [
            'id' => $this->c_id,
            'code' => $this->c_code,
            'name' => $this->c_name,
            'html' => $this->c_html,
            'keywords' => $this->c_keywords,
            'meta_title' => $this->c_meta_title,
            'meta_desc' => $this->c_meta_desc,
            'h1' => $this->c_h1,
            'h2' => $this->c_h2,
            'p' => $this->c_p,
            'filename' => $this->c_image_filename,
            'extension' => $this->c_image_ext,
            'alt' => $this->c_image_alt,
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
        
    public function formatDownloadsPage()
    {
        // return [
        //     'id' => $this->c_id,
        //     'code' => $this->c_code,
        //     'name' => $this->c_name,
        //     'html' => $this->c_html,
        //     'filename' => $this->c_image_filename,
        //     'extension' => $this->c_image_ext,
        //     'alt' => $this->c_image_alt,
        //     'products' => $this->products()
        //     ->orderBy('p_order')
        //     ->where(function($query) {
        //         $query->where('p_coming_soon','!=',1)
        //         ->orWhereNull('p_coming_soon');
        //     })
        //     ->where('active','1')
        //     ->get()->map->formatDownloadsPage(),
        //     'created_at' => $this->created_at->diffForHumans(),
            
        //     'parent' => $this->parent()->get()->map->formatNoProducts()->first(),
        // ];

        return [
            'id' => $this->c_id,
            'code' => $this->c_code,
            'name' => $this->c_name,
            'html' => $this->c_html,
            'keywords' => $this->c_keywords,
            'meta_title' => $this->c_meta_title,
            'meta_desc' => $this->c_meta_desc,
            'h1' => $this->c_h1,
            'h2' => $this->c_h2,
            'p' => $this->c_p,
            'filename' => $this->c_image_filename,
            'extension' => $this->c_image_ext,
            'alt' => $this->c_image_alt,
            'created_at' => $this->created_at->diffForHumans(),
            
            'active' => $this->active,

            'children' => $this->children()->orderBy('c_order')->where('active','1')->get()->map->formatWithComingSoon(),

        ];        

    }



    public function products()
    {
        return $this->hasMany('App\Product\Product', 'p_c_id', 'c_id');
    }    

    public function parent()
    {
        return $this->belongsTo('App\Category\Category', 'c_c_id', 'c_id');
    }    

    public function children()
    {
        return $this->hasMany('App\Category\Category', 'c_c_id', 'c_id');
    }    

}