<?php

namespace App\Interfaces\Category;

interface CategoryInterface
{
    public function all($limit);

    public function find($code);

}
