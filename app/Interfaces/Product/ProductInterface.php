<?php

namespace App\Interfaces\Product;

interface ProductInterface
{
    public function all($limit);

    public function find($code);
    
    public function create($data);
}
