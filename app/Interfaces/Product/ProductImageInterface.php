<?php

namespace App\Interfaces\Product;

interface ProductImageInterface
{
    public function find($data);
}
