<?php

namespace App\Interfaces\Product;

interface ProductCertificateInterface
{

    public function find($code);
}
