<?php

namespace App\Interfaces\Blog;

interface BlogInterface
{
    public function all($limit);

    public function find($code);
    
    public function create($data);
}
