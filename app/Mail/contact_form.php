<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


use App\Repositories\Product\ProductRepository;



class Contact extends Mailable
{
    use Queueable, SerializesModels;
    protected $request;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->user_details = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($this->user_details);

        $pi = new ProductRepository;

        $products = $pi->all();

        $p_names = [];

        foreach($products as $pnvalue){
            // dd($pnvalue['p_name']);
            $p_names[] = [
                'p_checkbox' => $pnvalue['p_name'].'_b2b',
                'p_quantity' => $pnvalue['p_name'].'_qty',
                'p_name' => $pnvalue['p_name']
            ];
        }

            // dd($p_names);

        return $this->markdown('mails.contactus')
                    ->subject( 'New Contact Form Message')
                    ->with([
                        'user_details' => $this->user_details,
                        'p_names' => $p_names
                    ]);
    }
}
