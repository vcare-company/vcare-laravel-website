<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Interfaces\Product\ProductInterface;



class RespiratorController extends Controller
{

	/**
	 * Initialize Interface
	 */
	public function __construct(ProductInterface $pi)
	{
		$this->pi = $pi;
	}



    public function respiratorComparison()
    {
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Products','route'=>'all-products'],
            ['title'=>'Respirator Comparison','route'=>''],
        ];

        $maskTypeTable = [];
        $maskTypeTable[] = [
            'title'=>'Overview',
            'ffp1'=>'Protects against large solid particles, irritating but not harmful substances. Minimum filter efficiency 78%. FFP1 uses too wide filter for filtering the air, so for breathing in, it won’t stop the virus from penetrating the mask, but for breathing out, it stops the aerosol and small water droplets which often contain most of the virus. Protects surrounding people but not the wearer. FFP1 is equivalent to a basic Surgical Mask.',
            'ffp2'=>'Masks protects the person and surrounding people. FFP2 protects against solid and liquid irritating aerosols. Minimum filter efficiency of 92%. Protects the wearer and surrounding people. FFP2 is equivalent to N95 and KN95.',
            'ffp3'=>'Protects against solid and liquid toxic aerosols. Minimum filter efficiency of 98%. FFP3 advisable for high levels of airborne particles and toxic conditions. Reduces amount of dust by 20.',
            'mask'=>'This is a very basic mask, and people wear it when they are experiencing mild symptoms and are worried to infect other people, so this help not infect other people, but if you do not mild symptoms or do not have viral infection and you were it, it does not protect you from others around you who might have symptoms or a viral infection as it can pass through the mask when breathing in.',
        ];
        $maskTypeTable[] = [
            'title'=>'Protection',
            'ffp1'=>'Respirator protects the person wearing it, but not the surrounding people',
            'ffp2'=>'Respirator protects the person wearing it and the surrounding people.',
            'ffp3'=>'Heavy duty protection for everyone',
            'mask'=>'Basic mask, and people wear it when they are experiencing mild symptoms',
        ];
        $maskTypeTable[] = [
            'title'=>'Dust Reduction',
            'ffp1'=>'By 4',
            'ffp2'=>'By 10',
            'ffp3'=>'By 20',
            'mask'=>'None',
        ];
        $maskTypeTable[] = [
            'title'=>'Prevents',
            'ffp1'=>'It stops the aerosol and small water droplets which often contain most of the virus.',
            'ffp2'=>'Protects against solid and liquid irritating aerosols.',
            'ffp3'=>'Heavy duty protection for everyone',
            'mask'=>'It does not protect you from others around you who might have symptoms or a viral infection.',
        ];
        $maskTypeTable[] = [
            'title'=>'Usage',
            'ffp1'=>'Agriculture, Cleaning Works',
            'ffp2'=>'Health Care - Sanitaries, Service Sectors',
            'ffp3'=>'Health Care - Sanitaries, Service Sectors',
            'mask'=>'Health Care - Sanitaries',
        ];




        $maskTable = [];    

        $maskTable['ffp2'] = [];

        $code = 'ffp2-ross';
        $maskTable['ffp2'][] = [
            'mask-reference-no'         =>  'ROSS 2B',
            'mask-reference-no-image'   =>  'vcare-earth-ffp2-respirator-ross.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Foldable',
            'wearing-style'             =>  'Earloop',
            'usage'                     =>  'Health Care - Sanitaries, Service Sectors',
        ];

        
        $code = 'ffp2-max';
        $maskTable['ffp2'][] = [
            'mask-reference-no'         =>  'MAX 4B',
            'mask-reference-no-image'   =>  'vcare-earth-ffp2-respirator-max.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Foldable',
            'wearing-style'             =>  'Earloop',
            'usage'                     =>  'Health Care - Sanitaries, Service Sectors',
        ];
        
        $code = 'ffp2-ann';
        $maskTable['ffp2'][] = [
            'mask-reference-no'         =>  'ANN 5B',
            'mask-reference-no-image'   =>  'vcare-earth-ffp2-respirator-ann.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Foldable',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry, Health Care - Sanitaries, Service Sectors',
        ];
        
        $code = 'ffp2-jane';
        $maskTable['ffp2'][] = [
            'mask-reference-no'         =>  'JANE 7B',
            'mask-reference-no-image'   =>  'vcare-earth-ffp2-respirator-jane.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Foldable',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry, Health Care - Sanitaries, Service Sectors',
        ];
        
        $code = 'ffp2-kent';
        $maskTable['ffp2'][] = [
            'mask-reference-no'         =>  'KENT 9B',
            'mask-reference-no-image'   =>  'vcare-earth-ffp2-respirator-kent.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Fish Type',
            'wearing-style'             =>  'Earloop',
            'usage'                     =>  'Industry, Health Care - Sanitaries',
        ];
        
        $code = 'ffp2-kai';
        $maskTable['ffp2'][] = [
            'mask-reference-no'         =>  'KAI 10B',
            'mask-reference-no-image'   =>  'vcare-earth-ffp2-respirator-kai.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Cup Type',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry, Health Care - Sanitaries',
        ];
        
        $code = 'ffp2-dax';
        $maskTable['ffp2'][] = [
            'mask-reference-no'         =>  'DAX 11B',
            'mask-reference-no-image'   =>  'vcare-earth-ffp2-respirator-dax.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Cup Type with Valve',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry',
        ];




        $maskTable['ffp3'] = [];
        
        $code = 'ffp3-star';
        $maskTable['ffp3'][] = [
            'mask-reference-no'         =>  'STAR 2C',
            'mask-reference-no-image'   =>  'vcare-earth-ffp3-respirator-star.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Cup Type with Valve',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry',
        ];
        
        $code = 'ffp3-tani';
        $maskTable['ffp3'][] = [
            'mask-reference-no'         =>  'TANI 3C',
            'mask-reference-no-image'   =>  'vcare-earth-ffp3-respirator-tani.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Cup Type',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry, Health Care - Sanitaries, Service Sectors',
        ];
        
        $code = 'ffp3-jojo';
        $maskTable['ffp3'][] = [
            'mask-reference-no'         =>  'JOJO 4C',
            'mask-reference-no-image'   =>  'vcare-earth-ffp3-respirator-jojo.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Duck Bill Type',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Health Care - Sanitaries, Service Sectors',
        ];
        
        $code = 'ffp3-luna';
        $maskTable['ffp3'][] = [
            'mask-reference-no'         =>  'LUNA 5C',
            'mask-reference-no-image'   =>  'vcare-earth-ffp3-respirator-luna.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Duck Bill Type with Valve',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Health Care - Sanitaries',
        ];
        
        $code = 'ffp3-skye';
        $maskTable['ffp3'][] = [
            'mask-reference-no'         =>  'SKYE 6C',
            'mask-reference-no-image'   =>  'vcare-earth-ffp3-respirator-skye.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Duck Bill Foldable',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Health Care - Sanitaries',
        ];
        
        $code = 'ffp3-tia';
        $maskTable['ffp3'][] = [
            'mask-reference-no'         =>  'TIA 7C',
            'mask-reference-no-image'   =>  'vcare-earth-ffp3-respirator-tia.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Duck Bill Foldable with Valve',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry',
        ];
        
        $code = 'ffp3-wei';
        $maskTable['ffp3'][] = [
            'mask-reference-no'         =>  'WEI 8C',
            'mask-reference-no-image'   =>  'vcare-earth-ffp3-respirator-wei.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Fish Type with Valve',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry, Health Care - Sanitaries',
        ];



        return view('respirator-comparison')
        ->with('maskTypeTable',$maskTypeTable)
        ->with('maskTable',$maskTable)
        ->with('breadcrumbs',$breadcrumbs);

    }
   







    public function respiratorPromotion()
    {

        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Products','route'=>'all-products'],
            ['title'=>'Respirator Comparison','route'=>''],
        ];

        $maskTypeTable = [];
        $maskTypeTable[] = [
            'title'=>'Overview',
            'ffp1'=>'Protects against large solid particles, irritating but not harmful substances. Minimum filter efficiency 78%. FFP1 uses too wide filter for filtering the air, so for breathing in, it won’t stop the virus from penetrating the mask, but for breathing out, it stops the aerosol and small water droplets which often contain most of the virus. Protects surrounding people but not the wearer. FFP1 is equivalent to a basic Surgical Mask.',
            'ffp2'=>'Masks protects the person and surrounding people. FFP2 protects against solid and liquid irritating aerosols. Minimum filter efficiency of 92%. Protects the wearer and surrounding people. FFP2 is equivalent to N95 and KN95.',
            'ffp3'=>'Protects against solid and liquid toxic aerosols. Minimum filter efficiency of 98%. FFP3 advisable for high levels of airborne particles and toxic conditions. Reduces amount of dust by 20.',
            'mask'=>'This is a very basic mask, and people wear it when they are experiencing mild symptoms and are worried to infect other people, so this help not infect other people, but if you do not mild symptoms or do not have viral infection and you were it, it does not protect you from others around you who might have symptoms or a viral infection as it can pass through the mask when breathing in.',
        ];
        $maskTypeTable[] = [
            'title'=>'Protection',
            'ffp1'=>'Respirator protects the person wearing it, but not the surrounding people',
            'ffp2'=>'Respirator protects the person wearing it and the surrounding people.',
            'ffp3'=>'Heavy duty protection for everyone',
            'mask'=>'Basic mask, and people wear it when they are experiencing mild symptoms',
        ];
        $maskTypeTable[] = [
            'title'=>'Dust Reduction',
            'ffp1'=>'By 4',
            'ffp2'=>'By 10',
            'ffp3'=>'By 20',
            'mask'=>'None',
        ];
        $maskTypeTable[] = [
            'title'=>'Prevents',
            'ffp1'=>'It stops the aerosol and small water droplets which often contain most of the virus.',
            'ffp2'=>'Protects against solid and liquid irritating aerosols.',
            'ffp3'=>'Heavy duty protection for everyone',
            'mask'=>'It does not protect you from others around you who might have symptoms or a viral infection.',
        ];
        $maskTypeTable[] = [
            'title'=>'Usage',
            'ffp1'=>'Agriculture, Cleaning Works',
            'ffp2'=>'Health Care - Sanitaries, Service Sectors',
            'ffp3'=>'Health Care - Sanitaries, Service Sectors',
            'mask'=>'Health Care - Sanitaries',
        ];




        $maskTable = [];    

        $maskTable['ffp2'] = [];

        $code = 'ffp2-ross';
        $maskTable['ffp2'][] = [
            'mask-reference-no'         =>  'ROSS 2B',
            'mask-reference-no-image'   =>  'vcare-earth-ffp2-respirator-ross.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Foldable',
            'wearing-style'             =>  'Earloop',
            'usage'                     =>  'Health Care - Sanitaries, Service Sectors',
        ];

        
        $code = 'ffp2-max';
        $maskTable['ffp2'][] = [
            'mask-reference-no'         =>  'MAX 4B',
            'mask-reference-no-image'   =>  'vcare-earth-ffp2-respirator-max.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Foldable',
            'wearing-style'             =>  'Earloop',
            'usage'                     =>  'Health Care - Sanitaries, Service Sectors',
        ];
        
        $code = 'ffp2-ann';
        $maskTable['ffp2'][] = [
            'mask-reference-no'         =>  'ANN 5B',
            'mask-reference-no-image'   =>  'vcare-earth-ffp2-respirator-ann.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Foldable',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry, Health Care - Sanitaries, Service Sectors',
        ];
        
        $code = 'ffp2-jane';
        $maskTable['ffp2'][] = [
            'mask-reference-no'         =>  'JANE 7B',
            'mask-reference-no-image'   =>  'vcare-earth-ffp2-respirator-jane.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Foldable',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry, Health Care - Sanitaries, Service Sectors',
        ];
        
        $code = 'ffp2-kent';
        $maskTable['ffp2'][] = [
            'mask-reference-no'         =>  'KENT 9B',
            'mask-reference-no-image'   =>  'vcare-earth-ffp2-respirator-kent.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Fish Type',
            'wearing-style'             =>  'Earloop',
            'usage'                     =>  'Industry, Health Care - Sanitaries',
        ];
        
        $code = 'ffp2-kai';
        $maskTable['ffp2'][] = [
            'mask-reference-no'         =>  'KAI 10B',
            'mask-reference-no-image'   =>  'vcare-earth-ffp2-respirator-kai.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Cup Type',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry, Health Care - Sanitaries',
        ];
        
        $code = 'ffp2-dax';
        $maskTable['ffp2'][] = [
            'mask-reference-no'         =>  'DAX 11B',
            'mask-reference-no-image'   =>  'vcare-earth-ffp2-respirator-dax.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Cup Type with Valve',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry',
        ];




        $maskTable['ffp3'] = [];
        
        $code = 'ffp3-star';
        $maskTable['ffp3'][] = [
            'mask-reference-no'         =>  'STAR 2C',
            'mask-reference-no-image'   =>  'vcare-earth-ffp3-respirator-star.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Cup Type with Valve',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry',
        ];
        
        $code = 'ffp3-tani';
        $maskTable['ffp3'][] = [
            'mask-reference-no'         =>  'TANI 3C',
            'mask-reference-no-image'   =>  'vcare-earth-ffp3-respirator-tani.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Cup Type',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry, Health Care - Sanitaries, Service Sectors',
        ];
        
        $code = 'ffp3-jojo';
        $maskTable['ffp3'][] = [
            'mask-reference-no'         =>  'JOJO 4C',
            'mask-reference-no-image'   =>  'vcare-earth-ffp3-respirator-jojo.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Duck Bill Type',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Health Care - Sanitaries, Service Sectors',
        ];
        
        $code = 'ffp3-luna';
        $maskTable['ffp3'][] = [
            'mask-reference-no'         =>  'LUNA 5C',
            'mask-reference-no-image'   =>  'vcare-earth-ffp3-respirator-luna.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Duck Bill Type with Valve',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Health Care - Sanitaries',
        ];
        
        $code = 'ffp3-skye';
        $maskTable['ffp3'][] = [
            'mask-reference-no'         =>  'SKYE 6C',
            'mask-reference-no-image'   =>  'vcare-earth-ffp3-respirator-skye.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Duck Bill Foldable',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Health Care - Sanitaries',
        ];
        
        $code = 'ffp3-tia';
        $maskTable['ffp3'][] = [
            'mask-reference-no'         =>  'TIA 7C',
            'mask-reference-no-image'   =>  'vcare-earth-ffp3-respirator-tia.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Duck Bill Foldable with Valve',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry',
        ];
        
        $code = 'ffp3-wei';
        $maskTable['ffp3'][] = [
            'mask-reference-no'         =>  'WEI 8C',
            'mask-reference-no-image'   =>  'vcare-earth-ffp3-respirator-wei.png',
            'main-image'                =>  $this->getProductMainImage($code),
            'code'                      =>  $code,
            'standard'                  =>  'EN 149:2001+A1+2009',
            'product-type'              =>  'Fish Type with Valve',
            'wearing-style'             =>  'Headband',
            'usage'                     =>  'Industry, Health Care - Sanitaries',
        ];



        return view('respirator-promotion')
        ->with('maskTypeTable',$maskTypeTable)
        ->with('maskTable',$maskTable)
        ->with('breadcrumbs',$breadcrumbs);


    }
   







    private function getProductMainImage($product_code){
        
        $product = $this->pi->find($product_code)->map->format()->first();
        
        if($product && $product['main-image']) {            
            $retVal = 'files/products/'.$product['public_id'].'/images/thumbnails/main/'.$product['main-image']['filename'].'.'.$product['main-image']['ext'];
        }
        else
        $retVal = '/files/logos/vcareplaceholder.svg';


        return $retVal;
    }


}