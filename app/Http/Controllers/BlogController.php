<?php

namespace App\Http\Controllers;

use App\Interfaces\Blog\BlogInterface;



class BlogController extends Controller
{

	/**
	 * Initialize Interface
	 */
	public function __construct(BlogInterface $bi)
	{
		$this->bi = $bi;
	}

	function blog(){
		// $blogs = $this->bi->all(3)->items->map->format();

		$blogsPaginated = $this->bi->all(10);
		$blogs = $blogsPaginated->getCollection()->map->format();

		// foreach($blogsPaginated as &$bp){
		// 	$bp
		// }


        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Blog','route'=>''],
        ];		
		
		return view('blog.blog')
		->with('blogsPaginated',$blogsPaginated)
		->with('blogs',$blogs)
		->with('breadcrumbs',$breadcrumbs);
	}

	function blogSinglePage($blog_public_id, $blog_title){
		$blog = $this->bi->find($blog_public_id)->map->format()->first();

		$title = $blog['title'];
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Blog','route'=>'blog'],
            ['title'=>$title,'route'=>''],
        ];				
		
		return view('blog.blog-single')
		->with('blog',$blog)
		->with('breadcrumbs',$breadcrumbs);
	}

}