<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\Statistics\StatisticsRepository;
use Illuminate\Support\Facades\Storage;

class StatisticsController extends Controller
{
    //

    public function covid19Statistics(){

        $stats = StatisticsRepository::getCOVID19Data();
        $stats30Days = StatisticsRepository::getCOVID19GraphData();
        
        $statsInMillions = [
            "total_confirmed_cases" => intval($stats['globalStats']['total_confirmed_cases']/1000000),
            "total_recovered" => intval($stats['globalStats']['total_recovered']/1000000),
            "total_deaths" => intval($stats['globalStats']['total_deaths']/1000000),
        ];
        // dd($stats30Days);

        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'COVID19 Statistics','route'=>''],
        ];


        return view('covid19-statistics')
        ->with('statsInMillions',$statsInMillions)
        ->with('countryStats',$stats['countryStats'])
        ->with('globalStats',$stats['globalStats'])
        ->with('stats30Days',$stats30Days)
        ->with('breadcrumbs',$breadcrumbs);

    }

    public function updateFiles(){
        
        $allCountries = file_get_contents(StatisticsRepository::getAllCountriesURL());
        $allCountriesDecoded = json_decode($allCountries, true);
        if(count($allCountriesDecoded))
        Storage::put('files/statistics/all-countries.json', $allCountries);        

        $last365Days = file_get_contents(StatisticsRepository::getLast365URL());
        $last365DaysDecoded = json_decode($last365Days, true);
        if(count($last365DaysDecoded))
        Storage::put('files/statistics/all-last-365-days.json', $last365Days);        

        echo '<h1>Files Updated! '.date("Y-m-d").' </h1>';
    }


}
