<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\Product\ProductInterface;
use App\Interfaces\Blog\BlogInterface;

class SitemapController extends Controller
{
	//
	

	public $pages = [
		[ 'route' => 'home',                      'title'=>'Home'],
		[ 'route' => 'all-products',              'title'=>'Products'],
		[ 'route' => 'contact',                   'title'=>'Contact'],
		[ 'route' => 'charity',                   'title'=>'Charity'],
		[ 'route' => 'about',                     'title'=>'About'],
		[ 'route' => 'downloads',                 'title'=>'Downloads'],
		[ 'route' => 'faq',                       'title'=>'FAQ'],
		[ 'route' => 'faqs',                      'title'=>'FAQs'],
		[ 'route' => 'contact',                   'title'=>'Contact'],
		[ 'route' => 'careers',                   'title'=>'Careers'],
		[ 'route' => 'privacy',                   'title'=>'Privacy'],
		[ 'route' => 'terms',                     'title'=>'Terms'],
		[ 'route' => 'legal',                     'title'=>'Legal'],
		[ 'route' => 'refund',                    'title'=>'Refund'],
		[ 'route' => 'sample',                    'title'=>'Sample'],
		[ 'route' => 'blog',                      'title'=>'Blog'],
		[ 'route' => 'respirator-comparison',     'title'=>'Respirator Comparison'],
		[ 'route' => 'covid19-statistics',        'title'=>'COVID19 Statistics'],
	];
		

	public $images = [
		['path'=>'/files/about/images/ceo/vcare-earth-johannes-nepomuk-eidens.jpg','caption'=>'VCare.Earth Johannes Nepomuk Eidens CEO'],
		// ['path'=>'/files/about/images/ceo/vcare-earth-marc-vazquez.jpg','caption'=>'VCare.Earth Marc Vazquez CEO'],
		['path'=>'/files/logos/vcare-earth-logo.svg','caption'=>'VCare.Earth Logo'],
	];


	/**
	 * Initialize Interface
	 */
	public function __construct(ProductInterface $pi, BlogInterface $bi)
	{
		$this->pi = $pi;
		$this->bi = $bi;
	}


	public function sitemap() {

        $products = $this->pi->all()->map->format();
		$blogs = $this->bi->all()->map->format();

		// dd($blogs);

        $data['products'] = $products;
        $data['pages'] = $this->pages;
        $data['blogs'] = $blogs;
        $data['images'] = $this->images;

		return response()
					->view('sitemap.xml',$data,200)
					->header('Content-Type', 'text/xml');

	}    


	public function sitemapHTML() {

        $products = $this->pi->all()->map->format();
		$blogs = $this->bi->all()->map->format();

		// dd($blogs);

        $data['products'] = $products;
        $data['pages'] = $this->pages;
        $data['blogs'] = $blogs;
		$data['images'] = $this->images;
		


		return view('sitemap.html',$data);

	}    




}
