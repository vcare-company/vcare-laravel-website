<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\Product\ProductInterface;
use App\Interfaces\Product\ProductCertificateInterface;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class RedirectController extends Controller
{
    //

	/**
	 * Initialize Interface
	 */
	public function __construct(ProductInterface $pi, ProductCertificateInterface $pci)
	{
		$this->pi = $pi;
		$this->pci = $pci;
	}

    public function datasheetLanguage($language=null,$product_code){
        
        // dd (parse_url(route('datasheetLanguage',['product_code'=>$product_code,'language'=>$language])) );
        
        $prod =  $this->pi->find($product_code)->map->format()->first();
        // $datasheet_filename =  $prod['datasheet'];

        $lang = ($language)?'/'.$language:'';

        $datasheet_path = public_path(). '/files/products/'.$prod['public_id'].'/lang'.$lang.'/datasheets/datasheet';
        $headers = [ 'Content-Type' => 'application/pdf', ];
        
        // return $language.' '.$product_code;
        // return $datasheet_path;

        return response()->file($datasheet_path, $headers);

    }


    public function datasheet($product_code){
        
        $prod =  $this->pi->find($product_code)->map->format()->first();

        $datasheet_path = public_path(). '/files/products/'.$prod['public_id'].'/datasheets/datasheet';
        $headers = [ 'Content-Type' => 'application/pdf', ];
        

        return response()->file($datasheet_path, $headers);

    }

    public function manualLanguage($language=null,$product_code){
        
        // dd (parse_url(route('datasheetLanguage',['product_code'=>$product_code,'language'=>$language])) );
        
        $prod =  $this->pi->find($product_code)->map->format()->first();
        // $datasheet_filename =  $prod['datasheet'];

        $lang = ($language)?'/'.$language:'';

        $datasheet_path = public_path(). '/files/products/'.$prod['public_id'].'/lang'.$lang.'/manuals/manual';
        $headers = [ 'Content-Type' => 'application/pdf', ];
        
        // return $language.' '.$product_code;
        // return $datasheet_path;

        return response()->file($datasheet_path, $headers);

    }


    public function manual($product_code){
        
        $prod =  $this->pi->find($product_code)->map->format()->first();
        // $datasheet_filename =  $prod['datasheet'];

        $datasheet_path = public_path(). '/files/products/'.$prod['public_id'].'/manuals/manual';
        $headers = [ 'Content-Type' => 'application/pdf', ];
        
        // return $datasheet_path;

        return response()->file($datasheet_path, $headers);

    }


    public function companyProfile(){

        $file_path = public_path(). '/files/downloads/vcare-company-profile.pdf';
        $headers = [ 
            'Content-Type' => 'application/pdf', 
            'filename' =>  'vcare-company-profile.pdf', 
        ];

        return response()->file($file_path, $headers);

    }


    public function detailedComparisonMasks(){

        // $file_path = public_path(). '/files/downloads/vcare-detailed-comparison-for-masks.pdf';
        $file_path = public_path(). '/files/downloads/vcare-respirator-comparison.pdf';
        $headers = [ 
            'Content-Type' => 'application/pdf', 
            'filename' =>  'vcare-detailed-comparison-for-masks.pdf', 
        ];

        return response()->file($file_path, $headers);

    }




    public function respiratorComparison(){

        $file_path = public_path(). '/files/downloads/vcare-respirator-comparison.pdf';
        $headers = [ 
            'Content-Type' => 'application/pdf', 
            'filename' =>  'vcare-respirator-comparison.pdf', 
        ];

        return response()->file($file_path, $headers);

    }





    public function classStandardsComparison(){

        // $file_path = public_path(). '/files/downloads/vcare-detailed-comparison-for-masks.pdf';
        $file_path = public_path(). '/files/downloads/vcare-class-standards-comparison.pdf';
        $headers = [ 
            'Content-Type' => 'application/pdf', 
            'filename' =>  'vcare-detailed-comparison-for-masks.pdf', 
        ];

        return response()->file($file_path, $headers);

    }











    // Certificates 1
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////

    public function certificate(Request $rqst, $product_code, $file_code){
        $prod =  $this->pi->find($product_code)->map->format()->first();
        $pci =  $this->pi->getCert($product_code, $file_code);

        $product_public_id = $prod['public_id'];

        $lisp = $rqst->session()->get('loggedin-status-certificates-'.$pci['product_code']);
        $lisp2 = $rqst->session()->get('loggedin-status-original_certificates-'.$pci['product_code']);
        $lisp3 = $rqst->session()->get('loggedin-status-obm_certificates-'.$pci['product_code']);


        $lss = $rqst->session()->get('loggedin-status-sora');

        $lisp = (is_null($lisp)||$lisp=='0')?false:true;
        $lisp2 = (is_null($lisp2)||$lisp2=='0')?false:true;
        $lisp3 = (is_null($lisp3)||$lisp3=='0')?false:true;
        $lss = (is_null($lss)||$lss=='0')?false:true;

        // fix for when loading pdf on browser it loads the complete pdf
        // not sure why but this fixed it
        echo "".rand(0,999999999);

		if($lisp || $lss) {
			$loggedinCertificates = true;
		} else {
            $loggedinCertificates = false;
        }
        
        if($lisp2 || $lss) {
			$loggedinCertificates2 = true;
		} else {
            $loggedinCertificates2 = false;
        }

        if($lisp3 || $lss) {
			$loggedinCertificates3 = true;
		} else {
            $loggedinCertificates3 = false;
        }

    
        // remove this if issue is fixed
        // $loggedinCertificates = true;

    
		if($loggedinCertificates) {
            $filename =  $pci['filename'].'.'.$pci['ext'];
            $filename = html_entity_decode($filename);
            $path = public_path(). '/files/products/'.$product_public_id.'/certificates/'.$pci['id'];
            $headers = [ 
                'Content-Type' => 'application/pdf', 
            ];
            return response()->file($path, $headers);
            // return response()->download($path,$filename,$headers);
            
        } elseif ($loggedinCertificates2){

      
                $filename =  $pci['filename'].'.'.$pci['ext'];
                $filename = html_entity_decode($filename);
                $path = public_path(). '/files/products/'.$product_public_id.'/certificates/'.$pci['id'];
                $headers = [ 
                    'Content-Type' => 'application/pdf', 
                ];
                return response()->file($path, $headers);
                // return response()->download($path,$filename,$headers);



        } elseif ($loggedinCertificates3) {

                $filename =  $pci['filename'].'.'.$pci['ext'];
                $filename = html_entity_decode($filename);
                $path = public_path(). '/files/products/'.$product_public_id.'/certificates/'.$pci['id'];
                $headers = [ 
                    'Content-Type' => 'application/pdf', 
                ];
                return response()->file($path, $headers);
                // return response()->download($path,$filename,$headers);


        } else {
            return redirect()->route('single-product-login', 
            ['product_code' => $pci['product_code'], 'login_type' => 'certificates',]);
        }
    }




    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    // Certificates 1



    // Certificates 2
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////

    public function certificateTwo(Request $rqst, $product_code, $file_code){
        $prod =  $this->pi->find($product_code)->map->format()->first();
        $pci =  $this->pi->getCert($product_code, $file_code);

        $product_public_id = $prod['public_id'];

        $lss = $rqst->session()->get('loggedin-status-sora');

        $lisp = (is_null($lisp)||$lisp=='0')?false:true;
        $lss = (is_null($lss)||$lss=='0')?false:true;

        // fix for when loading pdf on browser it loads the complete pdf
        // not sure why but this fixed it
        echo "".rand(0,999999999);

		if($lisp || $lss) {
			$loggedinCertificates = true;
		} else {
            $loggedinCertificates = false;
        }
        

        // remove this if issue is fixed
        // $loggedinCertificates = true;

    
		if($loggedinCertificates) {
            $filename =  $pci['filename'].'.'.$pci['ext'];
            $filename = html_entity_decode($filename);
            $path = public_path(). '/files/products/'.$product_public_id.'/certificates/'.$pci['id'];
            $headers = [ 
                'Content-Type' => 'application/pdf', 
            ];
            return response()->file($path, $headers);
            // return response()->download($path,$filename,$headers);
        } else {
            return redirect()->route('single-product-login', 
            ['product_code' => $pci['product_code'], 'login_type' => 'original_certificates',]);
        }

    }




    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    // Certificates 2


        // Certificates 3
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////

    public function certificateThree(Request $rqst, $product_code, $file_code){
        $prod =  $this->pi->find($product_code)->map->format()->first();
        $pci =  $this->pi->getCert($product_code, $file_code);

        $product_public_id = $prod['public_id'];

        $lisp = $rqst->session()->get('loggedin-status-obm_certificates-'.$pci['product_code']);
        $lss = $rqst->session()->get('loggedin-status-sora');

        $lisp = (is_null($lisp)||$lisp=='0')?false:true;
        $lss = (is_null($lss)||$lss=='0')?false:true;

        // fix for when loading pdf on browser it loads the complete pdf
        // not sure why but this fixed it
        echo "".rand(0,999999999);

		if($lisp || $lss) {
			$loggedinCertificates = true;
		} else {
            $loggedinCertificates = false;
        }
        

        // remove this if issue is fixed
        // $loggedinCertificates = true;

    
		if($loggedinCertificates) {
            $filename =  $pci['filename'].'.'.$pci['ext'];
            $filename = html_entity_decode($filename);
            $path = public_path(). '/files/products/'.$product_public_id.'/certificates/'.$pci['id'];
            $headers = [ 
                'Content-Type' => 'application/pdf', 
            ];
            return response()->file($path, $headers);
            // return response()->download($path,$filename,$headers);
        } else {
            return redirect()->route('single-product-login', 
            ['product_code' => $pci['product_code'], 'login_type' => 'certificates',]);
        }

    }




    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    // Certificates 3



    // Packaging
    /////////////////////////////////////////////////////////////////////////////////////////

    public function packagingLanguage($language=null,$product_code){
        
        
        $prod =  $this->pi->find($product_code)->map->format()->first();

        $lang = ($language)?'/'.$language:'';

        $packaging_path = public_path(). '/files/products/'.$prod['public_id'].'/lang'.$lang.'/datasheets/packaging';
        $headers = [ 'Content-Type' => 'application/pdf', ];
        
        return response()->file($packaging_path, $headers);

    }


    public function packaging($product_code){
        
        $prod =  $this->pi->find($product_code)->map->format()->first();

        $packaging_path = public_path(). '/files/products/'.$prod['public_id'].'/datasheets/packaging';
        $headers = [ 'Content-Type' => 'application/pdf', ];
        

        return response()->file($packaging_path, $headers);

    }

    /////////////////////////////////////////////////////////////////////////////////////////
    // Packaging




}

