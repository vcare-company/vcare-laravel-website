<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\Statistics\StatisticsRepository;


class PageController extends Controller
{

    public function home(Request $rqst) {
        $stats = StatisticsRepository::getCOVID19Data();

        return view('home')
        ->with('message',$rqst->input('message'))
        ->with('countryStats',$stats['countryStats'])
        ->with('globalStats',$stats['globalStats']);
    }

    public function promotions(Request $rqst) {

        return view('medical-equipment-supplier')
        ->with('message',$rqst->input('message'));
     
    }




    public function about() {
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'About','route'=>''],
        ];

        return view('about')
        ->with('breadcrumbs',$breadcrumbs);
    }

    public function messageFromTheCeo() {
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Message From The CEO','route'=>''],
        ];

        return view('message-ceo')
        ->with('breadcrumbs',$breadcrumbs);
    }

    public function faq() {
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'FAQ','route'=>''],
        ];

        return view('faq')
        ->with('breadcrumbs',$breadcrumbs);
    }

    public function faqs() {
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'FAQs','route'=>''],
        ];

        return redirect()->route('faq')
        ->with('breadcrumbs',$breadcrumbs);
    }

    public function comingSoonProduct() {
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Coming Soon','route'=>''],
        ];

        return view('product.coming-soon-product')->with('product',['code'=>''])
        ->with('breadcrumbs',$breadcrumbs);
    }

    public function comingSoonProductCode($product_code) {
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Coming Soon','route'=>''],
        ];

        return view('product.coming-soon-product')->with('product',['code'=>$product_code])
        ->with('breadcrumbs',$breadcrumbs);
    }

    public function privacy() {
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Privacy Policy','route'=>''],
        ];

        return view('privacy')
        ->with('breadcrumbs',$breadcrumbs);
    }

    public function terms() {
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Terms & Condition','route'=>''],
        ];

        return view('terms')
        ->with('breadcrumbs',$breadcrumbs);
    }

    public function legal() {
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Legal','route'=>''],
        ];

        return view('legal')
        ->with('breadcrumbs',$breadcrumbs);
    }

    public function refund() {
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Refund','route'=>''],
        ];

        return view('refund')
        ->with('breadcrumbs',$breadcrumbs);
    }

    public function careers() {
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Careers','route'=>''],
        ];

        return view('careers')
        ->with('breadcrumbs',$breadcrumbs);
    }

    public function charity(){

        $stats = StatisticsRepository::getCOVID19Data();
        $statsInMillions = [
            "total_confirmed_cases" => intval($stats['globalStats']['total_confirmed_cases']/1000000),
            "total_recovered" => intval($stats['globalStats']['total_recovered']/1000000),
            "total_deaths" => intval($stats['globalStats']['total_deaths']/1000000),
        ];

        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Charity','route'=>''],
        ];

        return view('charity')
        ->with('statsInMillions',$statsInMillions)
        ->with('breadcrumbs',$breadcrumbs);

    }
}
