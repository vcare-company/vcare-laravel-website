<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Helper\Helper;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use App\Interfaces\Product\ProductInterface;
use App\Interfaces\Product\ProductAccessLogInterface;
use App\Interfaces\Category\CategoryInterface;

class ProductController extends Controller
{

	/**
	 * Initialize Interface
	 */
	public function __construct(CategoryInterface $ci, ProductInterface $pi,ProductAccessLogInterface $pali)
	{
		$this->pi = $pi;
		$this->ci = $ci;
		$this->pali = $pali;
	}


	// public function allProducts()
	// {
    //     $products = $this->pi->all()->map->format();
        
    //     return $products;
	// }

	public function singleProduct(Request $rqst, $product_code)
	{
		$product = $this->pi->find($product_code)->map->format()->first();
		
		// $loggedInAdmin = $rqst->session()->get('loggedin-status-admin');

		// dd($product);
		
		if($product['coming_soon'])
		$product_view_type = 'product.coming-soon-product';
		else 
		$product_view_type = 'product.single-product';


		$title = $product['name_type'].' '.$product['name'].' '.$product['name_code'];

		
		if($product['category'] && count($product['category'])) {
			if($product['category']['parent'] && count($product['category']['parent'])) {
				$breadcrumbs =[
					['title'=>'Home','route'=>'home'],
					['title'=>'Products','route'=>'all-products'],
					['title'=>$product['category']['parent']['name'],'route'=>'cat-'.$product['category']['parent']['code']],
					['title'=>$product['category']['name'],'route'=>'cat-'.$product['category']['code']],
					['title'=>$title,'route'=>''],
				];
			} else {
				$breadcrumbs =[
					['title'=>'Home','route'=>'home'],
					['title'=>'Products','route'=>'all-products'],
					['title'=>$product['category']['name'],'route'=>'cat-'.$product['category']['code']],
					['title'=>$title,'route'=>''],
				];
			}

		} else {			
			$breadcrumbs =[
				['title'=>'Home','route'=>'home'],
				['title'=>'Products','route'=>'all-products'],
				['title'=>$title,'route'=>''],
			];
		}		



		
		return view($product_view_type)
		->with('product',$product)
		->with('breadcrumbs',$breadcrumbs);
		// ->with('loggedInAdmin',$loggedInAdmin);
	}


	public function singleProductPricelist(Request $rqst, $product_code)
	{

		// $rqst->session()->put('loggedin-status-pricelist', '1');
		$lisp = $rqst->session()->get('loggedin-status-pricelist-'.$product_code);

		if($lisp) {
			$loggedinPricelist = true;
		} else {
			$loggedinPricelist = false;
		}

		$product = $this->pi->find($product_code)->map->formatPricelist()->first();
	

		if($loggedinPricelist) {
			return view('product.single-product-pricelist')->with('product',$product);
		} else {
			return redirect()->route('single-product-login', 
			['product_code' => $product_code,'login_type' => 'pricelist',]);
		}
	}


	public function singleProductCertificates(Request $rqst, $product_code)
	{

		$lisp = $rqst->session()->get('loggedin-status-certificates-'.$product_code);

        $lss = $rqst->session()->get('loggedin-status-sora');


		if($lisp || $lss) {
			$loggedinCertificates = true;
		} else {
			$loggedinCertificates = false;
		}
		

		// dd($product);

		if($loggedinCertificates) {
			$product = $this->pi->find($product_code)->map->formatCertificates()->first();

			// temporary remove certificates for Max 4B
			/////////////////////////////////////////////////////
			if($product['id']==9) {
				foreach($product['certificates'] as $key => $cert) {
					if(!in_array($cert['id'],[38,40,41,123,214])) {
						$product['certificates']->forget($key);
					}
				}
			}
			/////////////////////////////////////////////////////
			// temporary remove certificates for Max 4B


			return view('product.single-product-certificates')
			->with('product',$product);
		} else {
			return redirect()->route('single-product-login', 
			['product_code' => $product_code,'login_type' => 'certificates',]);
		}
	}

	public function singleProductCertificatesTwo(Request $rqst, $product_code)
	{

		$lisp = $rqst->session()->get('loggedin-status-original_certificates-'.$product_code);

        $lss = $rqst->session()->get('loggedin-status-sora');


		if($lisp || $lss) {
			$loggedinCertificates = true;
		} else {
			$loggedinCertificates = false;
		}
		

		// dd($product);

		if($loggedinCertificates) {
			$product = $this->pi->find($product_code)->map->formatCertificates()->first();

			// temporary remove certificates for Max 4B
			/////////////////////////////////////////////////////
			if($product['id']==9) {
				foreach($product['certificates'] as $key => $cert) {
					if(!in_array($cert['id'],[38,40,41,123,214])) {
						$product['certificates']->forget($key);
					}
				}
			}
			/////////////////////////////////////////////////////
			// temporary remove certificates for Max 4B


			return view('product.single-product-main-certificates')
			->with('product',$product);
		} else {
			return redirect()->route('single-product-login', 
			['product_code' => $product_code,'login_type' => 'original_certificates',]);
		}
	}


	public function singleProductCertificatesThree(Request $rqst, $product_code)
	{

		$lisp = $rqst->session()->get('loggedin-status-obm_certificates-'.$product_code);

        $lss = $rqst->session()->get('loggedin-status-sora');


		if($lisp || $lss) {
			$loggedinCertificates = true;
		} else {
			$loggedinCertificates = false;
		}
		

		// dd($product);

		if($loggedinCertificates) {
			$product = $this->pi->find($product_code)->map->formatCertificates()->first();

			// temporary remove certificates for Max 4B
			/////////////////////////////////////////////////////
			if($product['id']==9) {
				foreach($product['certificates'] as $key => $cert) {
					if(!in_array($cert['id'],[38,40,41,123,214])) {
						$product['certificates']->forget($key);
					}
				}
			}
			/////////////////////////////////////////////////////
			// temporary remove certificates for Max 4B


			return view('product.single-product-obm-certificates')
			->with('product',$product);
		} else {
			return redirect()->route('single-product-login', 
			['product_code' => $product_code,'login_type' => 'obm_certificates',]);
		}
	}



	public function singleProductLogin($product_code, $login_type) {

		$product = $this->pi->find($product_code)->map->formatPricelist()->first();
		$error_message = '';

		// dd($login_type);

		return view('product.single-product-login')
		->with('product_code',$product_code)
		->with('login_type',$login_type)
		->with('error_message',$error_message);


	}


	public function singleProductLogout(Request $rqst) {

		// LOGOUT ALL
		$rqst->session()->flush();
		// return redirect()->route('single-product-login', 
		// ['product_code' => $product_code,'login_type' => $login_type,]);

	}


	public function singleProductLoginPost(Request $rqst, $product_code, $login_type) {

		$user = $rqst->input('username');
		$pass = $rqst->input('password');

		$product = $this->pi->find($product_code)->map->formatMin()->first();
		$error_message = '';	

		// Check email format
		$email = $user;
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$invalid_email_format = true;
		}	else
		$invalid_email_format = false;
		
		
		if(!$user || $invalid_email_format) {
			$rqst->session()->forget('loggedin-status-'.$login_type.'-'.$product_code);

			// return redirect()->route('single-product-login', 
			// ['product_code' => $product_code,'login_type' => $login_type,]);

			$error_message = 'Email is invalid!';

			return view('product.single-product-login')
			->with('product_code',$product_code)
			->with('login_type',$login_type)		
			->with('error_message',$error_message);

		} 

		else if(Hash::check($pass, $product[$login_type.'_password'])) {
			$rqst->session()->put('loggedin-status-'.$login_type.'-'.$product_code, '1');



			
			
			$data = [
				'username'=>$user,
				'product_code'=>$product_code,
				'access_type'=>$login_type,
				'country' => Helper::ip_info("Visitor", "Country"), // India
				'country_code' => Helper::ip_info("Visitor", "Country Code"), // IN
				'state' => Helper::ip_info("Visitor", "State"), // Andhra Pradesh
				'city' => Helper::ip_info("Visitor", "City"), // Proddatur
				'address' => Helper::ip_info("Visitor", "Address"), // Proddatur, Andhra Pradesh, India						
				'ip' => Helper::ip_info("Visitor", "IP"), // Proddatur, Andhra Pradesh, India						
			];

			// insert log
			$this->pali->insertLog($data);

			return redirect()->route('single-product-'.$login_type, 
			['product_code' => $product_code]);

		} else {
			$rqst->session()->forget('loggedin-status-'.$login_type.'-'.$product_code);

			// return redirect()->route('single-product-login', 
			// ['product_code' => $product_code,'login_type' => $login_type,]);

			if($login_type == 'certificates' || $login_type == 'original_certificates' || $login_type == 'obm_certificates')
			$error_message = 'Password is invalid!';
			else
			$error_message = 'Email or Password is invalid!';

			return view('product.single-product-login')
			->with('product_code',$product_code)
			->with('login_type',$login_type)		
			->with('error_message',$error_message);

		}

	}















































}