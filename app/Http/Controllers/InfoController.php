<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InfoController extends Controller
{


    public function info() {
     

        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'More Product Information','route'=>''],
        ];		
		
        return view('info.info')
        ->with('breadcrumbs',$breadcrumbs);
    }

    public function en49ffp1(){

        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'More Product Information','route'=>'more-info'],
            ['title'=>'EN 149 FFP1','route'=>''],
        ];		
		
        return view('info.en149ffp1')
        ->with('breadcrumbs',$breadcrumbs);
    }

    public function reusablerespirator(){

        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'More Product Information','route'=>'more-info'],
            ['title'=>'Reusable Respirator','route'=>''],
        ];		
		
        return view('info.reusable-respirator')
        ->with('breadcrumbs',$breadcrumbs);
    }

    public function disposablerespirator(){

        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'More Product Information','route'=>'more-info'],
            ['title'=>'Disposable Respirator','route'=>''],
        ];		


        return view('info.disposable-respirator')
        ->with('breadcrumbs',$breadcrumbs);
    }

}