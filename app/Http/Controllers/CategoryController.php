<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;


use App\Interfaces\Category\CategoryInterface;


class CategoryController extends Controller
{

	/**
	 * Initialize Interface
	 */
	public function __construct(CategoryInterface $ci)
	{
		$this->ci = $ci;
	}


	public function allProducts()
	{
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Products','route'=>''],
		];
				
        $categories = $this->ci->parentsOnly()->map->formatAllProductsPage();

        // dd($categories);

		return view('category.all-products')->with('categories',$categories)
		->with('breadcrumbs',$breadcrumbs);

	}

	// SEO Category pages
	//////////////////////////////////////////////////////////////////////
	public function categoryPage($cat_code){

		$category = $this->ci->find($cat_code)->map->formatCategoryPage()[0];

		// dd($category); 
		
		if($category['parent'] && count($category['parent'])) {
			$breadcrumbs =[
				['title'=>'Home','route'=>'home'],
				['title'=>'Products','route'=>'all-products'],
				['title'=>$category['parent']['name'],'route'=>'cat-'.$category['parent']['code']],
				['title'=>$category['name'],'route'=>''],
			];

		} else {			
			$breadcrumbs =[
				['title'=>'Home','route'=>'home'],
				['title'=>'Products','route'=>'all-products'],
				['title'=>$category['name'],'route'=>''],
			];
		}
		
        
		return view('category.single-category')
		->with('category',$category)
		->with('breadcrumbs',$breadcrumbs);
	}


	// special category, doesnt exist on database
	public function respirators() {

		$cat_codes = ['ffp2','ffp3','kn95','n95'];

		$cats = $this->ci->findMany($cat_codes)->map->formatCategoryPage();

		$category['id'] = '0';
		$category['meta_title'] = 'Shop Personal Respirators | Breathing Protection Equipment | VCare.Earth';
		$category['meta_desc'] = 'Shop personal respirators and breathing protection equipment at VCare.Earth. We are safety equipment suppliers and provide organic respirators and healthcare equipment, respirators, and PPE supplies to B2B customers worldwide. Visit us today!';
		$category['h2'] = 'Our Respirators and Breathing Protection Equipment';	
		$category['h1'] = 'Shop Respirators and Breathing Protection Equipment at VCare.Earth';
		$category['p'] = '
		<p>
			We are <a href="https://vcare.earth/" style="color:black; font-weight:300"> safety equipment suppliers</a> and provide organic respirators and healthcare equipment, respirators, and PPE supplies to B2B customers worldwide.
			<br><br>
			It is important to invest in breathing protection equipment that can make a true difference in your experience. Over the years, several different types of breathing protection equipment have been introduced into the market. This makes it difficult for potential buyers to make a choice. Yet, when you shop personal respirators, there are few factors to keep in mind. These are basic qualities any organic respirator or sophisticated equipment needs to suffice. 
		</p>
		
		
		<div class="read-more-content">
			<h2 class="title is-5 mt-6">
				The Clip
			</h2>
			<p class="mb-6">
				Firstly, breathing protection equipment should fit you perfectly. The equipment must not be too big, or tight. Instead, it needs to be sized appropriately. This reduces the chances of infectious microbes from entering through the mask or the respirators.
			</p>
		
		
			<h2 class="title is-5">
				Filter Type
			</h2>
			<p class="mb-6">
				Often, respirators are categorized based on the type of gases they filter. Today, there are several different types of filters, each with the ability to keep solids, oil, and smoke away. The organic and conventional respirators are uniquely identified with special colors too. For instance, category A is capable of keeping gas and smoke away. These filtering systems are identified using “Brown”. Category E is used to keep smoke and acid gas away. It is identified using the color yellow. Organic respirators that can keep organic amines and ammonia away are identified with the code X, and the color “green”. If the color code is red, it keeps mercury away too. 
			</p>
		
		
			<h2 class="title is-5">
				Standards
			</h2>
			<p class="mb-6">
				When you shop personal respirators you need to be aware of the three different standards. This covers both the <a href="https://vcare.earth/en149-ffp1" style="color:black; font-weight:300"> EN 149 </a>: 2001 and EN 149: 2009 categories. Each of these categories has FFP1,<a href="https://vcare.earth/products/personal-protective-equipment-ppe/ffp2" style="color:black; font-weight:300"> FFP2</a>, and<a href="https://vcare.earth/products/personal-protective-equipment-ppe/ffp3" style="color:black; font-weight:300"> FFP3</a>. These are specialized categories that can handle non-toxic particles of different sizes. For example, FFP1 can keep non-toxic particles as much as 4 times the default limit. On the other hand, FFP2 can handle particles as much as 10 times the default limit. 
			</p>
		
		
			<h2 class="title is-5"> 
				Mask Range 
			</h2>
			<p class="mb-3">
				Before you pick any kind of mask, you need to be aware of the hazards you would be exposed to. The composition and type of hazardous substance need to be determined using approved methods. It is necessary to understand the properties of these hazardous substances. TIt will help you choose respirators that can actually make a difference in the hazardous environment. Also, the environment should have adequate oxygen for the wearer to carry the EN 149 standard masks to be worn, without any hassles or tussles. The percentage of oxygen should be already calibrated and known.
			</p>
		</div>
		<div class="read-more-action">
			<button class="button is-ghost is-small btn-read-more">Read more...</button>
			<button class="button is-ghost is-small btn-read-less">Read less...</button>
		</div>
		';
		$category['name'] = 'Respirators';
		$category['html'] = 'Respirators';
		$category['products'] = [];
		$category['children'] = $cats;

		
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Products','route'=>'all-products'],
            ['title'=>$category['name'],'route'=>''],
		];
        
		return view('category.single-category')
		->with('category',$category)
		->with('breadcrumbs',$breadcrumbs);
	}




	//////////////////////////////////////////////////////////////////////
	// SEO Category pages

		

	// public function singleCategory($code)
	// {
    //     $product = $this->ci->find($code)->map->format()->first();
        
    //     // dd($product['images']);
    //     // return array_shift($product['images']);
    //     // return $product['images'];
        
    //     return view('product.single-product')->with('product',$product);
	// }


}