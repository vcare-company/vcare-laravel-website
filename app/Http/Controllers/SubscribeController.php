<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SubscriberRepository;

class SubscribeController extends Controller
{
    //


    public function subscribeNewsletter(Request $rqst) {
        $data = [];
        $data['name'] = $rqst->input('subscriber-name');
        $data['email'] = $rqst->input('subscriber-email');
        
        if(!$rqst->input('subscriber-name'))
        $data['name'] = 'no name';
        
        if(!$rqst->input('subscriber-email'))
        $data['email'] = 'no email';

        if(SubscriberRepository::add($data)) {
            return redirect()->route('home',['message'=>'Thank you for subscribing! <br>You will hear from us soon!']);
        }


    }
    
}
