<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\Interfaces\Product\ProductInterface;
use App\Interfaces\Category\CategoryInterface;
use Illuminate\Support\Facades\Storage;


class TestController extends Controller
{
	/**
	 * Initialize Interface
	 */
	public function __construct(CategoryInterface $ci, ProductInterface $pi)
	{
		$this->pi = $pi;
		$this->ci = $ci;
    }
    
    function test($product_code){
        $product = $this->pi->find($product_code)->map->format()->first();
    
        dd($product);
        // return $product['images-main']->max('order');
	}
    
    function test2(){
        
		$products = $this->pi->all()->map->format();
		
		// dd($products);
    
        foreach ($products as $key => $product) {

			$meta_title = strtoupper(str_replace('-',' - ',$product['code']));

			if($product['category']['parent']['code']=='covid-19-testing')
			$meta_desc = 'An Effective Screening Point-Of-Care Test.';
			else if($product['category']['parent']['code']=='medical-equipment')
			$meta_desc = 'We deliver worldwide, to any destination of your preferred location and we promise to make for a speedy delivery within the shortest time possible.';
			else if(in_array($product['category']['code'],['ffp2','ffp3','kn95','kn95-usa','niosh-n95']))
			$meta_desc = 'Our particulate protection respirator masks have comfortable inner materials and helps provide respiratory protection against certain airborne particles.';
			else
			$meta_desc = 'We deliver worldwide, to any destination of your preferred location and we promise to make for a speedy delivery within the shortest time possible.';

			echo $product['id'].'<br>';
			// echo $product['code'].'<br>';
			echo $product['name_code_group'].' - '.$product['name_code'].'<br>';
			echo '<input value="'.$meta_title.'" style="width:100%;"/> <br>';
			echo '<input value="'.$meta_desc.'" style="width:100%;"/> ';
			
			echo '<br><br>';
		}
        // return $product['images-main']->max('order');
	}
	
    
    function test3(){
				
			
		echo rand(100000000,999999999).'<br><br><br><br>';
		
		// dd($products);
		$passwords = [
			'ffp2-ross'=>'nfj3kckw',
			'ffp2-max'=>'s8sj2gj4',
			'kn95-lou'=>'klo8dsh3',
			'rtest-finn'=>'sjhf63nk',
			'rtest-marc'=>'3fnb3kv',
			'mfmask-gem'=>'dhg832jv',
			'srgwn-ria'=>'s83jfnjld',
			'srgwn-dawn'=>'kjdj39dn',
			'isgwn-lei'=>'j4kl3ln4',
			'cvgwn-kat'=>'hvu3mnv',
		];
    
        foreach ($passwords as $key => $pass) {


			$hash = Hash::make($pass);

			echo $key.' <br> <input type="" value="'.$hash.'">  <br><br>';			
		}
        // return $product['images-main']->max('order');
	}


	function statsTest(){
		
        Storage::put('files/statistics/test.txt', 'test');        
		
	}
	
}