<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use App\Interfaces\Product\ProductCertificateInterface;
use App\Interfaces\Product\ProductInterface;
use App\Interfaces\Product\ProductImageInterface;
use App\Interfaces\Category\CategoryInterface;
use App\Interfaces\Blog\BlogInterface;

class AdminController extends Controller
{

    /**
	 * Initialize Interface
	 */
	public function __construct(
		CategoryInterface $ci, 
		ProductInterface $pi, 
		ProductCertificateInterface $pci, 
		ProductImageInterface $pii, 
		BlogInterface $bi
	) {
        $this->pi = $pi;
		$this->ci = $ci;
        $this->pci = $pci;
        $this->pii = $pii;
        $this->bi = $bi;
	}





	

































	public function editProduct()
	{
        $products = $this->pi->all()->map->formatMin();
        return view('admin.edit-product')->with('products',$products);
	}

	public function addProduct()
	{
        $categories = $this->ci->all()->map->formatAddProduct();
        return view('admin.add-product')->with('categories',$categories);
	}

	public function addProductPost(Request $rqst)
	{

		
		$code  = $rqst->product_name_code_group.'-'.$rqst->product_name;
		

		// Product
		///////////////////////////////////////////////////////////////////////////////////////////
		$prod = [
			'category_id'=>$rqst->product_category,
			'name'=>$rqst->product_name, 
			'code'=>$rqst->product_code, 
			'name_type'=>$rqst->product_name_type, 
			'name_code_group'=>$rqst->product_name_code_group, 
			'name_code'=>$rqst->product_name_code, 
			'overview'=>$rqst->product_overview, 
            'certifications'=>$rqst->product_certifications, 
			'meta_desc'=>$rqst->product_meta_desc,
			'meta_title'=>$rqst->product_meta_title,            
			// 'features'=>$rqst->product_features, 
			// 'benefits'=>$rqst->product_benefits, 
			// 'text'=>$rqst->product_text, 
			'html_content'=>$rqst->product_html, 
			// 'datasheet'=>$this->filenameFormat($code).'-datasheet.pdf', 
			// 'datasheet'=>0, 
		];

		$this->pi->create($prod);
		///////////////////////////////////////////////////////////////////////////////////////////
		// Product

		if(true) {
			return redirect()->route('single-product',['product_code'=>$rqst->product_code]);
		}
	}


































	public function editProductDatasheets($product_code)
	{
        $product = $this->pi->find($product_code)->map->format()->first();
        return view('admin.edit-product-datasheets')->with('product',$product);
	}

	public function editProductDatasheetsPost(Request $rqst)
	{


		$product_id  = $rqst->product_id;
		$product_code  = $rqst->code;
		
        $product = $this->pi->find($product_code)->map->format()->first();
		$product_public_id  = $product['public_id'];

		// Datasheet
		///////////////////////////////////////////////////////////////////////////////////////////
		if($rqst->product_datasheet_file) {
			$this->uploadFile(
				$rqst->product_datasheet_file,
				'datasheet',
				'files/products/'.$product_public_id.'/datasheets'
			);
		}
		///////////////////////////////////////////////////////////////////////////////////////////
		// Datasheet

        echo "<h1>Datasheet uploaded</h1>";
        echo 'files/products/'.$product_public_id.'<br>';
        echo 'files/products/'.$product_public_id.'/datasheets';
    }















































	public function editProductImages($product_code)
	{
		$product = $this->pi->find($product_code)->map->format()->first();
		
		// dd($product);

        return view('admin.edit-product-images')->with('product',$product);
	}

	public function addProductImagesPost(Request $rqst)
	{


		$product_id  = $rqst->product_id;
		$product_code  = $rqst->code;
		
        $product = $this->pi->find($product_code)->map->format()->first();
        
		$product_public_id  = $product['public_id'];

		// dd($product['images-main']);
		// dd(
		// 	$product_id.'   <br>   '.
		// 	$product_public_id.'   <br>   '.
		// 	$product_code.'   <br>   '.
		// 	$this->filenameFormat($product_code).'-main'.'   <br>   '.
		// 	'Main'.'   <br>   '.
		// 	'main'.'   <br>   '.
		// 	$order.'   <br>   '.
		// 	$rqst->image_mai.'   <br>   '
		// );

		// Main image
		//////////////////////////////////////////////////////////////////////////////
		// So that you can add multiple main images
		// if($product['images-main'] && count($product['images-main'])>0 && count($product['images-main'])) {
		if(true) {
			$order = $product['images-main']->max('order')+1;
			// Main
			$this->addImage(
				$product_id, 
				$product_public_id, 
				$product_code, 
				$this->filenameFormat($product_code).'-main', 
				'Main', 
				'main', 
				$order, 
				$rqst->image_main
			);
		} 
		//////////////////////////////////////////////////////////////////////////////
		// Main image




		// 3D View image
		//////////////////////////////////////////////////////////////////////////////
		if(true) {
		// if(count($product['images-3d-view'])<1) {
			$fis = [
				['pp_bag','PP Bag'],
				['middle_box','Middle Box'],
				['master_box','Master Box'],
				['display_box','Display Box'],
				['display_box_empty','Display Box (Empty)'],
				['packages_3','3 Packages'],
			];
			// $fis_image_id = [
			// 	'pp_bag'=>0,
			// 	'middle_box'=>0,
			// 	'master_box'=>0,
			// 	'display_box'=>0,
			// 	'display_box_empty'=>0,
			// 	'packages_3'=>0,
			// ];

			foreach($fis as $k => $fi) {
				if($rqst['image_3d_view_'.$fi[0]]) {
					// $fis_image_id[$fi[0]] = 
					$this->addImage(
						$product_id, 
                        $product_public_id, 
						$product_code, 
						$this->filenameFormat($product_code).'-'.str_replace('_','-',$fi[0]), 
						$fi[1], 
						'3d-view', 
						$k, 
						$rqst['image_3d_view_'.$fi[0]]
					);
				}
			}
		}
		//////////////////////////////////////////////////////////////////////////////
		// 3D View image



		// Package Details image
		//////////////////////////////////////////////////////////////////////////////
		if(count($product['images-package-details'])<1) {
			$fis = [
				['pp_bag','PP Bag'],
				['middle_box','Middle Box'],
				['master_box','Master Box'],
			];
			foreach($fis as $k => $fi) {


				$pi_id = $this->addImage(
					$product_id, 
                    $product_public_id, 
					$product_code, 
					$this->filenameFormat($product_code).'-'.str_replace('_','-',$fi[0]), 
					$fi[1], 
					'package-details', 
					$k, 
					$rqst['image_package_details_'.$fi[0]]
				);

				// Expanded
				$pi_expanded_id = $this->addImage(
					$product_id, 
                    $product_public_id, 
					$product_code, 
					$this->filenameFormat($product_code).'-'.str_replace('_','-',$fi[0]), 
					$fi[1], 
					'package-details-expanded', 
					$k, 
					$rqst['image_package_details_expanded_'.$fi[0]]
				);

				if($pi_id>0 && $pi_expanded_id>0) {
					$this->pii->insertPackageDetails(
						[
							'pi_id'=>$pi_id,
							'pi_expanded_id'=>$pi_expanded_id,
						]
					);
				}


			}
		}
		//////////////////////////////////////////////////////////////////////////////
		// Package Details image

	

		// Package Details Expanded image
		//////////////////////////////////////////////////////////////////////////////
		// if(count($product['images-package-details'])<1) {
		// 	$fis = [
		// 		['pp_bag','PP Bag'],
		// 		['middle_box','Middle Box'],
		// 		['master_box','Master Box'],
		// 	];
		// 	foreach($fis as $k => $fi) {
		// 		$this->addImage(
		// 			$product_id, 
        //             $product_public_id, 
		// 			$product_code, 
		// 			$this->filenameFormat($product_code).'-'.str_replace('_','-',$fi[0]), 
		// 			$fi[1], 
		// 			'package-details-expanded', 
		// 			$k, 
		// 			$rqst['image_package_details_expanded_'.$fi[0]]
		// 		);
		// 	}
		// }
		//////////////////////////////////////////////////////////////////////////////
		// Package Details Expanded image



		if(true) {
			// return redirect()->route('edit_product');
			return redirect()
			->route('edit_product_images',['product_code'=>$product_code])
			->with( ['message' => 'Product Images Updated!'] );					
		}

	}












	public function editProductImagesPost(Request $rqst)
	{

		$edit_image_filenames  = $rqst->edit_image_filenames;
		$edit_image_files  = $rqst->edit_image_files;
		$edit_image_ids  = $rqst->edit_image_ids;
		$edit_image_order  = $rqst->edit_image_order;
		$edit_image_title2  = $rqst->edit_image_title2;

		$add_product_details_expanded_parent_ids  = $rqst->add_product_details_expanded_parent_ids;
		$add_product_details_expanded_files  = $rqst->add_product_details_expanded_files;


		$product_id  = $rqst->product_id;
		$product_code  = $rqst->code;
		
        $product = $this->pi->find($product_code)->map->format()->first();
        
		$product_public_id  = $product['public_id'];

		$files_for_replacement = [];

		// Images order, title2
		//////////////////////////////////////////////////////////////////////////////
		foreach ($edit_image_order as $key => $order) {
			$img_id = $edit_image_ids[$key];
			$order = $edit_image_order[$key];
			$title2 = $edit_image_title2[$key];

			$this->pii->edit(
				[
					'id'=>$img_id,
					'order'=>$order,
					'title2'=>$title2,
				]
			);

		}
		//////////////////////////////////////////////////////////////////////////////
		// Images order, title2
			

		// Images
		//////////////////////////////////////////////////////////////////////////////
		if($edit_image_files && count($edit_image_files)>0 && count($edit_image_files)) {
			$filename = "";
			$path = "";
			$path_thumbnail = "";

			foreach ($edit_image_files as $key => $file) {
				$files_for_replacement[] = [
					'id'=>$edit_image_ids[$key],
					'filename'=>$edit_image_filenames[$key],
					'file'=>$file,
				];
			}

			foreach($files_for_replacement as $ffr){

				$prod_img = $this->pi->findProductImage($ffr['id']);


				if($prod_img && $ffr['file']->getSize()) {

					$filename = $prod_img['filename'].'.'.$prod_img['ext'];
					$image_group = $prod_img['type'];
					$image_path = 'files/products/'.$product_public_id.'/images';

					// echo $filename.'<br>';
					// echo $image_group.'<br>';
					// echo $image_path.'<br>';
					// dd($prod_img);

					// image
					if (Storage::exists($image_path.'/'.$image_group.'/'.$filename)) {
						Storage::delete($image_path.'/'.$image_group.'/'.$filename);
					}
					$this->uploadFile(
						$ffr['file'],
						$filename,
						$image_path.'/'.$image_group
					);

					// image thumbnails
					if($prod_img['type']!='package-details-expanded') {
						if (Storage::exists($image_path.'/thumbnails/'.$image_group.'/'.$filename)) {
							Storage::delete($image_path.'/thumbnails/'.$image_group.'/'.$filename);
						}
						Storage::copy(
							$image_path.'/'.$image_group.'/'.$filename,
							$image_path.'/thumbnails/'.$image_group.'/'.$filename
						);
					}
				}
			}
		}
		//////////////////////////////////////////////////////////////////////////////
		// Image


		// Add Package Details Expanded image
		//////////////////////////////////////////////////////////////////////////////
		if( $add_product_details_expanded_files && count($add_product_details_expanded_files)>0 && count($add_product_details_expanded_files) ) {
			$filename = "";
			$path = "";
			$path_thumbnail = "";

			foreach ($add_product_details_expanded_files as $key => $file) {
				$files_for_replacement[] = [
					'parent_id'=>$add_product_details_expanded_parent_ids[$key],
					'file'=>$file,
				];
			}
			foreach($files_for_replacement as $ffr){
				if($ffr['file']) {
					$prod_img = $this->pi->findProductImage($ffr['parent_id']);

					$img_title = $prod_img['title2']; // PP Bag
					$order = $prod_img['order']; 

					$pi_id = $ffr['parent_id'];

					// Expanded
					$pi_expanded_id = $this->addImage(
						$product_id, 
						$product_public_id, 
						$product_code, 
						$this->filenameFormat($product_code).'-'.str_replace(' ','-',strtolower($img_title)), 
						$img_title, 
						'package-details-expanded', 
						$order, 
						$ffr['file']
					);

					if($pi_id>0 && $pi_expanded_id>0) {
						$this->pii->insertPackageDetails(
							[
								'pi_id'=>$pi_id,
								'pi_expanded_id'=>$pi_expanded_id,
							]
						);
					}
				}
				
			}
		}		
		//////////////////////////////////////////////////////////////////////////////
		// Add Package Details Expanded image



		if(true) {
			// return redirect()->route('edit_product');

			// dd('test');
			
			return redirect()
			->route('edit_product_images',['product_code'=>$product_code])
			->with( ['message' => 'Product Images Updated!'] );				
		}

	}




































	public function editProductContent($product_code)
	{
        $product = $this->pi->find($product_code)->map->format()->first();
        return view('admin.edit-product-content')->with('product',$product);
	}

	public function editProductContentPost(Request $rqst)
	{


		$product_id  = $rqst->product_id;
		$product_code  = $rqst->code;

		$data = [
			'overview'=>$rqst->product_overview,
			'certifications'=>$rqst->product_certifications,
			'html_content'=>$rqst->product_html,
			'meta_desc'=>$rqst->product_meta_desc,
			'meta_title'=>$rqst->product_meta_title,
			'name_type'=>$rqst->product_name_type, 
			'name_code_group'=>$rqst->product_name_code_group, 
			'name_code'=>$rqst->product_name_code, 
			'certificates_password'=>$rqst->product_certificates_password, 
			'pricelist_password'=>$rqst->product_pricelist_password, 
			'code'=>$rqst->product_code, 
			'order'=>$rqst->product_order,
		];

		// dd($data);

		$product_id = $this->pi->editProductContent($product_id, $data);



		if(true) {
			// return redirect()->route('edit_product_content',['product_code'=>$product_code]);
			return redirect()
			->route('edit_product_content',['product_code'=>$product_code])
			->with( ['message' => 'Product Content Updated!'] );			
		}

	}


































	public function renameProductImages($product_code){


        $product = $this->pi->find($product_code)->map->format()->first();
        
        $allimages = [];
        $allimages = array_merge($allimages,$product['images-main']->toArray());
        $allimages = array_merge($allimages,$product['images-3d-view']->toArray());
        $allimages = array_merge($allimages,$product['images-package-details']->toArray());
        $allimages = array_merge($allimages,$product['images-package-details-expanded']->toArray());
        
        
        // dd($product);
        // dd($allimages);
        
        
        $product_id = $product['id'];
        $product_public_id = $product['public_id'];

        foreach ($allimages as $key => $ai) {
            $image_group = $ai['type'];
            $filename = $ai['filename'];
            $filenameNew = 'vcare-earth-'.$product['code'].'-'.$ai['title2'];
            $filenameNew = str_replace(' ','-',strtolower($filenameNew));
            $filenameNew = str_replace('/','',$filenameNew);
            $filenameNew = str_replace('\\','',$filenameNew);
            $filenameNew = str_replace('(','',$filenameNew);
            $filenameNew = str_replace(')','',$filenameNew);
            $filenameNew = str_replace('+','',$filenameNew);
            $filenameNew = str_replace('[','',$filenameNew);
            $filenameNew = str_replace(']','',$filenameNew);

            $filenameNew = $filenameNew.'-'.$ai['id'];

            $data = [
                'filename'=>$filenameNew,
            ];




            $fileOldPath = 'files/products/'.$product_public_id.'/images/'.$image_group.'/'.$filename.'.'.$ai['ext'];
            $fileNewPath = 'files/products/'.$product_public_id.'/images/'.$image_group.'/'.$filenameNew.'.'.$ai['ext'];
            $fileOldThumbPath = 'files/products/'.$product_public_id.'/images/thumbnails/'.$image_group.'/'.$filename.'.'.$ai['ext_thumb'];
            $fileNewThumbPath = 'files/products/'.$product_public_id.'/images/thumbnails/'.$image_group.'/'.$filenameNew.'.'.$ai['ext_thumb'];

            $espFileOldPath = 'files/products/'.$product_public_id.'/lang/esp/images/'.$image_group.'/'.$filename.'.'.$ai['ext'];
            $espFileNewPath = 'files/products/'.$product_public_id.'/lang/esp/images/'.$image_group.'/'.$filenameNew.'.'.$ai['ext'];
            $espFileOldThumbPath = 'files/products/'.$product_public_id.'/lang/esp/images/thumbnails/'.$image_group.'/'.$filename.'.'.$ai['ext_thumb'];
            $espFileNewThumbPath = 'files/products/'.$product_public_id.'/lang/esp/images/thumbnails/'.$image_group.'/'.$filenameNew.'.'.$ai['ext_thumb'];

            $fileOldExists = Storage::exists($fileOldPath);
            $fileOldThumbExists = Storage::exists($fileOldThumbPath);
            $fileNewExists = Storage::exists($fileNewPath);
            $fileNewThumbExists = Storage::exists($fileNewThumbPath);

            // For espanol
            $espFileOldExists = Storage::exists($espFileOldPath);
            $espFileOldThumbExists = Storage::exists($espFileOldThumbPath);
            $espFileNewExists = Storage::exists($espFileNewPath);
            $espFileNewThumbExists = Storage::exists($espFileNewThumbPath);

              
            echo $product_public_id."<br>";
            echo (($fileOldExists)?'1':'0').' - '.$filename." <- old<br>";
            echo (($fileNewExists)?'1':'0').' - '.$filenameNew." <- new<br>";
            echo "thumbnails: <br>";
            echo (($fileOldThumbExists)?'1':'0').' - '.$filename." <- old<br>";
            echo (($fileNewThumbExists)?'1':'0').' - '.$filenameNew." <- new<br>";
                        
            echo "<br>Espanol: <br>";
            echo (($espFileOldExists)?'1':'0').' - '.$filename." <- old<br>";
            echo (($espFileNewExists)?'1':'0').' - '.$filenameNew." <- new<br>";
            echo "thumbnails: <br>";
            echo (($espFileOldThumbExists)?'1':'0').' - '.$filename." <- old<br>";
            echo (($espFileNewThumbExists)?'1':'0').' - '.$filenameNew." <- new<br>";

            echo "<br><br><br><br>";

            if ($fileOldExists && !$fileNewExists) 
            Storage::move(
                $fileOldPath,
                $fileNewPath
            );

            if ($fileOldThumbExists && !$fileNewThumbExists) 
            Storage::move(
                $fileOldThumbPath,
                $fileNewThumbPath
            );

            // Espanol
            if ($espFileOldExists && !$espFileNewExists) 
            Storage::move(
                $espFileOldPath,
                $espFileNewPath
            );

            if ($espFileOldThumbExists && !$espFileNewThumbExists) 
            Storage::move(
                $espFileOldThumbPath,
                $espFileNewThumbPath
            );
        
            $this->pi->editProductImage($ai['id'],$data);

        }




	}



	public function renameAllProductImages(){







	}











	























	public function editProductCertificates($product_code)
	{
        $product = $this->pi->find($product_code)->map->format()->first();
		return view('admin.edit-product-certificates')
		->with('product',$product);
	}


	public function editProductCertificatesPost(Request $rqst)
	{


		$product_id  = $rqst->product_id;
		$product_code  = $rqst->code;
		if($product_id) {

			$product = $this->pi->find($product_code)->map->format()->first();
            $product_public_id = $product['public_id'];

			if(count($product['certificates'])<1 || !count($product['certificates'])) {	
				$order = 0;
				$fn_end = '';
			}
			else {
				$order = $product['certificates']->max('order')+1;
				$fn_end = '-'.$order;
			}

			// Product
			if($rqst->certifications_file) {
				$x = $order;
				foreach ($rqst->certifications_file as $key => $file) {
					if($file) {
						$cert_data = 
						[
							'title'=>$rqst->certifications_title[$key],
							// 'code'=>$this->filenameFormat($product_code.'-'.$rqst->certifications_title[$key]),
							'code'=>$rqst->certifications_title[$key],
							'ext'=>'pdf',
							'type'=>'certificate',
							'order'=>$x,
						];
						
						$cert_id = $this->pi->createProductCertificate($product_id,$cert_data);
						
						$this->uploadFile(
							$file,
							$cert_id,
							'files/products/'.$product_public_id.'/certificates'
						);
						$x++;
					}
				}
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////
		// Product


		
		if(true) {
			// return redirect()->route('edit_product');
			return redirect()
			->route('edit_product_certificates',['product_code'=>$product_code])
			->with( ['message' => 'Product Certificate Updated!'] );
		}

	}



	public function editProductCertificateNamesPost(Request $rqst)
	{

		$rcn_id = $rqst->certification_names_id;
		$rcn_title = $rqst->certification_names_title;
		$rcn_filename = $rqst->certification_names_filename;
		$rcn_order = $rqst->certification_names_order;


		$product_id  = $rqst->product_id;
		$product_code  = $rqst->code;
		if($product_id) {

			$product = $this->pi->find($product_code)->map->format()->first();

			foreach ($rcn_id as $key => $cn_id) {
				if($cn_id) {
					
					// echo $rcn_title[$key]." = ".$rcn_filename[$key].'<br><br>';
					// $filename = trim(strtolower(str_replace(' ','-',$rcn_filename[$key])));
					$filename = $rcn_filename[$key];
					$filename = htmlentities($filename, ENT_QUOTES, 'UTF-8');
					// $filename = trim(strtolower(str_replace(' ','-',$filename)));
					$filename = html_entity_decode($filename);

					$data =  [
						'title'=>$rcn_title[$key],
						'filename'=> $filename,
						'order'=> $rcn_order[$key],
					];

					$this->pci->editById($cn_id, $data);
					
					
				}
			}
			
		}


		
		if(true) {
			return redirect()->route('edit_product_certificates',['product_code'=>$product_code]);
		}

	}

























	
















	public function addArticle()
	{
        return view('admin.add-article');
	}

	public function addArticlePost(Request $rqst)
	{

		
				
		$article = [
			'article_title'=>$rqst->article_title,
			'article_short_content'=>$rqst->article_short_content,
			'article_html_content'=>$rqst->article_html_content,
			'article_meta_title'=>$rqst->article_meta_title,
			'article_meta_desc'=>$rqst->article_meta_desc,
			'article_url'=>$rqst->article_url,
		];

		$articleAdded = $this->bi->create($article);

		if($articleAdded) {
			return redirect()->route('blog_single_page',
			[
				'blog_public_id'=>$articleAdded->b_public_id,
				'blog_title'=>str_replace(' ','-',strtolower($rqst->article_title)),
			]);
		} else 
		dd('ERROR!');
	}









	public function editArticleContent($public_id)
	{
        $blog = $this->bi->find($public_id)->map->format()->first();
        return view('admin.edit-article-content')->with('blog',$blog);
	}

	public function editArticleContentPost(Request $rqst)
	{


		$public_id  = $rqst->public_id;

		
				
		$data = [
			'article_title'=>$rqst->article_title,
			'article_short_content'=>$rqst->article_short_content,
			'article_html_content'=>$rqst->article_html_content,
			'article_meta_title'=>$rqst->article_meta_title,
			'article_meta_desc'=>$rqst->article_meta_desc,
			'article_url'=>$rqst->article_url,
		];

		// dd($data);

		$article = $this->bi->editArticleContent($public_id, $data);



		if(true) {
			// return redirect()->route('edit_product_content',['product_code'=>$product_code]);
			return redirect()
			->route('edit_article_content',['public_id'=>$public_id])
			->with( ['message' => 'Article Content Updated!'] );			
		}

	}















	

































	























































	// public function addProductMainImage($product_code)
	// {
    //     $product = $this->pi->find($product_code)->map->format()->first();
    //     return view('admin.edit-product-main-image')->with('product',$product);
	// }

	// public function addProductMainImagePost(Request $rqst)
	// {


	// 	$product_id  = $rqst->product_id;
	// 	$product_code  = $rqst->code;
		
    //     $product = $this->pi->find($product_code)->map->format()->first();
    //     $product_public_id = $product['public_id'];

	// 	if(count($product['main-image'])<1 || !count($product['main-image'])) {	
	// 		$order = 0;
	// 	}
	// 	else {
	// 		$order = $product['main-image']->max('order')+1;
	// 	}

	// 	// Main
	// 	$this->addImage(
	// 		$product_id, 
	// 		$product_public_id, 
	// 		$product_code, 
	// 		$this->filenameFormat($product_code).'-main', 
	// 		'Main', 
	// 		'main', 
	// 		$order, 
	// 		$rqst->image_main
	// 	);


	// 	if(true) {
	// 		return redirect()->route('edit_product');
	// 	}

	// }















































    
	private function addImage($product_id, $product_public_id, $product_code, $filename, $image_type, $image_group, $order=0, $image_file){

        $product_image_id = 0;
        
		if($image_file) {
            $ext = $image_file->getClientOriginalExtension();
            $extThumb = $image_file->getClientOriginalExtension();
    
    
			$img = [
				'image_name'=>$this->nameFormat($product_code),
				'image_title'=>$image_type,
				'filename'=>'vcare-earth-'.$filename,
				'ext'=>$ext,
				'ext_thumb'=>$extThumb,
				'type'=>$image_group,
				'order'=>$order,
				'product_id'=>$product_id,
			];
			$product_image_id = $this->pi->createProductImage($img);     		
			
			$this->uploadFile(
				$image_file,
				'vcare-earth-'.$filename.'-'.$product_image_id.'.'.$ext,
				'files/products/'.$product_public_id.'/images/'.$image_group
			);

			// thumbnails
			Storage::copy(
				'files/products/'.$product_public_id.'/images/'.$image_group.'/vcare-earth-'.$filename.'-'.$product_image_id.'.'.$ext,
				'files/products/'.$product_public_id.'/images/thumbnails/'.$image_group.'/vcare-earth-'.$filename.'-'.$product_image_id.'.'.$ext
			);
		}


		return $product_image_id;
	}

	private function uploadFile($file, $filename, $filepath = 'temp'){
   
		return $file->move(public_path($filepath), $filename);
       		
	}

	private function filenameFormat($filename){
		
		$retVal = strtolower($filename);
		$retVal = str_replace(' ','-',$retVal);
		return $retVal;       		
	}

	private function nameFormat($name){
		
		$retVal = strtoupper($name);
		$retVal = str_replace('-',' ',$retVal);
		$retVal = str_replace('_',' ',$retVal);
		return $retVal;       		
	}

}
