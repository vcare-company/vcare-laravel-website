<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Helper\Helper;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use App\Interfaces\Product\ProductInterface;
use App\Interfaces\Product\ProductAccessLogInterface;
use App\Interfaces\Category\CategoryInterface;


use App\Product\ProductInventory;
use App\Product\ProductInventoryComments;
use App\Product\ProductInventoryLog;

class SoraController extends Controller
{

	/**
	 * Initialize Interface
	 */
	public function __construct(CategoryInterface $ci, ProductInterface $pi,ProductAccessLogInterface $pali)
	{
		$this->pi = $pi;
		$this->ci = $ci;
		$this->pali = $pali;
	}





	public function sora(){

        $lisp = session('loggedin-status-sora');

		if($lisp) {
			$loggedinSora = true;
		} else {
			$loggedinSora = false;
		}
		
		if($loggedinSora) {
			return view('sora.sora');
		} else {
			return redirect()->route('sora-login');			
		}
	}



	public function soraInventoryPost(Request $rqst){

        $lisp = session('loggedin-status-sora');

		if($lisp) {
			$loggedinSora = true;
		} else {
			$loggedinSora = false;
		}
		
		if($loggedinSora) {



			
			$data = [
				'pin_status' =>$rqst->inventory_status,
				'pin_quantity' =>$rqst->inventory_quantity,
				'pin_client' => $rqst->inventory_client,
				'pin_added_by' => $rqst->inventory_added_by,
				'pin_p_id' => $rqst->inventory_product_id,
				'pin_added_by_ip' => $rqst->inventory_added_by_ip,
			];
				


			if(ProductInventory::where('pin_id', $rqst['inventory_id'])->exists()) {
				ProductInventory::where('pin_id', $rqst['inventory_id'])->update($data);
				ProductInventoryLog::create([
					'pil_status' =>$rqst->inventory_status,
					'pil_quantity' =>$rqst->inventory_quantity,
					'pil_client' => $rqst->inventory_client,
					'pil_added_by' => $rqst->inventory_added_by,
					'pil_pin_id' => $rqst->inventory_id,
					'pil_added_by_ip' => $rqst->inventory_added_by_ip,
				]);

			} else {
				ProductInventory::create($data);
				ProductInventoryLog::create([
					'pil_status' =>$rqst->inventory_status,
					'pil_quantity' =>$rqst->inventory_quantity,
					'pil_client' => $rqst->inventory_client,
					'pil_added_by' => $rqst->inventory_added_by,
					'pil_pin_id' => ProductInventory::orderBy('created_at', 'desc')->first()->pin_id,
					'pil_added_by_ip' => $rqst->inventory_added_by_ip,
				]);
			}









			$categories_parent = $this->ci->parentsOnly()->map->formatDownloadsPage();
			$data = ProductInventory::all();




			foreach($categories_parent as $cat_par){
				foreach($cat_par['children'] as $cat) {
					foreach($cat['products'] as $p) { 
						foreach($data as $d){
							if($d['pin_p_id'] == $p['id']){
								$q_data[$p['id']] = [
									'Production' => ProductInventory::where([
										'pin_status' => 'Production',
										'pin_p_id' => $p['id']
									])->sum('pin_quantity'),

									'Shipping' => ProductInventory::where([
										'pin_status' => 'Shipping',
										'pin_p_id' => $p['id']
									])->sum('pin_quantity'),

									'In-stock' => (ProductInventory::where([
										'pin_status' => 'In-stock',
										'pin_p_id' => $p['id']
									])->sum('pin_quantity')) - (ProductInventory::where([
										'pin_status' => 'Sold',
										'pin_p_id' => $p['id']
									])->sum('pin_quantity')),
									
									'Sold' => ProductInventory::where([
										'pin_status' => 'Sold',
										'pin_p_id' => $p['id']
									])->sum('pin_quantity'),
								];
							}
						}
					}
				}
			}

	// dd($q_data);

		
			
			
			return redirect()->route('sora-inventory-page');	

		} else {
			return redirect()->route('sora-login');			
		}
	
	
	}


	public function soraInventoryCommentPost(Request $rqst){

		$lisp = session('loggedin-status-sora');

		if($lisp) {
			$loggedinSora = true;
		} else {
			$loggedinSora = false;
		}
		
		if($loggedinSora) {



			
			$data = [
				'pic_comment' => $rqst->inventory_comment,
				'pic_added_by' => $rqst->inventory_comment_added_by,
				'pic_p_id' => $rqst->inventory_comment_product_id,
			];

			ProductInventoryComments::create($data);			


		return redirect()->route('sora-inventory-page');	

		} else {
			return redirect()->route('sora-login');			
		}
	

	}

	public function soraInventory(){

        $lisp = session('loggedin-status-sora');

		if($lisp) {
			$loggedinSora = true;
		} else {
			$loggedinSora = false;
		}
		
		if($loggedinSora) {
				
			$categories_parent = $this->ci->parentsOnly()->map->formatDownloadsPage();

			// dd($categories_parent);

			$data = ProductInventory::all();
			$ldata = ProductInventoryLog::all();
			$cdata = ProductInventoryComments::all();


			$q_data=[];



		
			foreach($categories_parent as $cat_par){
				foreach($cat_par['children'] as $cat) {
					foreach($cat['products'] as $p) { 
						foreach($data as $d){
							if($d['pin_p_id'] == $p['id']){
								$q_data[$p['id']] = [
									'Production' => ProductInventory::where([
										'pin_status' => 'Production',
										'pin_p_id' => $p['id']
									])->sum('pin_quantity'),

									'Shipping' => ProductInventory::where([
										'pin_status' => 'Shipping',
										'pin_p_id' => $p['id']
									])->sum('pin_quantity'),

									'In-stock' => (ProductInventory::where([
										'pin_status' => 'In-stock',
										'pin_p_id' => $p['id']
									])->sum('pin_quantity')) - (ProductInventory::where([
										'pin_status' => 'Sold',
										'pin_p_id' => $p['id']
									])->sum('pin_quantity')),
									
									'Sold' => ProductInventory::where([
										'pin_status' => 'Sold',
										'pin_p_id' => $p['id']
									])->sum('pin_quantity'),
								];
							}
						}
					}
				}
			}
			
			
			// 'production' => ProductInventory::where('pin_status','=','production')->sum('pin_quantity'),
			// 'shipped' => ProductInventory::where('pin_status','=','shipped')->sum('pin_quantity'),
			// 'shipping' => ProductInventory::where('pin_status','=','shipping')->sum('pin_quantity'),


			// dd($q_data);





			// dd($data->pin_added_by);


			return view('sora.sora-inventory')
			->with('inventory_data' , $data)
			->with('inventory_log_data' , $ldata)
			->with('inventory_comment_data' , $cdata)
			->with('q_data' , $q_data)
			->with('categories_parent',$categories_parent);

		} else {
			return redirect()->route('sora-login');			
		}
	
	
	}



	public function soraCertificates(){

        $lisp = session('loggedin-status-sora');

		if($lisp) {
			$loggedinSora = true;
		} else {
			$loggedinSora = false;
		}
		
		if($loggedinSora) {


			$passwords = [
				'ffp2-ross' => ['FFP2 ROSS',env('APP_URL').'/products/ffp2-ross/certificates','nfj3kckw',env('APP_URL').'/products/ffp2-ross/original_certificates', '36oyypfrpo'],
				'ffp2-max' => ['FFP2 MAX',env('APP_URL').'/products/ffp2-max/certificates','s8sj2gj4'],
				'ffp2-ann' => ['FFP2 ANN',env('APP_URL').'/products/ffp2-ann/certificates','kjd8roq',env('APP_URL').'/products/ffp2-ann/original_certificates', '027jik73wm'],
				'ffp2-jane' => ['FFP2 JANE',env('APP_URL').'/products/ffp2-jane/certificates','dh7lss23',env('APP_URL').'/products/ffp2-jane/original_certificates', 'lciir6p1il'],
				'ffp2-kent' => ['FFP2 KENT',env('APP_URL').'/products/ffp2-kent/certificates','bdmj84jf',env('APP_URL').'/products/ffp2-kent/original_certificates', 'qjynppzq71'],
				'ffp2-kai' =>   ['FFP2 KAI',env('APP_URL').'/products/ffp2-kai/certificates','dh6sjsk9',env('APP_URL').'/products/ffp2-kai/original_certificates', '2gyptgn0ti'],
				'ffp2-dax' =>   ['FFP2 DAX',env('APP_URL').'/products/ffp2-dax/certificates','vvj8shjsd',env('APP_URL').'/products/ffp2-dax/original_certificates', 'm0517ed45y'],
				'ffp2-kate' =>   ['FFP3 KATE',env('APP_URL').'/products/ffp2-kate/certificates','u7sa1yxg06',env('APP_URL').'/products/ffp2-kate/original_certificates', 'pgl0k3pttw'],

				
				'ffp3-star' => ['FFP3 STAR',env('APP_URL').'/products/ffp3-star/certificates','mko93kssd',env('APP_URL').'/products/ffp3-star/original_certificates', 'cmrq18l456'],
				'ffp3-tani' =>  ['FFP3 TANI',env('APP_URL').'/products/ffp3-tani/certificates','vb97sdj3',env('APP_URL').'/products/ffp3-tani/original_certificates', 'or1b3xvw6p'],
				'ffp3-jojo' =>  ['FFP3 JOJO',env('APP_URL').'/products/ffp3-jojo/certificates','kchb38dh',env('APP_URL').'/products/ffp3-jojo/original_certificates', 'r2es097km1'],
				'ffp3-luna' =>  ['FFP3 LUNA',env('APP_URL').'/products/ffp3-luna/certificates','dnsd7j2s',env('APP_URL').'/products/ffp3-luna/original_certificates', '96t291514j'],
				'ffp3-skye' =>  ['FFP3 SKYE',env('APP_URL').'/products/ffp3-skye/certificates','c732hjssf',env('APP_URL').'/products/ffp3-skye/original_certificates', 'qz4uq65kj5'],
				'ffp3-tia' =>   ['FFP3 TIA',env('APP_URL').'/products/ffp3-tia/certificates','67fgssvb7',env('APP_URL').'/products/ffp3-tia/original_certificates', 'guni5guhao'],
				'ffp3-wei' =>   ['FFP3 WEI',env('APP_URL').'/products/ffp3-wei/certificates','vx8sdhj3',env('APP_URL').'/products/ffp3-wei/original_certificates', 'hsf9awn4ca'],
				'ffp3-sue' =>   ['FFP3 SUE',env('APP_URL').'/products/ffp3-sue/certificates','Zk6hNsyQdJ',env('APP_URL').'/products/ffp3-sue/original_certificates', '1v9nvh3hsi'],
				'ffp3-kim' =>   ['FFP3 KIM',env('APP_URL').'/products/ffp3-kim/certificates','DFNinQCFfe',env('APP_URL').'/products/ffp3-kim/original_certificates', 'sob3b01v6d'],

				'n95-jet' =>   ['N95 JET',env('APP_URL').'/products/n95-jet/certificates','gfdss34fdd',env('APP_URL').'/products/n95-jet/original_certificates', 'try9s2urcg'],
				'n95-lei' =>   ['N95 LEI',env('APP_URL').'/products/n95-lei/certificates','vm8kds4fdd',env('APP_URL').'/products/n95-lei/original_certificates', 'bk5hgkxbyd'],


				'kn95-lou' => ['KN95 LOU',env('APP_URL').'/products/kn95-lou/certificates','klo8dsh3'],
				'acf-nas' => ['ACF NAS',env('APP_URL').'/products/acf-nas/certificates','fh74jsdl'],
				
				'rtest-finn' => ['RTEST FINN',env('APP_URL').'/products/rtest-finn/certificates','sjhf63nk'],
				'rtest-marc' => ['RTEST MARC',env('APP_URL').'/products/rtest-marc/certificates','3fnb3kv'],
				'rtest-reid' => ['RTEST REID',env('APP_URL').'/products/rtest-reid/certificates','sd344fd'],
				
				'mfmask-gem' => ['MFMASK GEM',env('APP_URL').'/products/mfmask-gem/certificates','dhg832jv',env('APP_URL').'/products/mfmask-gem/original_certificates', 'nscea3hy8e'],
				
				'srgwn-ria' => ['SRGWN RIA',env('APP_URL').'/products/srgwn-ria/certificates','s83jfnjld'],
				'srgwn-dawn' => ['SRGWN DAWN',env('APP_URL').'/products/srgwn-dawn/certificates','kjdj39dn'],
				
				'isgwn-lee' => ['ISGWN LEE',env('APP_URL').'/products/isgwn-lee/certificates','j4kl3ln4'],
				
				'cvgwn-kat' => ['CVGWN KAT',env('APP_URL').'/products/cvgwn-kat/certificates','hvu3mnv'],

				
				
				// 'kn95me-sap' => ['KN95 ME SAP',env('APP_URL').'/products/kn95me-sap/certificates','hd7illclf'],


				// new
				///////////////////
				///////////////////
				// new


			];

			$categories_parent = $this->ci->parentsOnly()->map->formatDownloadsPage();

		



			return view('sora.sora-certificates')
			->with('passwords',$passwords)
			->with('categories_parent',$categories_parent);

		} else {
			return redirect()->route('sora-login');			
		}
	
	
	}
	
	


	public function soraLogin(){

		return view('sora.sora-login')
		->with('error_message','');

	}
	
	


	public function soraLoginPost(Request $rqst) {

		$pass = $rqst->input('password');

		if($pass==env('APP_SORA_PASSWORD')) {
			$rqst->session()->put('loggedin-status-sora', '1');
			return redirect()->route('sora-page');
		} else {
			$rqst->session()->forget('loggedin-status-sora');
			return view('sora.sora-login')
			->with('error_message','Password is invalid!');

		}
	}









}