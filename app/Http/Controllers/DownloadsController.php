<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\Product\ProductInterface;
use App\Interfaces\Category\CategoryInterface;

class DownloadsController extends Controller
{
    
	/**
	 * Initialize Interface
	 */
	public function __construct(CategoryInterface $ci, ProductInterface $pi)
	{
		$this->pi = $pi;
		$this->ci = $ci;
	}


    public function index(){
        // $categories = $this->ci->all()->map->formatDownloadsPage();
		$categories_parent = $this->ci->parentsOnly()->map->formatDownloadsPage();
		
        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Downloads','route'=>''],
        ];


		// dd($categories);
		$company_resources = [];
		$company_resources[] = 
		[	
			'image'=>'company-profile.jpg', 
			'group'=>'ABOUT US',
			'name'=>'Company Profile', 
			'code'=>'company-profile', 
			'route'=>'companyProfile',
			'active'=>'is-active',
			'style'=>'',
		];
		$company_resources[] = 
		[
			'image'=>'respirator-comparison.jpg', 
			'group'=>'OUR PRODUCTS',
			'name'=>'VCare Respirator Comparison', 
			'code'=>'vcare-respirator-comparison', 
			'route'=>'respiratorComparison',
			'active'=>'',
			'style'=>'font-size: 0.9rem;',
		];
		$company_resources[] = 
		[
			'image'=>'vcare-class-standards-comparison.jpg', 
			'group'=>'OUR PRODUCTS',
			'name'=>'Class Standards Comparison', 
			'code'=>'class-standards-comparison', 
			'route'=>'classStandardsComparison',
			'active'=>'',
			'style'=>'font-size: 0.9rem;',
		];

		// $company_resources[] = ['image'=>'product-catalog.png', 'group'=>'OUR PRODUCTS','name'=>'PRODUCT CATALOG', 'filename'=>'vcare-company-profile.pdf'];
		

		return view('downloads')
		->with('company_resources',$company_resources)
		// ->with('categories',$categories);
		->with('categories_parent',$categories_parent)
		->with('breadcrumbs',$breadcrumbs);

    }


}
