<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\Category\CategoryInterface;
use App\Interfaces\Product\ProductInterface;


class SearchController extends Controller
{

	/**
	 * Initialize Interface
	 */
	public function __construct(CategoryInterface $ci,ProductInterface $pi)
	{
		$this->ci = $ci;
		$this->pi = $pi;
	}
    

    public function search(){
        
        $categories = $this->ci->parentsOnly()->map->formatAllProductsPage();

        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Search','route'=>''],
        ];

        return view('search.search')
        ->with('search_string_post','')
        ->with('categories',$categories)
        ->with('breadcrumbs',$breadcrumbs);


    }


    public function searchPost(Request $rqst){
        $categories = $this->ci->parentsOnly()->map->formatAllProductsPage();

        $breadcrumbs =[
            ['title'=>'Home','route'=>'home'],
            ['title'=>'Search','route'=>''],
        ];

        return view('search.search')
        ->with('search_string_post',$rqst->search_string)
        ->with('categories',$categories)
        ->with('breadcrumbs',$breadcrumbs);

    }

}
