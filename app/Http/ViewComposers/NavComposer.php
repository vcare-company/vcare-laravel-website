<?php

namespace App\Http\ViewComposers;


use Illuminate\View\View;
use App\Interfaces\Category\CategoryInterface;

class NavComposer
{


	/**
	 * Initialize Interface
	 */
	public function __construct(CategoryInterface $category)
	{
		$this->category = $category;
	}


    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        // get top categories 
        $parent_categories = $this->category->parentsOnly()->map->formatNav();

        // dd($parent_categories);
        $view->with('parent_categories', $parent_categories);
    }
}