<?php

namespace App\Http\ViewComposers;


use Illuminate\View\View;
use App\Interfaces\Product\ProductInterface;
use App\Interfaces\Category\CategoryInterface;

class FooterComposer
{


	/**
	 * Initialize Interface
	 */
	public function __construct(ProductInterface $product,CategoryInterface $category)
	{
		$this->category = $category;
		$this->product = $product;
	}


    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        // $products = $this->product->all()->map->formatMin();
        // $view->with('products', $products);
        // dd($products);

        // $categories = $this->category->all()->map->format();
        // $view->with('categories', $categories);

        $parent_categories = $this->category->parentsOnly()->map->formatNav();
        $view->with('parent_categories', $parent_categories);

    }
}