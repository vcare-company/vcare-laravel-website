<?php 

namespace App\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Helper
{

    public static function isLive(){
        
        if( url('/')=='https://vcare.earth' || url('/')=='http://vcare.earth')
        $retVal = true;
        else
        $retVal = false;
        
        return $retVal;
    }

    public static function adminMode(){
        
        // $rqst = new Request;

        // $loggedInAdmin = $rqst->session()->get('loggedin-status-admin');
        $loggedInAdmin = session('loggedin-status-admin');;
        
        if(env('APP_DEV_MODE') && $loggedInAdmin)
        $retVal = true;
        else
        $retVal = false;
        
        return $retVal;
    }

	public static function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
        

        if (isset($_SERVER['HTTP_CLIENT_IP']))
        $real_ip_address = $_SERVER['HTTP_CLIENT_IP'];
    
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $real_ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else
        $real_ip_address = $_SERVER['REMOTE_ADDR'];
    

		$output = NULL;
		if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
			$ip = $_SERVER["REMOTE_ADDR"];
			if ($deep_detect) {
				if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
					$ip = $_SERVER['HTTP_CLIENT_IP'];
			}
		}
		$purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
		$support    = array("country", "countrycode", "state", "region", "city", "location", "address", "ip");
		$continents = array(
			"AF" => "Africa",
			"AN" => "Antarctica",
			"AS" => "Asia",
			"EU" => "Europe",
			"OC" => "Australia (Oceania)",
			"NA" => "North America",
			"SA" => "South America"
		);
		if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
			$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
			if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
				switch ($purpose) {
					case "location":
						$output = array(
							"city"           => @$ipdat->geoplugin_city,
							"state"          => @$ipdat->geoplugin_regionName,
							"country"        => @$ipdat->geoplugin_countryName,
							"country_code"   => @$ipdat->geoplugin_countryCode,
							"continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
							"continent_code" => @$ipdat->geoplugin_continentCode
						);
						break;
					case "address":
						$address = array($ipdat->geoplugin_countryName);
						if (@strlen($ipdat->geoplugin_regionName) >= 1)
							$address[] = $ipdat->geoplugin_regionName;
						if (@strlen($ipdat->geoplugin_city) >= 1)
							$address[] = $ipdat->geoplugin_city;
						$output = implode(", ", array_reverse($address));
						break;
					case "city":
						$output = @$ipdat->geoplugin_city;
						break;
					case "state":
						$output = @$ipdat->geoplugin_regionName;
						break;
					case "region":
						$output = @$ipdat->geoplugin_regionName;
						break;
					case "country":
						$output = @$ipdat->geoplugin_countryName;
						break;
					case "countrycode":
						$output = @$ipdat->geoplugin_countryCode;
						break;
					case "ip":
						$output = $real_ip_address;
						break;
				}
			}
		}
		return $output;
	}


	public static function formatImageFiles($path){
		$images = [];
		
		foreach(Storage::files($path) as $file) {
			$filename = explode('/',$file);
			$filename = $filename[count($filename)-1];
			$filename = explode('.',$filename);
			if(count($filename)>1)
			array_pop($filename);
			$filename = implode($filename);

			$ext = explode('.',$file);
			$ext = $ext[count($ext)-1];
			
			$images[] = [
				'path'=> $file,
				'filename'=> $filename,
				'ext'=> $ext,
			]; 

		}
		
		return $images;


	}
	

}