<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class ProductPricelist extends Model
{
    protected $table = 'product_pricelists';
    protected $primaryKey = 'pp_id';
    
    
    public function format(){
        return [
            'content1' => $this->pp_content1,
            'content2' => $this->pp_content2,
            'table' => $this->pp_table,
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }

    public function product()
    {
        return $this->belongsTo('App\Product\Product', 'pp_p_id', 'p_id');
    }    

}
