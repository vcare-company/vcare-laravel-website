<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

use App\Util;


class ProductImage extends Model
{
    protected $table = 'product_images';
    protected $primaryKey = 'pi_id';
    
    
    public function format(){
        return [
            'id' => $this->pi_id,
            'title' => $this->pi_title,
            'title2' => $this->pi_title2,
            'filename' => $this->pi_filename,
            'ext' => $this->pi_ext,
            'ext_thumb' => $this->pi_ext_thumb,
            'order' => $this->pi_order,
            'type' => $this->pi_type,
            'product_id' => $this->pi_p_id,
        ];
    }


    public function formatWithPackageDetails(){
        return [
            'id' => $this->pi_id,
            'title' => $this->pi_title,
            'title2' => $this->pi_title2,
            'filename' => $this->pi_filename,
            'ext' => $this->pi_ext,
            'ext_thumb' => $this->pi_ext_thumb,
            'order' => $this->pi_order,
            'type' => $this->pi_type,
            'product_id' => $this->pi_p_id,
            'product_details_expanded_image' => $this->productDetailsExpanded()->get()->map->format()->first(),
        ];
    }

    public function productDetailsExpanded()
    {
        return $this->belongsToMany('App\Product\ProductImage', 'product_images_product_details', 'product_details_image_id', 'product_details_expanded_image_id');
    }

}