<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class ProductInventoryComments extends Model
{
    protected $table = 'product_inventory_comments';
    protected $primaryKey = 'pic_id';
    
    protected $guarded = [];

    
    public function product()
    {
        return $this->belongsTo('App\Product\Product', 'pic_p_id', 'p_id');
    }         
}
