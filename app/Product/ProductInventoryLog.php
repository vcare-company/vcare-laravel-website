<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class ProductInventoryLog extends Model
{
    protected $table = 'product_inventory_log';
    protected $primaryKey = 'pil_id';
    
    protected $guarded = [];

    
    public function inventory()
    {
        return $this->belongsTo('App\Product\ProductInventory', 'pil_pin_id', 'pin_id');
    }    
    
}
