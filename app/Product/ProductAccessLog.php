<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class ProductAccessLog extends Model
{
    protected $table = 'product_access_logs';
    protected $primaryKey = 'pal_id';
    // public $timestamps = true;
            
}
