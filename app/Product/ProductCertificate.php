<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class ProductCertificate extends Model
{
    //
    protected $table = 'product_certificates';
    protected $primaryKey = 'pc_id';
    
    
    public function format(){
        return [
            'id' => $this->pc_id,
            'title' => $this->pc_title,
            'filename' => $this->pc_code,
            'ext' => $this->pc_ext,
            'order' => $this->pc_order,
            'type' => $this->pc_type,
            'product_id' => $this->pc_p_id,
            'product_code' => $this->product->p_code,
        ];
    }

    
    public function product()
    {
        return $this->belongsTo('App\Product\Product', 'pc_p_id', 'p_id');
    }    
    

}
