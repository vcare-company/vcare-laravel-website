<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class ProductInventory extends Model
{
    protected $table = 'product_inventory';
    protected $primaryKey = 'pin_id';
    
    protected $guarded = [];

    
     public function inventorylog()
    {
        return $this->hasMany('App\Product\ProductInventoryLog', 'pil_pin_id', 'pin_id');
    }    
    
}
