<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;
use App\Helper\Helper;
use Illuminate\Support\Facades\Storage;

use App\Util;


class Product extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'p_id';
    
    
    public function format(){
        // dd($this->category()->get());
        return [
            'id' => $this->p_id,
            'public_id' => $this->p_public_id,
            'code' => $this->p_code,
            'name' => $this->p_name,
            'name_type' => $this->p_name_type,
            'name_code' => $this->p_name_code,
            'name_code_group' => $this->p_name_code_group,
            'coming_soon' => $this->p_coming_soon,

            'overview' => $this->p_overview,
            'certifications' => $this->p_certifications,
            'other-text' => $this->p_text,
            'html_content' => $this->p_html_content,


            // 'datasheet' => $this->p_datasheet,

            // 'features' => $this->p_features,
            // 'benefits' => $this->p_benefits,
            // 'packaging' => $this->packaging()->get()->map->format(),

            'meta_title' => $this->p_meta_title,
            'meta_desc' => $this->p_meta_desc,

            'order' => $this->p_order,

            // 'category_name' => $this->category->c_name,
            // 'category_html' => $this->category->c_html,
            // 'category_code' => $this->category->c_code,

            'category' => $this->category()->get()->map->formatNoProducts()->first(),

            'main-image' => $this->images()->where('pi_type','main')->orderBy('pi_order')->get()->map->format()->first(),
            'images' => $this->images()->whereIn('pi_type', ['main','3d-view'])->orderBy('pi_order')->get()->map->format(),
            // 'images' => $this->images()->orderBy('pi_order')->get()->map->format(),

            'images-main' => $this->images()->where('pi_type','main')->orderBy('pi_order')->get()->map->format(),
            'images-3d-view' => $this->images()->whereIn('pi_type', ['3d-view'])->orderBy('pi_order')->get()->map->format(),
            'images-package-details' => $this->images()->whereIn('pi_type', ['package-details'])->orderBy('pi_order')->get()->map->formatWithPackageDetails(),
            'images-package-details-expanded' => $this->images()->whereIn('pi_type', ['package-details-expanded'])->orderBy('pi_order')->get()->map->format(),
            'certificates' => $this->certificates()->whereIn('pc_type', ['certificate'])->orderBy('pc_order')->get()->map->format(),

            'files_with_markings' => $this->filesWithMarkings(),

            'created_at' => $this->created_at->diffForHumans(),
        ];
    }


    // public function formatDownloadsPage(){
    //     return [
    //         'id' => $this->p_id,
    //         // 'code' => strtolower($this->p_name_code_group.'-'.str_replace(' ','-',$this->p_name)),
    //         'code' => $this->p_code,
    //         'name' => $this->p_name,
    //         'name_type' => $this->p_name_type,
    //         'name_code' => $this->p_name_code,
    //         'name_code_group' => $this->p_name_code_group,
    //         'coming_soon' => $this->p_coming_soon,
            
    //         'datasheet' => $this->p_datasheet,
    //         // 'category_name' => $this->category->c_name,
    //         // 'category_html' => $this->category->c_html,
    //         // 'category_code' => $this->category->c_code,
    //         'category' => $this->category()->get()->map->formatNoProducts()->first(),

    //         'main-images' => $this->images()->where('pi_type','main')->orderBy('pi_order')->get()->map->format(),
    //         'images-3d-view' => $this->images()->whereIn('pi_type', ['3d-view'])->orderBy('pi_order')->get()->map->format(),
    //         'images-package-details' => $this->images()->whereIn('pi_type', ['package-details'])->orderBy('pi_order')->get()->map->format(),
    //         'certificates' => $this->certificates()->whereIn('pc_type', ['certificate'])->orderBy('pc_order')->get()->map->format(),

    //         'created_at' => $this->created_at->diffForHumans(),
    //     ];
    // }
    
    
    public function formatMin(){
        return [
            'id' => $this->p_id,
            'public_id' => $this->p_public_id,
            // 'code' => strtolower($this->p_name_code_group.'-'.str_replace(' ','-',$this->p_name)),
            'code' => $this->p_code,
            'name' => $this->p_name,
            'name_type' => $this->p_name_type,
            'name_code' => $this->p_name_code,
            'name_code_group' => $this->p_name_code_group,
            'coming_soon' => $this->p_coming_soon,
            
            'meta_title' => $this->p_meta_title,
            'meta_desc' => $this->p_meta_desc,

            'order' => $this->p_order,
            
            'pricelist_password' => $this->p_pricelist_password,
            'certificates_password' => $this->p_certificates_password,
            'original_certificates_password' => $this->p_original_certificates_password,
            'obm_certificates_password' => $this->p_obm_certificates_password,
            
            // 'category_name' => $this->category->c_name,
            // 'category_html' => $this->category->c_html,
            // 'category_code' => $this->category->c_code,
            'category' => $this->category()->get()->map->formatNoProducts()->first(),
            
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
    
    
    public function formatPricelist(){
        
        $pricelist = [
            'content' => '',
            'certification' => [],
            'table' => [],
        ];
        if($this->pricelist) {
            $pricelist = [
                'content' => $this->pricelist->pp_content,
                'certification' => json_decode($this->pricelist->pp_certifications),
                'table' => json_decode($this->pricelist->pp_table),
            ];
        }
        
        
        $formatPricelist = [
            // 'id' => $this->p_id,
            // 'public_id' => $this->p_public_id,
            // 'code' => $this->p_code,
            // 'name' => $this->p_name,
            // 'name_type' => $this->p_name_type,
            // 'name_code' => $this->p_name_code,
            // 'name_code_group' => $this->p_name_code_group,
            // 'coming_soon' => $this->p_coming_soon,
            
            // 'meta_title' => $this->p_meta_title,
            // 'meta_desc' => $this->p_meta_desc,
                        
            // 'order' => $this->p_order,
            
            // 'category' => $this->category()->get()->map->formatNoProducts()->first(),

            'main-image' => $this->images()->where('pi_type','main')->orderBy('pi_order')->get()->map->format()->first(),
            '3packages-image' => $this->images()->where('pi_filename','LIKE','%3packages')->get()->map->format()->first(),
            // 'datasheet' => $this->p_datasheet,
            
            'content' => $pricelist['content'],
            'certification' => $pricelist['certification'],
            'table' => $pricelist['table'],
            
        ];

        return array_merge($this->format(),$formatPricelist);

    }
    
    
    
    public function formatCertificates(){
        
        return [
            'id' => $this->p_id,
            'public_id' => $this->p_public_id,
            // 'code' => strtolower($this->p_name_code_group.'-'.str_replace(' ','-',$this->p_name)),
            'code' => $this->p_code,
            'name' => $this->p_name,
            'name_type' => $this->p_name_type,
            'name_code' => $this->p_name_code,
            'name_code_group' => $this->p_name_code_group,
            'coming_soon' => $this->p_coming_soon,
            
            'meta_title' => $this->p_meta_title,
            'meta_desc' => $this->p_meta_desc,
            
            'order' => $this->p_order,
            
            // 'category_name' => $this->category->c_name,
            // 'category_html' => $this->category->c_html,
            // 'category_code' => $this->category->c_code,
            'category' => $this->category()->get()->map->formatNoProducts()->first(),
            
            
            'files_with_markings' => $this->filesWithMarkings(),

            
            'main-image' => $this->images()->where('pi_type','main')->orderBy('pi_order')->get()->map->format()->first(),
            '3packages-image' => $this->images()->where('pi_filename','LIKE','%3packages')->get()->map->format()->first(),
            // 'datasheet' => $this->p_datasheet,
            
            'certificates' => $this->certificates()->whereIn('pc_type', ['certificate'])->orderBy('pc_order')->get()->map->format(),
            
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }


    public function filesWithMarkings() {

            foreach(['eng','esp'] as $lang) {
                $files_with_markings[$lang] = [
                    'images'=>[],
                    'datasheet'=>false,
                ];

                $langPath = ($lang=='eng')?'':'/lang/'.$lang;

                $path = 'files/products/'.$this->p_public_id.$langPath.'/with-markings/images/3d-view';
                $files_with_markings[$lang]['images'] = Helper::formatImageFiles($path);

                $path = 'files/products/'.$this->p_public_id.$langPath.'/with-markings/images/main';
                $temp = Helper::formatImageFiles($path);
                if(count($temp))
                $files_with_markings[$lang]['image-main'] = $temp[0];
                else
                $files_with_markings[$lang]['image-main'] = false;

                $path = 'files/products/'.$this->p_public_id.$langPath.'/with-markings/datasheets/datasheet';
                $files_with_markings[$lang]['datasheet'] = (Storage::exists($path))?$path:false;
            }

            // dd($files_with_markings);
            
            return $files_with_markings;
    }
    
    
    
    public function category()
    {
        return $this->belongsTo('App\Category\Category', 'p_c_id', 'c_id');
    }    
    
    // public function packaging()
    // {
    //     return $this->hasMany('App\Product\ProductPackaging', 'pp_p_id', 'p_id');
    // }    

    public function images()
    {
        return $this->hasMany('App\Product\ProductImage', 'pi_p_id', 'p_id');
    }    

    public function certificates()
    {
        return $this->hasMany('App\Product\ProductCertificate', 'pc_p_id', 'p_id');
    }    

    public function pricelist()
    {
        return $this->hasOne('App\Product\ProductPricelist', 'pp_p_id', 'p_id');
    }  
    
    public function inventory()
    {
        return $this->hasMany('App\Product\ProductInventory', 'pin_p_id', 'p_id');
    }  

    public function comments()
    {
        return $this->hasMany('App\Product\ProductInventoryComments', 'pic_p_id', 'p_id');
    }  


}