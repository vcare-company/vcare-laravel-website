<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Facade\Ignition\Exceptions\ViewException;

use Throwable;
use Exception;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    // public function render($request, Throwable $exception)
    // {
    //     return parent::render($request, $exception);
    // }    
    
    public function render($request, Throwable $exception)
    {
        
        // return all errors to home
        if(env('APP_DEV_MODE'))
        return parent::render($request, $exception);
        else        
        return redirect()->route('home', [], 301);
        
        
        // if($exception instanceof ViewException) {
        //     return response()->view('errors.500-error', [], 500);
        // }
        // else if($exception->getStatusCode()== 404) {            
        //     return response()->view('errors.404-error', [], 404);
        // } 
        // else  {
        //     return redirect()->route('home', [], 301);
        // }
        
    }
}
