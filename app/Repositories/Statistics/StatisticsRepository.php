<?php

namespace App\Repositories\Statistics;
use Illuminate\Support\Facades\Storage;

 
class StatisticsRepository
{


    public static function getLast365URL(){
        return 'https://corona.lmao.ninja/v2/historical/all?lastdays=365';
    }

    public static function getAllCountriesURL(){
        return 'https://corona.lmao.ninja/v2/countries?yesterday&sort';
    }

    public static function getCOVID19Data(){



        // Global
        //////////////////////////////////////////////////////////////////////////////
        $globalStats = [
            "total_confirmed_cases" => 0,
            "total_deaths" => 0,
            "total_recovered" => 0,
            "total_active_cases" => 0,
        ];

        //////////////////////////////////////////////////////////////////////////////
        // Global

        // By Country
        //////////////////////////////////////////////////////////////////////////////
        
        // Data Variables
        // "updated" => 1601215611372
        // "country" => "Afghanistan"
        // "countryInfo" => array:6 [ …6]
        // "cases" => 39227
        // "todayCases" => 35
        // "deaths" => 1453
        // "todayDeaths" => 0
        // "recovered" => 32642
        // "todayRecovered" => 7
        // "active" => 5132
        // "critical" => 93
        // "casesPerOneMillion" => 1002
        // "deathsPerOneMillion" => 37
        // "tests" => 110135
        // "testsPerOneMillion" => 2814
        // "population" => 39131909
        // "continent" => "Asia"
        // "oneCasePerPeople" => 998
        // "oneDeathPerPeople" => 26932
        // "oneTestPerPeople" => 355
        // "activePerOneMillion" => 131.15
        // "recoveredPerOneMillion" => 834.15
        // "criticalPerOneMillion" => 2.38
                      
        $contents = Storage::get('files/statistics/all-countries.json');
        $countryStats = [];

        if($contents !== false && $contents){
            $contents = json_decode($contents,true);
            // dd($contents);
            foreach($contents as $item) {
                $countryStats[] = [
                    'country' => $item['country'],
                    'newConfirmedCases' => $item['todayCases'],
                    'newDeaths' => $item['todayDeaths'],
                    'newRecovered' => $item['todayRecovered'],
                    'activeCases' => $item['active'],
                    'totalConfirmedCases' => $item['cases'],
                    'totalDeaths' => $item['deaths'],
                    'totalRecovered' => $item['recovered'],
                ];

                $globalStats['total_confirmed_cases']+= $item['cases'];
                $globalStats['total_deaths']+= $item['deaths'];
                $globalStats['total_recovered']+= $item['recovered'];
                $globalStats['total_active_cases']+= $item['active'];

            }
        }
        //////////////////////////////////////////////////////////////////////////////
        // By Country

        return [
            'countryStats' => $countryStats,
            'globalStats' => $globalStats,
        ];

    }

    public static function getCOVID19GraphData(){
        // Monthly data
        // cases
        // death
        // recovered
        $contents = Storage::get('files/statistics/all-last-365-days.json');

        $temp = [
            'dates' => [],
            'amount' => [],
        ];
        $cases = $temp;
        $recovered = $temp;
        $deaths = $temp;


        if($contents !== false && $contents){
            $contents = json_decode($contents,true);

            $day = 30;
            $x=1;
            $count = count($contents['cases']);
            foreach($contents['cases'] as $key => $val){
                if($x % $day != 0 && $x < $count) 
                unset($contents['cases'][$key]);
                $x++;
            }

            $x=1;
            $count = count($contents['recovered']);
            foreach($contents['recovered'] as $key => $val){
                if($x % $day != 0 && $x < $count) 
                unset($contents['recovered'][$key]);
                $x++;
            }

            $x=1;
            $count = count($contents['deaths']);
            foreach($contents['deaths'] as $key => $val){
                if($x % $day != 0 && $x < $count) 
                unset($contents['deaths'][$key]);
                $x++;
            }



            $cases['amount'] = array_values($contents['cases']);
            $cases['dates'] = array_keys($contents['cases']);

            $recovered['amount'] = array_values($contents['recovered']);
            $recovered['dates'] = array_keys($contents['recovered']);

            $deaths['amount'] = array_values($contents['deaths']);
            $deaths['dates'] = array_keys($contents['deaths']);

            foreach ($cases['dates'] as $key => &$val) {
                $val = date("M d, Y", strtotime($val));
            }            
            foreach ($recovered['dates'] as $key => &$val) {
                $val = date("M d, Y", strtotime($val));
            }            
            foreach ($deaths['dates'] as $key => &$val) {
                $val = date("M d, Y", strtotime($val));
            }

        }

        return [
            'cases' => $cases,
            'recovered' => $recovered,
            'deaths' => $deaths,
        ];

    }

}
