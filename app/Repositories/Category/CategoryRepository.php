<?php

namespace App\Repositories\Category;

use App\Interfaces\Category\CategoryInterface;
use App\Category\Category;

class CategoryRepository implements CategoryInterface
{
    
    public function all($limit = null){
        $category = new Category;
        if ($limit != null) {
            $categories = $category
            ->where('active','1')
            ->orderBy('c_order')
            ->paginate($limit);
        } else {
            $categories = $category
            ->where('active','1')
            ->orderBy('c_order')
            ->get();
        }

        return $categories;
    }

    public function parentsOnly($limit = null){
        $category = new Category;
        if ($limit != null) {
            $categories = $category
            ->where(function($query) {
                $query->where('c_c_id','0')
                ->orWhereNull('c_c_id');
            })
            ->where('active','1')
            ->orderBy('c_order')
            ->paginate($limit);
        } else {
            $categories = $category
            ->where(function($query) {
                $query->where('c_c_id','0')
                ->orWhereNull('c_c_id');
            })
            ->where('active','1')
            ->orderBy('c_order')
            ->get();
        }

        return $categories;
    }

    public function find($code, $active = 'active'){

        $category = new Category;
        if($active === 'active') {
            $category = $category
            ->where('active','1')
            ->where('c_code',$code)
            ->orderBy('c_order')
            ->get();
        } else {
            $category = $category
            ->where('c_code',$code)
            ->orderBy('c_order')
            ->get();
        }



        return $category;
    }



    public function findMany($codes, $active = 'active'){
        $category = new Category;
        $category = $category
        ->where('active','1')
        ->whereIn('c_code',$codes)
        ->orderBy('c_order')
        ->get();

        return $category;
    }




}