<?php

namespace App\Repositories\Blog;
use Illuminate\Support\Facades\Hash;

use App\Interfaces\Blog\BlogInterface;
use App\Blog\Blog;

class BlogRepository implements BlogInterface
{
    public function all($limit = null){
        $blog = new Blog;
        if ($limit != null) {
            $blogs = $blog
            ->where('active','1')     
            ->orderBy('created_at','desc')
            ->paginate($limit);
        } else {
            $blogs = $blog
            ->where('active','1')     
            ->orderBy('created_at','desc')
            ->get();
        }

        return $blogs;
    }


    public function find($public_id) {
        $blog = new Blog;
        return $blog
        ->where('b_public_id',$public_id)     
        ->where('active','1')     
        ->get();
    }
    
    public function create($data) {

        $digits = 9;
        
        $public_id = rand(pow(10, $digits-1), pow(10, $digits)-1);

        if(Blog::where('b_public_id',$public_id)->get()->count()>=1)
        $public_id = rand(pow(10, $digits-1), pow(10, $digits)-1);

        if(Blog::where('b_public_id',$public_id)->get()->count()>=1)
        $public_id = rand(pow(10, $digits-1), pow(10, $digits)-1);

        if(Blog::where('b_public_id',$public_id)->get()->count()>=1)
        $public_id = rand(pow(10, $digits-1), pow(10, $digits)-1);


        $blog = new Blog;
        $blog->b_public_id = $public_id;
        $blog->b_title = $data['article_title'];
        $blog->b_short_content = $data['article_short_content'];
        $blog->b_html_content = $data['article_html_content'];
        $blog->b_meta_title = $data['article_meta_title'];
        $blog->b_meta_desc = $data['article_meta_desc'];
        $blog->b_url = $data['article_url'];
        $blog->save();

        return $blog;
        
    }



    public function editArticleContent($public_id, $data){
		// dd($data);

        $blog = Blog::where('b_public_id',$public_id)
        ->where('active','1')     
        ->get()->first();
        
        $blog->b_title = $data['article_title'];
        $blog->b_short_content = $data['article_short_content'];
        $blog->b_html_content = $data['article_html_content'];
        $blog->b_meta_title = $data['article_meta_title'];
        $blog->b_meta_desc = $data['article_meta_desc'];
        $blog->b_url = $data['article_url'];

        $blog->save();

        
        return $blog;

    }


}