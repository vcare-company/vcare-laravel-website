<?php

namespace App\Repositories;
use Illuminate\Support\Facades\Storage;

use App\Subscriber;
 
class SubscriberRepository
{
    public static function add($data){
        
        $subscriber = new Subscriber;
        $subscriber->s_name = $data['name'];
        $subscriber->s_email = $data['email'];
        $subscriber->save();

        return $subscriber;

    }
}