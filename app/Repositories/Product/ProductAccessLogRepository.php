<?php

namespace App\Repositories\Product;

use App\Interfaces\Product\ProductAccessLogInterface;
use App\Product\ProductAccessLog;

class ProductAccessLogRepository implements ProductAccessLogInterface
{

    public function insertLog($data){
        $pa = new ProductAccessLog;
        // $pa->insert(
        //     [
        //         'pal_username' => $data['username'],
        //         'pal_product_code' => $data['product_code'],
        //         'pal_access_type' => $data['access_type'],
        //     ]
        // );

        $location = [
            'ip' => $data['ip'],
            'country' => $data['country'],
            'country_code' => $data['country_code'],
            'state' => $data['state'],
            'city' => $data['city'],
            'address' => $data['address'],
        ];

        $note = json_encode($location);

        $pa->pal_username = $data['username'];
        $pa->pal_product_code = $data['product_code'];
        $pa->pal_access_type = $data['access_type'];
        $pa->pal_note = $note;
        $pa->save();

    }
}