<?php

namespace App\Repositories\Product;

use App\Interfaces\Product\ProductCertificateInterface;
use App\Product\ProductCertificate;

class ProductCertificateRepository implements ProductCertificateInterface
{

    public function find($code){
        $pc = new ProductCertificate;
        $pc = $pc
        ->where('pc_code',$code)
        ->get();

        return $pc;
    }

    public function editById($id, $data){

        $pc = ProductCertificate::where('pc_id',$id)->get()->first();
        $pc->pc_title = $data['title'];
        $pc->pc_code = $data['filename'];

        if($data['order'])
        $pc->pc_order = $data['order'];
        
        $pc->save();

        return $pc->pc_id;
    }

}