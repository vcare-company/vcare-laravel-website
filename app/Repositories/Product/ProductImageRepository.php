<?php

namespace App\Repositories\Product;

use App\Interfaces\Product\ProductImageInterface;
use App\Product\ProductImage;

class ProductImageRepository  implements ProductImageInterface
{
    public function insertPackageDetails($data){
        $prod_img = ProductImage::find($data['pi_id']);
        $prod_img_expanded = ProductImage::find($data['pi_expanded_id']);

        $prod_img->productDetailsExpanded()->attach($prod_img_expanded);
        $prod_img->save();

    }

    public function edit($data){
        $prod_img = ProductImage::find($data['id']);
        $prod_img->pi_order = $data['order'];
        $prod_img->pi_title2 = $data['title2'];
        $prod_img->save();
    }

    public function editOrder($data){
        $prod_img = ProductImage::find($data['id']);
        $prod_img->pi_order = $data['order'];
        $prod_img->save();
    }

    public function editTitle($data){
        $prod_img = ProductImage::find($data['id']);
        $prod_img->pi_order = $data['title'];
        $prod_img->save();
    }

    public function find($data){
        return ProductImage::find($data['id']);
    }

}
