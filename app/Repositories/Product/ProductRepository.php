<?php

namespace App\Repositories\Product;
use Illuminate\Support\Facades\Hash;

use App\Interfaces\Product\ProductInterface;
use App\Product\Product;
use App\Product\ProductImage;
 
// use App\Product\ProductPackaging;
use App\Product\ProductCertificate;

use App\Repositories\Product\ProductImageRepository;

class ProductRepository implements ProductInterface
{

    public function all($limit = null){
        $Product = new Product;
        if ($limit != null) {
            $products = $Product
            ->where('active','1')
            ->where(function($query) {
                $query->where('p_coming_soon','!=',1)
                ->orWhereNull('p_coming_soon');
            })            
            ->orderBy('p_id')
            ->paginate($limit);
        } else {
            $products = $Product
            ->where('active','1')
            ->where(function($query) {
                $query->where('p_coming_soon','!=',1)
                ->orWhereNull('p_coming_soon');
            })
            ->orderBy('p_id')
            ->get();
        }

        return $products;
    }


    public function allWithComingSoon($limit = null){
        $Product = new Product;
        if ($limit != null) {
            $products = $Product
            ->where('active','1')
            ->orderBy('p_id')
            ->paginate($limit);
        } else {
            $products = $Product
            ->where('active','1')
            ->orderBy('p_id')
            ->get();
        }

        return $products;
    }


    // public function find($code){
        
    //     $codeArr = explode("-",$code);

    //     $name_code_group = $codeArr[0];
    //     $name = $codeArr[1];


    //     $product = new Product;
    //     $product = $product
    //     ->where('active','1')
    //     ->where('p_name_code_group',$name_code_group)
    //     ->where('p_name',$name)
    //     ->get();

    //     return $product;
    // }

    public function find($code){
        
        $product = new Product;
        $product = $product
        ->where('active','1')
        ->where('p_code',$code)
        ->get();

        return $product;
    }


    public function getCert($product_code, $cert_code){
        
        $product = new Product;
        $cert = $product
        ->where('active','1')
        ->where('p_code',$product_code)
        ->get()->first()
        ->certificates()
        ->whereIn('pc_type', ['certificate'])
        ->where('pc_code', $cert_code)
        ->orderBy('pc_order')->get()->map->format()->first();

        // dd($cert);

        return $cert;
    }


    public function create($data){

        $public_id = rand(100000000,999999999);

        if(Product::where('p_public_id',$public_id)->get()->count()>=1)
        $public_id = rand(100000000,999999999);

        if(Product::where('p_public_id',$public_id)->get()->count()>=1)
        $public_id = rand(100000000,999999999);

        if(Product::where('p_public_id',$public_id)->get()->count()>=1)
        $public_id = rand(100000000,999999999);

        $product = new Product;
        $product->p_public_id = $public_id;
        $product->p_c_id = $data['category_id'];
        $product->p_code = $data['code'];
        $product->p_name = $data['name'];
        $product->p_name_type = $data['name_type'];
        $product->p_name_code_group = $data['name_code_group'];
        $product->p_name_code = $data['name_code'];
        $product->p_overview = $data['overview'];
        $product->p_certifications = $data['certifications'];
        $product->p_meta_desc = $data['meta_desc'];
        $product->p_meta_title = $data['meta_title'];        
        $product->p_html_content = $data['html_content'];
        $product->active = 1;        
        // $product->p_features = $data['features'];
        // $product->p_benefits = $data['benefits'];
        // $product->p_text = $data['text'];
        // $product->p_datasheet = $data['datasheet'];
        $product->save();

        // $product->p_text;
        // $product->category->c_name;
        // $product->category->c_code;
        // $product->packaging->map->format();
        // $product->images()->where('pi_type','main')->get()->map->format()->first();
        // $product->images()->whereIn('pi_type', ['main','3d-view'])->orderBy('pi_order')->get()->map->format();
        // $product->created_at->diffForHumans();

        // $product->save();


        return $product;
    }




    // public function clearDatasheet($product_id){
    //     $product = Product::find($product_id);
    //     $product->p_datasheet = 0;
    //     $product->save();
    // }

    // public function createProductPackage($product_id, $data){

    //     $pp = new ProductPackaging;
    //     $pp->pp_title = $data['title'];
    //     $pp->pp_pi_id = $data['image_id'];
    //     $pp->pp_details = $data['details'];
    //     $pp->pp_p_id = $product_id;
    //     $pp->save();
    
    //     return true;
    // }

    public function createProductCertificate($product_id, $data){

        $pc = new ProductCertificate;
        
        $pc->pc_title = $data['title'];
        $pc->pc_ext = $data['ext'];
        $pc->pc_code = $data['code'];
        $pc->pc_type = $data['type'];
        $pc->pc_order = $data['order'];
        
        $pc->pc_p_id = $product_id;
        $pc->save();
        
        return $pc->pc_id;
    }



    public function editProductContent($product_id, $data){
		// dd($data);

        $product = Product::find($product_id);
        $product->p_overview = $data['overview'];
        $product->p_certifications = $data['certifications'];
        $product->p_html_content = $data['html_content'];
        $product->p_meta_desc = $data['meta_desc'];
        $product->p_meta_title = $data['meta_title'];
        $product->p_name_type = $data['name_type'];

        $product->p_name_code_group = $data['name_code_group'];
        $product->p_name_code = $data['name_code'];
        $product->p_code = $data['code'];

        if($data['certificates_password'])
        $product->p_certificates_password =  Hash::make($data['certificates_password']);

        if($data['original_certificates_password'])
        $product->p_original_certificates_password =  Hash::make($data['original_certificates_password']);
        
        if($data['pricelist_password'])
        $product->p_pricelist_password = Hash::make($data['pricelist_password']);
                
        $product->p_order = $data['order'];
        $product->save();
        
        return $product->p_id;

    }




    // Transfer Product Images related functions to ProductImageRepository soon
    public function editProductImage($product_image_id, $data){

        $product_image = ProductImage::find($product_image_id);
        $product_image->pi_filename = $data['filename'];
        $product_image->save();

    }
    public function findProductImage($product_image_id){

        return ProductImage::where('pi_id',$product_image_id)->get()->map->format()->first();
    }

    public function createProductImage($data){
        $product_image = new ProductImage;


        $product_image->pi_title = $data['image_name'];
        $product_image->pi_title2 = $data['image_title'];
        $product_image->pi_filename = $data['filename'];
        $product_image->pi_ext = $data['ext'];
        $product_image->pi_ext_thumb = $data['ext_thumb'];
        $product_image->pi_type = $data['type'];
        $product_image->pi_order = $data['order'];
        $product_image->pi_p_id = $data['product_id'];
        
        $product_image->save();
        
        // so that filename will have the product_image id 
        $product_image->pi_filename = $product_image->pi_filename.'-'.$product_image->pi_id;
        
        $product_image->save();
        
        return $product_image->pi_id;

    }

    // public function insertPackageDetails($data){
    //     // pi_id
    //     // pi_expanded_id

    //     $pir = new ProductImageRepository;

    //     $pir->insertPackageDetails($data);
    // }
    

}