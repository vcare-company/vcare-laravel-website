<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Interfaces\Product\ProductInterface',
            'App\Repositories\Product\ProductRepository'
        );
        $this->app->bind(
            'App\Interfaces\Product\ProductImageInterface',
            'App\Repositories\Product\ProductImageRepository'
        );
        $this->app->bind(
            'App\Interfaces\Product\ProductCertificateInterface',
            'App\Repositories\Product\ProductCertificateRepository'
        );
        $this->app->bind(
            'App\Interfaces\Product\ProductAccessLogInterface',
            'App\Repositories\Product\ProductAccessLogRepository'
        );

        $this->app->bind(
            'App\Interfaces\Category\CategoryInterface',
            'App\Repositories\Category\CategoryRepository'
        );

        $this->app->bind(
            'App\Interfaces\Blog\BlogInterface',
            'App\Repositories\Blog\BlogRepository'
        );
    }



    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // View composers for Nav and Footer
        View::composer('partials.nav', 'App\Http\ViewComposers\NavComposer');
        View::composer('partials.footer', 'App\Http\ViewComposers\FooterComposer');
    }


    

}
