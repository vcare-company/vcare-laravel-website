<?php

namespace App\Blog;

use Illuminate\Database\Eloquent\Model;

use App\Util;


class Blog extends Model
{
    protected $table = 'blog';
    protected $primaryKey = 'b_id';
    
    public function format(){
        return [
            'id' => $this->b_id,
            'public_id' => $this->b_public_id,
            'title' => $this->b_title,
            'short_content' => $this->b_short_content,
            'html_content' => $this->b_html_content,
            'meta_title' => $this->b_meta_title,
            'meta_desc' => $this->b_meta_desc,
            'url' => $this->b_url,
            'created_at' => $this->created_at,
        ];
    }

}