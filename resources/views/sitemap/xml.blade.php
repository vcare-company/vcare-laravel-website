<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
    <url>
        <loc>https://vcare.earth</loc>
        <changefreq>monthly</changefreq>
        <priority>0.5</priority>
    </url>

	{{-- image with Vcare.Earth in filename Google SEO test --}} 
	{{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}} 
	{{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}} 
	@foreach($products as $product)
    <url>
        <loc>{{route('single-product',['product_code'=>$product['code']])}}</loc>
        <changefreq>weekly</changefreq>
        <priority>0.5</priority>
        <image:image>
            <image:loc>{{ env('APP_URL').'/files/products/'. $product['public_id'] .'/images/'. $product['main-image']['type'] .'/'. $product['main-image']['filename'].'.'.$product['main-image']['ext'] }}</image:loc>
            <image:caption>Vcare.Earth {{$product['name']}} {{$product['code']}}</image:caption>
        </image:image>
    </url>
	@endforeach 
	{{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}} 
	{{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}} 
	{{-- image with Vcare.Earth in filename Google SEO test --}} 
	
	@foreach($products as $product)
    <url>
        <loc>{{route('single-product',['product_code'=>$product['code']])}}</loc>
        <changefreq>weekly</changefreq>
        <priority>0.9</priority>
        <image:image>
            <image:loc>{{ env('APP_URL').'/files/products/'. $product['public_id'] .'/images/'. $product['main-image']['type'] .'/'. $product['main-image']['filename'].'.'.$product['main-image']['ext'] }}</image:loc>
            <image:caption>{{$product['name']}} {{$product['code']}}</image:caption>
        </image:image>
    </url>
    <url>
        <loc>{{route('single-product',['product_code'=>$product['code']])}}</loc>
        <changefreq>weekly</changefreq>
        <priority>0.9</priority>
    </url>
	@endforeach 
	
	{{-- Blog articles --}}
	@foreach($blogs as $blog)
    <url>
        <loc>{{route('blog_single_page',[
			'blog_public_id'=>$blog['public_id'],
			'blog_title'=>str_replace(' ','-',strtolower($blog['title'])),
		])}}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
	@endforeach
	
	
	
	@foreach($pages as $page)
    <url>
        <loc>{{route($page['route'])}}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
	@endforeach
    
    
	@foreach($images as $image)
    <url>
        
        <loc>{{route('about')}}</loc>
        <changefreq>weekly</changefreq>
        <priority>0.5</priority>
        <image:image>
            <image:loc>{{ env('APP_URL').$image['path'] }}</image:loc>
            <image:caption>VCare.Earth {{$image['caption']}}</image:caption>
        </image:image>
    </url>
	@endforeach 
    
</urlset>
