@extends('layout.main')



@section('head-end')
<link rel="stylesheet" href="{{ mix('css/sitemap-html-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/sitemap-html-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection

@section('head-title')
<title>HTML Sitemap | Vcare.earth</title>
@endsection



@section('seo-meta-title')
<?php 
    $meta_title = "VCare.Earth Commitment to Care - HTML Sitemap" ; 
    $meta_desc = "Our products and services are available to customers worldwide. For any questions on terms and conditions please refer on this page.";
?>    
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 


@section('content')

<section class="section section-header">
    <div class="container">
        <div class="columns">
            <div class="column title-column">
                <h1 class="title is-1">HTML Sitemap</h1>
            </div>
        </div>        
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="columns">
            <div class="column">
                <ul>
                    @foreach($pages as $page)
                        <li>
                            <a href="{{route($page['route'])}}" target="_blank">{{$page['title']}}</a>
                            @if($page['route']=='all-products')
                            <ul>
                                @foreach($products as $product)
                                    <li><a href="{{route('single-product',['product_code'=>$product['code']])}}" target="_blank">
                                        {{ ucwords($product['name']).' '.strtoupper($product['name_code']) }}
                                    </a></li>
                                @endforeach
                            </ul>
                            @endif

                            @if($page['route']=='blog')
                            <ul>
                                @foreach($blogs as $blog)
                                    <li><a href="{{route('blog_single_page',[
                                        'blog_public_id'=>$blog['public_id'],
                                        'blog_title'=>str_replace(' ','-',strtolower($blog['title'])),
                                    ])}}" target="_blank">
                                        {{ ucwords($blog['title']) }}
                                    </a></li>
                                @endforeach
                            </ul>                            
                            @endif
                        </li>

                    @endforeach
                </ul>
            </div>
           
        </div>        
    </div>
</section>                

@endsection


@section('twitter')
@endsection
