@extends('layout.main') @section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('careers') }}" 
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/careers-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}" />
<script src="{{ mix('js/careers-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection @section('content')

@section('head-title')
<title>Careers | Vcare.earth</title>
@endsection


@section('seo-meta-title')
<?php 
    $meta_title = "VCare.Earth Commitment to Care - Careers" ; 
    $meta_desc = "VCare.Earth provides customers worldwide with essential, medical and healthcare equipment, supporting the safety of people, patients and frontline workers.";
?>    
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 


<section class="section">
    <div class="container">
        <div class="columns is-flex-end">
            <div class="column title-column">
                <h2 class="subtitle">
                    CAREERS
                </h2>
                <h1 class="title is-1">We are Hiring</h1>
                <p>
                    VCare.Earth provides customers worldwide with essential, high-quality medical and healthcare equipment, supporting the safety of people, patients and frontline workers in multiple industries today.
                </p>
            </div>
            <div class="column has-text-centered follow-cause">
                <h1 class="subtitle is-4">
                    FOLLOW US
                    <span class="social-icons">
                        <a href="https://linkedin.com/company/vcare-earth"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a>
                        <a href="https://instagram.com/vcare_earth"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                        <a href="https://twitter.com/VCare_Earth"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                    </span>
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="section blue-vcare-bg" style="background-image: url({{ env('APP_URL') }}/files/careers/banner.jpg);">
    <div class="container">
        <div class="columns is-vcentered is-centered">
            <div class="column is-7">
                <h2 class="subtitle is-2 has-text-centered has-text-white px-4">
                    {{-- Our Journey Together,<br />
                    to Kill the Novel Coronavirus --}}

                    {{-- Together, we are all stepping into the unknown in a bid to defeat the Novel Coronavirus. --}}
                </h2>
            </div>
        </div>
    </div>
</section>

<section class="section careers-text">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-three-quarters">
                <div class="content">
                    
                    <div class="has-text-centered">
                        <h2 class="position-title subtitle mt-5 mb-0">4 x SALES MANAGERS</h2>
                    </div>
                   
                    <h3 class="subtitle is-size-5 has-text-centered mb-6"
                    style="font-weight: 400;">
                        (Italy, France, Germany, Scandinavia)
                    </h3>
                   
                    <h2 class="subtitle is-size-5">
                        Job Description:
                    </h2>
                    <p class="mb-6">
                        We are looking for 4 experienced Sales Managers (m/f) to follow-up on key accounts mainly Medical Distributors as well as big Industrial Clients. The goal is to contribute to existing customer relationship management as well as growing new accounts for long-term mutual success.
                    </p>

                    <h2 class="subtitle is-size-5">
                        Assigned Regions:
                    </h2>                    
                    <p class="mb-6">
                        Italy, France, Germany, Scandinavia
                    </p>


                    <h2 class="subtitle is-size-5">
                        Your Responsibilities:
                    </h2>
                    <ul class="mb-6">
                        <li>Holding responsibility for an annual revenue target.</li>
                        <li>Acquiring new clients and managing existing customer relations.</li>
                        <li>Setting up strategic sales and marketing relationships in order to position the company as the prime player for Respirators/PPE products.</li>
                        <li>Development of revenue growth plans and coordination of company resources to ensure efficient and growing sales results.</li>
                        <li>Close cooperation with customer care and marketing to ensure a world-class sales process.</li>
                        <li>Achieving and surpassing weekly and monthly targets and goals.</li>
                        <li>Conducting daily coaching and weekly feedback sessions with all team members.</li>
                        <li>Implementing new processes and tools to drive sales performance through innovation.</li>
                        <li>Leading by example through calling clients and closing deals on your own.</li>
                        <li>Cooperating closely with the senior leadership to improve our sales operations & processes.</li>
                    </ul>


                    <h2 class="subtitle is-size-5">
                        Your Profile:
                    </h2>
                    <ul class="mb-6">
                        <li>You have an outstanding degree from a good university.</li>
                        <li>You have at least 3 years of experience in sales from the medical/sales/distribution industry.</li>
                        <li>You have excellent English and native written and verbal communication skills plus the language of the country you are being hired for.</li>
                        <li>You have an in-depth knowledge about the technical aspects of Respirators/PPE products.</li>
                        <li>You are experienced working in highly successful and talented teams.</li>
                        <li>You are goal oriented with a track record of over-achievement (consistently beating targets, Rep of the Year, etc.).</li>
                        <li>You have a passion for sales and you are always setting the right note.</li>
                        <li>You have outstanding analytical abilities and a high comfort to make data-driven decisions.</li>
                        <li>You are able to achieve great results independently without much guidance.</li>
                        <li>You are enthusiastic, competitive, self-motivated and hands-on with a strong work ethic.</li>
                        <li>You enjoy working in a fast-paced, target-driven and team-oriented environment.</li>
                        <li>You can deal in a friendly manner both with coworkers and clients.</li>
                        <li>You possess outstanding web and computer skills: MS Office.</li>
                        <li>The job is Home Office based and requires max 20% travel activity.</li>
                    </ul>



                    <h2 class="subtitle is-size-5">
                        What We Offer:
                    </h2>
                    <ul class="mb-6">
                        <li>A fast-paced working environment in a rapidly developing European company located in Barcelona, Spain.</li>
                        <li>Challenges which let you grow day by day: Continuous development of your skills.</li>
                        <li>Varied, responsible tasks and projects with virtually unlimited career opportunities.</li>
                        <li>A short decision-making process and great conceptual freedom.</li>
                        <li>Excellent medical products with great sales tools to support your sales efforts.</li>
                        <li>An attractive remuneration + commission.</li>
                    </ul>

                    <p>
                        Email your CV at: <strong>info<span>@vc</span>are.ea<span>rt</span>h</strong> and <strong>alejandr<span>o@</span>vca<span>re.e</span>arth</strong>
                    </p>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
