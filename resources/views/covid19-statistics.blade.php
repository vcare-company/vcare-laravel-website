@extends('layout.main') 

@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('covid19-statistics') }}"
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/covid19-statistics-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}" />
<script src="{{ mix('js/covid19-statistics-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>

<script>



let covid19Data = {
    cases: {
        dates: {!! json_encode($stats30Days['cases']['dates']) !!},
        amount: {!! json_encode($stats30Days['cases']['amount']) !!},
    },
    recovered: {
        dates: {!! json_encode($stats30Days['recovered']['dates']) !!},
        amount: {!! json_encode($stats30Days['recovered']['amount']) !!},
    },
    deaths: {
        dates: {!! json_encode($stats30Days['deaths']['dates']) !!},
        amount: {!! json_encode($stats30Days['deaths']['amount']) !!},
    },
};

// console.log(covid19Data);

</script>
@endsection 





@section('head-title')
<title>C19 Updates | Vcare.earth</title>
@endsection





@section('content')


@section('seo-meta-title')
<?php 
    $meta_title = "VCare.Earth Commitment to Care - COVID19 Statistics" ; 
    $meta_desc = "COVID-19 has infected over ".$statsInMillions['total_confirmed_cases']." million people globally, regardless of age, ethnicity, race and religion.";
?>    
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 


<section class="section">
    <div class="container">
        <div class="columns is-flex-end">
            <div class="column title-column">
                <h2 class="subtitle">
                    WORLDWIDE
                </h2>
                <h1 class="title is-1">COVID19 Statistics</h1>
            </div>
            <div class="column has-text-centered follow-cause">
                <h1 class="subtitle is-4">
                    FOLLOW OUR CAUSE
                    <span class="social-icons">
                        <a href="https://linkedin.com/company/vcare-earth"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a>
                        <a href="https://instagram.com/vcare_earth"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                        <a href="https://twitter.com/VCare_Earth"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                    </span>
                </h1>
            </div>
        </div>
    </div>
</section>

{{-- <section class="section blue-vcare-bg" style="background-image: url({{ env('APP_URL') }}/files/charity/charity.png);"> --}}
{{-- <section class="section blue-vcare-bg position-relative" style="background-image: url(/files/home/images/homepage-prideinquality.jpg?u={{ env('APP_ASSET_TIMESTAMP') }});"> --}}
<section class="section blue-vcare-bg position-relative" style="background-image: url(/files/faq/images/faq.png?u={{ env('APP_ASSET_TIMESTAMP') }});">
    <div class="transparent-bg"></div>
    <div class="container">
        <div class="columns is-vcentered is-centered">
            <div class="column is-12">
                {{-- <h2 class="subtitle is-2 has-text-centered has-text-white px-4">
                    Total Cases: <span id="cases-global"></span><br/>
                    Total Recovered: <span id="recovered-global"></span><br/>
                    Total Deaths: <span id="deaths-global"></span><br/>
                </h2> --}}

                <div class="statistics">
                    <div class="stat">
                        <h1 class="subtitle is-spaced is-size-5 mb-2">Total Cases </h1>
                        <h1 class="subtitle is-size-1 amount"><span id="cases-global">{{ number_format($globalStats['total_confirmed_cases'],0,'.',',') }}</span></h1>
                    </div>
                    <div class="stat">
                        <h1 class="subtitle is-spaced is-size-5 mb-2">Total Recovered </h1>
                        <h1 class="subtitle is-size-1 amount"><span id="recovered-global">{{ number_format($globalStats['total_recovered'],0,'.',',') }}</span></h1>
                    </div>
                    <div class="stat">
                        <h1 class="subtitle is-spaced is-size-5 mb-2">Total Deaths </h1>
                        <h1 class="subtitle is-size-1 amount"><span id="deaths-global">{{ number_format($globalStats['total_deaths'],0,'.',',') }}</span></h1>
                    </div>
                    <div class="stat">
                        <h1 class="subtitle is-spaced is-size-5 mb-2">Total Active Cases </h1>
                        <h1 class="subtitle is-size-1 amount"><span id="active-cases-global">{{ number_format($globalStats['total_active_cases'],0,'.',',') }}</span></h1>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>




<section id="charts" class="section">
    <div class="container">
        <div class="columns">
            <div class="column">        
                <h1 class="title mb-5 mt-5 has-text-centered">Monthly COVID19 Statistics</h1>
                <canvas id="covid19-statistics-graph" height="100"></canvas>
            </div>
        </div>
    </div>
</section>


<section class="section">
    <div class="container">
        <div class="columns">
            <div class="column">
                <div id="covid19-table-container">
                    <div class="card">
                        <div class="card-content">

                            <h1 class="title mb-5 mt-5 has-text-centered">Country Statistics</h1>
                            <div id="covid19-table-global-wrap">
                                <table id="covid19-table-global" class="table is-hoverable is-striped responsive">
                                    <thead>
                                    <tr>
                                        {{-- <th><abbr title="Position">Pos</abbr></th> --}}
                                        <th>Country</th>
                                        <th>New Confirmed Cases</th>
                                        <th>New Deaths</th>
                                        <th>Newly Recovered</th>

                                        <th>Total Confirmed Cases</th>
                                        <th>Total Deaths</th>
                                        <th>Total Recovered</th>
                                        <th>Active Cases</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($countryStats as $item)
                                        <tr>
                                            <td>
                                                <div class="main-col">
                                                    {{ $item['country'] }} 
                                                    <span class="hide-on-desktop expand-row">
                                                        <i class="fas fa-chevron-up"></i>
                                                        <i class="fas fa-chevron-down"></i>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>{{ number_format($item['newConfirmedCases'],0,'.',',') }}</td>
                                            <td>{{ number_format($item['newDeaths'],0,'.',',') }}</td>
                                            <td>{{ number_format($item['newRecovered'],0,'.',',') }}</td>
                                            <td>{{ number_format($item['totalConfirmedCases'],0,'.',',') }}</td>
                                            <td>{{ number_format($item['totalDeaths'],0,'.',',') }}</td>
                                            <td>{{ number_format($item['totalRecovered'],0,'.',',') }}</td>
                                            <td>{{ number_format($item['activeCases'],0,'.',',') }}</td>
                                        </tr>
                                        @endforeach
                                        


                                    </tbody>
                                </table>      
                            </div>              

                            <div class="is-hidden-tablet has-text-centered">
                                <button class="button vcare-button-outlined mt-3 mb-6" id="show-more-covid">
                                    <span class="vcare-button-text is-size-7 pr-4 mr-5">SHOW MORE</span>
                                    <i class="fas fa-chevron-down" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column">
                <div>
                    <a class="twitter-timeline" href="https://twitter.com/VCare_Earth/lists/health"></a>
                </div>
            </div>
        </div>
    </div>
</section>

<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

@endsection
