@extends('layout.main')

@section('head-end')



<link rel="stylesheet" href="{{ mix('css/home-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/home-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>

<script>
    var message = '{{ $message }}';
</script>
@endsection


@section('seo-meta')
<meta name="keywords" content="respirators,face masks,surgical masks,medical gowns,covid-19 testers,respirator,face mask,surgical mask,medical gown,covid-19 tester"/>
@parent
@endsection

@section('breadcrumbs')
@endsection


@section('content')



        <!-- Hero -->
        <section id="hero" class="hero is-large vcare-hero" style=" background-image: url({{ env('APP_URL') }}/files/home/images/banner.jpg?u={{ env('APP_ASSET_TIMESTAMP') }});">
            <div class="hero-body">
                <div class="container">
                    <div class="columns">
                        <div class="column is-7">
                            <svg width="200" height="7">
                                <rect width="155" height="7" class="first-rect"/>
                            </svg>
                            <h2 class="is-size-1 is-size-3-mobile has-vcare-text"> Commitment to Care </h2>
                            <p class="has-vcare-text has-text-weight-light">
                                VCare.Earth provides customers worldwide with essential, high-quality <a href="{{ route('all-products') }}">medical and healthcare equipment</a>, supporting the safety of people, patients and frontline workers in multiple industries today - and every day. 
                            </p>
                            <div class="mt-4">
                                <a class="button vcare-button" href="{{ route('all-products') }}">
                                    <span class="vcare-button-text is-size-7 pr-4">OUR PRODUCTS</span><i class="fas fa-chevron-right"></i>
                                </a>
                                <a class="button vcare-button" style="background-color: #FF9900; border: #FF9900; color: black;" target="_blank" href="https://www.amazon.es/CARE-EARTH-Mascarilla-respiraci%C3%B3n-FFP2-piezas/dp/B08RWQ2FFK/ref=sr_1_4?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=vcare&qid=1614676675&sr=8-4">
                                    <span class="vcare-button-text is-size-7 pr-4">BUY NOW ON AMAZON <i class="fab fa-amazon"></i></span><i class="fas fa-chevron-right"></i>
                                </a>
                                <a class="button vcare-button-outlined vcare-button-eci" style="" target="_blank" href="https://www.elcorteingles.es/parafarmacia/A38773929-mascarilla-ffp2-nr-4b-vcare/">
                                    <span class="vcare-button-text is-size-7 pr-3">BUY NOW ON </span>
                                    <img class="" alt="Elcorte logo" src="/files/home/images/eci-logo.png" style="width:40%"/>&nbsp;
                                    <i class="fas fa-chevron-right"></i>
                                </a>

                                {{-- <button class="button vcare-button-outlined">
                                    <a href="{{ route('about') }}"><span class="vcare-button-text is-size-7 pr-4">LEARN MORE</span><i class="fas fa-chevron-right"></i></a>
                                </button>
                                <button class="button vcare-button-outlined sample-button">
                                    <a href="{{ route('sample') }}">
                                        <span class="vcare-button-text is-size-7 pr-4">TRY US OUT!</span><i class="fas fa-chevron-right"></i>
                                    </a>
                                </button> --}}
                            </div>
                        </div>
                        {{-- <div class="column"></div> --}}

                    </div>
                </div>
            </div>
        </section>
        <!-- END Hero -->

        <!-- Intro -->
        <section id="vcare-intro" class="section pb-0">
            <div class="container">
                <div class="columns">
                    <div class="column is-half is-offset-one-quarter has-text-centered">
                        <img src="{{ env('APP_URL') }}/files/logos/vcareicon.svg" alt="VCare.Earth Logo"/>
                        <h2 class="is-size-1 is-size-4-mobile has-vcare-text mt-3">We're in this together</h2>
                        <p class=" has-text-weight-light mt-3 ">
                            By manufacturing and sourcing essential medical and non-medical equipment, VCare.Earth is assisting customers globally to preserve lives, delivering high quality personal safety products worldwide to any destination or location and doing so with efficiency and care.   
                        </p>
                    </div>
                </div>
                <div class="columns mt-6 is-variable is-5">
                    <div class="column has-text-centered vision-text">
                        <h5 class="has-vcare-text">Our Vision</h5>
                        <p class="mt-5 has-text-weight-light">To help Safeguard lives through <br>compassionate, responsive delivery of <br>effective healthcare solutions.</p>
                    </div>
                    <div class="column has-text-centered mission-text">
                        <h5 class="has-vcare-text">Our Mission</h5>
                        <p class="mt-5 has-text-weight-light">To provide quick solutions and <a href="{{ route('cat-medical-equipment') }}">best <br>medical products</a> that are verifiably <br>effective and fair priced.</p>
                    </div>
                </div>

                <div class="columns mt-6 is-centered">
                    <div class="column is-5 has-text-centered">
                        <h1 class="is-size-4 is-size-4-mobile has-vcare-text mb-2" style="">
                            The Best Personal Protective Equipment at a Fair Price
                        </h1>
                    </div>
                </div>

                <div class="columns mt-4 is-centered">
                    <div class="column has-text-centered">
                        <a href="{{ route('about') }}" class="button vcare-button-outlined mb-1 px-5">
                            <span class="vcare-button-text is-size-7 pr-4">ABOUT US</span><i class="fas fa-chevron-right"></i>
                        </a>
                    </div>
                </div>

            </div>
        </section>
        <!-- END Intro -->

        <!-- Advantages -->
        <?php

            $advs = [
                [
                    'src'=> env('APP_URL').'/files/home/images/advantage/fastdeliver.svg',
                    'title'=>'Fast Delivery Time',
                    'text'=>'Shipping personal safety equipment to your door quickly and efficiently.',
                    'imgStyle'=>'',
                ],
                [
                    'src'=> env('APP_URL').'/files/home/images/advantage/qualityprod.svg',
                    'title'=>'Quality Products',
                    'text'=>'Ensuring every item is manufactured to the correct standards.',
                    'imgStyle'=>'',
                ],
                [
                    'src'=> env('APP_URL').'/files/home/images/advantage/availability.svg',
                    'title'=>'Availability & Accessibility',
                    'text'=>'Providing the essentials when you need them, wherever you are.',
                    'imgStyle'=>'',
                ],
                [
                    'src'=> env('APP_URL').'/files/home/images/advantage/competitivelyprice.svg',
                    'title'=>'Competitively Priced',
                    'text'=>'Assuring fair pricing across our product range for every customer.',
                    'imgStyle'=>'',
                ],
            ];
        ?>        
        <section id="vcare-adv" class="section">
            <div class="container is-fluid" style=" background-image: url({{ env('APP_URL') }}/files/home/images/homepage-vcareadvantages.jpg?u={{ env('APP_ASSET_TIMESTAMP') }});">
                <h2 class="is-size-3 is-size-5-mobile has-text-centered has-text-white">Vcare.Earth Advantages</h2>
                <div class="container content">
                    <div class="columns is-multiline is-mobile">
                        
                        @foreach ($advs as $adv)
                        <div class="column is-3-desktop is-12-mobile has-text-centered advantage-text">
                            <img class="adv-icon" alt="{{ $adv['title'] }}" src="{{ $adv['src'] }}" style="{{ $adv['imgStyle'] }}"/>
                            <p class="mt-3 mb-3 adv-title">{{ $adv['title'] }}</p>
                            <p class="advant-text">{{ $adv['text'] }}</p>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </section>
        <!-- END Advantages -->











        <!-- Standard -->
        {{-- <section id="vcare-standard" class="section">
            <div class="container">
                <div class="columns">
                    <div class="column">
                        <svg width="200" height="7">
                            <rect width="155" height="7" class="second-rect"/>
                        </svg>
                        <h1 class="is-size-3 is-size-5-mobile vcare-head">Setting the Standard</h1>
                    </div>
                    <div class="column">
                        <h1 class="is-size-4 has-text-weight-light">VCare.Earth has a wide network of international clients and distributors across continents.</h1>
                        <p class="mt-5 has-text-weight-light">
                            All products are manufactured to the highest safety standards in regulated sites that meet the applicable GMP manufacturing, environmental, GDP distribution and labelling standards. We have robust systems in place to ensure all our products are produced ethically and meet national standards.
                        </p>
                        <p class="mt-5 has-text-weight-light">
                             We are continuously looking to expand our reach and strengthen our global footprint, through collaboration with new partners as well as attending and exhibiting at pharmaceutical events as well as medical conferences and exhibitions globally.
                         </p>   
                    </div>
                </div>
            </div>
        </section> --}}
        <!-- END Standard -->


        {{-- <section id="vcare-standard" class="section">
            <div class="container">
                <div class="columns is-centered is-vcentered">
                    <div class="column is-narrow has-text-centered">
                        <img class="mx-6 my-6" style="height: 80px;" src="{{ env('APP_URL') }}/files/logos/vcareicon.svg"/>
                    </div>
                </div>
            </div>
        </section> --}}

        <!-- <section class="section">
            <div class="container">
                <div class="columns is-vcentered is-centered is-multiline">
                    <div class="column is-12 has-text-centered">
                        <img class="my-3 my-3" 
                        style="height: 55px;" 
                        alt="VCare.Earth Logo"
                        src="{{ env('APP_URL') }}/files/logos/vcareicon.svg"/>

                        <h2 class="subtitle color2">
                        {{-- <h2 class="subtitle is-size-6-mobile  mb-2 color2"> --}}
                            WORLDWIDE
                        </h2>

                        <h2 class="title is-2 mb-0 color2">
                        {{-- <h1 class="is-size-3 is-size-5-mobile mt-1 has-vcare-text"> --}}
                            <span class="covid19-statistics-title">
                                COVID19 Statistics
                            </span>
                        </h2>

                    </div>                    
                    <div class="column is-12">

                        <div class="statistics">
                            <div class="stat">
                                <h2 class="subtitle is-spaced mb-0 color2 has-text-centered">Total Confirmed Cases </h2>
                                <h2 class="subtitle is-size-1 color2"><span id="cases-global">{{ number_format($globalStats['total_confirmed_cases'],0,'.',',') }}</span></h2>
                            </div>
                            <div class="stat">
                                <h2 class="subtitle is-spaced mb-0 color2 has-text-centered">Total Recovered </h2>
                                <h2 class="subtitle is-size-1 color2"><span id="recovered-global">{{ number_format($globalStats['total_recovered'],0,'.',',') }}</span></h2>
                            </div>
                            <div class="stat">
                                <h2 class="subtitle is-spaced mb-0 color2 has-text-centered">Total Deaths </h2>
                                <h2 class="subtitle is-size-1 color2"><span id="deaths-global">{{ number_format($globalStats['total_deaths'],0,'.',',') }}</span></h2>
                            </div>
                            <div class="stat">
                                <h2 class="subtitle is-spaced mb-0 color2 has-text-centered">Total Active Cases </h2>
                                <h2 class="subtitle is-size-1 color2"><span id="active-cases-global">{{ number_format($globalStats['total_active_cases'],0,'.',',') }}</span></h2>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="column is-12 has-text-centered">
                        <a class="button vcare-button-outlined mt-3 mb-6" href="{{ route('covid19-statistics') }}" target="_blank">
                            <span class="vcare-button-text is-size-7 pr-4 mr-5">SEE MORE</span>
                            <i class="fas fa-chevron-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </section> -->
            <!-- Products -->
            <section id="vcare-products" class="section mb-6">
                <div class="container">
                    <div class="columns">
                        <div class="column">
                            <svg width="200" height="7">
                                <rect width="80" height="3" class="second-rect"/>
                            </svg>
                            <h2 class="is-size-3 is-size-5-mobile mt-4 has-vcare-text ">Our Products</h2>
                            <p class="mt-4 has-text-weight-light">
                                All VCare.Earth products are designed and manufactured to high standards, both durable and reliable, and competitively priced for our global clients.
                            </p>
                            <div class="products-btn-group">
                                <a class="button vcare-button-outlined mt-3" href="{{ route('all-products') }}">
                                    <span class="vcare-button-text is-size-7 pr-4">OUR PRODUCTS</span><i class="fas fa-chevron-right"></i>
                                </a>
                                <a class="button vcare-button mt-3" href="{{ route('sample') }}">
                                    <span class="vcare-button-text is-size-7 pr-4">REQUEST A SAMPLE</span><i class="fas fa-chevron-right"></i>
                                </a>
                                <a class="button vcare-button-outlined mt-3" href="{{ route('respirator-comparison') }}">
                                    <span class="vcare-button-text is-size-7 pr-4">RESPIRATOR COMPARISON</span><i class="fas fa-chevron-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="column column-two">
                            <img style="width:75%;"
                            alt="VCare.Earth Respirators, Face Masks, Covid19 Rapid Test" 
                            src="{{ env('APP_URL') }}/files/home/images/vcare-earth-respirators-face-masks-covid19-rapid-test.jpg?u={{ env('APP_ASSET_TIMESTAMP') }}"/>
    
                        </div>
                    </div>
                </div>
            </section>
            <!-- END Products -->
        
        <!-- Pride -->
        <section id="vcare-pride" class="section">
            <div class="container is-fluid" style=" background-image: url({{ env('APP_URL') }}/files/home/images/homepage-prideinquality.jpg?u={{ env('APP_ASSET_TIMESTAMP') }});">
                <div class="transparent-bg"></div>
                <div class="container content">
                    {{-- <svg width="200" height="7">
                        <rect width="155" height="7" class="second-rect"/>
                    </svg>
                    <h1 class="is-size-3 is-size-3-mobile vcare-head has-text-white mt-3 main-title">Setting the Standard</h1> --}}
                    <div class="columns is-multiline has-text-white">

                        <div class="column is-12 mb-6">
                            <svg width="200" height="7">
                                <rect width="90" height="4" class="third-rect"/>
                            </svg>
                            {{-- <div class="is-size-4 is-size-5-mobile vcare-head">Pride in our Products</div> --}}
                            {{-- <h2 class="is-size-4 is-size-5-mobile vcare-head mt-2 mb-0 has-text-white">Pride in our Products</h2> --}}
                            <h2 class="is-size-4 is-size-5-mobile vcare-head mt-2 mb-0 has-text-white">Leaders in Personal Equipment Protection and Medical products</h2>
                            <p class="mt-5">
                                VCare.Earth personal safety equipment and medical products is always high in quality, and fair in price.
                            </p>
                            <p class="mt-5 has-text-weight-light">
                                Our products are manufactured to the highest safety standards in regulated sites to adhere with GMP manufacturing, environmental, GDP distribution and labelling requirements. Using robust systems ensures that each item produced is done so ethically, and always to the same exceptional standards.
                            </p>
                        </div>

                        <div class="column is-6 mb-6">
                            <svg width="200" height="7">
                                <rect width="90" height="4" class="third-rect"/>
                            </svg>
                            {{-- <div class="is-size-4 is-size-5-mobile  vcare-head"> Setting the Standard </div> --}}
                            {{-- <h2 class="is-size-4 is-size-5-mobile vcare-head mt-2 mb-0 has-text-white"> Setting the Standard </h2> --}}
                            <h2 class="is-size-4 is-size-5-mobile vcare-head mt-2 mb-0 has-text-white">The Highest Standards in Safety Equipment</h2>
                            <p class="mt-5">
                                Using consistent, controlled procedures ensures VCare.Earth delivers maximum quality and minimum risk.
                            </p>
                            <p class="mt-5 has-text-weight-light">
                                Our products are manufactured to the highest safety standards in regulated sites to adhere with GMP manufacturing, environmental, GDP distribution and labelling requirements. Using robust systems ensures that each item produced is done so ethically, and always to the same exceptional standards.
                             </p>   
                        </div>


                        <div class="column is-6 mb-6">
                            <svg width="200" height="7">
                                <rect width="90" height="4" class="third-rect"/>
                            </svg>
                            {{-- <div class="is-size-4 is-size-5-mobile  vcare-head"> A Global Network </div> --}}
                            <h2 class="is-size-4 is-size-5-mobile vcare-head mt-2 mb-0 has-text-white"> A Global Network </h2>
                            <p class="mt-5">
                                Our multi-lingual team has over 40 years collective experience in servicing international clients and partners. 
                            </p>
                            <p class="mt-5 has-text-weight-light">
                                We are continuously expanding our reach and strengthening our footprint, collaborating with new global partners, as well as attending pharmaceutical events, medical conferences and exhibiting at global tradeshows, to bring our clients best-in-class equipment wherever it’s needed.
                            </p>
                        </div>



                        <div class="column is-12 mb-0">
                            <a class="button vcare-white-button" href="{{ route('companyProfile').'?u='.env('APP_ASSET_TIMESTAMP') }}" target="_blank">
                                <span class="vcare-button-text is-size-7 pr-4">COMPANY PROFILE</span>
                                <i class="fas fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END Pride -->









    




























 




















  

        <!-- Comnpanies -->
        <?php
            $companies = [
                [
                    'src'=>'/files/home/images/companies/ue-logo.png',
                    'alt'=>'Universidad Europea',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/logo-pascual.png',
                    'alt'=>'Calidad pascua',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/hospital-levante.png',
                    'alt'=>'Hospital intemutual de Levante',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/ib-salut.png',
                    'alt'=>'IB salut',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/guadalajara.png',
                    'alt'=>'Ayuntamiento de Guadalajara',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/jcyl.png',
                    'alt'=>'Gerencia de Servicios sociales, de castilla y león',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/psa1.png',
                    'alt'=>'Plataforma de almeria',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/garrigues.png',
                    'alt'=>'GARRIGUES',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/gruporecoletas-logo.png',
                    'alt'=>'Hospital de Palencia',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/cordoba.png',
                    'alt'=>'Plataforma de córdoba',
                    'style'=>'background-size: 70%;',
                ],
                [
                    'src'=>'/files/home/images/companies/TituloWebAvila.png',
                    'alt'=>'Complejo asistencial de avila',
                    'style'=>'background-size: 70%;',
                ],
                [
                    'src'=>'/files/home/images/companies/TituloWebLeon.png',
                    'alt'=>'Hospital de león',
                    'style'=>'background-size: 70%;',
                ],
                // [
                //     'src'=>'/files/home/images/companies/osakidetza.png',
                //     'alt'=>'Osakidetza',
                //     'style'=>'background-size: 70%;',
                // ],
                [
                    'src'=>'/files/home/images/companies/logo_12octubre.png',
                    'alt'=>'12 de octubre',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/sacyl.png',
                    'alt'=>'SACYL',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/pmm-logo.png',
                    'alt'=>'Policía municipal de Madrid',
                    'style'=>'background-size: 30%;',
                ],
                [
                    'src'=>'/files/home/images/companies/logo-quiron-salud.png',
                    'alt'=>'Quirón',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/logo-centro-medico-teknon-barcelona1.png',
                    'alt'=>'Teknon',
                    'style'=>'background-size: 70%;',
                ],
                [
                    'src'=>'/files/home/images/companies/icatme.png',
                    'alt'=>'iCATME',
                    'style'=>'background-size: 30%;',
                ],
                [
                    'src'=>'/files/home/images/companies/cofares.png',
                    'alt'=>'COFARES',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/caixa.png',
                    'alt'=>'Caixa',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/elcorte.png',
                    'alt'=>'Elcorte',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/iberdola.png',
                    'alt'=>'Iberdola',
                    'style'=>'background-size: 50%;',
                ],
                [
                    'src'=>'/files/home/images/companies/palencia.jpg',
                    'alt'=>'Iberdola',
                    'style'=>'background-size: 50%;',
                ],
            ];                      
        ?>
        <section id="companies" class="section ">
            <div class="container">
                <div class="columns">
                    <div class="column is-12">
                        <h2 class="is-size-2 is-size-5-mobile has-vcare-text has-text-centered">Our Clients</h2>
                        <div class="image-container">
                            <div class="images">
                                @foreach ($companies as $key => $comp )
                                <div>
                                    <div class="image" 
                                    style="background-image: url({{ $comp['src'] }}); {{ $comp['style'] }}" 
                                    title="{{ $comp['alt'] }}"></div>
                                </div>
                                @endforeach
                            </div>
                            <i class="prev fas fa-chevron-left"></i>
                            <i class="next fas fa-chevron-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END Comnpanies --> 

        
        <!-- Contact -->
        <section id="vcare-contact" class="section">
            <div class="container">
                <div class="columns is-centered">
                    <div class="column is-8 has-text-centered">
                        <h2 class="is-size-2 is-size-4-mobile has-vcare-text"> Subscribe To Our Newsletter</h2>
                        <p class="mt-4">Stay up to date with our latest news and products.</p>
                        <div>
                            <form id="subscribe-newsletter" action="{{ route('subscribeNewsletter') }}" method="POST">
                                @csrf
                                <div class="field is-horizontal mt-5">
                                    <div class="field-body">
                                        <div class="field subscriber-name-field">
                                            <p class="control is-expanded has-icons-left">
                                                <input class="input subscriber-name" name="subscriber-name" type="text" placeholder="Name">
                                                <span class="icon is-small is-left">
                                                    <i class="fas fa-user"></i>
                                                </span>                                  
                                            </p>
                                            <p class="help is-danger" style="display: none;">Name is unavailable.</p>
                                        </div>

                                        <div class="field subscriber-email-field">
                                            <p class="control is-expanded has-icons-left">
                                                <input class="input subscriber-email" name="subscriber-email" type="text" placeholder="Email">
                                                <span class="icon is-small is-left">
                                                    <i class="fas fa-at"></i>
                                                </span>                                    
                                            </p>
                                            <p class="help is-danger" style="display: none;">Email is unavailable.</p>
                                        </div>

                                        <div class="field">
                                            <p class="control">
                                                <button class="button vcare-button" style="width: 95%;">
                                                    <span class="vcare-button-text is-size-7 pr-4">SUBSCRIBE</span><i class="fas fa-chevron-right"></i>
                                                </button>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            {{-- <button class="button vcare-button-outlined mt-4">
                                <span class="vcare-button-text is-size-7 pr-4">SUBSCRIBE</span><i class="fas fa-chevron-right"></i>
                            </button> --}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END Contact -->

        <!-- <section class="section">
            <div class="container has-text-centered">
                <img src="{{ env('APP_URL') }}/files/logos/dosnetwork.png" alt="Vcare Dos Network" width="50%">
            </div>
        </section> -->


        {{-- <hr class="mb-0 pb-0">

        <!-- Contact -->
        <section id="vcare-contact" class="section">
            <div class="container">
                <div class="columns">
                    <div class="column is-half is-offset-one-quarter has-text-centered">
                        <h2 class="is-size-2 is-size-5-mobile has-vcare-text"> Get in Touch with us</h2>
                        <p class="mt-4">Interested in becoming a partner or a supplier?<br> We'd love to hear from you.</p>
                        <div>
                            <button class="button vcare-button-outlined mt-4">
                                <a href="{{ route('contact') }}">
                                    <span class="vcare-button-text is-size-7 pr-4">SEND US A MESSAGE</span><i class="fas fa-chevron-right"></i>
                                </a>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END Contact --> --}}



        <div class="modal" id="message">
            <div class="modal-background"></div>
            <div class="modal-content">
                <div class="box has-text-centered px-6 py-6">

                    <img class="vcare-logo" 
                    src="/files/logos/vcare-earth-logo.svg?u=20200922" 
                    alt="VCare.Earth Logo">
                    
                    @if($message)
                    <h2 class="subtitle my-5">{!! $message !!}</h2>
                    @endif

                    <button class="button vcare-button close-message">
                        <span class="vcare-button-text is-size-5">OK</span>
                    </button>
                </div>
            </div>
            
        </div>        

@endsection