@component('mail::message')
    <p>Hello Admin,</p>
    <p>Here are the details of the of the user who contacted.</p>
    <p><b>Name:</b> {{$user_details['name']}}</p>
    <p><b>Email:</b> {{$user_details['email']}}</p>
    <p><b>Number:</b> {{$user_details['number_code']}} - {{$user_details['number']}}</p>
        @if($user_details['whatsapp'])
    <p><b>Available on Whatsapp</b></p> 
        @endif
    <p><b>Company Name:</b> {{$user_details['company']}}</p>
    <p><b>Company Website:</b> {{$user_details['company_website']}}</p>
        @if ($user_details['interested_in'] == 'sample_request')
    <p><b>Address:</b> {{$user_details['address']}}</p>
        @if ($user_details['same_courier_number'] == 'No')
    <p><b>Number for courier:</b> {{$user_details['courier_number_code']}} - {{$user_details['courier_number']}}</p>
        @else
    <p><b>Same number for courier</b></p>
        @endif
        @endif
    <p><b>Message:</b> {{$user_details['message']}}</p>
    @if($user_details['interested_in'] != 'chat')
    <p><b>Who they are:</b> {{$user_details['who']}}</p>
        @if($user_details['who'] == 'Other')
    <p><b>Other Option (Who they are):</b> {{$user_details['who_other_option']}}</p> 
        @endif
    <p><b>How did they hear us?:</b> {{$user_details['how']}}</p>
        @if($user_details['how'] == 'Referral')
    <p><b>Referral Option:</b> {{$user_details['referral_option']}}</p>
        @endif
        @if($user_details['how'] == 'Other')
    <p><b>Other Option (How they know us):</b> {{$user_details['how_other_option']}}</p> 
        @endif
    @if($user_details['interested_in'] == 'b2b')
    <p><b>In PPE Industry/Medical Industry?:</b> {{$user_details['ppe']}}</p>
        @if($user_details['ppe'] == 'None')
    <p><b>Other Option (PPE Industry/Medical Industry?):</b> {{$user_details['ppe_other_option']}}</p> 
        @endif
    <p><b>Target Market?:</b> {{$user_details['target_market']}}</p>
    <p><b>Intention of purchase:</b> {{$user_details['intention']}}</p>
        @if($user_details['intention'] == 'Other')
    <p><b>Other Option (Intention of purchase):</b> {{$user_details['intention_other_option']}}</p> 
        @endif
    <p><b>Sold in Hospitals/Pharmacies?:</b> {{$user_details['sold']}}</p>
    <p><b>Budget:</b> {{$user_details['budget']}}</p><br>
    <p><b>Products</b></p>
        @foreach($p_names as $pn)
            @if($user_details[$pn['p_checkbox']])
    <p><b>Name:</b> {{ $pn['p_name'] }}</p>
    <p><b>Quantity:</b> {{ $user_details[$pn['p_quantity']] }}</p>        
                <br><br>
            @endif
        @endforeach
    <p><b>Interested in:</b> B2B</p>
    @elseif ($user_details['interested_in'] == 'partner')
    <p><b>In PPE Industry/Medical Industry?:</b> {{$user_details['ppe']}}</p>
        @if($user_details['ppe'] == 'None')
    <p><b>Other Option (PPE Industry/Medical Industry?):</b> {{$user_details['ppe_other_option']}}</p> 
        @endif
    <p><b>Target Market?:</b> {{$user_details['target_market']}}</p>
    <p><b>Interested in:</b> Partnering with us</p>
    @elseif ($user_details['interested_in'] == 'distributor')
    <p><b>In PPE Industry/Medical Industry?:</b> {{$user_details['ppe']}}</p>
        @if($user_details['ppe'] == 'None')
    <p><b>Other Option (PPE Industry/Medical Industry?):</b> {{$user_details['ppe_other_option']}}</p> 
        @endif
    <p><b>Target Market?:</b> {{$user_details['target_market']}}</p>
    <p><b>Intention of purchase:</b> {{$user_details['intention']}}</p>
        @if($user_details['intention'] == 'Other')
    <p><b>Other Option (Intention of purchase):</b> {{$user_details['intention_other_option']}}</p> 
        @endif
    <p><b>Sold in Hospitals/Pharmacies?:</b> {{$user_details['sold']}}</p>
    <p><b>Interested in:</b> Being a distributor</p>
    @elseif ($user_details['interested_in'] == 'supplier')
    <p><b>Products supply?:</b> {{$user_details['products_supply']}}</p>
    <p><b>WeChat ID:</b> {{$user_details['wechat']}}</p>
    <p><b>Interested in:</b> Becoming a supplier</p>
    @elseif ($user_details['interested_in'] == 'sample_request')
    <p><b>In PPE Industry/Medical Industry?:</b> {{$user_details['ppe']}}</p>
        @if($user_details['ppe'] == 'None')
    <p><b>Other Option (PPE Industry/Medical Industry?):</b> {{$user_details['ppe_other_option']}}</p> 
        @endif
    <p<b>Products</b> </p>
            @foreach($p_names as $pn)
                @if($user_details[$pn['p_checkbox']])
    <p><b>Name:</b> {{ $pn['p_name'] }}</p>
    <p><b>Quantity:</b> {{ $user_details[$pn['p_quantity']] }}</p>        
         <br><br>
                @endif
            @endforeach   
    <p><b>Interested in:</b> Sample request</p>
    @else
    @endif
    @else
    <p><b>Interested in:</b> Used the normal contact form</p>
    @endif









@endcomponent