@component('mail::message')

<div>
    <p>
    Hello {{$name}},<br>
    Thank you so much for your message and reaching out.<br>
    Due to the global events, we are experiencing a large number of inquiries and promise to get back to you within 24 hours.<br>
    If you have an urgent request, please call us on +44 7707 797799 and we will do our best to revert back to you within the hour.
    </p>
</div>
<br>
<div>
    <p>
        Chat soon!
    </p>
    <p>
        VCare Team
    </p>
</div>
@endcomponent