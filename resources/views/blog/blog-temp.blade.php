<section class="section pt-0">
    <div class="container">
        <div class="columns is-centered is-vcentered">
            <div class="column">
                <div class="blog-image">
                    <img src="/files/blog/%%BLOG_PUBLIC_ID%%/images/vcare-earth-respirators-face-masks-covid19-rapid-test.jpg%%APP_ASSET_TIMESTAMP%%" alt="">
                </div>
            </div>
        </div>
        <div class="columns is-centered is-vcentered">
            <div class="column">
                <div class="blog-text">
                    

                    <p>
                        <b>VCare.Earth</b> <br>
                        Product Portfolio
                    </p>
                    
                    <p>
                        <b>Introduction</b>
                    </p>
                    
                    <p>
                        VCare.Earth provides customers worldwide with essential, high-quality medical and healthcare equipment, supporting the safety of people, communities, and businesses.
                    </p>
                    
                    <p>
                        Established during a global pandemic, when the demand for healthcare equipment has increased, we believe everything we manufacture, or supply should be well produced to the highest standards while staying accessible and affordable. In doing so, we are capable of supporting millions of patients and frontline workers across the globe today, and ongoing.
                    </p>
                    
                    <p>
                        In this article, we’ll explain the many types of protective equipment VCare.Earth is supplying to its global customer base, their intended uses, what the terms ‘medical’ versus ‘non-medical’ mean, and the benefits of each one in the workplace.
                    </p>


                    <div class="blog-image">
                        <img src="/files/blog/%%BLOG_PUBLIC_ID%%/images/vcare-earth-ppe.jpg%%APP_ASSET_TIMESTAMP%%" alt="">
                    </div>
                    <b>
                    <p class="mb-3">
                        PPE
                    </p>
                    <p>
                        Personal Protective Equipment - or PPE - is a term used for items which protect its user against health and safety hazards while at work. Helmets, gloves, eye protection, footwear and high visibility clothing are all types of PPE; so are items that fall under the category of facial protective equipment (FPE). 
                    </p>
                    </b>
                    
                    <p>
                        Such equipment - in particular, FPE - has long been used by businesses whose employees are regularly exposed to hazards including dust particles, fibres, mist, and aerosols, such as in the construction sector. Currently, the term PPE is used most commonly toward single-use filtering face-piece respirators being used by multiple sectors and the general public to offer protection against the spread of COVID-19. PPE is classed as ‘non-medical, although it is used by medical professionals to offer protection between themselves and their patients.
                    </p>
                    
                    <p>
                        VCare.Earth offer a range in filtering face-piece protection (FFP) respirators of varying strength, as well as masks for basic protection from hazards and to help minimize spread of any virus their wearer carries.
                    </p>
                    
                    <p>
                        FFP1 - Filter efficiency of 78%.  Equivalent of a surgical mask, it offers the wearer protection against large solid particles and prevents small water droplets that contain a virus from spreading to others.
                    </p>
                    
                    <p>
                        FFP2 - Filter efficiency of 92%. Offers the wearer protection from solid and liquid irritating aerosols, and those surrounding the wearer from viral droplets.
                    </p>
                    
                    <p>
                        FFP3 - Filter efficiency of 98%. Offers the wearer protection from solid and liquid toxic aerosols, and those surrounding the wearer from viral droplets.
                    </p>
                    
                    <p>
                        FFP respirators come in a variety of designs including foldable, cup, fish and duck shape (some with a valve).*
                    </p>
                    <p>
                        <b>Typical customers</b> of our PPE masks include industrial, manufacturing, healthcare, sanitary and the services sector, or those on-selling our products through retail outlets and online stores.
                    </p>
                    
                    <p class="mb-3">
                        The <b>key benefits</b> are:
                    </p>
                    <ul>
                        <li>Protection against harmful hazards in the workplace </li>
                        <li>Help to minimize the spread of viral infections</li>
                        <li>Verified products manufactured to high standards</li>
                    </ul>
                    
                    <p class="is-italic">
                        *N95 NIOSH face protection will become available on <a target="_blank" href="https://vcare.earth">www.vcare.earth</a> for purchase in the coming weeks.
                    </p>


                    <div class="blog-image">
                        <img src="/files/blog/%%BLOG_PUBLIC_ID%%/images/vcare-earth-testing-kits.jpg%%APP_ASSET_TIMESTAMP%%" alt="">
                    </div>
                    <b>
                    <p class="mb-3">
                        COVID-19 Rapid & PCR Testing Kits 
                    </p>
                    <p>
                        During the current pandemic, our team has responded quickly to manufacture a range in kits to assure fast and accurate positive/negative results for patients or frontline workers testing for COVID-19.
                    </p>
                    </b>                    

                    <p>
                        Three kits are available to our customers.
                    </p>

                    <p>
                        Our <a target="_blank" href="https://vcare.earth/products/rtest-finn">Immunofluorescence antibody rapid test kit FINN</a> require only two drops of capillary blood and three drops of flux to provide a result in just ten minutes. Each single use test can qualitatively detect IgM and IgG SARS-CoV-2 antibodies simultaneously. The Sensitivity is 95.65% and the Specificity 98.14%. Watch how it works <a target="_blank" href="https://vcare.earth/products/rtest-finn#how-to-video">here</a>.
                    </p>
                    
                    <p>
                        The first of two <b>Colloidal Gold</b> options, our rapid strip <a target="_blank" href="https://vcare.earth/products/rtest-marc">MARC</a> uses immunochromatography to qualitatively detect IgM and IgG antibodies in the blood simultaneously, with a result given in fifteen minutes. It is suitable for screening patients suspected with COVID-19 and testing asymptomatic carriers in order to prevent wider transmission. The Sensitivity is 96.3% and the Specificity 99.6%. Watch how it works <a target="_blank" href="https://vcare.earth/products/rtest-marc#how-to-video">here</a>.
                    </p>
                    
                    <p>
                        Our <b>Colloidal Gold</b> rapid cassette, <a target="_blank" href="https://vcare.earth/products/rtest-reid">REID</a>, also uses the immunochromatography method to test for antibodies using human serum, plasma, or whole blood, providing results in ten to fifteen minutes. The relative Sensitivity is 90.85% and the relative Specificity 100.00%.
                    </p>
                    
                    <p>
                        Several VCare.Earth <b>COVID-19 PCR testing kits</b> are going to be available very soon.
                    </p>
                    
                    <p>
                        All VCare.Earth testing kits provide a fast, effective response rate of above 99% accuracy and are available to the public through commercial channels. 
                    </p>
                    
                    <p>
                        Typical customers of our Testing Kits are those looking to test their patients - hospitals, clinics, healthcare services - as well as other customers acquiring kits in bulk who have employees regularly exposed to the medical sector, such as logistics companies. 
                    </p>
                    
                    <p class="mb-3">
                        The <b>key benefits</b> are:
                    </p>
                    <ul class="mb-6">
                        <li>Fast, accurate detection of Covid-19 antibodies</li>
                        <li>Readily available for any business looking to test their employees</li>
                        <li>Minimizing the spread of viral infection amongst the workplace</li>
                    </ul>
                    
                    <p class="mb-3">
                        <b>Medical Equipment</b>
                    </p>
                    
                    <p class="mb-6">
                        Used in healthcare environments such as hospitals, dentists, clinics, and any establishments that deal in treating patients, medical equipment including masks and gowns are designed to protect their user from multiple bodily fluids and secretions. The term ‘medical’ applies to the forum in which they are commonly used and does not guarantee full protection from the exposure of viruses.
                    </p>
                    
                    <p>
                        VCare.Earth currently provides its customers with the following range in medical equipment:
                    </p>
                    
                    <p>
                        Our <a target="_blank" href="https://vcare.earth/products/mfmask-gem">Surgical Face Mask GEM</a> covers the nose and mouth areas and is held in place by ear loops. It can reduce the wearer against exposure to certain airborne bacteria found in medical environments although it does not provide full protection to viral infection such as COVID-19.
                    </p>



                    <div class="blog-image">
                        <img src="/files/blog/%%BLOG_PUBLIC_ID%%/images/vcare-earth-gowns.jpg%%APP_ASSET_TIMESTAMP%%" alt="">
                    </div>                    
                    <p>
                        <b>The Surgical Gown</b> - <a target="_blank" href="https://vcare.earth/products/srgwn-ria">standard</a> or <a target="_blank" href="https://vcare.earth/products/srgwn-dawn">reinforced</a> - provides resistance from fluid penetration with critical areas of the garment reinforced to increase protection. They are designed for short-medium length procedures where low levels of fluid are expected, and disposable after use.
                    </p>

                    <p>
                        The <a target="_blank" href="https://vcare.earth/products/isgwn-lei">Isolation Gown LEI</a> is a lightweight option designed to protect patients and professionals from cross contamination, and used where minimal fluids are expected. Each one is generous in size for increased coverage and flexibility, with neck ties, waist ties and knitted cuffs. 
                    </p>

                    <p>
                        The <a target="_blank" href="https://vcare.earth/products/cvgwn-kat">Coveralls Gown KAT</a> is a 95 gsm, polyethylene film and non-woven fabric hoody and trouser suit, which offers its user maximum protection when working in close proximity to patients or in areas of increased viral activity.  
                    </p>

                    <p>
                        Typical customers of our Medical Equipment are professionals in hospitals, clinics, and healthcare services working closely with patients or other sectors where maximum protection from viral activity is apparent. 
                    </p>

                    <p class="mb-3">
                        The <b>key benefits</b> are:
                    </p>
                    <ul class="mb-6">
                        <li>Disposable equipment in a range of sizes and gsm to suit the wearer and product use</li>
                        <li>Dispatched in protective, recyclable packaging</li>
                        <li>Available in bulk boxes to optimise the purchase cost</li>
                    </ul>
                    
                    
                    <p>
                        For further company information and orders, please visit <a target="_blank" href="http://www.vcare.earth">www.vcare.earth</a> or contact <a href="mailto:info@vcare.earth">info@vcare.earth</a>.
                    </p>
                    




                </div>
            </div>
        </div>
    </div>
</section>