@extends('layout.main') 

<?php
$route_param = [
    'blog_public_id'=>$blog['public_id'],
    'blog_title'=>str_replace(' ','-',strtolower($blog['title'])),
];
?>

@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('blog_single_page',$route_param) }}" 
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/blog-single-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}" />
<script src="{{ mix('js/blog-single-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection 

<?php 
    $meta_title = $blog['meta_title'].'';
    $meta_desc = $blog['meta_desc'].'';
?>   

@section('head-title')
<title>Vcare.earth - {{ $blog['title'] }}</title>
@endsection

 
@section('seo-meta-title')
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 


<?php
$meta_image = '/files/blog/'.$blog['public_id'].'/images/thumbnail';

if(!File::exists(public_path().$meta_image)) {
    $meta_image = '/files/logos/meta-image.jpg';
} 

?>
@section('seo-meta-image')
<meta property="og:image" content="{{ env('APP_URL'). $meta_image }}" />
<meta property="og:image:secure_url" content="{{ env('APP_URL'). $meta_image }}" />
@endsection




@section('content')



<section class="section pt-0 pb-0">
    <div class="container">
        <div class="columns">
            <div class="column title-column">
                <div class="blog-heading1">
                    <h1 class="title is-1 is-size-3-mobile mb-0">
                        {{ $blog['title'] }}
                    </h1>
                </div>
                <hr>
                <div class="blog-heading2">
                    <div class="category-date">
                        {{-- <h1><a href="" class="subtitle is-5 mr-6">NEWS</a></h1>
                        <h1><a href="" class="subtitle is-5">SEPT 9, 2020</a></h1> --}}

                        <h1 class="subtitle is-5 mb-0 mr-6">NEWS</h1>
                        <h1 class="subtitle is-5 mb-0 mr-6">{{ strtoupper($blog['created_at']->format('M d, Y'))  }}</h1>
                </div>

                    <div class="social">
                        <h1 class="subtitle is-5">
                            SHARE
                            <span class="social-icons">

                                <a target="_blank" 
                                href="https://www.linkedin.com/shareArticle?mini=true&url={{ route('blog_single_page',$route_param) }}">
                                    <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                                </a>
                                
                                <a target="_blank" 
                                href="https://www.facebook.com/sharer.php?u={{ route('blog_single_page',$route_param) }}h">
                                    <i class="fab fa-facebook" aria-hidden="true"></i>
                                </a>
                                
                                <a target="_blank" 
                                href="https://api.whatsapp.com/send?text=We believe you would be interested with this article -> {{ route('blog_single_page',$route_param) }}.">
                                    <i class="fab fa-whatsapp" aria-hidden="true"></i>
                                    {{-- <i class="fab fa-whatsapp-square" aria-hidden="true"></i> --}}
                                </a>
                                
                                
                                <a target="_blank" 
                                href="https://twitter.com/home?status={{ route('blog_single_page',$route_param) }}">
                                    <i class="fab fa-twitter" aria-hidden="true"></i>
                                </a>

                            </span>
                        </h1>                
                    </div>
                </div>                
                <hr>
            </div>
        </div>
    </div>
</section>




<?php
$html_content = str_replace('%%APP_ASSET_TIMESTAMP%%','?u='.env('APP_ASSET_TIMESTAMP'),$blog['html_content']);
$html_content = str_replace('%%BLOG_PUBLIC_ID%%',$blog['public_id'],$html_content);
?>
{!! $html_content !!} 



{{-- @include('blog.blog-temp') --}}




<section class="section pt-0 pb-0">
    <div class="container">
        <div class="columns is-centered is-vcentered">
            <div class="column my-0 py-0">
                        
                @if ($blog['url'])          
                <button class="button vcare-button-outlined goto-original-link">
                    <a target="_blank" href="{{ $blog['url'] }}"> 
                        <span class="vcare-button-text">GOTO ORIGINAL LINK</span>
                        <i class="fas fa-chevron-right"></i> 
                    </a>
                </button>
                @endif                
            </div>
        </div>
    </div>
</section>


<section class="section pt-0">
    <div class="container">
        <div class="columns is-centered is-vcentered">
            <div class="column">
                <div class="blog-social-bottom">
                    <div class="social">
                        <h1 class="subtitle is-5">
                            SHARE THIS ARTICLE
                        </h1>
                        <h1 class="subtitle is-5">
                            <span class="social-icons">
                                <a target="_blank" 
                                href="https://www.linkedin.com/shareArticle?mini=true&url={{ route('blog_single_page',$route_param) }}">
                                    <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                                </a>

                                <a target="_blank" 
                                href="https://www.facebook.com/sharer.php?u={{ route('blog_single_page',$route_param) }}h">
                                    <i class="fab fa-facebook" aria-hidden="true"></i>
                                </a>
                                
                                <a target="_blank" 
                                href="https://api.whatsapp.com/send?text=We believe you would be interested with this article -> {{ route('blog_single_page',$route_param) }}.">
                                    <i class="fab fa-whatsapp" aria-hidden="true"></i>
                                    {{-- <i class="fab fa-whatsapp-square" aria-hidden="true"></i> --}}
                                </a>
                                

                                <a target="_blank" 
                                href="https://twitter.com/home?status={{ route('blog_single_page',$route_param) }}">
                                    <i class="fab fa-twitter" aria-hidden="true"></i>
                                </a>
                            </span>
                        </h1>                
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
@if(Helper::adminMode())
<div class="buttons admin">
    <a class="button is-link is-small" target="_blank" 
    href="{{ route('edit_article_content',['public_id'=>$blog['public_id']]) }}">
        EDIT CONTENT
    </a>
    
    <span class="tag  is-medium is-success mb-2">{{$blog['public_id'] }}</span>
</div>
@endif
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}






@endsection
