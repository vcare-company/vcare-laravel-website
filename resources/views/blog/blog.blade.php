@extends('layout.main') 

@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('covid19-statistics') }}"  
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/blog-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}" />
<script src="{{ mix('js/blog-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection 

@section('head-title')
<title>Blog | Vcare.earth</title>
@endsection


<?php 
    $meta_title = "VCare.Earth Commitment to Care - Blog" ; 
    $meta_desc = "Established as the pandemic took hold of the world, VCare.Earth is helping to protect and safeguard lives today, during this unprecedented time.";
?>    
@section('seo-meta-title')
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 

@section('seo-meta-image')
<meta property="og:image" content="{{ env('APP_URL').'/files/logos/meta-image.jpg' }}" />
<meta property="og:image:secure_url" content="{{ env('APP_URL').'/files/logos/meta-image.jpg' }}" />
@endsection




@section('content')


<section class="section">
    <div class="container">
        <div class="columns is-flex-end">
            <div class="column title-column">
                <h2 class="subtitle">
                    BLOGS & FEATURES
                </h2>
                <h1 class="title is-1 mb-0">Latest News</h1>
            </div>
        </div>
        <div class="columns is-flex-end">
            <div class="column is-7 pt-0 title-column">
                <p>
                    To date, COVID-19 has infected over 26 million people globally, regardless of age, ethnicity, race and religion. Established as the global pandemic took hold of the world, VCare.Earth is helping to protect and safeguard
                    lives today, and ongoing, during this unprecedented time.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">

        @foreach ($blogs as $key => $blog)
        <div class="columns is-vcentered">
            <div class="column is-4">
                <div class="blog-image">
                    <?php 
                        $thumbnail = '/files/blog/'.$blog['public_id'].'/images/thumbnail'
                    ?>
                    @if(File::exists(public_path().$thumbnail)) 
                    <img src="{{ $thumbnail }}" alt="VCare.Earth - Helping to protect and safeguard lives today, during this unprecedented time." />
                    @else
                    <img src="/files/logos/meta-image.jpg" alt="VCare.Earth - Helping to protect and safeguard lives today, during this unprecedented time." />
                    @endif
                </div>
            </div>
            <div class="column">
                <div class="blog-heading">
                    {{-- <h1><a href="" class="title is-6">NEWS</a></h1>
                    <span class="mx-3">|</span>
                    <h1><a href="" class="title is-6">SEPT 9, 2020</a></h1> --}}

                    <h1 class="">NEWS</h1>
                    <span class="mx-5">|</span>
                    <h1>{{ strtoupper($blog['created_at']->format('M d, Y'))  }}</h1>
                </div>
                <div class="blog-title my-3">
                    <h1 class="subtitle my-4">
                        {{ $blog['title'] }}
                    </h1>
                    <p>
                        {{ $blog['short_content'] }}
                    </p>
                </div>

                <div class="links-and-social">
                    <div class="blog-buttons">
                        <button class="button vcare-button-outlined mr-5">
                            <?php
                                $route_param = [
                                    'blog_public_id'=>$blog['public_id'],
                                    'blog_title'=>str_replace(' ','-',strtolower($blog['title'])),
                                ];
                            ?>
                            <a href="{{ route('blog_single_page',$route_param) }}"> 
                                <span class="vcare-button-text">READ MORE</span>
                                <i class="fas fa-chevron-right"></i> 
                            </a>
                        </button>
                        
                        @if ($blog['url'])          
                        <button class="button vcare-button-outlined">
                            <a target="_blank" href="{{ $blog['url'] }}"> 
                                <span class="vcare-button-text">GOTO ORIGINAL LINK</span>
                                <i class="fas fa-chevron-right"></i> 
                            </a>
                        </button>
                        @endif
                    </div>
                    <div class="blog-social">
                        <h1 class="subtitle is-5">
                            SHARE
                            <span class="social-icons">
                                
                                <?php
                                $bt = strtolower($blog['title']);
                                $bt = str_replace('-','',$bt);
                                $bt = str_replace(' ','-',$bt);
                                $bt = str_replace('--','-',$bt);
                                $bt = str_replace('---','-',$bt);
                                $route_param = [
                                    'blog_public_id'=>$blog['public_id'],
                                    'blog_title'=>$bt,
                                ];
                                ?>
                                <a target="_blank" 
                                href="https://www.linkedin.com/shareArticle?mini=true&url={{ route('blog_single_page',$route_param) }}">
                                    <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                                </a>
                                
                                <a target="_blank" 
                                href="https://www.facebook.com/sharer.php?u={{ route('blog_single_page',$route_param) }}h">
                                    <i class="fab fa-facebook" aria-hidden="true"></i>
                                </a>
                                
                                <a target="_blank" 
                                href="https://api.whatsapp.com/send?text=We believe you would be interested with this article -> {{ route('blog_single_page',$route_param) }}.">
                                    <i class="fab fa-whatsapp" aria-hidden="true"></i>
                                    {{-- <i class="fab fa-whatsapp-square" aria-hidden="true"></i> --}}
                                </a>

                                <a target="_blank" 
                                href="https://twitter.com/home?status={{ route('blog_single_page',$route_param) }}">
                                    <i class="fab fa-twitter" aria-hidden="true"></i>
                                </a>

                            </span>
                        </h1>                
                    </div>
                </div>
            </div>
        </div>

        @if ($key<count($blogs)-1)
        <hr>
        @endif

        @endforeach

        {{-- <div class="columns is-vcentered is-centered">
            <div class="column has-text-centered">
                <button class="button vcare-button my-6 load-more-btn">
                    <a href=""> 
                        <span class="vcare-button-text">LOAD MORE</span>    
                    </a>
                </button>
            </div>
        </div> --}}

        <div class="columns is-vcentered is-centered">
            <div class="column has-text-centered">
                {{ $blogsPaginated->links('blog.pagination.pages', ['foo' => 'bar']) }}
            </div>
        </div>        

        <br><br>

    </div>
</section>





{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
@if(Helper::adminMode())
<div class="buttons admin">
    <a class="button is-link is-small" target="_blank" 
    href="{{ route('add_article') }}">
        ADD ARTICLE
    </a>    
    
</div>
@endif
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}




@endsection
