@extends('layout.main')



@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('terms') }}"
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/terms-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/terms-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection

@section('head-title')
<title>Terms & Conditions | Vcare.earth</title>
@endsection



@section('seo-meta-title')
<?php 
    $meta_title = "VCare.Earth Commitment to Care - Terms & Conditions" ; 
    $meta_desc = "Our products and services are available to customers worldwide. For any questions on terms and conditions please refer on this page.";
?>    
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 


@section('content')

<section class="section section-header">
    <div class="container">
        <div class="columns">
            <div class="column title-column">
                <h1 class="title is-1">Terms & Conditions</h1>
            </div>
        </div>        
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="columns">
            <div class="column">
                <p class="">
                    PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY.
                </p>
                <br>
                <p>
                    THESE TERMS AND CONDITIONS SHALL BE BINDING UPON USERS OF WEBSITE WWW.VCARE.EARTH 
                </p>
                <br>
                <p>
                    OUR PRODUCTS AND SERVICES ARE AVAILABLE TO CUSTOMERS WORLDWIDE.
                </p>
                <br>
                <p>
                    General terms and conditions and consumer information in the context of purchase contracts concluded via the online shop between VCARE HEALTH IBERICA, S.L. – hereinafter referred to as “seller” – and the customer – hereinafter referred to as “customer”.
                </p>
            </div>
        </div>        
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Introduction</b></p><br>
        <p>
            Please carefully read the Terms and Conditions (“Terms”) for the website located at www.vcare.earth (the “Website”) including its sub-domains and mobile optimised version, as set out hereinafter. The Website is operated by VCARE HEALTH IBERICA, S.L. (hereinafter referred to also as “Company”). Any ancillary terms, guidelines, the privacy policy (the “Policy”) and other documents made available by the Website from time to time and as incorporated herein by reference, shall be deemed as an integral part of the Terms. These terms set forth the legally binding agreement between you as the user(s) of the Website (hereinafter referred to as “you”, “your” or “User”) and the Company. If you are using the Website or its services on behalf of an entity, organization, or company (collectively “Subscribing Organization”), you declare that you are an authorized representative of that Subscribing Organization with the authority to bind such organization to these Terms; 
            and agree to be bound by these Terms on behalf of such Subscribing Organization. In such a case, “you” in these Terms refers to your Subscribing Organization, and any individual authorized to use the Service on behalf of the Subscribing Organization, including you. 
        </p>
        <br>
        <p>
            By using company provided services, accessing or using the Website in any manner as laid down herein, including, but not limited to, visiting or browsing it, or contributing content or other materials to it, you agree to be bound by these Terms. 
        </p>
        <br>
        <p>
            These Terms, and any rights and licenses granted here under, may not be transferred or assigned by you, but may be assigned by the Website without restriction. Any attempted transfer or assignment in violation hereof shall be null and void.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Acceptance of the terms</b></p><br>
        <p>
            Each time by viewing, using, accessing, browsing, or submitting any content or material on the Website, including the webpages contained or hyperlinked therein and owned or controlled by the Website and its services, whether through the Website itself or through such other media or media channels, devices, software, or technologies as the Website may choose from time to time, you are agreeing to abide by these Terms, as amended from time to time with or without your notice. 
        </p>
        <br>
        <p>
            The Website reserves the right to modify or discontinue, temporarily or permanently, and at any time, the Website and/or the Website Services (or any part thereof) with or without notice. You agree that the Website shall not be liable to you or to any third party for any modification, suspension or discontinuance of the Website Services. 
        </p>
        <br>
        <p>
            Website or the Website management may modify these Terms from time to time, and any change to these Terms will be reflected on the Website with the updated version of the Terms and you agree to be bound to any changes to these Terms when you use the Website or the Website Services. The Website may also, in its sole and absolute discretion, choose to alert via email all users with whom it maintains email information of such modifications. 
        </p>
        <br>
        <p>
            Also, occasionally there may be information on the Website or within the Website Services that contains typographical errors, inaccuracies or omissions that may relate to service descriptions, pricing, availability, and various other information, and the Website management reserves the right to correct any errors, inaccuracies or omissions and to change or update the information at any time, without prior notice.
        </p>
        <br>
        <p>
            When you register an account on the Website and/or upload, submit, enter any information or material to the Website or use any of the Website Services, you shall be deemed to have agreed to and understand the Terms.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Scope and general information</b></p><br>
        <p>
            Subject to individual arrangements and agreements that take precedence over these General Terms and Conditions, the following General Terms and Conditions shall apply exclusively to the business relationship between the Seller and the Customer. Unless otherwise agreed, the inclusion of the customer’s own conditions is contradicted. 
        </p>
        <br>
        <p>
            The customer is a consumer insofar as he concludes the contract for purposes which can predominantly be attributed neither to his commercial nor his self-employed professional activity. On the other hand, an entrepreneur is any natural or legal person or a partnership with legal capacity who, when concluding a legal transaction, acts in the exercise of his commercial or self-employed professional activity.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Conclusion of contract</b></p><br>
        <p>
            The contract is concluded with:
        </p>
        <br>
        <p>
            VCARE HEALTH IBERICA, S.L. <br>
            Madrid, Spain <br>
        </p>
        <br>
        <p>
            The essential characteristics of the goods are defined in the respective product description provided by the seller.  
        </p>
        <br>
        <p>
            All offers in the online shop of the seller only represent a non-binding invitation to the customer to submit a corresponding purchase offer to the seller. As soon as the Seller has received the Customer’s order, the Customer will first be sent a confirmation of his order with the Seller, usually by e-mail (order confirmation). The order confirmation does not yet represent the acceptance of the order. After receipt of the customer’s order, the seller will check it at short notice and inform the customer within 2 working days whether he accepts the order (order confirmation). The ordering process in the online shop of the seller works as follows: 
        </p>
        <br>
        <p>
            The Customer can select products from the Seller’s range and collect them in a so-called shopping cart by clicking on the “Add to cart” button. By clicking on the “Shopping Cart” button, the Customer is given an overview of the selected products. By clicking on the button “Buy now”, he submits a binding request to purchase the goods in the shopping cart. Before sending the order, the customer can change and view the entered order and the entered data at any time by using the browser functions “Back” and “Next”, which are displayed as arrow buttons. The application can only be submitted and transmitted if the customer has accepted these contractual conditions by clicking on the checkbox “Accept GTC” and thereby included them in his application. The Seller will then send the Customer an automatic confirmation of receipt by e-mail, in which the Customer’s order is listed again and which the Customer can print out using the “Print” function. The automatic acknowledgement of receipt merely documents that the salesperson has received the customer’s order and does not constitute acceptance of the request. The contract is only concluded when the seller issues the declaration of acceptance, which is sent by a separate e-mail.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Subject matter of the contract, condition, delivery, availability of goods</b></p><br>
        <p>
            The subject matter of the contract is the goods and services specified by the customer in the order and mentioned in the order and/or order confirmation at the final prices stated in the online shop. Errors and mistakes there are reserved, especially with regard to the availability of goods. 
        </p>
        <br>
        <p>
            The quality of the ordered goods is determined by the product descriptions in the online shop. Illustrations on the website may not accurately represent the products; in particular colors may vary considerably for technical reasons. Pictures serve only as illustrative material and may differ from the product. Technical data, weight, dimension and performance descriptions are given as precisely as possible but may show the usual deviations. The characteristics described here do not represent defects of the products delivered by the seller. 
        </p>
        <br>
        <p>
            If no copies of the product selected by the customer are available at the time of the customer’s order, the seller shall inform the customer of this in the order confirmation. If the product is permanently unavailable, the seller will refrain from issuing a declaration of acceptance. In this case a contract is not concluded. 
        </p>
        <br>
        <p>
            If the product designated by the customer in the order is only temporarily unavailable, the seller shall also inform the customer of this immediately in the order confirmation. In the event of a delivery delay of more than two weeks, the customer has the right to withdraw from the contract. In this case, the seller is also entitled to withdraw from the contract. In this case, he will immediately refund any payments already made by the customer.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Delivery, prices, shipping costs</b></p><br>
        <p>
            The delivery to the shipping company will be made at the latest two days after receipt of money, in case of cash on delivery payment at the latest two days after the order confirmation. The delivery time is up to five days. The seller will point out possible differing delivery times on the respective product page. 
        </p>
        <br>
        <p>
            All item prices are net prices. We do not charge value-added tax. The stated prices are final sales prices including worldwide shipping costs. The customer receives an invoice for his purchase via email. Custom and/or import duties are at the expense of the buyer. 
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Delay</b></p><br>
        <p>
            VCARE HEALTH IBERICA, S.L. will use reasonable efforts to meet shipment or delivery dates specified byVCARE HEALTH IBERICA, S.L., but such dates are estimates only. 
        </p>
        <br>
        <p>
            VCARE HEALTH IBERICA, S.L. will not be liable for any delay or non-delivery in shipping for any reason but not limited to delay or non-delivery caused directly or indirectly by Acts of God, fire, flood, strike or lockout or other labor dispute, accident, civil commotion, riot, war, governmental regulation or order, whether or not it later proves to be invalid, or from any other cause or beyond VCARE HEALTH IBERICA, S.L. control.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Payment Online Shop</b></p><br>
        <p>
            The payment is made directly via our payment gateway (Foloosi.com). We accept Visa, Mastercard, American Express (AMEX) credit cards. Payment must be done always 100% in advance.
        </p>
    </div>
</section>


<section class="section">
    <div class="container">
        <p><b>Damage in transit</b></p><br>
        <p>
            If goods with obvious transport damages are delivered, the customer is asked to complain about these errors immediately to the deliverer and to contact the seller as soon as possible. 
        </p>
        <br>
        <p>
            Failure to make a complaint or to contact the seller has no consequences for the customer’s statutory warranty rights but helps the seller to be able to assert his own claims against the carrier or the transport insurance company.
        </p>
    </div>
</section>


<section class="section">
    <div class="container">
        <p><b>Warranty for material defects</b></p><br>
        <p>
            The provider is liable for material defects in accordance with the applicable statutory provisions, in particular §§ 434 ff BGB. 
        </p>
        <br>
        <p>
            A guarantee for the goods delivered by the vendor only exists if this was expressly stated in the order confirmation for the respective article.     
        </p>
        <br>
        <p>
            Complaints and claims for liability for defects can be made at the address given in the provider identification.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Retention of title</b></p><br>
        <p>
            The delivered goods remain the property of the seller until full payment has been received.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Liability</b></p><br>
        <p>
            The statutory provisions apply.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Text of the contract</b></p><br>
        <p>
            The contract text is stored on the internal systems of the seller. The customer can view the general terms and conditions at any time in his customer account. The order data and the general terms and conditions are sent to the customer by e-mail. After completion of the order, the order data is no longer accessible via the Internet for security reasons.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Service availability</b></p><br>
        <p>
            The Website shall use commercially reasonable efforts to keep it up and running 24 hours a day, seven days a week; provided, however, that it may carry out scheduled and unscheduled maintenance work as necessary from time to time and such maintenance work may impact the availability of the Website.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Account registration and membership</b></p><br>
        <p>
            In order to use some or all of the functionalities and Services provided through the Website, you may be required to register an account with the Website as mentioned above. At the time of registration of account, you will be asked to complete a registration form which shall require you to provide personal information such as name, address, phone number, email address, username and other personal information. 
        </p>
        <br>
        <p>
            Upon verification of details, Website may accept account registration application. 
        </p>
        <br>
        <p>    
            You represent, warrant and covenant that: (i) you have full power and authority to accept these Terms, to grant any license and authorization and to perform any of your obligations hereunder; (ii) you will undertake the use the Website and Services for personal purposes only; and (iii) the address you provide when registering is your personal address. 
        </p>
        <br>
        <p>    
            You must not allow any other person to use your account to access the Website.<br> 
            You must notify us in writing immediately if you become aware of any unauthorized use of your account.<br> 
            You must not use any other person’s account to access the Website, unless you have that person’s express written permission to do so.<br>
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>User IDs and passwords</b></p><br>
        <p>
            If you register for an account with the Website, you will be asked to choose a user ID and password. Your user ID must not be misleading and must comply with the content rules set out in this document; you must not use your account or user ID for or in connection with the impersonation of any person. 
        </p>
        <br>
        <p> 
            You shall be responsible to maintain the confidentiality of your password and shall be responsible for all uses via your registration and/or login, whether authorized or unauthorized by you. 
        </p>
        <br>
        <p>   
            You agree to immediately notify us of any unauthorized use or your registration, user account or password. You must notify the Website by emailing us at info@vcare.earth if you have reason to believe that your account is no longer secure for any reason (for example, in the event of a loss, theft or unauthorized disclosure or use of your password). 
        </p>
        <br>
        <p>  
            You are responsible for any activity and content on the account arising out of any failure to keep your password confidential, and may be held liable for any losses arising out of such a failure. 
        </p>
        <br>
        <p>   
            Registration data and other personally identifiable information that we may collect is subject to the terms of our Privacy Policy.
        </p>
     
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Termination of account and discontinuation of use</b></p><br>
        <p>
            If you engage in Prohibited Conduct or otherwise violate any of the Terms, your permission to use the Website will be terminated. 
        </p>
        <br>
        <p>     
            You also agree that we may, at any time and without notice to you, suspend or revoke your access to and use of the Website, and any accounts you may have in connection with the Service including: (i) where we determine in our sole discretion that such action is reasonable in order to comply with legal requirements or to protect the rights or interests of Company or any third party; or (ii) in connection with any general discontinuation of the Website services. 
        </p>
        <br>
        <p>     
            We will have no liability whatsoever on account of any change to the Service or any suspension or revocation of your access to or use of the Website. You may terminate your account at any time by sending us an email to info@vcare.earth.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Electronic signature consent</b></p><br>
        <p>
            You agree that your “Electronic Signature” is the legal equivalent of your manual signature for this Agreement, thereby indicating your consent to do business electronically. 
        </p>
        <br>
        <p>  
            By clicking on the applicable button in the Website, you will be deemed to have executed these Terms electronically via your Electronic Signature with Company; effective on the date you first click to accept these Terms.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Electronic delivery of communications</b></p><br>
        <p>
            You agree to receive communications from Website in electronic form. Such electronic communications may include, but will not be limited to, any and all current and future notices and/or disclosures that various laws or regulations require that we provide to you, as well as such other documents, statements, data, records and any other communications regarding your relationship with the Website. 
        </p>
        <br>
        <p> 
            You accept that the electronic documents, files and associated records provided via your account with Website are reasonable and proper notice, for the purpose of any and all laws, rules, and regulations, and you acknowledge and agree that such electronic form fully satisfies any requirement that such communications be provided to you in writing or in a form that you may keep. Website reserves the right to require ink signatures on hard copy documents from the related parties, at any time.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>User responsibility</b></p><br>
        <p>
            Users are solely responsible for all of the transactions conducted on, through or as a result of use of the Website or Services. 
            </p>
        <p>
            You agree that the use of the Website and/or the Website Services on the Website is subject to all applicable local, state and federal laws and regulations. 
            </p>
        <p>
            You also agree: Not to access the Website or services using a third-party’s account/registration without the express consent of the account holder; 
            </p>
        <p>
            Not to use the Website for illegal purposes; Not to commit any acts of infringement on the Website or with respect to content on the Website; 
            </p>
        <p>
            Not to copy any content for republication in print or online; 
            </p>
        <p>
            Not to create reviews or blog entries for or with any purpose or intent that does not in good faith comport with the purpose or spirit of the Website; Not to attempt to gain unauthorized access to other computer systems from or through the Website; 
            </p>
        <p>
            Not to interfere with another person’s use and enjoyment of the Website or another entity’s use and enjoyment of the Website; 
            </p>
        <p>
            Not to upload or transmit viruses or other harmful, disruptive or destructive files; 
            </p>
        <p>
            and/or Not to disrupt, interfere with, or otherwise harm or violate the security of the Website, or any services, system restores, accounts, passwords, servers or networks connected to or accessible through the Website or affiliated or linked website. Not to use the Website in any way or take any action that causes, or may cause, damage to the Website or impairment of the performance, availability or accessibility of the Website; 
            </p>
        <p>
            Not to use the Website in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity; 
            </p>
        <p>
            Not to conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to the Website without the express written consent of the Website owner; 
            </p>
        <p> 
            Not to access or otherwise interact with the Website using any robot, spider or other automated means; 
            </p>
        <p>
            Not to violate the directives set out in the robots.txt file for the website; 
            </p>
        <p>
            Not to use data collected from the website for any direct marketing activity (including without limitation email marketing, SMS marketing, telemarketing and direct mailing); 
            </p>
        <p>
            Not to infringe these Terms or allow, encourage or facilitate others to do the same; 
            </p>
        <p>
            Not to plagiarize and/or infringe the intellectual property rights or privacy rights of any third party; 
            </p>
        <p>
            Not to disturb the normal flow of Services provided within the Website; 
            </p>
        <p>
            Not to create a link from the Website to another website or document without Company’s prior written consent; 
            </p>
        <p>
            Not to obscure or edit any copyright, trademark or other proprietary rights Notice or mark appearing on the Website; 
            </p>
        <p>
            Not to create copies or derivate works of the Website or any part thereof Not to reverse engineer, decompile or extract the Website’s source code; 
            </p>
        <p>
            Not to remit or otherwise make or cause to deliver unsolicited advertising, email spam or other chain letters; 
            </p>
        <p>
            Not to collect, receive, transfer or disseminate any personally identifiable information of any person without consent from title holder; 
            </p>
        <p>
            and/or Not to pretend to be or misrepresent any affiliation with any legal entity or third party. In addition to the above clause, unless specifically endorsed or approved by the Website, the following uses and activities of and with respect to the Website and the Website Services are prohibited: criminal or tortuous activity, including child pornography, fraud, trafficking in obscene material, drug dealing, gambling, harassment, stalking, spamming, copyright infringement, patent infringement, or theft of trade secrets; 
            </p>
        <p>
            Transmitting chain letters or junk email; 
            </p>
        <p>
            Engaging in any automated use of the Website or the Website Services. Interfering with, disrupting, or creating an undue burden on the Website or the Website Services or the networks or services connected or linked thereto; 
            </p>
        <p>
            Attempting to impersonate another user or person; 
            </p>
        <p>
            Using the username of another user; 
            </p>
        <p>
            Selling or otherwise transferring your profile; 
            </p>
        <p>
            Using any information obtained from the Website or the Website Services in order to harass, abuse, or harm another person; 
            </p>
        <p>
            Deciphering, decompiling, disassembling or reverse engineering any of the software comprising or in any way making up a part of the Website or the Website Services; 
            </p>
        <p>
            Attempting to bypass any measures of the Website or the Website Services designed to prevent or restrict access to the Website or the Website Services, or any portion of the Website or the Website Services; 
            </p>
        <p>
            Harassing, annoying, intimidating or threatening any the Website employees or agents engaged in providing any portion of the Website Services; 
            </p>
        <p>
            Using the Website and/or the Website Services in any manner inconsistent with any and all applicable laws and regulations.
            </p>
        <p>  
            Using data collected from the website to contact individuals, companies or other persons or entities. 
            </p>
        <p> 
            Supplying false, untrue, expired, incomplete or misleading information through the Website. 
            </p>
        <p>    
            You also acknowledge and accept that any violation of the aforementioned provisions may result in the immediate termination of your access to the Website and use of our Services, without refund, reimbursement, or any other credit on our part. Access to the Website may be terminated or suspended without prior notice or liability of Company. You represent and warrant to us that you have all right, title, and interest to any and all content you may post, upload or otherwise disseminate through the Website. You hereby agree to provide Company with all necessary information, materials and approval, and render all reasonable assistance and cooperation necessary for our Services.
        </p>
        <p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Third party websites</b></p><br>
        <p>
            The Website includes hyperlinks to other websites owned and operated by third parties; such hyperlinks are not recommendations. Goods and services of third parties may be advertised and/or made available on or through this web site. Representations made regarding products and services provided by third parties are governed by the policies and representations made by these third parties. The Website shall not be liable for or responsible in any manner for any of your dealings or interaction with third parties.
        </p>
        <br>
        <p> 
            The management of the Website has no control over third party websites and their contents, and subject to the Terms it accepts no responsibility for them or for any loss or damage that may arise from your use of them. 
        </p>
        <br>
        <p>     
            The Website may contain links from third party websites. External hyperlinks to or from the site do not constitute the Website’s endorsement of, affiliation with, or recommendation of any third party or its website, products, resources or other information. The Website is not responsible for any software, data or other information available from any third party website. You are solely responsible for complying with the terms and conditions for the third party sites. You acknowledge that Company shall have no liability for any damage or loss arising from your access to any third party website, software, data or other information. 
        </p>
        <br>
        <p>     
            We do not always review the information, pricing, availability or fitness for use of such products and services and they will not necessarily be available or error free or serve your purposes, and any use thereof is at your sole risk. We do not make any endorsements or warranties, whether express or implied, regarding any third party websites (or their products and services). Any linked websites are ruled by their privacy policies, terms and conditions and legal disclaimers. Please read those documents, which will rule any interaction thereof. 
        </p>
        <br>
        <p>     
            The Website may provide tools through the Service that enable you to export information to third party services, including through use of an API or by linking your account on the Website with an account on the third party service, such as Twitter or Facebook. By using these tools, you agree that we may transfer such User Content and information to the applicable third party service. Such third party services are not under our control, and we are not responsible for the contents of the third party service or the use of your User Content or information by the third party service. The Service, including our websites, may also contain links to third-party websites. The linked sites are not under our control, and we are not responsible for the contents of any linked site. We provide these links as a convenience only, and a link does not imply our endorsement of, sponsorship of, or affiliation with the linked site. You should make whatever investigation you feel necessary or appropriate before proceeding with any transaction with any of these third parties services or websites.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Third party rights</b></p><br>
        <p>
            Contract under the Terms is for our benefit and your benefit, and is not intended to benefit or be enforceable by any third party. 
        </p>
        <br>
        <p>   
            The exercise of the parties’ rights under a contract under the Terms is not subject to the consent of any third party. 
        </p>
        <br>
        <p>     
            You agree not to; modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, scrape, gather, market, rent, lease, re-license, reverse engineer, or sell any information published by other users without the original publishers written consent.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Ownership</b></p><br>
        <p>
            The term hereof shall begin on the date that comes first among: (i) first access to the Website; (ii)your first access or execution of our Services; or (iii) Company begins providing its Services to you. 
        </p>
        <br>
        <p>   
            The term hereof will automatically end on the earlier date of either your: (i) account deactivation, suspension, freezing or deletion; (ii) access termination or access revocation for our Services or the Website; (iii) Company’s termination of these Terms or its Services, at its sole and final discretion; (iv) the termination date indicated by Company to you from time to time; or (v) Company’ decision to make the Website or Services no longer available for use, at its sole and final discretion.  
        </p>
        <br>
        <p>     
            Upon expiration of these Terms or termination of your subscription to our Services, you shall thereafter immediately cease any and all use of our Services, along with any and all information and data collected therefrom
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Amendments</b></p><br>
        <p>
            Company hereby reserves the right to update, modify, change, amend, terminate or discontinue the Website, the Terms and/or the Policy, at any time and at its sole and final discretion. Company may change the Website’s functionalities and (any) applicable fees at any time. Any changes to these Terms will be displayed in the Website, and we may notify you through the Website or by email. Please, refer to the date shown above for the date where effective changes were last undertook by us. Your use of our Services after the effective date of any update– either by an account registration or simple use – thereby indicates your acceptance thereof.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Indemnification</b></p><br>
        <p>
            You agree to indemnify, defend and hold Company and its independent contractors, affiliates, subsidiaries, officers, employees and agents, and their respective employees, agents and representatives, harmless from and against any and all actual or threatened proceedings (at law or in equity), suits, actions, damages, claims, deficiencies, payments, settlements, fines, judgments, costs, liabilities, losses and expenses (including, but not limited to, reasonable expert and attorney fees and disbursements) arising out of, caused or resulting from: (i) your conduct and any user content; (ii) your violation of these Terms or the Policy; and (iii) your violation of the rights of any third-party. 
        </p>
        <br>
        <p>
            You indemnify the Website and its management for any time that the Website may be unavailable due to routine maintenance, updates or any other technical or non-technical reasons. You agree to indemnify the Website and its management for any error, omission, interruption, deletion, defect, delay in operation or transmission, communication line failure, theft or destruction or unauthorized access to your published content, damages from lost profits, lost data or business interruption. 
        </p>
        <br>
        <p>   
            You hereby indemnify the Website and its management and will not hold them responsible for copyright theft, reverse engineering and use of your content by other users on the website.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Generals</b></p><br>
        <p>
            Advertisements and Promotions. From time to time, we may place ads and promotions from third party sources in the Website. Accordingly, your participation or undertakings in promotions of third parties other than Company, and any terms, conditions, warranties or representations associated with such undertakings, are solely between you and such third party. Company is not responsible or liable for any loss or damage of any sort incurred as the result of any such dealings or as the result of the presence of third party advertisers on the Website. 
        </p>
        <br>
        <p>
            No Assignment. You may not assign or transfer these Terms by operation of law or otherwise without our prior written consent. Notwithstanding the foregoing, we may assign any rights or obligations hereunder to any current or future affiliated company and to any successor in interest. Any rights not expressly granted herein are thereby reserved. These terms will inure to the benefit of any successors of the parties. We reserve the right, at any time, to transfer some or all of Company’s assets in connection with a merger, acquisition, reorganization or sale of assets or in the event of bankruptcy. 
        </p>
        <br>
        <p>    
            Force Majeure. Company is no liable for any failure of performance on its obligations as set forth herein, where such failure arises from any cause beyond Company’s reasonable control, including but not limiting to, electronic, power, mechanic or Internet failure, from acts of nature, forces or causes beyond our control, including without limitation, Internet failures, computer, telecommunications or any other equipment failures, electrical power failures, strikes, labor disputes, riots, insurrections, civil disturbances, shortages of labor or materials, fires, flood, storms, explosions, acts of God, war, governmental actions, orders of domestic or foreign courts or tribunals or non-performance of third parties. 
        </p>
        <br>
        <p>
            Headings. The titles of paragraphs in these Terms are shown only for ease of reference and will not affect any interpretation therefrom. 
        </p>
        <br>
        <p>    
            No Waiver. Failure by Company to enforce any rights hereunder shall not be construed as a waiver of any rights with respect to the subject matter hereof. 
        </p>
        <br>
        <p>    
            No Relationship. You and Company are independent contractors, and no agency, partnership, joint venture, employee-employer, or franchiser-franchisee relationship is intended or created by these Terms. 
        </p>
        <br>
        <p>    
            All legal notices or demands to or upon Company shall be made in writing and sent to Company personally, by courier, certified mail, or facsimile, and shall be delivered to any address the parties may provide. For communications by e-mail, the date of receipt will be the one in which confirmation receipt notice is obtained. You agree that all agreements, notices, demands, disclosures and other communications that Company sends to you electronically satisfy the legal requirement that such communication should be in writing. 
        </p>
        <br>
        <p>    
            If any provision of these Terms is held unenforceable, then such provision will be modified to reflect the parties’ intention. All remaining provisions of these Terms will remain in full force and effect. The failure of either party to exercise in any respect any right provided for herein will not be deemed a waiver of any further rights hereunder. 
        </p>
        <br>
        <p>    
            For any inquires or complaints regarding the Service or Website, please contact by email at info@vcare.earth.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Final provisions</b></p><br>
        <p>
            The contract language is English. 
        </p>
        <br>
        <p>
            The law of the United Arab Emirates shall apply to contracts between the Seller and the customers, excluding the laws on the international purchase of movable goods.  
        </p>
        <br>
        <p>   
            If the customer is a merchant, a legal entity under public law or a special fund under public law, the place of jurisdiction for all disputes arising from contractual relationships between the customer and the seller is Barcelona, Spain. This also applies if the customer does not have a general place of jurisdiction in Barcelona, or if his place of residence or habitual abode is not known at the time the action is filed.
        </p>
    </div>
</section>
@endsection