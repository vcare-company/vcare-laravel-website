@extends('layout.main')



@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('search') }}"
        },
        @endforeach
    ]

}
</script>
<script>
    let search_string_post = '{{ $search_string_post }}';
</script>

<link rel="stylesheet" href="{{ mix('css/search-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/search-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection


@section('head-title')
<title>Search Products | Vcare.earth</title>
@endsection

@section('seo-meta-title')
<?php 
    $meta_title = "VCare.Earth Commitment to Care - Search" ; 
    $meta_desc = "Established with a clear purpose to provide premium medical and healthcare products to clients local and global.";
?>    
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 


@section('content')

<section class="section">
    <div class="container">
        <div class="columns is-flex-end">
            <div class="column is-half title-column">
                <h2 class="subtitle">
                    VCare Products
                </h2>
                <h1 class="title is-1">Search</h1>
                <p>
                  We are pleased to present our distinctive array of products which were
                  carefully made and designed with high-quality, durability, and reliability.
                  We are happy to serve our customers and we guarantee products and 
                  professional services at the most reasonable and economical prices.
                </p>
            </div>
            <div class="column has-text-right-desktop has-text-left-mobile search-bar-page">
                {{-- <form action="{{ route('search-post') }}" method="POST">
                    @csrf --}}
                    <div class="field has-addons has-addons-centered">
                        <div class="control is-expanded">
                            <input class="input search-string" type="text" placeholder="Search product here.." value="">
                        </div>
                        <p class="control">
                          <button class="button vcare-btn search-submit">
                            <i class="fas fa-search"></i>
                          </button>
                        </p>
                    </div>
                                     
                {{-- </form> --}}

            </div>
        </div>
        <div class="columns">
            <div class="column">
                <hr>
            </div>
        </div>                
    </div>
</section>



<section class="section resources">
<div class="container">
<div class="columns">

    {{-- right section --}}
    <div class="column">


    <div class="columns is-multiline">
    @foreach ($categories as $i => $categ)
        @foreach ($categ['children'] as $cat)
            @foreach($cat['products'] as $k =>  $p)
                <div class="column is-3 is-flex product-col hide">
                    <input type="hidden" name="product_search_data" class="product_search_data" value="{{ $p['overview'] }}">
                    <div class="tile scrollto-{{ $p['code'] }}">
                        <h1 class="name">
                            <a href="{{ route('single-product',['product_code'=>$p['code']]) }}">
                            {{ $cat['name'] }}
                            </a>
                        </h1>
                        <h1 class="name_type">{{ $p['name_type'] }}&nbsp;</h1>
                        <div class="img-container">
                            @if ($p['main-image'])

                            <a href="{{ route('single-product',['product_code'=>$p['code']]) }}">
                                <img 
                                src="{{ env('APP_URL').'/files/products/'.$p['public_id'].'/images/thumbnails/'.$p['main-image']['type'].'/'.$p['main-image']['filename'].'.'.$p['main-image']['ext_thumb'].'?u='.env('APP_ASSET_TIMESTAMP') }}" 
                                alt="Vcare Earth {{ $p['main-image']['title'] }}">
                            </a>
                            @else
                            <a href="{{ route('single-product',['product_code'=>$p['code']]) }}">
                                <div class="img-placeholder-container">
                                    <img class="img-placeholder" alt="Vcare Earth" src="/files/logos/vcareplaceholder.svg?u={{ env('APP_ASSET_TIMESTAMP') }}"/>
                                    <h1 class="subtitle has-text-centered coming-soon-text">Coming Soon</h1>
                                </div>
                            </a>
                            @endif
                        </div>


                        <hr>
                        <a href="{{ route('single-product',['product_code'=>$p['code']]) }}">
                            {{ $p['name'] }} 
                            <span class="ml-1"> - {{ $p['name_code'] }}</span>
                            <i class="fas fa-chevron-right ml-1" style="float: right;"></i>
                        </a>
                    </div>
                </div>
            @endforeach
        @endforeach
    @endforeach

            
    <div class="column is-12 noresults">
        <h1 class="subtitle px-6 py-6">No results...</h1>
    </div>     

    </div> {{-- <div class="columns is-multiline"> --}}


    </div>



</div>
</div>
</section>





@endsection
