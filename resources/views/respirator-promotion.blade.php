@extends('layout.main') 

@section('head-end')
<link rel="stylesheet" href="{{ mix('css/respirator-promotion-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}" />
<script src="{{ mix('js/respirator-promotion-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection 



@section('head-title')
<title>Respirator Promotion | Vcare.earth</title>
@endsection




@section('content')


@section('seo-meta-title')
<?php 
    $meta_title = "VCare.Earth Commitment to Care - Respirator Promotion" ; 
    $meta_desc = "Masks protect you against solid dust particles, fibres, microorganisms, mists and aerosols where work environments often conceal hazards such as viruses.";
?>    
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 


<section class="section">
    <div class="container">
        <div class="columns is-flex-end">
            <div class="column is-8 title-column">
                <h1 class="title is-1 mb-5">VCARE RESPIRATOR PROMOTION</h1>
                <p class="is-size-5">
                    Masks protect you against solid dust particles, fibres, microorganisms, mists and aerosols (suspended droplets and dust). Work environments often conceal hazards such as viruses (Covid-19) that are not immediately recognised. Such a hazard can cause infection spread, permanent damage to your health, and in the worst case even be life-threatening.
                </p>
            </div>
            {{-- <div class="column has-text-centered follow-cause"></div> --}}
        </div>
    </div>
</section>


<section class="section ">
    <div class="container">
        <div class="columns is-centered">
            <div class="column">
                <h1 class="subtitle color2">Why a wear a Protective Mask?</h1>
                <p>It is important to get the maximum possible protection for your respiratory passages against the viral infections (Covid-19) to say the least. Protect yourself and protect people around you.</p>
            </div>
            <div class="column">
                <h1 class="subtitle color2">What is FFP?</h1>
                <p>Masks marked with the code FFP stands for ‘filtering facepiece’. This shows what and how many particles of suspended dust, mist, fibres, bacteria, are filtered.</p>
            </div>
        </div>
        <div class="columns is-centered">
            <div class="column is-6 has-text-centered-desktop">
                <p class="mt-6 mb-4 my-3-mobile">Three classes of FFP: FFP1, FFP2 and FFP3; higher the number, better the protection. Let’s COMPARE, FFP1, FFP2, FFP3 & the often term used as Mask.</p>
            </div>            
        </div>
    </div>
</section>


<section class="section respirator-comparison">
    <div class="container">
        {{-- <h4 class="title is-4 mb-6">RESPIRATOR COMPARISON</h4> --}}
        <div class="columns">
            <div class="column">
                <div class="card">
                    <div class="card-content">


                        <table class="table is-fullwidth is-hoverable hide-on-mobile">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>FFP1</th>
                                    <th>FFP2</th>
                                    <th>FFP3</th>
                                    <th>MASK</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($maskTypeTable as $row)
                                <tr>
                                    <th><h1 class="title is-6 whitespace-nowrap">{{ $row['title'] }}</h1></th>
                                    <td><p class="">{{ $row['ffp1'] }}</p></td>
                                    <td><p class="">{{ $row['ffp2'] }}</p></td>
                                    <td><p class="">{{ $row['ffp3'] }}</p></td>
                                    <td><p class="">{{ $row['mask'] }}</p></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>


                        <div class="hide-on-desktop">
                            <?php 
                                $data_mobile = ['ffp1','ffp2','ffp3','mask']    
                            ?>
                                        
                            @foreach ($data_mobile as $dm)

                            <table class="table is-fullwidth is-hoverable mb-6">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th> <h1 class="title is-4 is-size-4">{{ strtoupper($dm) }}</h1> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($maskTypeTable as $row)
                                    <tr>
                                        <th>{{ $row['title'] }}</th>
                                        <td><p class="">{{ $row[$dm] }}</p></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @endforeach
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

        {{-- <div class="columns is-centered mt-6">
            <div class="column is-narrow has-text-centered-mobile">
                <a class="button is-outlined vcare-btn is-medium" target="_blank" href="/vcare-detailed-comparison-for-masks-pdf">
                    <span>Detailed Comparison</span>
                    <span class="icon">
                        <i class="fas fa-chevron-right" aria-hidden="true"></i>
                    </span>
                </a>
            </div>
        </div> --}}
    </div>
</section>



<section class="section ">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-6 has-text-centered-desktop">
                <p class="mt-6 mb-6 my-3-mobile">
                    A technical difference between a “mask” and a “respirator”? Day to day we often say mask, when referring to what are technically called respirators.    
                </p>
            </div>            
        </div>
        <div class="columns is-centered">
            <div class="column">
                <h1 class="subtitle color2">Uses for Masks:</h1>
                <p>
                    Masks are loose fitting, covering the nose and mouth. Designed for one way protection, to capture bodily fluid leaving the wearer. I.e. Worn during surgery to prevent coughing, sneezing, etc on the vulnerable patient. 
                    <br>
                    Contrary to belief, masks are NOT designed to protect the wearer. Vast majority of masks do not have a safety rating assigned to them, i.e. NIOSH / EN.
                </p>
            </div>
            <div class="column">
                <h1 class="subtitle color2">Uses for Respirators:</h1>
                <p>
                    Respirators are tight fitting masks, designed to create a facial seal. Non-valved respirators provide good two way protection, filtering both inflow and outflow of air. 
                    <br>
                    Designed to protect the wearer (If worn properly), up to the safety rating of the mask Available as disposable, half face or full face.</p>
            </div>
        </div>
        <div class="columns is-centered">
            <div class="column is-6 has-text-centered-desktop">
                <p class="mt-6 mb-6 my-3-mobile">
                    Whilst surgical style masks are not redundant by any means, they aren’t designed to protect the wearer, whilst respirators are; educate the Buyer when necessary.
                </p>
            </div>            
        </div>                
    </div>
</section>










<section class="section ">
    <div class="container">
        <h1 class="title is-5 has-text-centered color2"
        style="line-height: 2rem;">
            VCARE FFP2/FFP3 <br>
            RESPIRATORS & RECOMMENDED USAGE
        </h1>
        <div class="columns is-centered">
            <div class="column">
            <div class="mask-table">
                
                <div class="hide-on-mobile">
                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth has-text-centered mt-4 ">
                    <thead>
                        <tr class="">
                            <th><h1 class="title my-5 is-6">MASK REFERENCE NO.</h1></th>
                            <th><h1 class="title my-5 is-6">PRODUCT TYPE</h1></th>
                            <th><h1 class="title my-5 is-6">WEARING STYLE</h1></th>
                            <th><h1 class="title my-5 is-6">USAGE</h1></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th class="py-5">FFP2</th>
                            <td><p></p></td>
                            <td><p></p></td>
                            <td><p></p></td>
                        </tr>
                        @foreach ($maskTable['ffp2'] as $row)
                        <tr>
                            <th>
                                <div class="mask-left-title">
                                    <a class="datasheet"
                                    target="_blank"
                                    href="{{ env('APP_URL').'/datasheet/'.$row['code'].'?u='.env('APP_ASSET_TIMESTAMP') }}"
                                    title="{{ $row['mask-reference-no'] }} Datasheets">
                                    <span class="icon">
                                        <i class="fas fa-file-download"></i>
                                    </span>
                                    </a> 

                                    <div class="mask-image">
                                        <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                        target="_blank">
                                            <img src="/files/respirator-comparison/table-images/{{ $row['mask-reference-no-image'] }}" 
                                            {{-- <img src="{{ $row['main-image'] }}"  --}}
                                            alt="VCare.Earth FFP2 Respirator {{ $row['mask-reference-no'] }}">
                                        </a>                                       
                                    </div>
                                    <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                    target="_blank">
                                        <h1 class="subtitle is-6">{{ $row['mask-reference-no'] }}</h1>
                                    </a>
                                </div>
                            </th>
                            <td><p>{{ $row['product-type'] }}</p></td>
                            <td><p>{{ $row['wearing-style'] }}</p></td>
                            <td><p>{{ $row['usage'] }}</p></td>
                        </tr>
                        @endforeach

                        <tr>
                            <th class="py-5">FFP3</th>
                            <td><p></p></td>
                            <td><p></p></td>
                            <td><p></p></td>
                        </tr>
                        @foreach ($maskTable['ffp3'] as $row)
                        <tr>
                            <th>
                                <div class="mask-left-title">
                                    <a class="datasheet"
                                    target="_blank"
                                    href="{{ env('APP_URL').'/datasheet/'.$row['code'].'?u='.env('APP_ASSET_TIMESTAMP') }}"
                                    title="{{ $row['mask-reference-no'] }} Datasheets">
                                    <span class="icon">
                                        <i class="fas fa-file-download"></i>
                                    </span>
                                    </a> 

                                    <div class="mask-image">
                                        <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                        target="_blank">
                                        <img src="/files/respirator-comparison/table-images/{{ $row['mask-reference-no-image'] }}" 
                                        {{-- <img src="{{ $row['main-image'] }}"  --}}
                                        alt="VCare.Earth FFP2 Respirator {{ $row['mask-reference-no'] }}">

                                        </a>
                                    </div>
                                    <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                    target="_blank">
                                    <h1 class="subtitle is-6">{{ $row['mask-reference-no'] }}</h1>
                                    </a>
                                </div>
                            </th>
                            <td><p>{{ $row['product-type'] }}</p></td>
                            <td><p>{{ $row['wearing-style'] }}</p></td>
                            <td><p>{{ $row['usage'] }}</p></td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                </div>

                <div class="hide-on-desktop">
                    <?php 
                        $data_mobile = ['product-type','wearing-style','usage'];
                    ?>
    
    
                    <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth has-text-centered mt-4 ">
                        <thead>
                            <tr class="">
                                <th><h1 class="title my-5 is-5">FFP2</h1></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($maskTable['ffp2'] as $row)
                            <tr>
                                <th>
                                    <div class="mask-left-title">
                                        <a class="datasheet"
                                        target="_blank"
                                        href="{{ env('APP_URL').'/datasheet/'.$row['code'].'?u='.env('APP_ASSET_TIMESTAMP') }}"
                                        title="{{ $row['mask-reference-no'] }} Datasheets">
                                        <span class="icon">
                                            <i class="fas fa-file-download"></i>
                                        </span>
                                        </a>

                                        <div class="mask-image">
                                            <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                            target="_blank">
                                            <img src="/files/respirator-comparison/table-images/{{ $row['mask-reference-no-image'] }}" 
                                            {{-- <img src="{{ $row['main-image'] }}"  --}}
                                            alt="VCare.Earth FFP2 Respirator {{ $row['mask-reference-no'] }}">
                                            </a>
                                        </div>
                                        <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                        target="_blank">
                                        <h1 class="subtitle is-5">FFP2 {{ $row['mask-reference-no'] }}</h1>
                                        </a>
                                    </div>
                                    <div class="mask-content">
                                        <span class="subtitle is-5 is-size-7">PRODUCT TYPE:</span>
                                        <p class="mb-3">{{ $row['product-type'] }}</p>

                                        <span class="subtitle is-5 is-size-7">WEARING STYLE:</span>
                                        <p class="mb-3">{{ $row['wearing-style'] }}</p>

                                        <span class="subtitle is-5 is-size-7">USAGE:</span>
                                        <p class="mb-3">{{ $row['usage'] }}</p>

                                        {{-- <span class="subtitle is-5 is-size-7">DATASHEET:</span> --}}
                                        {{-- <a class="datasheet"
                                        target="_blank"
                                        href="{{ env('APP_URL').'/datasheet/'.$row['code'].'?u='.env('APP_ASSET_TIMESTAMP') }}"
                                        title="{{ $row['mask-reference-no'] }} Datasheets">
                                        DATASHEET
                                        <span class="icon">
                                            <i class="fas fa-file-download"></i>
                                        </span>
                                        </a> --}}

                                    </div>
                                </th>
                            </tr>
                            @endforeach
    
                        </tbody>
                    </table>
    
    
                    <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth has-text-centered mt-6 ">
                        <thead>
                            <tr class="">
                                <th><h1 class="title my-5 is-5">FFP3</h1></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($maskTable['ffp3'] as $row)
                            <tr>
                                <th>
                                    <div class="mask-left-title">
                                        <a class="datasheet"
                                        target="_blank"
                                        href="{{ env('APP_URL').'/datasheet/'.$row['code'].'?u='.env('APP_ASSET_TIMESTAMP') }}"
                                        title="{{ $row['mask-reference-no'] }} Datasheets">
                                        <span class="icon">
                                            <i class="fas fa-file-download"></i>
                                        </span>
                                        </a>

                                        
                                        <div class="mask-image">
                                            <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                            target="_blank">
                                            <img src="/files/respirator-comparison/table-images/{{ $row['mask-reference-no-image'] }}" 
                                            {{-- <img src="{{ $row['main-image'] }}" --}}
                                            alt="VCare.Earth FFP2 Respirator {{ $row['mask-reference-no'] }}">
                                            </a>
                                        </div>
                                        <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                        target="_blank">
                                        <h1 class="subtitle is-5">FFP3 {{ $row['mask-reference-no'] }}</h1>
                                        </a>
                                    </div>
                                    <div class="mask-content">
                                        <span class="subtitle is-5 is-size-7">PRODUCT TYPE:</span>
                                        <p class="mb-3">{{ $row['product-type'] }}</p>

                                        <span class="subtitle is-5 is-size-7">WEARING STYLE:</span>
                                        <p class="mb-3">{{ $row['wearing-style'] }}</p>

                                        <span class="subtitle is-5 is-size-7">USAGE:</span>
                                        <p>{{ $row['usage'] }}</p>

                                    </div>
                                </th>
                            </tr>
                            @endforeach
    
                        </tbody>
                    </table>
    
                    </div>
    

            </div>
            </div>
        </div>
    </div>
</section>




<section class="section mb-6" id="summary">
    <div class="container">
        <h1 class="title is-5 has-text-centered color2">SUMMARY</h1>

        <div class="columns is-centered">
            <div class="column is-6">
                <ul>
                    <li>Always protect yourself as well as possible. Choose the appropriate Protective Mask, to work not only work safely, but also better and more comfortably.</li>
                    <li>Dust mask with FFP1 score protects you against non-harmful particles.</li>
                    <li>Dust mask with FFP2 score protects you against irritating and harmful particles.</li>
                    <li>Dust mask with FFP3 score protects you against toxic and harmful particles.</li>
                    <li>Only use a dust mask once (NR), unless indicated otherwise.</li>
                    <li>Dust masks with the indications V and/or D give higher respiratory comfort.</li>
                    <li>Only choose dust masks that meet the EN 149 standard.</li>
                    <li>Take the possible risks into account and use maximum protection.</li>
                    <li>Always work comfortably and safely.</li>
                </ul>
            </div>            
        </div>
    </div>
</section>

@endsection
