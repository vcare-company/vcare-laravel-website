@extends('layout.main')



@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('sample') }}"  
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/sample-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/sample-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
<script src="https://www.google.com/recaptcha/api.js?render={{ env('CAPTCHA_KEY') }}"></script>
@endsection


@section('head-title')
<title>Samples | Vcare.earth</title>
@endsection



@section('seo-meta-title')
<?php 
    $meta_title = "VCare.Earth Commitment to Care - Sample" ; 
    $meta_desc = "Request your free sample from VCare.Earth today. Provide your contact details and your free sample package will be dispatched within 48 hours.";
?>    
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 



@section('body-script')
    <script>
            grecaptcha.ready(function() {
            grecaptcha.execute('{{ env('CAPTCHA_KEY') }}', {action: 'submit'}).then(function(token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                    recaptchaResponse.value = token;
            });
            });
    </script>
    @if(\Session::has('success'))
    <script>
        Swal.fire({
            icon: 'success',
            title: 'Thank you for your Interest',
            text: "{!! session('success') !!}",
            showConfirmButton: true,
        })
    </script>
    @elseif(\Session::has('danger'))
    <script>
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: "{!! session('danger') !!}",
        })
    </script>
    @endif
@endsection





@section('content')
<section id="vcare-sample" class="section">
    <div class="container is-fluid" style=" background-image: url({{ env('APP_URL') }}/files/sample/banner.jpg);">
        <div class="container content">
            <div class="columns">
                <div class="column column-one">
                    <div class="is-size-5 is-size-6-mobile has-vcare-text vcare-head">TRY OUR PRODUCTS.</div>
                    <p class="is-size-3 is-size-3-mobile vcare-big-text ">
                        Request your free sample from VCare.Earth today.
                    </p>
                </div>
                <div class="column is-8"> 
                    <img src="{{ env('APP_URL') }}/files/sample/products.png" width="100%"/>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="vcare-sample-contact" class="section">
    <div class="container">
        <p class="has-text-centered is-size-4 is-size-5-mobile px-6">
            Please select your preferred product(s)
            from the list below, provide your contact details and your free sample package 
            will be dispatched within 48 hours.  
        </p>
    </div>
    <div class="container mt-5">
        <form method="post" action="/contact-us">
            {{ csrf_field() }}
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <p class="control is-expanded">
                            <input class="input" type="text" name="name" placeholder="Name" required>
                        </p>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <p class="control is-expanded">
                            <input class="input" type="email" name="email" placeholder="Email" required>
                        </p>
                    </div>
                </div>
            </div>

                <div class="columns">
                    <div class="column">
                        <div class="field has-addons has-addons-centered">
                            <p class="control">
                                <span class="select">
                                    <select name="number_type" required>
                                        <option value="" disabled selected>Select</option>
                                        <option value="Mobile">Mobile</option>
                                        <option value="Landline">Landline</option>
                                    </select>
                                </span>
                            </p>
                            <p class="control">
                                <input class="input" type="number" name="number_code" placeholder="Country code" required>
                            </p>
                            <p class="control is-expanded">
                                <input class="input" type="tel" name="number" placeholder="Number" required>
                            </p>
                        </div>
                        <div class="control is-expanded whatsapp-available">
                            <div class="field is-horizontal">
                                <label class="label has-text-weight-light">Available on Whatsapp? &nbsp;</label>
                                <div class="control">
                                    <label class="radio">
                                        <input type="radio" name="whatsapp" value="Yes"> 
                                        Yes
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="whatsapp" value="No">
                                        No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field is-horizontal">
                            <div class="field-body">
                                <div class="field">
                                    <p class="control is-expanded">
                                        <input class="input" type="text" name="company" placeholder="Company name" required>
                                    </p>
                                </div>
                                <div class="field">
                                    <p class="control is-expanded">
                                        <input class="input" type="text" name="company_website" placeholder="Company website" required>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <div class="field-body">
                                <div class="field">
                                    <p class="control is-expanded">
                                        <textarea class="textarea" name="address" placeholder="Enter your full delivery address including zip/postal code and country" required></textarea>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="column">
                                <div class="field is-horizontal">
                                    <label class="label has-text-weight-light">Use the same contact number for your delivery? &nbsp;</label>
                                    <div class="control">
                                        <label class="radio">
                                            <input type="radio" name="same_courier_number" value="Yes" required> 
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="same_courier_number" value="No" required>
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="column courier-number">
                                <div class="field  has-addons has-addons-centered">
                                    <p class="control">
                                        <input class="input" type="number" name="courier_number_code" placeholder="Country Code">
                                    </p>
                                    <p class="control is-expanded">
                                        <input class="input" type="number" name="courier_number" placeholder="Mention your number for courier to contact you">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="column">
                        <div class="field mt-5">
                            <div class="field-label has-text-left">
                                <label class="label has-text-weight-light">What is your business type?</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <div class="select is-fullwidth">
                                            <select name="who">
                                                <option value="" selected disabled >Select an Option</option>
                                                <option value="Distributor">Distributor</option>
                                                <option value="Wholesaler">Wholesaler</option>
                                                <option value="Retailer">Retailer</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field mt-2">
                                <p class="control is-expanded who-other-option">
                                    <input class="input" type="text" name="who_other_option" placeholder="Mention your other option">
                                </p>
                            </div>
                        </div>
                        <div class="field mt-5">
                            <div class="field-label has-text-left">
                                <label class="label has-text-weight-light">Where did you hear about us?</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <div class="select is-fullwidth">
                                            <select name="how">
                                                <option value="" selected disabled >Select an Option</option>
                                                <option value="Social Media">Social Media</option>
                                                <option value="Amazon">Amazon</option>
                                                <option value="Google Ads">Google Ads</option>
                                                <option value="Online Search">Organic Online Search</option>
                                                <option value="Referral">Referral</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field mt-2">
                                <p class="control is-expanded referral-option">
                                    <input class="input" type="text" name="referral_option" placeholder="Mention your referral option">
                                </p>
                            </div>
                            <div class="field mt-2">
                                <p class="control is-expanded how-other-option">
                                    <input class="input" type="text" name="how_other_option" placeholder="Mention your other option">
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field mt-5">
                            <div class="field-label has-text-left">
                                <label class="label has-text-weight-light">Are you already in the PPE or Medical Equipment industry?</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <div class="select is-fullwidth">
                                            <select name="ppe">
                                                <option value="" selected disabled >Select an Option</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="b2b-contact">
                    <div class="field mt-5">
                        <div class="field-label has-text-left">
                            <label class="label has-text-weight-light">Select Products(s)?
                                <span class="vcare-button-outlined add-product button is-small" id="add-product">
                                    <i class="fas fa-plus"></i>&nbsp;Add Product
                                </span>
                            </label>
                        </div>
                        <div class="columns is-multiline mt-1" id="cloned-div">
                            <div class="column is-4" id="clone-div">
                                <div class="field-body">
                                    <div class="field">
                                        <div class="control">
                                            <div class="select is-fullwidth">
                                                <select name="" class="b2b_select" data-select="1">
                                                    <option value="" selected disabled>Select a Product</option>
                                                    @foreach ($categories as $categ)
                                                    @if (count($categ['children']))

                                                    <option disabled class="has-text-weight-bold "> </option>
                                                    <option disabled class="has-text-weight-bold "> {!! $categ['name'] !!} </option>
                                            
                                                    @foreach ($categ['children'] as $cat)
                                                    @if (count($cat['products']))
                                                    <option disabled class="has-text-weight-semibold"><small> {{ $cat['name'] }}</small></option>
                                                    @foreach($cat['products'] as $k =>  $p)
                                                    <option value="{{ $p['name'] }}_b2b" data-value="{{ $p['name'] }}" class="has-text-weight-light">
                                                            @if ($p['name'] && $p['name_type'])
                                                            {{ $p['name'] }}  - {{ $p['name_code'] }} ({{ $p['name_code_group'] }} - {{ $p['name_type'] }}) 
                                                            @elseif ($p['name'])
                                                            {{ $p['name'] }}  - {{ $p['name_code'] }} ({{ $p['name_code_group'] }}) 
                                                            @else
                                                            {{ $p['name_type'] }} *Coming Soon
                                                            @endif 
                                                        </option>
                                                    @endforeach
                                                    @else
                                                    @endif
                                                    @endforeach
                                                    @else
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field mt-2 b2b_quantity" data-quantity="1" id="b2b_quantity">
                                    <div class="control is-expanded">
                                        <input class="input" type="number" name="" placeholder="Quantity">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field mt-5">
                    <div class="field-body">
                        <div class="field">
                            <p class="control is-expanded">
                                <input class="input" type="text" name="subject" placeholder="Subject" required>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="field mt-5">
                    <div class="field-body">
                        <div class="field">
                            <p class="control is-expanded">
                                <textarea class="textarea" name="message" placeholder="Your message" required></textarea>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="field has-text-centered mt-5">
                    <div class="control">
                        <input type="hidden" value="" name="recaptcha_response" id="recaptchaResponse">
                        <input type="hidden" value="sample_request" name="interested_in" class="interested-text">
                        <button class="button vcare-button-outlined" type="submit">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
    </div>




</section>
@endsection