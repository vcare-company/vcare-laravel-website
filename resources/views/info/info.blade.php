@extends('layout.main') 

@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('more-info') }}"
        },
        @endforeach
    ]

}
</script>


<link rel="stylesheet" href="{{ mix('css/info-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}" />
<script src="{{ mix('js/info-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection 

@section('head-title')
<title>More Product Information | Vcare.earth</title>
@endsection


<?php 
    $meta_title = "VCare.Earth Commitment to Care - More Product Information" ; 
    $meta_desc = "Established as the pandemic took hold of the world, VCare.Earth is helping to protect and safeguard lives today, during this unprecedented time.";
?>    
@section('seo-meta-title')
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 

@section('seo-meta-image')
<meta property="og:image" content="{{ env('APP_URL').'/files/logos/meta-image.jpg' }}" />
<meta property="og:image:secure_url" content="{{ env('APP_URL').'/files/logos/meta-image.jpg' }}" />
@endsection




@section('content')


<section class="section">
    <div class="container">
        <div class="columns is-flex-end">
            <div class="column title-column">
                <h2 class="subtitle">
                    
                </h2>
                <h1 class="title is-1 mb-0">More Product Information</h1>
            </div>
        </div>
        <div class="columns is-flex-end">
            <div class="column is-7 pt-0 title-column">
                <p>
                    
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="columns is-vcentered">
            <div class="column is-4">
                <div class="blog-image">
                    <?php 
                        $thumbnail = '/files/info/mask-img-3.png'
                    ?>
                    @if(File::exists(public_path().$thumbnail)) 
                    <img src="{{ $thumbnail }}" alt="VCare.Earth - Helping to protect and safeguard lives today, during this unprecedented time." />
                    @else
                    <img src="/files/logos/meta-image.jpg" alt="VCare.Earth - Helping to protect and safeguard lives today, during this unprecedented time." />
                    @endif
                </div>
            </div>
            <div class="column">
                <div class="blog-heading">
                    <p class="">PRODUCT INFORMATION</p>
                </div>
                <div class="blog-title my-3">
                    <h1 class="subtitle my-4">
                        EN 149 FFP1
                    </h1>
                    <p>
                        The EN149 FFP1 respirators are also known as half masks. These masks are used to protect the wearer from certain particles. It falls in line with European standards. The mask has to be used after a thorough lab and practical testing. Most of the time, half masks are used for escape reasons. According to the EN149 FFP1 standards, the face piece has to substantially or fully cover the mask. 
                    </p>
                </div>

                <div class="links-and-social">
                    <div class="blog-buttons">
                        <button class="button vcare-button-outlined mr-5">
                            <a href="{{ route('en149-ffp1') }}"> 
                                <span class="vcare-button-text">READ MORE</span>
                                <i class="fas fa-chevron-right"></i> 
                            </a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        <hr>
        <div class="columns is-vcentered">
            <div class="column is-4">
                <div class="blog-image">
                    <?php 
                        $thumbnail = '/files/info/reusable-img.jpg'
                    ?>
                    @if(File::exists(public_path().$thumbnail)) 
                    <img src="{{ $thumbnail }}" alt="VCare.Earth - Helping to protect and safeguard lives today, during this unprecedented time." />
                    @else
                    <img src="/files/logos/meta-image.jpg" alt="VCare.Earth - Helping to protect and safeguard lives today, during this unprecedented time." />
                    @endif
                </div>
            </div>
            <div class="column">
                <div class="blog-heading">
                    <p class="">PRODUCT INFORMATION</p>
                </div>
                <div class="blog-title my-3">
                    <h1 class="subtitle my-4">
                        REUSABLE RESPIRATORS
                    </h1>
                    <p>
                        Healthcare professionals need to work with the right kind of protection. And, masks play an integral role in their day to day activities. One of the most commonly used types of respiratory protection would be the 3m n100 masks.
                    </p>
                </div>

                <div class="links-and-social">
                    <div class="blog-buttons">
                        <button class="button vcare-button-outlined mr-5">
                            <a href="{{ route('reusable-respirator') }}"> 
                                <span class="vcare-button-text">READ MORE</span>
                                <i class="fas fa-chevron-right"></i> 
                            </a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        <hr>
        <div class="columns is-vcentered">
            <div class="column is-4">
                <div class="blog-image">
                    <?php 
                        $thumbnail = '/files/info/disposable-img.jpg'
                    ?>
                    @if(File::exists(public_path().$thumbnail)) 
                    <img src="{{ $thumbnail }}" alt="VCare.Earth - Helping to protect and safeguard lives today, during this unprecedented time." />
                    @else
                    <img src="/files/logos/meta-image.jpg" alt="VCare.Earth - Helping to protect and safeguard lives today, during this unprecedented time." />
                    @endif
                </div>
            </div>
            <div class="column">
                <div class="blog-heading">
                    <p class="">PRODUCT INFORMATION</p>
                </div>
                <div class="blog-title my-3">
                    <h1 class="subtitle my-4">
                        DISPOSABLE RESPIRATORS
                    </h1>
                    <p>
                        The overall demand for 3m 100 has increased drastically in the past few months. These are essential healthcare masks which are also known as the particulate respirator. 
                    </p>
                </div>

                <div class="links-and-social">
                    <div class="blog-buttons">
                        <button class="button vcare-button-outlined mr-5">
                            <a href="{{ route('disposable-respirator') }}"> 
                                <span class="vcare-button-text">READ MORE</span>
                                <i class="fas fa-chevron-right"></i> 
                            </a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        <hr>
        
    </div>
</section>

@endsection
