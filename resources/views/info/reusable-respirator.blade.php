@extends('layout.main') 

@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('reusable-respirator') }}" 
        },
        @endforeach
    ]

}
</script>

<link rel="stylesheet" href="{{ mix('css/info-single-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}" />
<script src="{{ mix('js/info-single-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection 

<?php 
    $meta_title = 'Reusable Respirator 6503QL 3M | 3M 6503QL Mask | VCare.Earth';
    $meta_desc = 'Know about the 3M reusable respirator 6503ql atVCare.Earth. They offer a wide variety of high-quality and reusable 6503QL masks at the lowest possible price. Visit us today!';
?>   

@section('head-title')
<title>Vcare.earth - Reusable Respirators</title>
@endsection

 
@section('seo-meta-title')
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 


<?php
$meta_image = '/files/info/images/thumbnail';

if(!File::exists(public_path().$meta_image)) {
    $meta_image = '/files/logos/meta-image.jpg';
} 

?>
@section('seo-meta-image')
<meta property="og:image" content="{{ env('APP_URL'). $meta_image }}" />
<meta property="og:image:secure_url" content="{{ env('APP_URL'). $meta_image }}" />
@endsection




@section('content')


<section class="section">
    <div class="container">
        <div class="columns">
            <div class="column title-column">
                <div class="blog-heading1">
                    <h1 class="title is-1 is-size-3-mobile mb-0">
                        Resuable Respirators
                    </h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section blue-vcare-bg" 
style="background-image: url({{ env('APP_URL') }}/files/info/main-image.png);">
	<div class="container">
		<div class="columns is-vcentered is-centered">
			<div class="column is-8">
				<h2 class="subtitle is-2 has-text-centered has-text-white">
				    Find the best Resuable Respirators
				</h2>
			</div>
		</div>
	</div>
</section>

<section class="section">
    <div class="container">
        <p class="is-size-4">
            Healthcare professionals need to work with the right kind of protection. And, masks play an integral role in their day to day activities. One of the most commonly used types of respiratory protection would be the 3m n100 masks.
        </p>
        <br>
        <p>
            These are useful and extremely efficient masks in keeping at least 99.97% of airborne pathogens away from the wearer. When compared to the standard N95 masks, the N100 3m <a href="https://vcare.earth/disposable-respirator" style="color:black; font-weight:300"> disposable respirator</a> comes with a wide range of perks and features. At VCare, we ensure that all our clients are given access to the finest disposable respirator.
        </p>
    </div>
</section>
<section class="">
	<div class="container mid-vcare-bg" style="background-image: url({{ env('APP_URL') }}/files/info/reusable-img.jpg);">
		<div class="columns is-vcentered is-centered">
			<div class="column is-8">
			</div>
		</div>
	</div>
</section>
<section class="section">
    <div class="container">
        <p>
            <b>
                Features of the 3M N100 masks 
            </b>
        </p>
        <br>
        <p>
            To begin with, the 3m <a href="https://vcare.earth/products/personal-protective-equipment-ppe/ffp3r" style="color:black; font-weight:300"> face mask FFP3</a> are designed using ultra-lightweight material and is non-woven This ensures that the wearer does not face any kind of inconvenience by wearing the mask. It offers high levels of stability, durability and comfort too. The face seal in our 3M masks is extremely soft. This means you don’t need to worry about permanent scars on your face. Many times, healthcare professionals face issues when their masks don’t latch to their face properly. This can be avoided by picking the right kind of masks, with perfect fitting. The quick latch mechanism in our masks will let you drop the mask, without worrying about the head straps.   
        </p>
    </div>
</section>
<section class="">
	<div class="container mid-vcare-bg" style="background-image: url({{ env('APP_URL') }}/files/info/reusable-img-1.jpg);">
		<div class="columns is-vcentered is-centered">
			<div class="column is-8">
			</div>
		</div>
	</div>
</section>
<section class="section">
    <div class="container">
        <h2>
            <b>
                3M Reusable Respirators | 3M N100 | 3M 5200
            </b>
        </h2>
        <br>
        <p>
            Another important feature that gives our masks an edge in the industry would be the 3M Coop Flow protocol. This is a state of the art technology that ensures the comfort of the wearer. The wearer will not feel cold, warm or humid because of the masks. This way, breathing becomes extremely simple and more relaxing for the wearer. The valve in the mask makes a big difference for the wearer’s overall experience.    
        </p>
        <br>
        <p>
            Finally, the mask features a low profile face piece. The half face piece allows the wearer to get a better overview of what is around and in front of them. Your sight will never be obstructed because of the mask!  
        </p>
    </div>
</section>

@endsection
