@extends('layout.main') 

@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('en149-ffp1') }}"
        },
        @endforeach
    ]

}
</script>

<link rel="stylesheet" href="{{ mix('css/info-single-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}" />
<script src="{{ mix('js/info-single-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection 

<?php 
    $meta_title = 'EN149FFP1 | EN 149 FFP1 Respirators | VCare.Earth';
    $meta_desc = 'Get the vital details of EN149FFP1 respirators at VCare.Earth and know about the EN 149 FFP1 masks classification based on their filtering capacities. Visit us now!';
?>   

@section('head-title')
<title>Vcare.earth - EN 149 FFP1</title>
@endsection

 
@section('seo-meta-title')
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 


<?php
$meta_image = '/files/info/images/thumbnail';

if(!File::exists(public_path().$meta_image)) {
    $meta_image = '/files/logos/meta-image.jpg';
} 

?>
@section('seo-meta-image')
<meta property="og:image" content="{{ env('APP_URL'). $meta_image }}" />
<meta property="og:image:secure_url" content="{{ env('APP_URL'). $meta_image }}" />
@endsection




@section('content')


<section class="section">
    <div class="container">
        <div class="columns">
            <div class="column title-column">
                <div class="blog-heading1">
                    <h1 class="title is-1 is-size-3-mobile mb-0">
                        EN 149 FFP1 Standard Respirators Devices
                    </h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section blue-vcare-bg" 
style="background-image: url({{ env('APP_URL') }}/files/info/main-image.png);">
	<div class="container">
		<div class="columns is-vcentered is-centered">
			<div class="column is-8">
				<h2 class="subtitle is-2 has-text-centered has-text-white">
				    Details of EN 149 FFP1 Standard 
				</h2>
			</div>
		</div>
	</div>
</section>

<section class="section">
    <div class="container">
        <p class="is-size-4">
            The EN149 ffp1 respirators are also known as half masks. These masks are used to protect the wearer from certain particles.
        </p>
        <br>
        <p>
            It falls in line with European standards. The mask has to be used after a thorough lab and practical testing. Most of the time, half masks are used for escape reasons. According to the EN149 ffp1 standards, the face piece has to substantially or fully cover the mask. The device doesn’t allow its wearer to separate the filters from the mask. This means the EN149 ffp1 standards need the filters to be attached to the mask permanently.
        </p>
    </div>
</section>
<section class="">
	<div class="container mid-vcare-bg" style="background-image: url({{ env('APP_URL') }}/files/info/mask-img.png);">
		<div class="columns is-vcentered is-centered">
			<div class="column is-8">
			</div>
		</div>
	</div>
</section>
<section class="section">
    <div class="container">
        <p>
            <a href="{{ route('home') }}" style="color:black; font-weight:300">EN149 ffp1 product suppliers</a> need to be aware of multiple standards. This includes the 1991 standard, which further classified as FFP1S, FFP2S, FFP2SL, FFP3S, and FFP3SL. It is crucial for the supplier to understand the difference and requirements imposed by these classifications. In fact, this is one of the main areas that VCare specializes in. We have taken every measure required to help clients receive products that meet all requirements of a specific en149 ffp1 standard. Furthermore, we understand that the EN149 ffp1 standards are refreshed periodically. Now, industries follow the 2001 standard that is further classified into three categories. Technically, the EN149 ffp1 respirators (2001 standard) are designed to filter solids, oil, and aerosol-based particles. This is where the filter enjoys an edge from the 1991 standard. The 1991 standard expects the filter to filter only solid particles whereas the 2001 standard takes care of oil and aerosol particles too.
        </p>
        <br>
        <p>
            Another important difference between the 1991 and 2001 standards is reusability. With the EN149 ffp1 2001 standard, the masks became absolutely reusable. You will notice the symbol “R” to indicate if the <a href="{{ route('reusable-respirator') }}" style="color:black; font-weight:300"> masks are reusable</a> or not. Also, you will see the symbol “NR”, if the masks cannot be reused.  
        </p>
        <br>
        <p>
            Another important difference between the 1991 and 2001 standards is reusability. With the EN149 ffp1 2001 standard, the masks became absolutely reusable. You will notice the symbol “R” to indicate if the masks are reusable or not. Also, you will see the symbol “NR”, if the masks cannot be reused.  
        </p>
    </div>
</section>
<section class="section">
    <div class="container">
        <div class="columns">
            <div class="column">
                <p class="is-size-3"><b>The technical aspects</b></p>
                <p class="pt-3">
                    On the whole, the EN149 ffp1 specification focuses on half masks. The filter penetration limit for inward leakage is as much as 95 L/min of airflow. More than 98 percent of airborne particles can be blocked by the en149 ffp1 filter masks. And, 80 percent of respirable particles can be blocked. 
                </p>
            </div>
            <div class="column has-text-centered">
                <img style="width:75%;"
                        alt="EN 149 FFP1 Respirators" 
                        src="{{ env('APP_URL') }}/files/info/mask-img-3.png?u={{ env('APP_ASSET_TIMESTAMP') }}"/>

            </div>
        </div>
    </div>
</section>
@endsection
