@extends('layout.main') 

@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('disposable-respirator') }}" 
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/info-single-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}" />
<script src="{{ mix('js/info-single-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection 

<?php 
    $meta_title = 'Disposable Respirator 3M | 3M Face Mask FFP3 | VCare.Earth';
    $meta_desc = 'Know about the 3M disposable respirator atVCare.Earth.Theyoffer a wide variety of high-quality 3M N100 face masksFFP3 at the lowest possible price. Visit us today!';
?>   

@section('head-title')
<title>Vcare.earth - Disposable Respirators</title>
@endsection

 
@section('seo-meta-title')
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 


<?php
$meta_image = '/files/info/images/thumbnail';

if(!File::exists(public_path().$meta_image)) {
    $meta_image = '/files/logos/meta-image.jpg';
} 

?>
@section('seo-meta-image')
<meta property="og:image" content="{{ env('APP_URL'). $meta_image }}" />
<meta property="og:image:secure_url" content="{{ env('APP_URL'). $meta_image }}" />
@endsection




@section('content')


<section class="section">
    <div class="container">
        <div class="columns">
            <div class="column title-column">
                <div class="blog-heading1">
                    <h1 class="title is-1 is-size-3-mobile mb-0">
                        Disposable Respirator
                    </h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section blue-vcare-bg" 
style="background-image: url({{ env('APP_URL') }}/files/info/main-image.png);">
	<div class="container">
		<div class="columns is-vcentered is-centered">
			<div class="column is-8">
				<h2 class="subtitle is-2 has-text-centered has-text-white">
				    Find the best Disposable Respirator
				</h2>
			</div>
		</div>
	</div>
</section>

<section class="section">
    <div class="container">
        <p class="is-size-4">
            The overall demand for 3m 100 has increased drastically in the past few months. These are essential healthcare masks which are also known as the particulate respirator.
        </p>
        <br>
        <p>
            The role of a 3m disposable respirator is to ensure that more than 99.97 percent of the microbes and infection -causing pathogens are kept away from the wearer. These masks are uniquely designed to keep oil- based particles away from the wearer. All the 3M N100 masks take into consideration the wearer's convenience. It comes with a soft inner layer, which makes it easy for the wearer to use the mask in a highly infection-prone environment. These masks are also designed with adjustable nose clips. Even if the wearer is carrying eyeglasses, the masks will not cause any inconvenience.
        </p>
    </div>
</section>
<section class="">
	<div class="container mid-vcare-bg" style="background-image: url({{ env('APP_URL') }}/files/info/disposable-img.jpg);">
		<div class="columns is-vcentered is-centered">
			<div class="column is-8">
			</div>
		</div>
	</div>
</section>
<section class="section">
    <div class="container">
        <p>
            The 3m <a href="https://vcare.earth/products/personal-protective-equipment-ppe/ffp3" style="color:black; font-weight:300"> face mask</a> ffp3 consists of microfiber which enables easy breathing. The fiber in our masks is electrostatically charged. Hence, the wearer will not face any difficulties in performing their day to day chores, or breathing. As mentioned previously, these masks are highly compatible with a range of eye gears too. They are highly recommended for professionals who tend to get in touch with people who are infected with diseases that can spread the in air. 
        </p>
        <br>
        <h2 class="subtitle">
           <b> 3M Disposable Respirators | 3M N100 | 3M 5200 </b>   
        </h2>
        <br>
        <p>
        If you go through the 3m 5200 respirator guide, you will come across details that describe how the mask handles humidity and heat. Even if the wearer tends to use the mask for extended hours - its effectiveness will not be impacted. This is mainly because N100 has been manufactured with durability and robustness in mind. In fact, the wearer is less likely to face any discomfort wearing this particulate respirator for hours.   
        </p>
    </div>
</section>

@endsection
