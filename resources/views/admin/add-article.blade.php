@extends('admin.main')



@section('head-end')
<link rel="stylesheet" href="{{ mix('css/admin/add-product-page.css') }}">
<script src="{{ mix('js/admin/add-product-page.js') }}"></script>
@endsection


@section('head-title')
<title>Admin ADD ARTICLE</title>
@endsection

@section('content')

<section class="section">
    <div class="hero-body">
        <div class="container">
            <div class="columns">
                <div class="column">

                    









                    <form action="{{ route('add_article_post') }}"  method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="box">
                            <h1 class="title">Add Article</h1>

                            

                            <div class="field mb-6">
                                <label class="label">Title</label>
                                <div class="control">
                                <textarea name="article_title" class="textarea" placeholder="Title"></textarea>
                                </div>
                            </div>

                            <div class="field mb-6">
                                <label class="label">Short Content</label>
                                <div class="control">
                                <textarea name="article_short_content" class="textarea" placeholder="VCare.Earth and The Ricky Rubio Foundation have signed a collaboration agreement to contribute to the improvement of public health and the containment of Covid.19, which has become the new threat of the pandemic."></textarea>
                                </div>
                            </div>

                            <div class="field mb-6">
                                <label class="label">HTML Content</label>
                                <div class="control">
                                <textarea name="article_html_content" class="textarea" placeholder='
<section class="section pt-0">
    <div class="container">
        <div class="columns is-centered is-vcentered">
            <div class="column"> </div>
        </div>
        </div>
</section>
                                '></textarea>
                                </div>
                            </div>

                            <div class="field mb-6">
                                <label class="label">Meta Title</label>
                                <div class="control">
                                <textarea name="article_meta_title" class="textarea" placeholder="VCare.Earth and The Ricky Rubio Foundation donate masks to the Red Cross to protect the most vulnerable."></textarea>
                                </div>
                            </div>

                            <div class="field mb-6">
                                <label class="label">Meta Desc</label>
                                <div class="control">
                                <textarea name="article_meta_desc" class="textarea" placeholder="VCare.Earth and The Ricky Rubio Foundation have signed a collaboration agreement to the improvement of public health and the containment of Covid-19."></textarea>
                                </div>
                            </div>

                            <div class="field mb-6">
                                <label class="label">Original URL</label>
                                <div class="control">
                                <textarea name="article_url" class="textarea" placeholder="https://therickyrubiofoundation.org/en/2020/08/10/vcareearth-y-the-ricky-rubio-foundation-donan-mascarillas-a-la-cruz-roja-para-proteger-a-los-mas-vulnerables/"></textarea>
                                </div>
                            </div>


                            

                        </div>
                        
                        <div class="field mb-6 is-grouped">
                            <div class="control">
                            <button type="submit" class="button is-link">Submit</button>
                            </div>
                            <div class="control">
                            <button class="button is-link is-light">Cancel</button>
                            </div>
                        </div>

                    </form>









                </div>
            </div>
        </div>
    </div>
</section>







@endsection
