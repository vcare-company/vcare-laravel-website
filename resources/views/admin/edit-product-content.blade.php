@extends('admin.main')



@section('head-end')
<link rel="stylesheet" href="{{ mix('css/admin/add-product-page.css') }}">
<script src="{{ mix('js/admin/add-product-page.js') }}"></script>

<script>
    let message = '{{ Session::get('message') }}';

    if(message) {
        alert(message);
    }
</script>
@endsection


@section('head-title')
<title>Admin Vcare.earth {{ $product['meta_title'] }}</title>
@endsection



@section('content')

<section class="section">
    <div class="hero-body">
        <div class="container">
            <div class="columns">
                <div class="column">

                    


                    <form action="{{ route('edit_product_content_post') }}"  method="POST" enctype="multipart/form-data">
                        @csrf
                    <div class="box">
                        <h1 class="title">Add Certificates</h1>

                        <div class="field">
                            {{-- <label class="label">Product Name</label> --}}
                            <div class="control">
                                <h1 class="title">{{ $product['category']['name'] }} - {{ $product['name_type'] }}</h1>
                                <h1 class="title">
                                    <a class="" target="_blank" 
                                    href="{{ route('single-product',['product_code'=>$product['code']]) }}">
                                    ({{ $product['id'] }}) {{ $product['name'] }} - {{ $product['name_code'] }}
                                    </a>                                         
                                </h1>
                                <h1 class="subtitle">{{ $product['code'] }}</h1>
                                <input type="hidden" name="product_id" value="{{ $product['id'] }}">
                                <input type="hidden" name="code" value="{{ $product['code'] }}">
                            </div>
                        </div>
                        



                        <div class="field is-horizontal">
                            <div class="field-body">
                                <div class="field">
                                    <label class="label">Overview</label>
                                    <div class="control">
                                    <textarea 
                                    style="height: 250px;"
                                    name="product_overview" 
                                    class="textarea mb-6" 
                                    placeholder="">{{ $product['overview'] }}</textarea>
                                    </div>                                    
                                </div>
                                <div class="field">
                                    <label class="label">Certifications</label>
                                    <div class="control">
                                    <textarea 
                                    style="height: 250px;"
                                    name="product_certifications" 
                                    class="textarea mb-6" 
                                    placeholder="">{{ $product['certifications'] }}</textarea>
                                    </div>                                    
                                </div>
                            </div>
                          </div>                        


                        <div class="field">
                            <label class="label">HTML Content</label>
                            <div class="control">
                            <textarea 
                            style="height: 500px;"
                            name="product_html" 
                            class="textarea mb-6" 
                            placeholder="">{{ $product['html_content'] }}</textarea>
                            </div>
                        </div>


                        <div class="field">
                            <label class="label">Meta Description</label>
                            <div class="control">
                            <textarea 
                            style="height: 100px;"
                            name="product_meta_desc" 
                            class="textarea mb-6" 
                            placeholder="">{{ $product['meta_desc'] }}</textarea>
                            </div>
                        </div>


                        <div class="field">
                            <label class="label">Meta Title</label>
                            <div class="control">
                            <textarea 
                            style="height: 100px;"
                            name="product_meta_title" 
                            class="textarea mb-6" 
                            placeholder="">{{ $product['meta_title'] }}</textarea>
                            </div>
                        </div>


                        <div class="field mb-6">
                            <label class="label">Product Name Code Group</label>
                            <div class="control">
                            <input name="product_name_code_group"   value="{{ $product['name_code_group'] }}" 
                            class="input" type="text" placeholder="KN95">
                            </div>
                        </div>

                        <div class="field mb-6">
                            <label class="label">Product Name Code</label>
                            <div class="control">
                            <input name="product_name_code"  value="{{ $product['name_code'] }}" 
                            class="input" type="text" placeholder="1A">
                            </div>
                        </div>

                        <div class="field mb-6">
                            <label class="label">Product Code</label>
                            <div class="control">
                            <input name="product_code" value="{{ $product['code'] }}" 
                            class="input" type="text" placeholder="kn95-lou">
                            </div>
                        </div>


                        <div class="field mb-6">
                            <label class="label">Product Name Type</label>
                            <div class="control">
                            <input name="product_name_type" value="{{ $product['name_type'] }}" 
                            class="input" type="text" placeholder="Foldable, Duck, Cup">
                            </div>
                        </div>
                        



                        <div class="field mb-6">
                            <label class="label">Product Order</label>
                            <div class="control">
                            <input name="product_order" value="{{ ($product['order'])?$product['order']:0 }}" 
                            class="input" type="text" placeholder="1, 2, 2.1, 2.12, 3">
                            </div>
                        </div>


                        <div class="field mb-6">
                            <label class="label">Product Certificate Password</label>
                            <div class="control">
                            <input name="product_certificates_password" value="" 
                            class="input" type="text" placeholder="certificate password">
                            </div>
                        </div>


                        <div class="field mb-6">
                            <label class="label">Product Pricelist Password</label>
                            <div class="control">
                            <input name="product_pricelist_password" value="" 
                            class="input" type="text" placeholder="pricelists password">
                            </div>
                        </div>


                        
                        <div class="field is-grouped fixed-right-bottom">
                            <div class="control">
                            <button type="submit" class="button is-link">Update Content</button>
                            </div>
                            <div class="control">
                            <button class="button is-link is-light">Cancel</button>
                            </div>
                        </div>

                        
                    </div>
                    </form>










                </div>
            </div>
        </div>
    </div>
</section>







@endsection
