@extends('admin.main')



@section('head-end')
<link rel="stylesheet" href="{{ mix('css/admin/add-product-page.css') }}">
<script src="{{ mix('js/admin/add-product-page.js') }}"></script>
@endsection


@section('head-title')
<title>Vcare.earth EDIT PRODUCT</title>
@endsection

@section('content')

<section class="section">
    <div class="hero-body">
        <div class="container">
            <div class="columns">
                <div class="column">

                    








                    <h1 class="title">
                        <a class="button is-success" target="_blank" 
                        href="{{ route('create_product') }}">
                            Create Product
                        </a>                                         
                    </h1>





                    <div class="box">
                        <h1 class="title">Edit Product</h1>
    



                        @foreach ($products as $prod)
                        <div class="columns is-narrow is-vcentered">
                            <div class="column is-3">
                                <h1 class="subtitle">{{ $prod['category']['name'] }} <span style="box-title">{{ $prod['name_type'] }}</span></h1>
                                <h1 class="title">
                                    <a class="" target="_blank" 
                                    href="{{ route('single-product',['product_code'=>$prod['code']]) }}">
                                    ({{ $prod['id'] }}) {{ $prod['name'] }} - {{ $prod['name_code'] }}
                                    </a>                                    
                                </h1>
                                <h1 class="subtitle">{{ $prod['code'] }}</h1>
                            </div>
                            <div class="column">
                                <div class="buttons">
                                    <a class="button is-link is-small" target="_blank" 
                                    href="{{ route('edit_product_content',['product_code'=>$prod['code']]) }}">
                                        EDIT CONTENT
                                    </a>
                                    <a class="button is-link is-small" target="_blank" 
                                    href="{{ route('edit_product_datasheets',['product_code'=>$prod['code']]) }}">
                                        EDIT DATASHEETS
                                    </a>
                                    <a class="button is-link is-small" target="_blank" 
                                    href="{{ route('edit_product_certificates',['product_code'=>$prod['code']]) }}">
                                        EDIT CERTIFICATES
                                    </a>
                                    <a class="button is-link is-small" target="_blank" 
                                    href="{{ route('edit_product_images',['product_code'=>$prod['code']]) }}">
                                        EDIT / ADD PRODUCT IMAGES
                                    </a>
                                </div>
                            </div>
                        </div>
                        <hr>
                        @endforeach
                        

                    </div>
                        






                </div>
            </div>
        </div>
    </div>
</section>







@endsection
