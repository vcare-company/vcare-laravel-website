@extends('admin.main')



@section('head-end')
<link rel="stylesheet" href="{{ mix('css/admin/add-product-page.css') }}">
<script src="{{ mix('js/admin/add-product-page.js') }}"></script>
@endsection


@section('content')

<section class="section">
    <div class="hero-body">
        <div class="container">
            <div class="columns">
                <div class="column">

                    









                    <form action="{{ route('add_product_main_image_post') }}"  method="POST" enctype="multipart/form-data">
                        @csrf
                    <div class="box">
                        <h1 class="title">Edit Main Image</h1>

                        <div class="field">
                            {{-- <label class="label">Product Name</label> --}}
                            <div class="control">
                                <h1 class="title">{{ $product['category']['name'] }}</h1>
                                <h1 class="title">
                                    <a class="" target="_blank" 
                                    href="{{ route('single-product',['product_code'=>$product['code']]) }}">
                                    ({{ $product['id'] }}) {{ $product['name'] }} - {{ $product['name_code'] }}
                                    </a>                                        
                                </h1>
                                <h1 class="subtitle">{{ $product['code'] }}</h1>
                                <input type="hidden" name="product_id" value="{{ $product['id'] }}">
                                <input type="hidden" name="code" value="{{ $product['code'] }}">
                            </div>
                        </div>
                        


                        <div class="field">
                            <div class="control">
                                <h1 class="subtitle">Main</h1>
                                <div class="file has-name is-fullwidth">
                                <label class="file-label">
                                    <input class="file-input" type="file" name="image_main">
                                    <span class="file-cta">
                                        <span class="file-icon">
                                        <i class="fas fa-upload"></i>
                                        </span>
                                        Main
                                    </span>
                                    <span class="file-name">....</span>
                                </label>
                                </div>
                            </div>
                        </div>









                        <br>





                        

                    </div>
                        
                        <div class="field is-grouped">
                            <div class="control">
                            <button type="submit" class="button is-link">Submit</button>
                            </div>
                            <div class="control">
                            <button class="button is-link is-light">Cancel</button>
                            </div>
                        </div>

                    </form>









                </div>
            </div>
        </div>
    </div>
</section>







@endsection
