@extends('admin.main')



@section('head-end')
<link rel="stylesheet" href="{{ mix('css/admin/add-product-page.css') }}">
<script src="{{ mix('js/admin/add-product-page.js') }}"></script>

<script>
    let message = '{{ Session::get('message') }}';

    if(message) {
        alert(message);
    }
</script>
@endsection


@section('content')

<section class="section">
    <div class="hero-body">
        <div class="container">
            <div class="columns  is-multiline">



                <div class="column is-12">
                    <h1 class="title">Edit Product Images</h1>

                </div>
                <div class="column is-12">
                    <h1 class="title mb-0">{{ $product['category']['name'] }}</h1>
                    <h1 class="title">
                        <a class="" target="_blank" 
                        href="{{ route('single-product',['product_code'=>$product['code']]) }}">
                        ({{ $product['id'] }}) {{ $product['name'] }} - {{ $product['name_code'] }}
                        </a>                               
                    </h1>
                    <h1 class="subtitle">{{ $product['code'] }}</h1>
             
                </div>

                <div class="column is-12">












                    

                    <form action="{{ route('edit_product_images_post') }}"  method="POST" enctype="multipart/form-data">
                        @csrf
                    <div class="box mb-6">

                               
                        <input type="hidden" name="product_id" value="{{ $product['id'] }}">
                        <input type="hidden" name="code" value="{{ $product['code'] }}">       




                        <br>
                        <h1 class="title">UPDATE IMAGES</h1>
                        <div class="field is-grouped is-grouped-multiline">
                            @foreach ($product['images'] as $img)
                            <?php
                                $src = '/files/products/'.$product['public_id'].'/images/'.$img['type'].'/'.$img['filename'].'.'.$img['ext'];
                            ?>
                            <div class="control">
                                <div class="field">
                                    <img style="height: 300px;" src="{{ $src }}" alt="">
                                    <label class="label">{{ $img['filename'] }}</label>
                                    <div class="file has-name is-fullwidth">
                                        <label class="file-label">
                                                <input type="hidden" name="edit_image_filenames[]" value="{{ $img['filename'] }}">
                                                <input type="hidden" name="edit_image_ids[]" value="{{ $img['id'] }}">
                                            <input class="file-input" type="file" name="edit_image_files[]">
                                            <span class="file-cta">
                                                <span class="file-icon">
                                                <i class="fas fa-upload"></i> 
                                                </span>
                                                {{ $img['title2'] }}
                                            </span>
                                            <span class="file-name">....</span>
                                        </label>
                                    </div>                                      
                                </div>

                                <div class="field is-horizontal">
                                    <div class="field-label">
                                        <label class="label" style="width:50px;">Order</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <input name="edit_image_order[]" 
                                                value="{{ $img['order'] }}" 
                                                class="input" type="text" 
                                                placeholder="Image Order"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                

                                <div class="field is-horizontal">
                                    <div class="field-label">
                                        <label class="label" style="width:50px;">Title2</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <input name="edit_image_title2[]" 
                                                value="{{ $img['title2'] }}" 
                                                class="input" type="text" 
                                                placeholder="Image Order"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            @endforeach
                        </div>


                        <br>
                        <h1 class="title mt-6">UPDATE PACKAGE DETAIL IMAGES</h1>
                        @foreach ($product['images-package-details'] as $img)
                            <div class="field is-grouped is-grouped-multiline">
                                <div class="control">
                                    <?php
                                        $src = '/files/products/'.$product['public_id'].'/images/'.$img['type'].'/'.$img['filename'].'.'.$img['ext'];
                                    ?>
                                    <div class="field">
                                        <img style="height: 300px;" src="{{ $src }}" alt="">
                                        <label class="label">{{ $img['filename'] }}</label>
                                        <div class="file has-name is-fullwidth mb-6">
                                            <label class="file-label">
                                                <input type="hidden" name="edit_image_filenames[]" value="{{ $img['filename'] }}">
                                                <input type="hidden" name="edit_image_ids[]" value="{{ $img['id'] }}">
                                                <input class="file-input" type="file" name="edit_image_files[]">
                                                <span class="file-cta">
                                                    <span class="file-icon">
                                                    <i class="fas fa-upload"></i> 
                                                    </span>
                                                    {{ $img['title2'] }}
                                                </span>
                                                <span class="file-name">....</span>
                                            </label>

                                        </div>
                                    </div>
                                </div>


                                @if ($img['product_details_expanded_image'])
                                <div class="control">
                                    <?php
                                        $src = '/files/products/'.$product['public_id'].'/images/'.$img['product_details_expanded_image']['type'].'/'.$img['product_details_expanded_image']['filename'].'.'.$img['product_details_expanded_image']['ext'];
                                    ?>                                    
                                    <div class="field">
                                        <img style="height: 300px;" src="{{ $src }}" alt="">
                                        <label class="label">{{ $img['product_details_expanded_image']['filename'] }}</label>
                                        <div class="file has-name is-fullwidth mb-6">
                                            <label class="file-label">
                                                <input type="hidden" name="edit_image_filenames[]" value="{{ $img['product_details_expanded_image']['filename'] }}">
                                                <input type="hidden" name="edit_image_ids[]" value="{{ $img['product_details_expanded_image']['id'] }}">
                                                <input class="file-input" type="file" name="edit_image_files[]">
                                                
                                                <span class="file-cta">
                                                    <span class="file-icon">
                                                    <i class="fas fa-upload"></i>
                                                    </span>
                                                    {{ $img['product_details_expanded_image']['title2'] }} Expanded
                                                </span>
                                                <span class="file-name">....</span>
                                            </label>

                                        </div>
                                    </div>
                                </div>
                                @else 
                                <div class="control">
                                    <div class="field">
                                        <label class="label">EXPANDED IMAGE MISSING</label>
                                        <div class="file has-name is-fullwidth mb-6">
                                            <label class="file-label">
                                                <input type="hidden" name="add_product_details_expanded_parent_ids[]" value="{{ $img['id'] }}">
                                                <input class="file-input" type="file" name="add_product_details_expanded_files[]">
                                                <span class="file-cta">
                                                    <span class="file-icon">
                                                    <i class="fas fa-upload"></i>
                                                    </span>
                                                    Upload Expanded Image
                                                </span>
                                                <span class="file-name">....</span>
                                            </label>

                                        </div>                                        
                                    </div>
                                </div>                                
                                @endif


                            </div>
                        @endforeach


                        <br>

                                                
                        <div class="field is-grouped">
                            <div class="control">
                            <button type="submit" class="button is-link">UPDATE IMAGES</button>
                            </div>
                            <div class="control">
                            <button class="button is-link is-light">Cancel</button>
                            </div>
                        </div>
                    </div>


                    </form>


































                    <form action="{{ route('add_product_images_post') }}"  method="POST" enctype="multipart/form-data">
                        @csrf
                    <div class="box" style="margin-top: 300px;">

                               
                        <input type="hidden" name="product_id" value="{{ $product['id'] }}">
                        <input type="hidden" name="code" value="{{ $product['code'] }}">       



                        <div class="field">
                            <div class="control">
                                <h1 class="subtitle">Main</h1>
                                <div class="file has-name is-fullwidth">
                                <label class="file-label">
                                    <input class="file-input" type="file" name="image_main">
                                    <span class="file-cta">
                                        <span class="file-icon">
                                        <i class="fas fa-upload"></i>
                                        </span>
                                        Main
                                    </span>
                                    <span class="file-name">....</span>
                                </label>
                                </div>
                            </div>
                        </div>




                        <br>
                        <div class="field">
                            <div class="control">
                                <h1 class="subtitle">3D View</h1>
                                
                                <?php
                                    $fis = [
                                        ['pp_bag','PP Bag'],
                                        ['middle_box','Middle Box'],
                                        ['master_box','Master Box'],
                                        ['display_box','Display Box'],
                                        ['display_box_empty','Display Box (Empty)'],
                                        ['packages_3','3 Packages'],
                                    ];
                                ?>
                                @foreach ($fis as $fi)
                                <div class="file has-name is-fullwidth">
                                    <label class="file-label">
                                        <input class="file-input" type="file" name="image_3d_view_{{ $fi[0] }}">
                                        <span class="file-cta">
                                            <span class="file-icon">
                                            <i class="fas fa-upload"></i> 
                                            </span>
                                            {{ $fi[1] }}
                                        </span>
                                        <span class="file-name">....</span>
                                    </label>
                                </div>
                                @endforeach

                            </div>
                        </div>

                        <br>

                        <div class="field">
                            <div class="control">
                                <h1 class="subtitle">Package Details</h1>
                                
                                <?php
                                    $fis = [
                                        ['pp_bag','PP Bag'],
                                        ['middle_box','Middle Box'],
                                        ['master_box','Master Box'],
                                    ];
                                ?>
                                @foreach ($fis as $fi)
                                <div class="file has-name is-fullwidth">
                                    <label class="file-label">
                                        <input class="file-input" type="file" name="image_package_details_{{ $fi[0] }}">
                                        <span class="file-cta">
                                            <span class="file-icon">
                                            <i class="fas fa-upload"></i> 
                                            </span>
                                            {{ $fi[1] }}
                                        </span>
                                        <span class="file-name">....</span>
                                    </label>
                                </div>
                                <div class="file has-name is-fullwidth mb-6">
                                    <label class="file-label">
                                        <input class="file-input" type="file" name="image_package_details_expanded_{{ $fi[0] }}">
                                        <span class="file-cta">
                                            <span class="file-icon">
                                            <i class="fas fa-upload"></i> 
                                            </span>
                                            {{ $fi[1] }} Expanded
                                        </span>
                                        <span class="file-name">....</span>
                                    </label>
                                </div>                                
                                @endforeach

                            </div>
                        </div>

                        <br>

                        {{-- <div class="field">
                            <div class="control">
                                <h1 class="subtitle">Package Details Expanded</h1>
                                
                                @foreach ($fis as $fi)
                                <div class="file has-name is-fullwidth">
                                    <label class="file-label">
                                        <input class="file-input" type="file" name="image_package_details_expanded_{{ $fi[0] }}">
                                        <span class="file-cta">
                                            <span class="file-icon">
                                            <i class="fas fa-upload"></i> 
                                            </span>
                                            {{ $fi[1] }}
                                        </span>
                                        <span class="file-name">....</span>
                                    </label>
                                </div>
                                @endforeach

                            </div>
                        </div> --}}



                        <br>
                        <br>

                        <div class="field">
                            <div class="control has-text-right">
                                <a class="button is-danger is-small" target="_blank" 
                                href="{{ route('rename_product_images',['product_code'=>$product['code']]) }}">
                                    UPDATE PRODUCT IMAGE FILENAMES
                                </a>                                
                            </div>
                        </div>



                        <br>


                        <div class="field is-grouped">
                            <div class="control">
                            <button type="submit" class="button is-link">ADD IMAGES</button>
                            </div>
                            <div class="control">
                            <button class="button is-link is-light">Cancel</button>
                            </div>
                        </div>



                        

                    </div>
                        

                    </form>









                </div>
            </div>
        </div>
    </div>
</section>







@endsection
