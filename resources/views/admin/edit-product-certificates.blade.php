@extends('admin.main')



@section('head-end')
<link rel="stylesheet" href="{{ mix('css/admin/add-product-page.css') }}">
<script src="{{ mix('js/admin/add-product-page.js') }}"></script>

<script>
    let message = '{{ Session::get('message') }}';

    if(message) {
        alert(message);
    }
</script>
@endsection




@section('content')

<section class="section">
    <div class="hero-body">
        <div class="container">
            <div class="columns">
                <div class="column">

                    







                    <form action="{{ route('edit_product_certificate_names_post') }}"  method="POST" enctype="multipart/form-data">
                        @csrf
                    <div class="box">
                        <h1 class="title">Update CertificateS</h1>

                        <div class="field">
                            {{-- <label class="label">Product Name</label> --}}
                            <div class="control">
                                <h1 class="title">{{ $product['category']['name'] }} - {{ $product['name_type'] }}</h1>
                                <h1 class="title">
                                    <a class="" target="_blank" 
                                    href="{{ route('single-product',['product_code'=>$product['code']]) }}">
                                    ({{ $product['id'] }}) {{ $product['name'] }} - {{ $product['name_code'] }}
                                    </a>                                         
                                </h1>
                                <h1 class="subtitle">{{ $product['code'] }}</h1>
                                <input type="hidden" name="product_id" value="{{ $product['id'] }}">
                                <input type="hidden" name="code" value="{{ $product['code'] }}">
                            </div>
                        </div>
                        


                       <h1 class="title">Certification Names</h1>
                        @foreach ($product['certificates'] as $item)   
                        {{-- <div class="field is-horizontal">
                            <input type="hidden" name="certification_names_id[]" value="{{ $item['id'] }}">
                            
                            <div class="field-body">
                                <div class="field">
                                    <label class="label">Title</label>
                                    <div class="control">
                                        <input name="certification_names_title[]" 
                                        value="{{ $item['title'] }}" 
                                        class="input" type="text" 
                                        placeholder="Certification Title">
                                    </div>
                                </div>
                                <div class="field">
                                    <label class="label">Filename ({{ $item['ext'] }})</label>
                                    <div class="control">
                                        <input name="certification_names_filename[]" 
                                        value="{{ $item['filename'] }}"
                                        class="input" type="text" 
                                        placeholder="Certification Filename">
                                    </div>
                                </div>
                            </div>

                        </div> --}}


                        <input type="hidden" name="certification_names_id[]" value="{{ $item['id'] }}">

                        <div class="field">
                            <label class="label">ID: {{ $item['id'] }}</label>
                            <label class="label">File</label>
                            <div class="control">
                                <a href="{{ route('certificate',['file_code'=>$item['filename'],'product_code'=>$product['code']]).'?u='.env('APP_ASSET_TIMESTAMP') }}"
                                    target="_blank">
                                    {{ $item['title'].'.'.$item['ext'] }}
                                  </a>
                            </div>                            
                        </div>

                        <div class="field">
                            <label class="label">Title</label>
                            <div class="control">
                                <input name="certification_names_title[]" 
                                value="{{ $item['title'] }}" 
                                class="input" type="text" 
                                placeholder="Certification Title">
                            </div>                            
                        </div>

                        <div class="field">
                            <label class="label">Filename ({{ $item['ext'] }})</label>
                            <div class="control">
                                <input name="certification_names_filename[]" 
                                value="{{ $item['filename'] }}"
                                class="input" type="text" 
                                placeholder="Certification Filename">
                            </div>
                        </div>

                        <div class="field mb-6">
                            <label class="label">Order</label>
                            <div class="control">
                                <input name="certification_names_order[]" 
                                value="{{ $item['order'] }}"
                                class="input" type="text" 
                                placeholder="Certification Order">
                            </div>
                        </div>
                        @endforeach


                        
                        <div class="field is-grouped">
                            <div class="control">
                            <button type="submit" class="button is-link">Update Certification Names</button>
                            </div>
                            <div class="control">
                            <button class="button is-link is-light">Cancel</button>
                            </div>
                        </div>

                        
                    </div>
                    </form>


                    <br><br><br><br><br>




                    <form action="{{ route('edit_product_certificates_post') }}"  method="POST" enctype="multipart/form-data">
                        @csrf
                    <div class="box">
                        <h1 class="title">Add Certificates</h1>

                        <div class="field">
                            {{-- <label class="label">Product Name</label> --}}
                            <div class="control">
                                <h1 class="title">{{ $product['category']['name'] }}</h1>
                                <h1 class="title">
                                    <a class="" target="_blank" 
                                    href="{{ route('single-product',['product_code'=>$product['code']]) }}">
                                    ({{ $product['id'] }}) {{ $product['name'] }} - {{ $product['name_code'] }}
                                    </a>                                         
                                </h1>
                                <h1 class="subtitle">{{ $product['code'] }}</h1>
                                <input type="hidden" name="product_id" value="{{ $product['id'] }}">
                                <input type="hidden" name="code" value="{{ $product['code'] }}">
                            </div>
                        </div>
                        

                        <br>
                        @foreach ($product['certificates'] as $item)   
                        <div class="columns">
                            <div class="column">
                                <h1 class="subtitle mb-0">{{ $item['title'] }}</h1>
                                <h1 class="subtitle mt-0">{{ $item['filename'].'.'.$item['ext'] }}</h1>
                            </div>
                        </div>
                        @endforeach
                       <br>

                        <div class="field">
                            <div class="control">
                                <h1 class="title">Certifications</h1>

                                @for($x=0; $x<=10; $x++)
                                <span class="field-container">
                                <div class="field">
                                    <label class="label">Certification Title {{ $x }}</label>
                                    <div class="control">
                                    <input name="certifications_title[]" value="" class="input" type="text" placeholder="Certification Title">
                                    </div>
                                </div>                                
                                <div class="file has-name is-fullwidth mb-6">
                                    <label class="file-label">
                                        <input class="file-input" type="file" name="certifications_file[]">
                                        <span class="file-cta">
                                            <span class="file-icon">
                                            <i class="fas fa-upload"></i> 
                                            </span>
                                        </span>
                                        <span class="file-name">....</span>
                                    </label>
                                </div>
                                </span>
                                @endfor

                            </div>
                        </div>
                        <br>



                        
                        <div class="field is-grouped">
                            <div class="control">
                            <button type="submit" class="button is-link">Submit Certification Files and Names</button>
                            </div>
                            <div class="control">
                            <button class="button is-link is-light">Cancel</button>
                            </div>
                        </div>

                        

                    </div>


                    </form>









                </div>
            </div>
        </div>
    </div>
</section>







@endsection
