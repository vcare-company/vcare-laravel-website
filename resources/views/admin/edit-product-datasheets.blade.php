@extends('admin.main')



@section('head-end')
<link rel="stylesheet" href="{{ mix('css/admin/add-product-page.css') }}">
<script src="{{ mix('js/admin/add-product-page.js') }}"></script>
@endsection


@section('content')

<section class="section">
    <div class="hero-body">
        <div class="container">
            <div class="columns">
                <div class="column">

                    





                    <form action="{{ route('edit_product_datasheets_post') }}"  method="POST" enctype="multipart/form-data">
                        @csrf
                    <div class="box">


                        <div class="field">
                            {{-- <label class="label">Product Name</label> --}}
                            <div class="control">
                                <h1 class="title">{{ $product['category']['name'] }} - {{ $product['name_type'] }}</h1>
                                <h1 class="title">
                                    <a class="" target="_blank" 
                                    href="{{ route('single-product',['product_code'=>$product['code']]) }}">
                                    ({{ $product['id'] }}) {{ $product['name'] }} - {{ $product['name_code'] }}
                                    </a>                                         
                                </h1>
                                <h1 class="subtitle">{{ $product['code'] }}</h1>
                                <input type="hidden" name="product_id" value="{{ $product['id'] }}">
                                <input type="hidden" name="code" value="{{ $product['code'] }}">
                            </div>
                        </div>


                        <h1 class="title">Edit Datasheets</h1>

                        <div class="field mb-6">
                            <label class="label">Datasheet</label>
                            <div class="control">
                                <div class="file has-name is-fullwidth">
                                    <label class="file-label">
                                        <input class="file-input" type="file" name="product_datasheet_file">
                                        <span class="file-cta">
                                            <span class="file-icon">
                                            <i class="fas fa-upload"></i> 
                                            </span>
                                            Datasheet
                                        </span>
                                        <span class="file-name">....</span>
                                    </label>
                                </div>
                            </div>
                        </div>
    
    
                            

                    </div>
                        
                        <div class="field mb-6 is-grouped">
                            <div class="control">
                            <button type="submit" class="button is-link">Submit</button>
                            </div>
                            <div class="control">
                            <button class="button is-link is-light">Cancel</button>
                            </div>
                        </div>

                    </form>









                </div>
            </div>
        </div>
    </div>
</section>







@endsection
