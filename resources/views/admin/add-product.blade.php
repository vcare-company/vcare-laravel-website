@extends('admin.main')



@section('head-end')
<link rel="stylesheet" href="{{ mix('css/admin/add-product-page.css') }}">
<script src="{{ mix('js/admin/add-product-page.js') }}"></script>
@endsection


@section('head-title')
<title>Vcare.earth ADD PRODUCT</title>
@endsection

@section('content')

<section class="section">
    <div class="hero-body">
        <div class="container">
            <div class="columns">
                <div class="column">

                    









                    <form action="{{ route('create_product_post') }}"  method="POST" enctype="multipart/form-data">
                        @csrf
                    <div class="box">
                        <h1 class="title">Add Product</h1>

                        <div class="field mb-6">
                            <label class="label">Category</label>
                            <div class="control">
                            <div class="select">
                                <select name="product_category">
                                    <option value="">Select Category</option>
                                    @foreach ($categories as $cat)
                                        <option value="{{ $cat['id'] }}">
                                            {{ $cat['name'] }} &nbsp;&nbsp;==&nbsp;&nbsp; ({{ strtoupper($cat['code']) }})
                                        </option>                                        
                                    @endforeach
                                </select>
                            </div>
                            </div>
                        </div>
                        




                        <div class="field mb-6">
                            <label class="label">Product Name</label>
                            <div class="control">
                            <input name="product_name" class="input" type="text" placeholder="Lou">
                            </div>
                        </div>



                        <div class="field mb-6">
                            <label class="label">Product Name Code Group</label>
                            <div class="control">
                            <input name="product_name_code_group" class="input" type="text" placeholder="KN95">
                            </div>
                        </div>


                        <div class="field mb-6">
                            <label class="label">Product Name Type</label>
                            <div class="control">
                            <input name="product_name_type" class="input" type="text" placeholder="Foldable">
                            </div>
                        </div>


                        <div class="field mb-6">
                            <label class="label">Product Name Code</label>
                            <div class="control">
                            <input name="product_name_code" class="input" type="text" placeholder="1A">
                            </div>
                        </div>


                        <div class="field mb-6">
                            <label class="label">Product Code</label>
                            <div class="control">
                            <input name="product_code" class="input" type="text" placeholder="kn95-lou">
                            </div>
                        </div>






                        <div class="field mb-6">
                            <label class="label">Overview</label>
                            <div class="control">
                            <textarea name="product_overview" class="textarea" placeholder=""></textarea>
                            </div>
                        </div>


                        <div class="field mb-6">
                            <label class="label">Certifications</label>
                            <div class="control">
                            <textarea name="product_certifications" class="textarea" placeholder=""></textarea>
                            </div>
                        </div>


                        <div class="field mb-6">
                            <label class="label">HTML Content</label>
                            <div class="control">
                            <textarea name="product_html" class="textarea" placeholder=""></textarea>
                            </div>
                        </div>




                        <div class="field mb-6">
                            <label class="label">Meta Description</label>
                            <div class="control">
                            <textarea style="height: 100px;"
                            name="product_meta_desc" 
                            class="textarea" 
                            placeholder=""></textarea>
                            </div>
                        </div>


                        <div class="field mb-6">
                            <label class="label">Meta Title</label>
                            <div class="control">
                            <textarea style="height: 100px;"
                            name="product_meta_title" 
                            class="textarea" 
                            placeholder=""></textarea>
                            </div>
                        </div>
                        

{{-- 

                        <br>

                        <div class="field mb-6">
                            <div class="control">
                                <h1 class="title">Certifications</h1>

                                @for($x=0; $x<=7; $x++)
                                <div class="field mb-6">
                                    <label class="label">Certification Title {{ $x }}</label>
                                    <div class="control">
                                    <input name="certifications_title[]" value="" class="input" type="text" placeholder="Certification Title">
                                    </div>
                                </div>                                
                                <div class="file has-name is-fullwidth mb-6">
                                    <label class="file-label">
                                        <input class="file-input" type="file" name="certifications_file[]">
                                        <span class="file-cta">
                                            <span class="file-icon">
                                            <i class="fas fa-upload"></i> 
                                            </span>
                                        </span>
                                        <span class="file-name">....</span>
                                    </label>
                                </div>
                                @endfor

                            </div>
                        </div>
                        <br>


                        <hr class=" mt-7">

                        <h1 class="title is-3">Images</h1>

                        <div class="field mb-6">
                            <div class="control">
                                <h1 class="subtitle">Main</h1>
                                <div class="file has-name is-fullwidth">
                                <label class="file-label">
                                    <input class="file-input" type="file" name="image_main">
                                    <span class="file-cta">
                                        <span class="file-icon">
                                        <i class="fas fa-upload"></i>
                                        </span>
                                        Main
                                    </span>
                                    <span class="file-name">....</span>
                                </label>
                                </div>
                            </div>
                        </div>

                        <br>
                        <div class="field mb-6">
                            <div class="control">
                                <h1 class="subtitle">3D View</h1>
                                
                                <?php
                                    $fis = [
                                        ['pp_bag','PP Bag'],
                                        ['middle_box','Middle Box'],
                                        ['master_box','Master Box'],
                                        ['display_box','Display Box'],
                                        ['display_box_empty','Display Box (Empty)'],
                                        ['packages_3','3 Packages'],
                                    ];
                                ?>
                                @foreach ($fis as $fi)
                                <div class="file has-name is-fullwidth">
                                    <label class="file-label">
                                        <input class="file-input" type="file" name="image_3d_view_{{ $fi[0] }}">
                                        <span class="file-cta">
                                            <span class="file-icon">
                                            <i class="fas fa-upload"></i> 
                                            </span>
                                            {{ $fi[1] }}
                                        </span>
                                        <span class="file-name">....</span>
                                    </label>
                                </div>
                                @endforeach

                            </div>
                        </div>

                        <br>

                        <div class="field mb-6">
                            <div class="control">
                                <h1 class="subtitle">Package Details</h1>
                                
                                <?php
                                    $fis = [
                                        ['pp_bag','PP Bag'],
                                        ['middle_box','Middle Box'],
                                        ['master_box','Master Box'],
                                    ];
                                ?>
                                @foreach ($fis as $fi)
                                <div class="file has-name is-fullwidth">
                                    <label class="file-label">
                                        <input class="file-input" type="file" name="image_package_details_{{ $fi[0] }}">
                                        <span class="file-cta">
                                            <span class="file-icon">
                                            <i class="fas fa-upload"></i> 
                                            </span>
                                            {{ $fi[1] }}
                                        </span>
                                        <span class="file-name">....</span>
                                    </label>
                                </div>
                                @endforeach

                            </div>
                        </div>

                        <br>

                        <div class="field mb-6">
                            <div class="control">
                                <h1 class="subtitle">Package Details Expanded</h1>
                                
                                @foreach ($fis as $fi)
                                <div class="file has-name is-fullwidth">
                                    <label class="file-label">
                                        <input class="file-input" type="file" name="image_package_details_expanded_{{ $fi[0] }}">
                                        <span class="file-cta">
                                            <span class="file-icon">
                                            <i class="fas fa-upload"></i> 
                                            </span>
                                            {{ $fi[1] }}
                                        </span>
                                        <span class="file-name">....</span>
                                    </label>
                                </div>
                                @endforeach

                            </div>
                        </div>

 --}}









                        

                    </div>
                        
                        <div class="field mb-6 is-grouped">
                            <div class="control">
                            <button type="submit" class="button is-link">Submit</button>
                            </div>
                            <div class="control">
                            <button class="button is-link is-light">Cancel</button>
                            </div>
                        </div>

                    </form>









                </div>
            </div>
        </div>
    </div>
</section>







@endsection
