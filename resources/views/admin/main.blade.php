<!DOCTYPE html>
<html>

<head>
    @yield('head-start')

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @section('head-title')    
    <title>Admin Vcare.earth</title>
    @show

    {{-- favicon --}}
    <link rel="apple-touch-icon" sizes="180x180" href="{{ env('APP_URL') }}/files/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ env('APP_URL') }}/files/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ env('APP_URL') }}/files/favicon/favicon-16x16.png">
    <link rel="manifest" href="{{ env('APP_URL') }}/files/favicon/site.webmanifest">
    <link rel="mask-icon" href="{{ env('APP_URL') }}/files/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">



    {{-- <link href="https://fonts.googleapis.com/css?family=Calistoga|Open+Sans&display=swap" rel="stylesheet"> --}}
    {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tangerine|Montserrat"> --}}
    {{-- <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700&display=swap" rel="stylesheet"> --}}
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <!-- <script defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js"></script> -->
    <script src="https://kit.fontawesome.com/b1d8d70b35.js" crossorigin="anonymous"></script>

    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css" integrity="sha256-D9M5yrVDqFlla7nlELDaYZIpXfFWDytQtiV+TaH6F1I=" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.0/css/bulma.min.css">


    {{--  jquery  --}}
    {{--  <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>  --}}
    {{--  <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>  --}}
    {{--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>      --}}
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>    
  

    @yield('head-end')
</head>

<body>
    <!-- Wrapper -->
    {{-- <div id="wrapper" class="has-text-centered-mobile"> --}}
    <div id="wrapper">




        <div id="content">
            @yield('content')
        </div>


        

    </div>
    <!-- END Wrapper -->
</body></html>
