@extends('layout.main') 

@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('about') }}"  
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/about-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}" />
<script src="{{ mix('js/about-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection 



@section('head-title')
<title>About Us | Vcare.earth</title>
@endsection



@section('seo-meta-title')
<?php 
    $meta_title = "VCare.Earth Commitment to Care - About Us" ; 
    $meta_desc = "Established with a clear purpose to provide premium medical and healthcare products to clients local and global.";
?>    
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 



@section('content')

<section class="section" id="vcare-heading">
    <div class="container">
        <div class="columns is-flex-end">
            <div class="column title-column">
                <h2 class="subtitle">
                    ABOUT US
                </h2>
                <h1 class="title is-1">VCare.Earth</h1>
                <p>
                    Established with a clear purpose to provide premium medical and healthcare products to clients local and global, we strive continuously toward our 'commitment to care': to individuals, communities, and businesses the world over, at all times.
                </p>
            </div>
            <div class="column profile-column has-text-left-mobile">
                <button class="button vcare-button">
                    <a href="{{ route('all-products') }}" target="_blank">
                        <span class="vcare-button-text is-size-7 pr-4">OUR PRODUCTS</span>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </button>
            </div>
        </div>
    </div>
</section>



{{-- <section class="section blue-vcare-bg" style="background-image: url({{ env('APP_URL') }}/files/about/images/about.png?u={{ env('APP_ASSET_TIMESTAMP') }});"> --}}
<section class="section blue-vcare-bg" style="background-image: url({{ env('APP_URL') }}/files/about/images/about.jpg?u={{ env('APP_ASSET_TIMESTAMP') }});">
    <div class="transparent-bg"></div>
    <div class="container">
        <div class="columns is-vcentered  is-centered">
            <div class="column is-8">
                {{-- <p class="subtitle is-6 is-size-7-mobile has-text-centered has-text-white has-text-weight-light">
                    With a deep driven vision to deliver quality, VCare.Earth strengthened its position in<br class="is-desktop" />
                    terms of ability to expand its manufacturing capabilities as well as sourcing, supplying,<br class="is-desktop" />
                    importing & exporting of Personal Protective Equipment (PPE), Medical and Hospital products.
                </p>
                <p class="subtitle is-6 is-size-7-mobile has-text-centered has-text-white has-text-weight-light">
                    Over the many years in business, we have acquired extensive product expertise,<br class="is-desktop" />
                    gathering market trends to provide one-stop solutions to customer needs, on time <br class="is-desktop" />
                    delivery and competitive prices.
                </p>
                <h3 class="is-size-3 is-size-5-mobile has-text-centered has-text-white has-text-weight-light">
                    VCare.Earth will continue to fulfill its role as <br class="is-desktop" />
                    a global leader of Respiratory and medical products.
                </h3> --}}

                <p class="subtitle is-6 has-text-centered has-text-white has-text-weight-light">
                    By expanding our manufacturing capabilities and supplier network, increasing our production and range of critical items such as PPE safety products and respirators, we have strengthened our position as a leading provider meeting the demands of the world’s healthcare and medical equipment requirements.
                </p>
                <p class="subtitle is-6 has-text-centered has-text-white has-text-weight-light">
                    Through multiple years in business, we have acquired extensive product knowledge, market insights, and partnerships which together, have helped us to become a leading one-stop solutions provider to our customers, ensuring on-time delivery and competitive prices.
                </p>
                <h3 class="is-size-3 has-text-centered has-text-white has-text-weight-light">
                    VCare.Earth remains deeply driven to continuing its role as a global leader of respirators and medical equipment. Today - and every day.
                </h3>
            </div>
        </div>
    </div>
</section>

<section class="section padding-content" id="message-ceo">
    <div class="container">
        <h1 class="is-size-3 has-vcare-text mb-6">Messages from our CEOs</h1>
        <div class="columns mb-150px is-vcentered is-centered">
            <!-- <div class="column is-5">
                <div class="message-image">
                    {{-- <div class="image" style="background-image:url(/files/about/images/ceo/vcare-earth-marc-vazquez.jpg?u={{ env('APP_ASSET_TIMESTAMP') }})"> --}}
                    <div class="image">
                        <img 
                        style="height: 30%;
                        width: auto;
                        margin: auto;" 
                        {{-- src="/files/about/images/ceo/vcare-earth-marc-vazquez.jpg?u={{ env('APP_ASSET_TIMESTAMP') }}"  --}}
                        src="/files/logos/vcareicon.svg?u={{ env('APP_ASSET_TIMESTAMP') }}" 
                        alt="VCare.Earth">
                    </div>
                </div>                
            </div> -->
            <div class="column">
                <div class="message-text">
                    <p>
                        I am delighted with how VCare.Earth has evolved quickly, increasing our operations at speed to meet the changing demands of our customers around the world. 
                    </p>
                    <br />
                    <p>
                        This ability to adapt quickly and provide the products most in need, having them reach all corners of the globe with urgency, gives me a huge amount of pride in our capabilities and in our team, where everyone remains 100% focused on fulfilling our customer’s requirements.  
                    </p>
                    <br />
                    <p>
                        We believe all healthcare providers deserve access to reliable, well-produced medical equipment to help them feel safe at work. Our mission ‘to provide the best solutions quickly and at a fair price’ is a long-term view and our customers can be confident VCare.Earth will be here supporting future generations of professionals with our high-quality personal safety products for the years to come.
                    </p>
                    <br />
                    <p>
                        <strong>Marc Vázquez, CEO</strong>
                    </p>
                </div>
            </div>            
        </div>
        <div class="columns  is-vcentered is-centered">
            <!-- <div class="column is-5">
                <div class="message-image">
                    {{-- <div class="image" style="background-image:url(/files/about/images/ceo/vcare-earth-johannes-nepomuk-eidens.jpg?u={{ env('APP') }})"> --}}
                    <div class="image">
                        <img 
                        src="/files/about/images/ceo/vcare-earth-johannes-nepomuk-eidens.jpg?u={{ env('APP_ASSET_TIMESTAMP') }}" 
                        alt="VCare.Earth Johannes Nepomuk Eidens CEO">
                    </div>
                </div>                
            </div> -->
            <div class="column">
                <div class="message-text">
                    <p>
                        VCare.Earth was founded in Europe, Spain in 2020.
                    </p>
                    <br />
                    <p>
                        A team of business experts, innovators, and specialists, we forged ourselves a clear goal: to ensure the world of the highest quality healthcare and medical equipment at an affordable price - whoever the customer, wherever they are based. Quickly meeting our initial objectives, our product range and services have increased significantly to support more customers in new locations, thanks to our adaptability and an extensive network of suppliers and distributors who share our goal.
                    </p>
                    <br />
                    <p>
                        I feel extremely proud of our team, each of whom has a genuine ‘customer first’ attitude. Their commitment toward our company vision – helping to safeguard lives through compassionate, responsive delivery of effective healthcare solutions – shines through every day in all that they do. With the best people and robust systems in place, I am excited about our future and look forward to supporting our customers, current and new, with our world-class products. 
                    </p>
                    <br />
                    <p>
                        <strong>Wei Wu, CEO</strong>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="section padding-content add-padding-p" 
id="about-vcare"
style="background-image: url({{ env('APP_URL') }}/files/about/images/about-visionmission.png?u={{ env('APP_ASSET_TIMESTAMP') }});">
    <div class="container">
        <h1 class="is-size-3 ">About VCare.Earth</h1>
        <div class="columns mt-5 is-multiline">

            <div class="column is-12 mb-6">
                <p class=" about-q"><b>WHO ARE WE?</b></p>
                <br />
                <p class="">VCare.Earth provides essential healthcare and medical equipment to clients local and global. Owned and directed by Johannes Eidens and Marc Vázquez - two highly regarded business entrepreneurs with multi-market experience - VCare.Earth was created to support the increasing demand for high-quality healthcare and PPE safety products.</p>
            </div>
            <div class="column is-12 mt-3 mb-5">
                <p class=" about-q"><b>WHY WORK WITH US?</b></p>
                <br />
                <p class="">Committed to care, VCare.Earth assures its clients of products that are manufactured to the highest standards, of exceptional quality and fairly priced, delivered to locations across the world with speed and efficiency.</p>
            </div>


            {{-- <div class="column is-4 mb-5">
                <p class="has-vcare-text about-q"><b>WHO IS VCARE.EARTH?</b></p>
                <br />
                <p>VCare.Earth is a brand owned and run by Johannes Eidens and Marc Vázquez, both European business professionals, entrepreneurs, and with many years of experience and knowledge in businesses.</p>
            </div>

            <div class="column is-4 mb-5">
                <p class="has-vcare-text about-q"><b>HOW WAS VCARE.EARTH ESTABLISHED?</b></p>
                <br />
                <p>Given our business acumen and entrepreneurial ethos, we always find ourselves in search of the next significant challenge, and with the current global situation, we found our calling and VCare was born.</p>
            </div>

            <div class="column is-4 mb-5">
                <p class="has-vcare-text about-q"><b>WHY VCARE.EARTH?</b></p>
                <br />
                <p>With the current worldwide situation, VCare.Earth was formed in aim to challenge and defeat the current global pandemic, by supplying medical & non-medical solutions which are fit for purpose.</p>
            </div>

            <div class="column is-4 mb-5">
                <p class="has-vcare-text about-q"><b>WHERE IS VCARE.EARTH LOCATED?</b></p>
                <br />
                <p>VCare.Earth is currently located in Hong Kong, The United Arab Emirates, Spain, Germany and the United Kingdom.</p>
            </div>


            <div class="column is-4 mb-5">
                <p class="has-vcare-text about-q"><b>WHAT IS VCARE.EARTH?</b></p>
                <br />
                <p class="">VCare.Earth is a company operating in the healthcare and medical industry supplying PPE products.</p>
            </div> --}}

        </div>

        <div class="columns is-flex-end mb-6">
            <div class="column profile-column has-text-left-mobile">
                <button class="button vcare-white-button">
                    <a href="{{ route('companyProfile').'?u='.env('APP_ASSET_TIMESTAMP') }}" target="_blank">
                        <span class="vcare-button-text is-size-7 pr-4">COMPANY PROFILE</span>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </button>
            </div>
        </div>

        <div class="columns mt-6 pt-6 is-multiline">
            <div class="column mb-3">
                <h1 class="is-size-3 mb-3">VISION</h1>
                {{-- <p class=" about-q"><b>VISION</b></p> --}}
                <p class="">
                    To help Safeguard lives through compassionate, responsive delivery of effective healthcare solutions.
                </p>

            </div>
            <div class="column">
                <h1 class="is-size-3 mb-3">MISSION</h1>
                {{-- <p class=" about-q"><b>MISSION</b></p> --}}
                <p class="">
                    To provide quick solutions and best medical products that are verifiably effective and fair priced.
                </p>
            </div>
        </div>
        
    </div>
</section>

{{-- <section class="section blue-vcare-bg">
    <div class="container">
        <div class="columns">
            <div class="column">
                <p class="subtitle is-6 has-text-weight-light">
                    <b class="vision-text">VISION</b><br />
                    To help Safeguard lives through compassionate, responsive delivery of effective healthcare solutions.
                </p>
                <br />
                <p class="subtitle is-6 has-text-weight-light">
                    <b class="mission-text">MISSION</b><br />
                    To provide quick solutions and best medical products that are verifiably effective and fair priced.
                </p>
            </div>
            <div class="column"></div>
        </div>
    </div>
</section> --}}

{{-- <hr class=""> --}}

{{-- <section class="section padding-content" id="message-ceo">
    <div class="container">
        <h1 class="is-size-3 has-vcare-text mb-6">Message from our CEOs</h1>
        <div class="columns mb-6">
            <div class="column">
                <div class="message-image">
                    <div class="image" style="background-image:url(/files/about/images/ceo/joh.jpg?u={{ env('APP') }})"></div>
                </div>
                <div class="message-text">
                    <p>
                        VCare.Earth was founded in Europe, Spain as a response to the increased needs globally for premium healthcare equipment. 
                    </p>
                    <br />
                    <p>
                        A team of business experts, innovators, and specialists, we forged ourselves a clear goal: to ensure the world of the highest quality healthcare and medical products at an affordable price - whoever the customer, wherever they are based. Quickly meeting our initial objectives, our product range and services are now increasing rapidly to support more customers in new locations every day, thanks to our adaptability and an extensive network of suppliers and distributors who share our goal.
                    </p>
                    <br />
                    <p>
                        I feel extremely proud of our team, each of whom has a genuine ‘customer first’ attitude. Their commitment toward our company vision – helping to safeguard lives through compassionate, responsive delivery of effective healthcare solutions – shines through every day in all that they do. With the best people and robust systems in place, I am excited about our future and look forward to supporting our customers, current and new, with our world-class products. 
                    </p>
                    <br />
                    <p>
                        Johannes Eidens, CEO
                    </p>
                </div>
            </div>
            <div class="column contact">
                <div class="message-image">
                    <div class="image" style="background-image:url(/files/about/images/ceo/marc.jpg?u={{ env('APP') }})"></div>
                </div>
                <div class="message-text">
                    <p>
                        I am delighted with how VCare.Earth has risen to the challenge in healthcare, increasing our operations at speed to meet the changing demands of our customers around the world.  
                    </p>
                    <br />
                    <p>
                        This ability to adapt quickly and provide the products most in need, having them reach all corners of the globe with urgency, gives me a huge amount of pride in our capabilities and in our team, where everyone remains 100% focused on fulfilling our customer’s requirements.  
                    </p>
                    <br />
                    <p>
                        We believe all healthcare providers deserve access to reliable, well-produced equipment to help them feel safe at work. Our mission ‘to provide the best medical solutions quickly and at a fair price’ is a long-term view that stretches beyond the current situation, and our customers can be confident VCare.Earth will be here supporting future generations of professionals with our high-quality equipment. 
                    </p>
                    <br />
                    <p>
                        Marc Vázquez, CEO
                    </p>
                </div>

            </div>
        </div>
    </div>
</section> --}}


<!-- Contact -->
<section class="section inquiry-section padding-content">
    <div class="container">
        <div class="columns">
            <div class="column is-half is-offset-one-quarter has-text-centered">
                <h2 class="is-size-2 is-size-5-mobile has-vcare-text font-family-title"> Have a question? </h2>
                <p class="mt-4">
                    Talk to the team at VCare.Earth
                </p>
                <button class="button vcare-button-outlined mt-4">
                    <a href="{{ route('contact') }}#chatwithus" target="_blank">
                      <span class="vcare-button-text is-size-7 pr-4">CONTACT US</span><i class="fas fa-chevron-right"></i>
                    </a>
                </button>
            </div>
        </div>
    </div>
  </section>
  <!-- END Contact -->
  
  

@endsection
