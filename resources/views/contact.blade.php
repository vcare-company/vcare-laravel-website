@extends('layout.main')



@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('contact') }}" 
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/contact-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/contact-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
<script src="https://www.google.com/recaptcha/api.js?render={{ env('CAPTCHA_KEY') }}"></script>
@endsection


@section('head-title')
<title>Contact Us | Vcare.earth</title>
@endsection

@section('seo-meta-title')
<?php 
    $meta_title = "VCare.Earth Commitment to Care - Contact Us" ; 
    $meta_desc = "VCare.Earth is supporting customers the world over with our exceptional quality medical and healthcare products.";
?>    
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 



@section('body-script')
    <script>
            grecaptcha.ready(function() {
            grecaptcha.execute('{{ env('CAPTCHA_KEY') }}', {action: 'submit'}).then(function(token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                    recaptchaResponse.value = token;
            });
            });
    </script>
    @if(\Session::has('success'))
    <script>
        Swal.fire({
            icon: 'success',
            title: 'Thank you for your Interest',
            text: "{!! session('success') !!}",
            showConfirmButton: true,
        })
    </script>
    @elseif(\Session::has('danger'))
    <script>
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: "{!! session('danger') !!}",
        })
    </script>
    @endif
@endsection





@section('content')

<section class="section">
    <div class="container">
        <div class="columns is-flex-end">
            <div class="column title-column">
                <h2 class="subtitle">
                    CONTACT US
                </h2>
                <h1 class="title is-1">Get in touch with us</h1>
                <div class="columns">
                    <div class="column">
                        <p class="has-text-weight-light">
                            VCare.Earth is supporting customers the world over with the best in personal equipment protection, delivered quickly to any location thanks to our efficient service capabilities and extensive partner network. Allow us to help your organisation facilitate your healthcare and medical equipment needs the VCare.Earth way - with speed, efficiency and care.
                        </p>
                    </div>
                 </div>
            </div>
            <div class="column has-text-centered follow-cause">
                <div class="columns is-multiline">
                    <div class="column is-12">
                        <div class="email-address" style="background-image: url(/files/vcare-email.svg);"></div>
                    </div>
                    <div class="column is-12">
                        <h1 class="subtitle is-4">
                            CONNECT WITH US
                            <span class="social-icons">
                                <a href="https://linkedin.com/company/vcare-earth"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a>
                                <a href="https://instagram.com/vcare_earth"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                                <a href="https://twitter.com/VCare_Earth"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                            </span>
                        </h1>
                    </div>
                </div>                
            </div>            
        </div>        
    </div>
</section>

<section class="section">
    <div class="container has-text-centered">
        <p class="is-size-4 mb-4">Please select from the below options and our team will be in touch with you.</p>
        <div class="columns">
            <div class="column">
                <div class="card contact-card chat">
                    <div class="card-content">
                        <p class="has-text-centered contact-icon" style="height: 73px;">
                            <img src="{{ env('APP_URL') }}/files/contact/images/chaticon.svg" class="contact-card-image"/>
                        </p>
                        <p class="has-text-centered contact-text mt-3">ARRANGE <br>A CHAT</p>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="card contact-card partner">
                    <div class="card-content">
                        <p class="has-text-centered contact-icon" style="height: 73px;">
                            <img src="{{ env('APP_URL') }}/files/contact/images/partnerwithus.svg" />
                        </p>
                        <p class="has-text-centered contact-text mt-3">PARTNER <br>WITH US</p>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="card contact-card distributor">
                    <div class="card-content">
                        <p class="has-text-centered contact-icon" style="height: 73px;">
                            <img src="{{ env('APP_URL') }}/files/contact/images/distributor.svg"/>
                        </p>
                        <p class="has-text-centered contact-text mt-3">BECOME A <br>DISTRIBUTOR</p>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="card contact-card supplier">
                    <div class="card-content">
                        <p class="has-text-centered contact-icon" style="height: 73px;">
                            <img src="{{ env('APP_URL') }}/files/contact/images/supplier.svg"/>
                        </p>
                        <p class="has-text-centered contact-text mt-3">BECOME A <br>SUPPLIER</p>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="card contact-card b2b">
                    <div class="card-content">
                        <p class="has-text-centered contact-icon" style="height: 73px;">
                            <img src="{{ env('APP_URL') }}/files/contact/images/b2b.svg"/>
                        </p>
                        <p class="has-text-centered contact-text mt-3">B2B <br>REQUEST</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <section class="section topic-selector-text">
    <div class="container">
        <p class="has-text-centered">
            <i class="fas fa-chevron-up"></i>
        </p>
        <p class="has-text-centered">
            <b>Please select a topic you are interested in to continue</b>
        </p>
    </div>
</section> -->



<section class="section all-contact-form">
    <div class="container">
        <form method="post" action="/contact-us">
        {{ csrf_field() }}
        <div class="columns">
            <div class="column">
                <div class="field">
                    <p class="control is-expanded">
                        <input class="input" type="text" name="name" placeholder="Name" required>
                    </p>
                </div>
            </div>
            <div class="column">
                <div class="field">
                    <p class="control is-expanded">
                        <input class="input" type="email" name="email" placeholder="Email" required>
                    </p>
                </div>
            </div>
        </div>
            <div class="columns">
                <div class="column">
                    <div class="field has-addons has-addons-centered">
                        <p class="control">
                            <span class="select">
                                <select name="number_type" required>
                                    <option value="" disabled selected>Select</option>
                                    <option value="Mobile">Mobile</option>
                                    <option value="Landline">Landline</option>
                                </select>
                            </span>
                        </p>
                        <p class="control">
                            <input class="input" type="number" name="number_code" placeholder="Country Code" required>
                        </p>
                        <p class="control is-expanded">
                            <input class="input" type="tel" name="number" placeholder="Mobile/Landline" required>
                        </p>
                    </div>
                    <div class="control is-expanded whatsapp-available">
                        <div class="field is-horizontal">
                            <label class="label has-text-weight-light">Available on Whatsapp? &nbsp;</label>
                            <div class="control">
                                <label class="radio">
                                    <input type="radio" name="whatsapp" value="Yes">
                                    Yes
                                </label>
                                <label class="radio">
                                    <input type="radio" name="whatsapp" value="No"> 
                                    No
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field is-horizontal">
                        <div class="field-body">
                            <div class="field">
                                <p class="control is-expanded">
                                    <input class="input" type="text" name="company" placeholder="Company Name" required>
                                </p>
                            </div>
                            <div class="field">
                                <p class="control is-expanded">
                                    <input class="input" type="text" name="company_website" placeholder="Company Website" required>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns not-chat-with-us">
                <div class="column">
                    <div class="field mt-5">
                        <div class="field-label has-text-left">
                            <label class="label has-text-weight-light">Please tell us who you are?</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <div class="select is-fullwidth">
                                        <select name="who">
                                            <option value="" selected disabled >Select an Option</option>
                                            <option value="Distributor">Distributor</option>
                                            <option value="Wholesaler">Wholesaler</option>
                                            <option value="Supplier">Supplier</option>
                                            <option value="Retailer">Retailer</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field mt-2">
                            <p class="control is-expanded who-other-option">
                                <input class="input" type="text" name="who_other_option" placeholder="Mention your other option">
                            </p>
                        </div>
                    </div>
                    <div class="not-supplier-contact partner-contact">
                        <div class="field mt-5">
                            <div class="field-label has-text-left">
                                <label class="label has-text-weight-light">Are you already in the PPE or Medical Equipment industry?</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <div class="select is-fullwidth">
                                            <select name="ppe">
                                                <option value="" selected disabled >Select an Option</option>
                                                <option value="PPE">PPE</option>
                                                <option value="Medical Equipment Industry">Medical Equipment Industry</option>
                                                <option value="None">None of the above</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field mt-2">
                                <p class="control is-expanded ppe-other-option">
                                    <input class="input" type="text" name="ppe_other_option" placeholder="Mention your Industry">
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="b2b-contact">
                        <div class="field mt-5">
                            <div class="field-label has-text-left">
                                <label class="label has-text-weight-light">What is your monthly budget  <span class="ppe_option">for PPE</span> <span class="medical_option">for Medical Equipment</span>?</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control ">
                                        <div class="select is-fullwidth">
                                            <select name="budget">
                                                <option value="" selected disabled >Select an Option</option>
                                                <option value="< US $5,000">< US $5,000</option>
                                                <option value="< US $25,000">< US $25,000</option>
                                                <option value="< US $50,000">< US $50,000</option>
                                                <option value="< US $75,000">< US $75,000</option>
                                                <option value="< US $100,000">< US $100,000</option>
                                                <option value="US $100,000+"> US $100,000+</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="not-supplier-contact not-partner-contact ">
                        <div class="field mt-5">
                            <div class="field-label has-text-left">
                                <label class="label has-text-weight-light">Will our product(s) be used in Hospitals and/or sold in Pharmacies?</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <div class="select is-fullwidth">
                                            <select name="sold">
                                                <option value="" selected disabled >Select an Option</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="supplier-contact">
                        <div class="field mt-5">
                           <div class="field-label has-text-left">
                                <label class="label has-text-weight-light">Which products can you supply?</label>
                           </div>
                           <div class="field-body">
                               <div class="field">
                                   <p class="control is-expanded">
                                       <input class="input" type="text" name="products_supply" placeholder="Mention your Products">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field mt-5">
                        <div class="field-label has-text-left">
                            <label class="label has-text-weight-light">Where did you hear about us?</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <div class="select is-fullwidth">
                                        <select name="how">
                                            <option value="" selected disabled >Select an Option</option>
                                            <option value="Social Media">Social Media</option>
                                            <option value="Amazon">Amazon</option>
                                            <option value="Google Ads">Google Ads</option>
                                            <option value="Online Search">Organic Online Search</option>
                                            <option value="Referral">Referral</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field mt-2">
                            <p class="control is-expanded referral-option">
                                <input class="input" type="text" name="referral_option" placeholder="Mention your referral option">
                            </p>
                        </div>
                        <div class="field mt-2">
                            <p class="control is-expanded how-other-option">
                                <input class="input" type="text" name="how_other_option" placeholder="Mention your other option">
                            </p>
                        </div>
                    </div>
                    
                    <div class="b2b-contact not-supplier-contact partner-contact">
                        <div class="field mt-5">
                            <label class="label has-text-weight-light">Where is your target market?</label>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <div class="select is-fullwidth">
                                            <select name="target_market">
                                                <option value="" selected disabled >Select a Country</option>
                                                @foreach($countries as $country)
                                                    <option value="{{ $country }}">{{ $country }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="not-supplier-contact not-partner-contact">
                        <div class="field mt-5">
                            <div class="field-label has-text-left">
                                <label class="label has-text-weight-light">What is the intention for your purchase?</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <div class="select is-fullwidth">
                                            <select name="intention">
                                                <option value="" selected disabled >Select an Option</option>
                                                <option value="Resale">Resale</option>
                                                <option value="Internal Company Use">Internal Company Use</option>
                                                <option value="Free Delivery to Customers">Free Delivery to Customers</option>
                                                <option value="Charity">Charity</option>
                                                <option value="Pandemic Prevention">Pandemic Prevention</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field mt-2">
                                <p class="control is-expanded intention-other-option">
                                    <input class="input" type="text" name="intention_other_option" placeholder="Mention your other option">
                                </p>
                            </div>
                        </div>
                    </div>
                 
                    <div class="supplier-contact">
                        <div class="field mt-5">
                           <div class="field-label has-text-left">
                                <label class="label has-text-weight-light">Enter your WeChat ID</label>
                           </div>
                           <div class="field-body">
                               <div class="field">
                                   <p class="control is-expanded">
                                       <input class="input" type="text" name="wechat" placeholder="Mention your Wechat ID">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="b2b-contact">
                <div class="field mt-5">
                    <div class="field-label has-text-left">
                        <label class="label has-text-weight-light">Select Products(s)?
                            <span class="vcare-button-outlined add-product button is-small" id="add-product">
                                <i class="fas fa-plus"></i>&nbsp;Add Product
                            </span>
                        </label>
                    </div>
                    <div class="columns is-multiline mt-1" id="cloned-div">
                        <div class="column is-4" id="clone-div">
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <div class="select is-fullwidth">
                                            <select name="" class="b2b_select" data-select="1">
                                                <option value="" selected disabled>Select a Product</option>
                                                @foreach ($categories as $categ)
                                                @if (count($categ['children']))
                                                <option disabled class="has-text-weight-bold "> </option>
                                                <option disabled class="has-text-weight-bold "> {!! $categ['name'] !!}</option>
                                                @foreach ($categ['children'] as $cat)
                                                @if (count($cat['products']))
                                                <option disabled class="has-text-weight-semibold"><small> {{ $cat['name'] }}</small></option>
                                                @foreach($cat['products'] as $k =>  $p)
                                                <option value="{{ $p['name'] }}_b2b" data-value="{{ $p['name'] }}" class="has-text-weight-light" {{($product == $p['code'] ? 'selected' : '' )}} >
                                                        @if ($p['name'] && $p['name_type'])
                                                        {{ $p['name'] }}  - {{ $p['name_code'] }} ({{ $p['name_code_group'] }} - {{ $p['name_type'] }}) 
                                                        @elseif ($p['name'])
                                                        {{ $p['name'] }}  - {{ $p['name_code'] }} ({{ $p['name_code_group'] }}) 
                                                        @else
                                                        {{ $p['name_type'] }} *Coming Soon
                                                        @endif 
                                                    </option>
                                                @endforeach
                                                @else
                                                <option disabled class="has-text-weight-semibold">{{ $cat['name'] }} *Coming Soon</option>
                                                @endif
                                                @endforeach
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field mt-2 b2b_quantity" data-quantity="1" id="b2b_quantity">
                                <div class="control is-expanded">
                                    <input class="input" type="number" name="" placeholder="Quantity">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field mt-5">
                <div class="field-body">
                    <div class="field">
                        <p class="control is-expanded">
                            <input class="input" type="text" name="subject" placeholder="Subject" required>
                        </p>
                    </div>
                </div>
            </div>
            <div class="field mt-5">
                <div class="field-body">
                    <div class="field">
                        <p class="control is-expanded">
                            <textarea class="textarea" name="message" placeholder="Your message here" required></textarea>
                        </p>
                    </div>
                </div>
            </div>
            <div class="field has-text-centered mt-5">
                <div class="control">
                    <input type="hidden" value="" name="recaptcha_response" id="recaptchaResponse">
                    <input type="hidden" value="contact_form" name="interested_in" class="interested-text">
                    <button class="button vcare-button-outlined" type="submit">
                        Send message
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>




@endsection