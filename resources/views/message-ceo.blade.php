@extends('layout.main') @section('head-end')
<link rel="stylesheet" href="{{ mix('css/message-ceo-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}" />
<script src="{{ mix('js/message-ceo-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection @section('content')



@section('head-title')
<title>Message From Our CEOs | Vcare.earth</title>
@endsection


<section class="section">
    <div class="container">
        <div class="columns is-flex-end">
            <div class="column title-column">
                <h2 class="subtitle">
                    CHARITY
                </h2>
                <h1 class="title is-1">We are for Charity</h1>
                <p>
                    To date, COVID-19 has infected over 26 million people globally, regardless of age, ethnicity, race and religion. Established as the global pandemic took hold of the world, VCare.Earth is helping to protect and safeguard lives today, and ongoing, during this unprecedented time.

                </p>
            </div>
            <div class="column has-text-centered follow-cause">
                <h1 class="subtitle is-4">
                    FOLLOW OUR CAUSE
                    <span class="social-icons">
                        <a href="https://linkedin.com/company/vcare-earth"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a>
                        <a href="https://instagram.com/vcare_earth"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                        <a href="https://twitter.com/VCare_Earth"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                    </span>
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="section blue-vcare-bg" style="background-image: url({{ env('APP_URL') }}/files/charity/charity.png);">
    <div class="container">
        <div class="columns is-vcentered is-centered">
            <div class="column is-7">
                <h2 class="subtitle is-2 has-text-centered has-text-white px-4">
                    {{-- Our Journey Together,<br />
                    to Kill the Novel Coronavirus --}}

                    Together, we are all stepping into the unknown in a bid to defeat the Novel Coronavirus.
                </h2>
            </div>
        </div>
    </div>
</section>

<section class="section charity-text">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-three-quarters">
                <div class="content">
                    <p>
                        As a provider of multiple solutions currently in high demand across the world, and with an established global network of partners, VCare.Earth is able to help support this demand today and over the long term. You, too, can help us to support the world during the current pandemic.

                    </p>
                    <p>
                        VCare.Earth is proud to commit to a donation of 5%! of the price from any product purchase, which will be given to a number of charities who have become focused on the fight against COVID-19.

                    </p>
                    <p>
                        For every purchase you make, you are directly helping to safeguard the most vulnerable men, women and children, making a sustainable difference and protecting lives.

                    </p>
                    <p>
                        Together we are stepping into the unknown and changing our world for the better.
                    </p>
                    <p>
                        Thank you.
                    </p>

                    <br />
                    {{-- <p>Respectfully,</p>
                    <br />
                    
                    <p style="font-family: 'Tangerine', serif; font-size: 30px; margin-bottom: 10px;">
                        <strong>Marc Vázquez & Johannes Eidens</strong>
                    </p> --}}
                   
                    <p><strong>Team VCare.Earth</strong></p>
                    <p><strong>1 September, 2020</strong></p>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
