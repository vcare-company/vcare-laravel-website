<!DOCTYPE html>
<html lang="en">

<head>
    @yield('head-start')

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    @section('head-title')
    {{-- <title>Vcare.earth - Respirators, Face Masks, Covid19 Rapid Test, Medical Equipment</title> --}}
    {{-- <title>Vcare.earth</title> --}}
    <title>Personal Protective Equipment Supplier | PPE Safety Equipment Suppliers | VCare.Earth</title>
    @show
    

    {{-- favicon --}}
    <link rel="apple-touch-icon" sizes="180x180" href="{{ env('APP_URL') }}/files/favicon/apple-touch-icon.png?u={{ env('APP_ASSET_TIMESTAMP') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ env('APP_URL') }}/files/favicon/favicon-32x32.png?u={{ env('APP_ASSET_TIMESTAMP') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ env('APP_URL') }}/files/favicon/favicon-16x16.png?u={{ env('APP_ASSET_TIMESTAMP') }}">
    <link rel="manifest" href="{{ env('APP_URL') }}/files/favicon/site.webmanifest">
    <link rel="mask-icon" href="{{ env('APP_URL') }}/files/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


    {{-- <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-170281330-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-170281330-1');
    </script> --}}

    @if(!env('APP_DEV_MODE'))
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5LXH4ZF');</script>
    <!-- End Google Tag Manager -->
    @endif


    {{-- SEO stuff --}}
    @section('seo-meta')
    <?php 
        // $meta_title = 'VCare.Earth Commitment to Care - Personal Protective Equipment (PPE), Face Masks, Covid19 Rapid Test, Medical Equipment' ; 
        // $meta_desc = 'We deliver worldwide, to any destination of your preferred location and we promise to make for a speedy delivery within the shortest time possible.';
        $meta_title = 'Personal Protective Equipment Supplier | PPE Safety Equipment Suppliers | VCare.Earth' ; 
        $meta_desc = 'VCare.Earth, a safety equipment supplier, provides high-quality medical and healthcare equipment, respirators, and PPE safety equipment supplies to B2B customers worldwide. Visit us today!';
        $meta_url = Request::fullUrl();
    ?>    
    {{-- <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/> --}}
    <link rel="canonical" href="{{ $meta_url }}" />
    
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    
    @section('seo-meta-title')
    <meta property="og:title" content="{{ $meta_title }}" />
    <meta name="twitter:title" content="{{ $meta_title }}" />
    @show

    @section('seo-meta-description')
    <meta property="og:description" content="{{ $meta_desc }}" />
    <meta name="twitter:description" content="{{ $meta_desc }}" />
    <meta name="description" content="{{ $meta_desc }}"/>
    @show

    <meta property="og:url" content="{{ $meta_url }}" />
    <meta property="og:site_name" content="VCare.earth" />
    
    @section('seo-meta-image')
    {{-- <meta property="og:image" content="{{ env('APP_URL').'/files/logos/meta-image.jpg?u='.env('APP_ASSET_TIMESTAMP') }}" />
    <meta property="og:image:secure_url" content="{{ env('APP_URL').'/files/logos/meta-image.jpg?u='.env('APP_ASSET_TIMESTAMP') }}" /> --}}
    <meta property="og:image" content="{{ '/files/logos/meta-image.png?u='.env('APP_ASSET_TIMESTAMP') }}" />
    <meta property="og:image:secure_url" content="{{ '/files/logos/meta-image.png?u='.env('APP_ASSET_TIMESTAMP') }}" />
    {{-- <meta property="og:image:width" content="1600" /> --}}
    {{-- <meta property="og:image:height" content="1200" /> --}}
    @show
    
    
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@VCare_Earth" />
    <meta name="twitter:creator" content="@VCare_Earth" />
    @show


    
    <script>
        var env = [];
        env['APP_ASSET_TIMESTAMP'] = '{{ env('APP_ASSET_TIMESTAMP') }}';
    </script>


    {{-- <link href="https://fonts.googleapis.com/css?family=Calistoga|Open+Sans&display=swap" rel="stylesheet"> --}}
    {{-- <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,700;1,600&display=swap" rel="stylesheet"> --}}
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tangerine">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- <script defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js"></script> -->
    {{-- <script src="https://kit.fontawesome.com/b1d8d70b35.js?u={{ env('APP_ASSET_TIMESTAMP') }}" crossorigin="anonymous"></script> --}}
    {{-- <script defer src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" integrity="sha384-9/D4ECZvKMVEJ9Bhr3ZnUAF+Ahlagp1cyPC7h5yDlZdXs4DQ/vRftzfd+2uFUuqS" crossorigin="anonymous"></script> --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">

    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css" integrity="sha256-D9M5yrVDqFlla7nlELDaYZIpXfFWDytQtiV+TaH6F1I=" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.0/css/bulma.min.css">


    {{--  jquery  --}}
    {{--  <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>  --}}
    {{--  <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>  --}}
    {{--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>      --}}
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> 
    @if(!env('APP_DEV_MODE'))       
<link rel="alternate" hreflang="en" href="https://vcare.earth">
<link rel="alternate" hreflang="es" href="https://es.vcare.earth">
<link rel="alternate" hreflang="de" href="https://de.vcare.earth">
<link rel="alternate" hreflang="it" href="https://it.vcare.earth">
<link rel="alternate" hreflang="fr" href="https://fr.vcare.earth">
<link rel="alternate" hreflang="pt" href="https://pt.vcare.earth">
    <script type="text/javascript" src="https://cdn.weglot.com/weglot.min.js"></script>
    <script>
        Weglot.initialize({
            api_key: '{{ env('WEGLOT_API_KEY') }}'
        });
    </script>
    @endif

    <script type="application/ld+json">
        {
            "@context+": "https://schema.org",
            "@type": "MedicalOrganization",
            "name": "VCARE HEALTH IBERICA, S.L.",
            "url": "https://vcare.earth/",
            "logo": "https://vcare.earth/files/logos/vcare-earth-logo.svg?u={{ env('APP_ASSET_TIMESTAMP') }}",
            "sameAs": [
                "https://www.linkedin.com/company/vcare-earth",
                "https://twitter.com/VCare_Earth",
                "https://www.instagram.com/vcare_earth/"
            ]
        }
    </script>
    <script type="application/ld+json">
        {
            "@context": "https://schema.org/",
            "@type": "WebSite",
            "name": "Vcare.earth",
            "url": "https://vcare.earth/",
            "potentialAction": {
                "@type": "SearchAction",
                "target": "https://vcare.earth/search{search_term_string}",
                "query-input": "required name=search_term_string"
            }
        }
    </script>
    @yield('head-end')


</head>

<body>

    @if(!env('APP_DEV_MODE'))
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5LXH4ZF"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->    
    @endif

    <!-- Wrapper -->
    {{-- <div id="wrapper" class="has-text-centered-mobile"> --}}
    <div id="wrapper">

        @include('partials.nav')

        <div id="content">

            @section('breadcrumbs')
            @isset($breadcrumbs)
            <section class="section pt-6 pb-0">
                <div class="container">
                    <nav class="breadcrumb py-0" aria-label="breadcrumbs">
                        <ul>
                            @foreach ($breadcrumbs as $br)
                                @if($br['route'] && Route::has($br['route']))
                                <li>
                                    <a href="{{ route($br['route']) }}" style="white-space: normal;">
                                        {{ $br['title'] }}
                                    </a>
                                </li>
                                @else
                                <li class="is-active">
                                    <a href="#" aria-current="page" style="white-space: normal;">
                                        {{ $br['title'] }}
                                    </a>
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    </nav>
                </div>
            </section>
            @endisset
            @show
            
            @yield('content')
        </div>

        @if (\Request::is('medical-equipment-supplier'))
        @else
        @section('twitter')        
            @include('partials.twitter')
        @show
        @endif
        
        @include('partials.footer')

        @yield('body-script')

        @include('partials.privacy-policy')

        <div id="scrollToTop" style="display:none;">
            <i class="fas fa-chevron-up"></i>
        </div>


        <div id="chat-buttons">
            <div id="mylivechat-button" title="Live Chat">
                <i class="far fa-comment"></i>
                {{-- <i class="fas fa-comment"></i> --}}
            </div>

            <div id="whatsapp-button" title="Whatsapp Chat" style="background-color:#25D366; border-radius:0%">
                <a href=" https://wa.me/+971554150803?text=Hi%20VCare!%20I'm%20interested%20in%20your%20products.%20Please%20tell%20me%20more%20about..."
                target="_blank">
                    <i class="fab fa-whatsapp"></i>
                </a>
            </div>
        </div>
        


    </div>
    <!-- END Wrapper -->

    {{-- mylivechat.com --}}
    <script>function add_chatinline(){var hccid=50615919;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
    add_chatinline(); </script>
    {{-- mylivechat.com --}}
    <script>
        $(document).ready(function(){
            
            var l = $("html").attr("lang"), w = $("#whatsapp-button a"); 
            if (l == "es") {
                w.attr("href","https://wa.me/+34637801689"); 
            }
            else if(l == "de") { 
                w.attr("href","https://wa.me/+491736725740"); 
            }         
        });
    </script>


</body>
</html>
