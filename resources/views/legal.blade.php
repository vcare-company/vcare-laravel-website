@extends('layout.main')



@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('legal') }}" 
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/legal-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/legal-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection



@section('head-title')
<title>Legal Notice | Vcare.earth</title>
@endsection


@section('seo-meta-title')
<?php 
    $meta_title = "VCare.Earth Commitment to Care - Legal" ; 
    $meta_desc = "Our products and services are available to customers worldwide. For any questions on legal matters you can refer on this page.";
?>    
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 





@section('content')

<section class="section section-header">
    <div class="container">
        <div class="columns">
            <div class="column title-column">
                <h1 class="title is-1">Legal Notice </h1>
                <h2 class="subtitle">
                Our products and services are available to customers worldwide.
                </h2>
            </div>
        </div>        
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="columns">
            <div class="column">
                VCARE HEALTH IBERICA S.L. <br>
                Cl. Aribau, 185 3º Planta, 08021, Barcelona, Spain <br>
                CIF: B-02662203
            </div>
            <div class="column">
                E-Mail: info@vcare.earth  
            </div>
            <div class="column">
                Commercial register: Spain  <br>
                Registration No.: 20092441
            </div>
        </div>        
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="columns">
            <div class="column">
                <b>Management</b><br><br>
                <p>
                    Marc Vázquez, Director <br>
                    <!-- Johannes Eidens, Director <br> -->
                    Wei Wu, Director <br>
                </p>
            </div>
            <div class="column">
            </div>
            <div class="column">
            </div>
        </div>        
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Disclaimer</b></p><br>
        <p>
            Despite careful control of the contents, we do not assume any liability for the contents of external links. For the content of linked pages, the operators of these pages are solely responsible. 
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Legal notes</b></p><br>
        <p>
            Limitation of liability<br> 
            The contents of this website are created with the greatest possible care. However, the provider does not guarantee the accuracy, completeness and timeliness of the content provided. The use of the contents of the website is at the user’s own risk. Contributions identified by name reflect the opinion of the respective author and not always the opinion of the provider. The mere use of the provider’s website does not constitute a contractual relationship between the user and the provider.
        </p><br>
        <p>
            
            External links <br>
            This website contains links to third party websites (“external links”). These websites are subject to the liability of the respective operators. When the external links were first created, the provider checked the external content for any legal violations. At that time, no legal violations were apparent. The provider has no influence on the current and future design and content of the linked sites. The inclusion of external links does not mean that the provider adopts the content behind the reference or link as his own. The provider cannot reasonably be expected to constantly monitor these external links without concrete evidence of legal violations. However, if legal violations become known, such external links will be deleted immediately. 
            
        </p><br>
        <p>
            
            Copyrights and ancillary copyrights <br>
            The contents published on this website are subject to Hong Kong copyright and ancillary copyright law. Any use not permitted under Hong Kong copyright and ancillary copyright law requires the prior written consent of the provider or the respective rights holder. This applies in particular to copying, editing, translation, storage, processing or reproduction of contents in databases or other electronic media and systems. Contents and rights of third parties are marked as such. Unauthorized reproduction or distribution of individual contents or complete pages is not permitted and is punishable by law. Only the production of copies and downloads for personal, private and non-commercial use is permitted. The presentation of this website in external frames is only permitted with written permission. 

        </p><br>
        <p>
            Copyright 2020© Any reproduction of the website content of vcare.earth, even in extracts, requires written permission.
        </p>
    </div>
</section>
<br><br>
{{-- <section class="section  mb-6" id="in-the-course-of-incorporation">
    <div class="container">
        <hr>
        <p class="is-italic is-bold">
            *in the course of incorporation
        </p>
    </div>
</section> --}}

@endsection