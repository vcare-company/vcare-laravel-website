@extends('layout.main')

@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
            @if($br['route'])
                @if(Route::has($br['route']))
                    {
                        "@type": "ListItem", 
                        "position": {{ $brkey + 1 }}, 
                        "name": "{{ $br['title']}}",
                        "item": "{{ route($br['route'])}}"  
                    },
                    @elseif($br['route'])
                    {
                        "@type": "ListItem", 
                        "position": {{ $brkey + 1 }}, 
                        "name": "{{ $br['title']}}",
                        "item": ""  
                    },
                    @else
                    {
                        "@type": "ListItem", 
                        "position": {{ $brkey + 1 }}, 
                        "name": "{{ $br['title']}}",
                        "item": "{{ route('single-product',['product_code'=>$product['code']]) }}"  
                    },
                    @endif
                @else
                {
                        "@type": "ListItem", 
                        "position": {{ $brkey + 1 }}, 
                        "name": "{{ $br['title']}}",
                        "item": "{{ route('single-product',['product_code'=>$product['code']]) }}"  
                },
            @endif
      
        @endforeach
    ]

}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "Product", 
  "name": "{{ $br['title']}}",
  "image": {{ env('APP_URL').'/files/products/'.$product['public_id'].'/images/'.$product['main-image']['type'].'/'. $product['main-image']['filename'].'.'.$product['main-image']['ext'].'?u='.env('APP_ASSET_TIMESTAMP') }},
  "description":{{ $product['meta_desc'] }},
  "brand": "VCare.Earth",
  "sku": "",
  "review": {
    "@type": "Review",
    "reviewBody": "",
    "author": {"@type": "Person", "name": ""}
  }
}
</script>

<link rel="stylesheet" href="{{ mix('css/single-product-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/single-product-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>



{{-- <link rel="stylesheet" href="https://unpkg.com/bulma-modal-fx/dist/css/modal-fx.min.css" /> --}}
{{-- <script type="text/javascript" src="https://unpkg.com/bulma-modal-fx/dist/js/modal-fx.min.js"></script> --}}
@endsection


@section('head-title')
<title>Vcare.earth {{ $product['meta_title'] }}</title>
@endsection


@section('seo-meta')
<?php 
    $meta_url = route('single-product',['product_code'=>$product['code']]);
    
    
    // $meta_title = 'VCare.Earth Commitment to Care - '. strtoupper($product['name']) .' - '. strtoupper($product['code']) ; 
    // if($product['category']['code']=='covid-19-testing')
    // $meta_desc = 'An Effective Screening Point-Of-Care Test.';
    // else if($product['category']['code']=='ppe-respirators')
    // $meta_desc = 'Our particulate protection respirator masks have comfortable inner materials and helps provide respiratory protection against certain airborne particles.';

    if($product['category']['keywords'])
    $meta_keywords = $product['category']['keywords'];
    else
    $meta_keywords = 'respirators,face masks,surgical masks,medical gowns,covid-19 testers,respirator,face mask,surgical mask,medical gown,covid-19 tester'; 

    if($product['meta_title'])
    $meta_title = 'VCare.Earth Commitment to Care - '.$product['meta_title']; 
    else
    $meta_title = 'Personal Protective Equipment Supplier | PPE Safety Equipment Suppliers | VCare.Earth' ; 

    if($product['meta_desc'])
    $meta_desc = $product['meta_desc'];
    else
    $meta_desc = 'VCare.Earth, a safety equipment supplier, provides high-quality medical and healthcare equipment, respirators, and PPE safety equipment supplies to B2B customers worldwide. Visit us today!';    
?>

<meta name="keywords" content="{{ $meta_keywords }}"/>
<meta name="description" content="{{ $meta_desc }}"/>
{{-- <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/> --}}
<link rel="canonical" href="{{ $meta_url }}" />

<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{ $meta_title }}" />
<meta property="og:description" content="{{ $meta_desc }}" />
<meta property="og:url" content="{{ $meta_url }}" />
<meta property="og:site_name" content="VCare.earth" />


@if ($product['main-image'])
<?php $mi_url = env('APP_URL').'/files/products/'.$product['public_id'].'/images/'.$product['main-image']['type'].'/'.$product['main-image']['filename'].'.'.$product['main-image']['ext'].'?u='.env('APP_ASSET_TIMESTAMP'); ?>
<?php //$mi_url = env('APP_URL').'/files/products/'.$product['public_id'].'/images/meta/'.$product['main-image']['type'].'/'.$product['main-image']['filename'].'.jpg'.'?u='.env('APP_ASSET_TIMESTAMP'); ?>
<meta property="og:image" content="{{ $mi_url }}" />
<meta property="og:image:secure_url" content="{{ $mi_url }}" />    
@else
<meta property="og:image" content="{{ env('APP_URL').'/files/logos/meta-image.jpg' }}" />
<meta property="og:image:secure_url" content="{{ env('APP_URL').'/files/logos/meta-image.jpg' }}" />
@endif

{{-- <meta property="og:image:width" content="1600" /> --}}
{{-- <meta property="og:image:height" content="1200" /> --}}


<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
<meta name="twitter:site" content="@VCare_Earth" />
<meta name="twitter:creator" content="@VCare_Earth" />
@endsection



@section('content')


{{-- if sub cat has products but product coming soon--}}
@include('product.partials.product-content',[
    'product'=>$product,
])


<section class="section" id="disclaimer">
    <div class="container">
        <div class="columns is-vcentered is-centered">
            <div class="column is-10">
                <h1 class="subtitle is-6 mb-2">Disclaimer</h1>
                <p class="mb-6">
                    VCare.Earth makes no warranties, express or implied, of marketing or use for a particular purpose of this product. It is the user's responsibility to decide on its use and/or application, for which VCare.Earth will not be responsible for the possible damages and losses derived from the use of the product, regardless of whether they are direct, indirect, special, consequential, contractual, or any other nature. The sole and exclusive responsibility of VCare.Earth, in the event that the product is defective, will be the replacement of the product or refund of the purchase price.
                </p>
            </div>
            
        </div>
    </div>
</section>



{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
@if(Helper::adminMode())
<div class="buttons admin">
    <a class="button is-link is-small" target="_blank" 
    href="{{ route('edit_product_content',['product_code'=>$product['code']]) }}">
        EDIT CONTENT
    </a>
    <a class="button is-link is-small" target="_blank" 
    href="{{ route('edit_product_certificates',['product_code'=>$product['code']]) }}">
        EDIT CERTIFICATES
    </a>
    <a class="button is-link is-small" target="_blank" 
    href="{{ route('edit_product_datasheets',['product_code'=>$product['code']]) }}">
        EDIT DATASHEETS
    </a>
    <a class="button is-link is-small ask-first" target="_blank" 
    href="{{ route('edit_product_images',['product_code'=>$product['code']]) }}">
        EDIT / ADD PRODUCT IMAGES
    </a>
    {{-- <a class="button is-danger is-small" target="_blank" 
    href="{{ route('rename_product_images',['product_code'=>$product['code']]) }}">
        UPDATE PRODUCT IMAGE FILENAMES
    </a> --}}
    
    <span class="tag  is-medium is-success mb-2">({{$product['id'] }})  {{$product['code'] }}  {{$product['public_id'] }}</span>
</div>
@endif
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}





@endsection