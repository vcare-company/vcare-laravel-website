@extends('layout.main')

@section('head-end')
<link rel="stylesheet" href="{{ mix('css/single-product-pricelist-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/single-product-pricelist-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection



@section('head-title')
<title>VCare.Earth Pricelist: {{ $product['meta_title'] }}</title>
@endsection





@section('content')

<section id="title" class="section">
    <div class="container">

        <div class="columns is-vcentered">
            <div class="column title-column">
                
                {{-- <h2 class="subtitle">
                    B2B PRICELIST
                </h2>
                <div class="mb-3" style="display: flex; flex-flow: row wrap; align-items: center;">
                    <h1 class="title is-1 mb-0">{{ $product['name'] }} </h1>
                    @if ($product['name_code_group'] && $product['name_code'])
                        <h1 class="subtitle is-5 mt-0 ml-4 box-title" >{{ $product['name_code_group'] .' '. $product['name_code'] }}</h1>
                    @elseif ($product['name_code'])
                        <h1 class="subtitle is-5 mt-0 ml-4 box-title" >{{ $product['name_code'] }}</h1>
                    @elseif ($product['name_code_group'])
                        <h1 class="subtitle is-5 mt-0 ml-4 box-title" >{{ $product['name_code_group'] }}</h1>
                    @endif  
                </div> --}}

                <div class="name-group-title">
                    <h1 class="subtitle name-code-group">
                        {{ $product['category']['name'] }} 
                    </h1>
                    <h1 class="subtitle name-type-code is-5" >{{ $product['name_type'] .' '. $product['name_code'] }}</h1>
                    <h1 class="title product-name">{{ strtoupper($product['name']) }} </h1>
                </div>

                
            </div>
        </div>        

    </div>
</section>


<section class="section banner-section">
    {{-- <div class="banner-section-bg"
    style="background-image: url({{ env('APP_URL').'/files/single-product/pricelist/pricelistbanner.jpg?u='.env('APP_ASSET_TIMESTAMP') }})">
    </div> --}}
    <div class="container">
        <div class="columns">
            <div class="column is-12">
                <div class="banner" 
                style="background-image: url({{ env('APP_URL').'/files/single-product/pricelist/pricelistbanner.jpg?u='.env('APP_ASSET_TIMESTAMP') }})">
                    <div class="columns is-vcentered">
                        <div class="column">    
                        
                            {{-- @if ($product['category']['code']=='ppe-respirators')
                            <h1 class="font-family-title-thick">
                                Particulate Protection 
                            </h1>
                            <h1 class="font-family-title-thick">
                                Respirator Masks
                            </h1>          
                            @elseif ($product['category']['code']=='covid-19-tests')
                            <h1 class="font-family-title-thick">
                                COVID19 Tests
                            </h1>
                            <h1 class="font-family-title subtitle-red">
                                FINN
                            </h1>
                            @elseif ($product['category']['code']=='accesories')
                            <h1 class="font-family-title-thick">
                                Accesories 
                            </h1>
                            <h1 class="font-family-title subtitle-red">
                                SILICON Gadget
                            </h1>
                            @endif


                            @if ($product['name_code_group'] && $product['name_code'])
                            <h1 class="font-family-title subtitle-red">{{ $product['name_code_group'] .' '. $product['name_code'] }} PRICELIST</h1>
                            @elseif ($product['name_code'])
                            <h1 class="font-family-title subtitle-red">{{ $product['name_code'] }} PRICELIST</h1>
                            @elseif ($product['name_code_group'])
                            <h1 class="font-family-title subtitle-red">{{ $product['name_code_group'] }} PRICELIST</h1>
                            @endif --}}


                            <h1 class="font-family-title-thick">
                                {{ $product['category']['parent']['name'] }} 
                            </h1>
                            <h1 class="font-family-title subtitle-red">
                                PRICELIST
                            </h1>                            

                        </div>


                        <div class="column" style="position: relative;">

                            <div class="main-image" 
                            style="background-image: url({{ '/files/single-product/comingsoon-wgown.png'.'?u='.env('APP_ASSET_TIMESTAMP') }})"></div>

                            
                        </div>

                    </div>
                </div>                
            </div>
        </div>
    </div>
</section>



@if (count($product['certification']))
<section class="section">
    <div class="container">
        <div class="columns half-width is-multiline is-centered" 
        style="text-align: center;">
            @foreach ($product['certification'] as $c)                
            <div class="column is-4">
                <h1 class="title is-6">{{ $c->title }}</h1>
                <h1 class="subtitle is-6">{{ $c->value }}</h1>
            </div>
            @endforeach

        </div>
    </div>
</section>
@endif


@if (count($product['table']))
<section class="section table-section">
    <div class="container">
        <div class="columns half-width is-vcentered is-multiline">
            <div class="column is-12">
                <table class="table is-fullwidth is-hoverable is-bordered is-striped">
                    <thead>
                    <tr>
                        <th>QUANTITY</th>                        
                        <th>PRICE</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($product['table'] as $t)
                    <tr>
                        <td>{{ $t->quantity }}</td>
                        <td>{{ $t->price }}</td>
                    </tr>  
                    @endforeach
                </tbody>
                </table>              
            </div>
            <div class="column is-12">
                <div class="column">
                    <p class="subtitle is-6 all-prices" style="text-align: center;">
                        All prices include worldwide free shipment! <br> 
                        Import Duties and Taxes vary from country to country and are not included
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>









@include('product.partials.contact-section',['url'=>'?type=b2b&product='.$product['code']])

<section class="section resources mb-3 pt-6" style="border-top: 0px solid #f0f0f0;">
    <div class="container">        
        <div class="columns is-multiline is-vcentered is-centered">

            <div class="column is-12 has-text-centered">
                {{-- <img src="https://vcare.ddns.net/files/logos/vcareicon.svg"> --}}
                <img style="max-width: 250px;" src="https://vcare.ddns.net/files/logos/vcare-earth-logo.svg" alt="VCare.Earth Logo">
            </div>
            {{-- <div class="column is-narrow">
                <h1 class="title pt-6 pb-3 px-3 has-text-centered font-family-title" style="border-bottom: 2px solid #f0f0f0;">
                    Product Details
                </h1>
            </div> --}}
        
        </div>
    </div>
</section>



{{-- if sub cat has products but product coming soon--}}
@include('product.partials.product-content',[
    'product'=>$product,
])





@else
<section class="section resources">
    <div class="container">        
        <div class="columns is-multiline is-centered is-vcentered">
            
            <div class="column">
                <h1 class="subtitle my-6 py-6" style="text-align: center; font-size: 3em; font-weight: 100;">Coming soon</h1>
            </div>
    
        </div>
    </div>
</section>

@include('product.partials.contact-section',['url'=>'?type=b2b&product='.$product['code']])

@endif












@endsection