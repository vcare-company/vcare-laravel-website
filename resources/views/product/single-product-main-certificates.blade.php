@extends('layout.main')

@section('head-end')
<link rel="stylesheet" href="{{ mix('css/single-product-certificates-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/single-product-certificates-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection



@section('head-title')
<title>Certificates | Vcare.earth</title>
@endsection

@section('seo-meta')
<?php 
    $meta_url = route('single-product-certificates',['product_code'=>$product['code']]);
    
    
    // $meta_title = 'VCare.Earth Commitment to Care - '. strtoupper($product['name']) .' - '. strtoupper($product['code']) ; 
    // if($product['category']['code']=='covid-19-testing')
    // $meta_desc = 'An Effective Screening Point-Of-Care Test.';
    // else if($product['category']['code']=='ppe-respirators')
    // $meta_desc = 'Our particulate protection respirator masks have comfortable inner materials and helps provide respiratory protection against certain airborne particles.';

    if($product['category']['keywords'])
    $meta_keywords = $product['category']['keywords'];
    else
    $meta_keywords = 'respirators,face masks,surgical masks,medical gowns,covid-19 testers,respirator,face mask,surgical mask,medical gown,covid-19 tester'; 
    
    if($product['meta_title'])
    $meta_title = 'VCare.Earth Commitment to Care - '.$product['meta_title']; 
    else
    $meta_title = 'Personal Protective Equipment Supplier | PPE Safety Equipment Suppliers | VCare.Earth' ; 

    if($product['meta_desc'])
    $meta_desc = $product['meta_desc'];
    else
    $meta_desc = 'VCare.Earth, a safety equipment supplier, provides high-quality medical and healthcare equipment, respirators, and PPE safety equipment supplies to B2B customers worldwide. Visit us today!';    


?>

<meta name="keywords" content="{{ $meta_keywords }}"/>
<meta name="description" content="{{ $meta_desc }}"/>
{{-- <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/> --}}
<link rel="canonical" href="{{ $meta_url }}" />

<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{ $meta_title }}" />
<meta property="og:description" content="{{ $meta_desc }}" />
<meta property="og:url" content="{{ $meta_url }}" />
<meta property="og:site_name" content="VCare.earth" />

<?php
if ($product['main-image']) {
    $mi_url = env('APP_URL').'/files/products/'.$product['public_id'].'/images/'.$product['main-image']['type'].'/'.$product['main-image']['filename'].'.'.$product['main-image']['ext'].'?u='.env('APP_ASSET_TIMESTAMP'); 
} else {
    $mi_url = env('APP_URL').'/files/logos/meta-image.jpg?u='.env('APP_ASSET_TIMESTAMP'); 
    // $mi_url = env('APP_URL').'/files/single-product/comingsoon-wgown.png'.'?u='.env('APP_ASSET_TIMESTAMP'); 
}
?>

<meta property="og:image" content="{{ $mi_url }}" />
<meta property="og:image:secure_url" content="{{ $mi_url }}" />    


{{-- <meta property="og:image:width" content="1600" /> --}}
{{-- <meta property="og:image:height" content="1200" /> --}}


<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
<meta name="twitter:site" content="@VCare_Earth" />
<meta name="twitter:creator" content="@VCare_Earth" />
@endsection





@section('content')

<section id="title" class="section">
    <div class="container">

        <div class="columns is-vcentered">
            <div class="column title-column">
                

                <div class="name-group-title">
                    <h1 class="subtitle name-code-group">
                        {{ $product['category']['name'] }} 
                    </h1>
                    <h1 class="subtitle name-type-code is-5" >{{ $product['name_type'] .' '. $product['name_code'] }}</h1>
                    <h1 class="title product-name">{{ strtoupper($product['name']) }} </h1>
                </div>

            </div>
        </div>        

    </div>
</section>


<section class="section banner-section">
    {{-- <div class="banner-section-bg"
    style="background-image: url({{ env('APP_URL').'/files/single-product/pricelist/pricelistbanner.jpg' }})">
    </div> --}}
    <div class="container">
        <div class="columns">
            <div class="column is-12">
                <div class="banner" 
                style="background-image: url({{ env('APP_URL').'/files/single-product/pricelist/pricelistbanner.jpg?u='.env('APP_ASSET_TIMESTAMP') }})">
                    <div class="columns is-vcentered">
                        
                        <div class="column">
                        
                            <h1 class="font-family-title-thick">
                                {{ $product['category']['parent']['name'] }} 
                            </h1>
                            <h1 class="font-family-title subtitle-red">
                                CERTIFICATES
                            </h1>

                        </div>


                        <div class="column" style="position: relative;">

                            @if($product['files_with_markings']['eng']['image-main'])
                            <div class="main-image" 
                            style="background-image: url(/{{ $product['files_with_markings']['eng']['image-main']['path'].'?u='.env('APP_ASSET_TIMESTAMP') }})"></div>
                            @elseif ($product['main-image'])
                            <div class="main-image" 
                            style="background-image: url({{ '/files/products/'.$product['public_id'].'/images/'.$product['main-image']['type'].'/'. $product['main-image']['filename'].'.'.$product['main-image']['ext'].'?u='.env('APP_ASSET_TIMESTAMP') }})"></div>
                            @else
                            <div class="main-image" 
                            style="background-image: url({{ '/files/logos/vcareicon.svg?u='.env('APP_ASSET_TIMESTAMP') }});
                                width: 50%; margin: auto;"></div>
                            @endif

                        </div>

                    </div>
                </div>                
            </div>
        </div>
    </div>
</section>



@if (count($product['certificates']))

    <section class="section resources">
        <div class="container">

        <div class="columns is-multiline">
            @foreach($product['certificates'] as $pc)
            @if(strpos($pc['title'], 'ORIGINAL') == true)
            <div class="column is-3 is-flex">
                <div class="tile">
        
                    <div class="img-container">
                        <a href="{{ route('certificate',['file_code'=>$pc['filename'],'product_code'=>$product['code']]).'?u='.env('APP_ASSET_TIMESTAMP') }}"
                        target="_blank">
                        <i class="far fa-file-alt"></i>
                        </a>
                    </div>
                    <hr>
                    <a href="{{ route('certificate',['file_code'=>$pc['filename'],'product_code'=>$product['code']]).'?u='.env('APP_ASSET_TIMESTAMP') }}"
                        target="_blank">
                        {{ $pc['title'] }}
                    </a>
                </div>
            </div>
            @endif
            @endforeach

            @foreach($product['files_with_markings'] as $lang => $fwm)
            @if($fwm['datasheet'])
            <div class="column is-3 is-flex">
                <div class="tile">
                    <div class="img-container">
                        <a href="/{{ $fwm['datasheet'].'?u='.env('APP_ASSET_TIMESTAMP') }}"
                        target="_blank">
                        <i class="far fa-file-alt"></i>
                        </a>
                    </div>
                    <hr>
                    <a href="/{{ $fwm['datasheet'].'?u='.env('APP_ASSET_TIMESTAMP') }}"
                        target="_blank">
                        DATASHEET ({{ strtoupper($lang) }})
                    </a>        
                </div>
            </div>
            @endif
            @endforeach

        </div>
        </div>
    </section>

    @foreach($product['files_with_markings'] as $lang => $fwm)
    @if(count($fwm['images']))
    <section class="section resources">
        <div class="container">

        <h1 class="title is-5 tab-content-title mt-3 ml-3">3D PACKAGING ({{ strtoupper($lang) }})</h1>

        <div class="columns is-multiline">

            @foreach($fwm['images'] as $img)
                <div class="column is-3 is-flex">
                    <div class="tile" style="justify-content: center; align-items: center;">
        
                        <div class="img-container">
                            <a href="/{{ $img['path'].'?u='.env('APP_ASSET_TIMESTAMP') }}" 
                            data-title="3D Packaging Images"
                            data-lightbox="3d-packaging-images">
                                <img src="/{{ $img['path'].'?u='.env('APP_ASSET_TIMESTAMP') }}" 
                                alt="Vcare Earth {{ $img['filename'] }}">
                            </a>
                        </div>


                    </div>
                </div>
            @endforeach
        

        </div>
        </div>
    </section>
    @endif
    @endforeach

































@else 
<section class="section resources">
    <div class="container">        
        <div class="columns is-multiline is-centered is-vcentered">
            
            <div class="column">
                <h1 class="subtitle my-6 py-6" style="text-align: center; font-size: 3em; font-weight: 100;">Coming soon</h1>
            </div>
    
        </div>
    </div>
</section>
@endif


@include('product.partials.contact-section',['url'=>'?type=b2b&product='.$product['code']])















{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
@if(Helper::adminMode())
<div class="buttons admin">
    <a class="button is-link is-small" target="_blank" 
    href="{{ route('edit_product_content',['product_code'=>$product['code']]) }}">
        EDIT CONTENT
    </a>
    <a class="button is-link is-small" target="_blank" 
    href="{{ route('edit_product_certificates',['product_code'=>$product['code']]) }}">
        EDIT CERTIFICATES
    </a>
    <a class="button is-link is-small" target="_blank" 
    href="{{ route('edit_product_datasheets',['product_code'=>$product['code']]) }}">
        EDIT DATASHEETS
    </a>
    <a class="button is-link is-small ask-first" target="_blank" 
    href="{{ route('edit_product_images',['product_code'=>$product['code']]) }}">
        EDIT / ADD PRODUCT IMAGES
    </a>
    {{-- <a class="button is-danger is-small" target="_blank" 
    href="{{ route('rename_product_images',['product_code'=>$product['code']]) }}">
        UPDATE PRODUCT IMAGE FILENAMES
    </a> --}}
    
    <span class="tag  is-medium is-success mb-2">{{$product['code'] }}  {{$product['public_id'] }}</span>
</div>
@endif
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
{{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}









@endsection