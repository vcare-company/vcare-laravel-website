


			  {{-- Dont show language button if no images or images --}}
			  @if (count($p['images-main']) || count($p['images-3d-view']) || count($p['images-package-details']) || File::exists('files/products/'.$p['public_id'].'/datasheets/datasheet'))
			  <div class="lang-btn-container">
				<button class="button is-vcare vcare-btn lang-btn" data-lang="esp" title="Español">
				  <span>Español</span>
				</button>
				<button class="button is-vcare vcare-btn lang-btn" data-lang="eng" title="English" style="display: none;">
				  <span>English</span>
				</button>
			  </div>
			  @endif





















			  {{-- Main --}}
			  @if (count($p['images-main']))
			  <h2 class="subtitle is-6 tab-content-title"> PRODUCT IMAGES </h2>
			  {{-- <h1 class="subtitle is-6 tab-content-title mt-0"> &nbsp; </h1> --}}
			  @foreach($p['images-main'] as $pi)
			  <?php
			  $langs = ['eng'=>'','esp'=>''];
			  $img = $langs;
			  $img_url = $langs;
			  $img_thumbnail = $langs;
			  $img_thumbnail_url = $langs;

			  $prod_path = '/files/products/'.$p['public_id'];

			  $img_path = '/images/'.$pi['type'].'/'.$pi['filename'].'.'.$pi['ext'];
			  $img_thumbnail_path = '/images/thumbnails/'.$pi['type'].'/'.$pi['filename'].'.'.$pi['ext_thumb'];

			  $img['eng'] = $prod_path.$img_path;
			  $img_thumbnail['eng'] = $prod_path.$img_thumbnail_path;

			  $img_url['eng'] = env('APP_URL').$img['eng'].'?u='.env('APP_ASSET_TIMESTAMP');
			  $img_thumbnail_url['eng'] = env('APP_URL').$img_thumbnail['eng'].'?u='.env('APP_ASSET_TIMESTAMP');

			  $img['esp'] = $prod_path.'/lang/esp'.$img_path;
			  $img_thumbnail['esp'] = $prod_path.'/lang/esp'.$img_thumbnail_path;
			  if(File::exists(public_path().$img['esp']) && File::exists(public_path().$img_thumbnail['esp'])) {
				$img_url['esp'] = env('APP_URL').$img['esp'].'?u='.env('APP_ASSET_TIMESTAMP');
				$img_thumbnail_url['esp'] = env('APP_URL').$img_thumbnail['esp'].'?u='.env('APP_ASSET_TIMESTAMP');
			  }

			  $data_attr = 'data-img_url_eng="'.$img_url['eng'].'" ';
			  $data_attr.= 'data-img_url_esp="'.$img_url['esp'].'" ';
			  $data_attr.= 'data-img_thumbnail_url_eng="'.$img_thumbnail_url['eng'].'" ';
			  $data_attr.= 'data-img_thumbnail_url_esp="'.$img_thumbnail_url['esp'].'" ';
			  ?>

				<div class="column is-4 is-flex tile-container lang-container" {!! $data_attr !!}>

					<div class="tile has-lang">

					  <a class="zoom"
					  href="{{ $img_url['eng'] }}"
					  data-title="Main - {{ $p['name'].' '.$p['name_code_group'].' '.$p['name_code'] }}"
					  data-lightbox="{{ $p['name'] .'-'. $p['name_code_group'] .'-'. $p['name_code'] .'-'. $pi['id'] }}-zoom">
						  <i class="fas fa-search-plus"></i>
					  </a>

					  <div class="img-container">
						<a href="{{ $img_url['eng'] }}"
						data-title="Main - {{ $p['name'].' '.$p['name_code_group'].' '.$p['name_code'] }}"
						data-lightbox="{{ $p['name_code'] }}-images">
							<img src="{{ $img_thumbnail_url['eng'] }}"
							alt="Vcare Earth {{ $pi['title'] }}">
						</a>
					  </div>
					  {{-- <h1>{{ $p['name'] }}</h1> --}}
					</div>


					<div class="tile has-no-lang"  style="display: none;">
					  <div class="img-container">
						  <img alt="Vcare Earth Logo" src="/files/logos/vcareplaceholder.svg?u={{ env('APP_ASSET_TIMESTAMP') }}"/>
						  <h2 class="subtitle has-text-centered coming-soon-text">Coming Soon</h2>
					  </div>
					</div>

				</div>
			  @endforeach
			  @endif























			  {{-- 3d view --}}
			  @if (count($p['images-3d-view']))
			  <h2 class="subtitle is-6 tab-content-title"> 3D VIEW </h2>

			  @foreach($p['images-3d-view'] as $pi)
			  <?php
			  $langs = ['eng'=>'','esp'=>''];
			  $img = $langs;
			  $img_thumbnail = $langs;
			  $img_url = $langs;
			  $img_thumbnail_url = $langs;

			  $prod_path = '/files/products/'.$p['public_id'];

			  $img_path = '/images/'.$pi['type'].'/'.$pi['filename'].'.'.$pi['ext'];
			  $img_thumbnail_path = '/images/thumbnails/'.$pi['type'].'/'.$pi['filename'].'.'.$pi['ext_thumb'];

			  $img['eng'] = $prod_path.$img_path;
			  $img_thumbnail['eng'] = $prod_path.$img_thumbnail_path;
			  $img_url['eng'] = env('APP_URL').$img['eng'].'?u='.env('APP_ASSET_TIMESTAMP');
			  $img_thumbnail_url['eng'] = env('APP_URL').$img_thumbnail['eng'].'?u='.env('APP_ASSET_TIMESTAMP');

			  $img['esp'] = $prod_path.'/lang/esp'.$img_path;
			  $img_thumbnail['esp'] = $prod_path.'/lang/esp'.$img_thumbnail_path;
			  if(File::exists(public_path().$img['esp']) && File::exists(public_path().$img_thumbnail['esp'])) {
				$img_url['esp'] = env('APP_URL').$img['esp'].'?u='.env('APP_ASSET_TIMESTAMP');
				$img_thumbnail_url['esp'] = env('APP_URL').$img_thumbnail['esp'].'?u='.env('APP_ASSET_TIMESTAMP');
			  }


			  $data_attr = 'data-img_url_eng="'.$img_url['eng'].'" ';
			  $data_attr.= 'data-img_url_esp="'.$img_url['esp'].'" ';
			  $data_attr.= 'data-img_thumbnail_url_eng="'.$img_thumbnail_url['eng'].'" ';
			  $data_attr.= 'data-img_thumbnail_url_esp="'.$img_thumbnail_url['esp'].'" ';

			  ?>

				<div class="column is-4 is-flex tile-container lang-container" {!! $data_attr !!}>
					<div class="tile has-lang">

						<a class="zoom"
						href="{{ $img_url['eng'] }}"
						data-title="Main - {{ $p['name'].' '.$p['name_code_group'].' '.$p['name_code'] }}"
						data-lightbox="{{ $p['name'] .'-'. $p['name_code_group'] .'-'. $p['name_code'] .'-'. $pi['id'] }}-zoom">
							<i class="fas fa-search-plus"></i>
						</a>

						<div class="img-container">
							{{-- <i class="fas fa-circle-notch fa-spin loading"></i> --}}

							<a href="{{ $img_url['eng'] }}"
							data-title="3D View - {{ $p['name'].' '.$p['name_code_group'].' '.$p['name_code'] }}"
							data-lightbox="{{ $p['name'] .'-'. $p['name_code_group'] .'-'. $p['name_code'] .'-'. $pi['id'] }}-images">
								<img src="{{ $img_thumbnail_url['eng'] }}"
								alt="Vcare Earth {{ $pi['title'] }}">
							</a>

						</div>
						{{-- <h1>{{ $p['name'] }}</h1> --}}
						<hr>
						<a href="{{ $img_url['eng'] }}"
						data-title="3D View - {{ $p['name'].' '.$p['name_code_group'].' '.$p['name_code'] }}"
						data-lightbox="{{ $p['name'] .'-'. $p['name_code_group'] .'-'. $p['name_code'] .'-'. $pi['id'] }}-images2">
						  {{ $pi['title2'] }}
						  {{-- <i class="fas fa-chevron-right ml-1" style="float: right;"></i> --}}
						</a>
					</div>

					<div class="tile has-no-lang"  style="display: none;">
						<div class="img-container">
							<img alt="Vcare Earth Logo" src="/files/logos/vcareplaceholder.svg?u={{ env('APP_ASSET_TIMESTAMP') }}"/>
							<h2 class="subtitle has-text-centered coming-soon-text">Coming Soon</h2>
						</div>
						{{-- <h1>{{ $p['name'] }}</h1> --}}
						<hr>
						<a>
						  {{ $pi['title2'] }}
						  {{-- <i class="fas fa-chevron-right ml-1" style="float: right;"></i> --}}
						</a>
					</div>

				</div>
			  @endforeach
			  @endif


























			  {{-- package details --}}
			  @if (count($p['images-package-details']))
			  <h2 class="subtitle is-6 tab-content-title"> PACKAGE DETAILS </h2>
			  @foreach($p['images-package-details'] as $pi)

			  <?php

				$langs = ['eng'=>'','esp'=>''];
				$img = $langs;
				$img_url = $langs;
				$img_thumbnail = $langs;
				$img_thumbnail_url = $langs;

				$img_expanded = $langs;
				$img_expanded_url = $langs;

				// Check pivot table for expanded image
				if($pi['product_details_expanded_image'])
				$img_expanded_file = $pi['product_details_expanded_image']['filename'].'.'.$pi['product_details_expanded_image']['ext'];
				else
				$img_expanded_file = '';

				$prod_path = '/files/products/'.$p['public_id'];

				$img_path = '/images/package-details/'.$pi['filename'].'.'.$pi['ext'];
				$img_thumbnail_path = '/images/thumbnails/'.$pi['type'].'/'.$pi['filename'].'.'.$pi['ext_thumb'];
				$img_expanded_path = '/images/package-details-expanded/'.$img_expanded_file;

				$img['eng'] = $prod_path.$img_path;
				$img_thumbnail['eng'] = $prod_path.$img_thumbnail_path;
				$img_expanded['eng'] = $prod_path.$img_expanded_path;

				$img_url['eng'] = env('APP_URL').$img['eng'].'?u='.env('APP_ASSET_TIMESTAMP');
				$img_thumbnail_url['eng'] = env('APP_URL').$img_thumbnail['eng'].'?u='.env('APP_ASSET_TIMESTAMP');
				$img_expanded_url['eng'] = env('APP_URL').$img_expanded['eng'].'?u='.env('APP_ASSET_TIMESTAMP');

				$img['esp'] = $prod_path.'/lang/esp'.$img_path;
				$img_thumbnail['esp'] = $prod_path.'/lang/esp'.$img_thumbnail_path;
				$img_expanded['esp'] = $prod_path.'/lang/esp'.$img_expanded_path;

				if(
					File::exists(public_path().$img['esp']) &&
					File::exists(public_path().$img_thumbnail['esp']) &&
					File::exists(public_path().$img_expanded['esp'])
				) {
					$img_url['esp'] = env('APP_URL').$img['esp'].'?u='.env('APP_ASSET_TIMESTAMP');
					$img_thumbnail_url['esp'] = env('APP_URL').$img_thumbnail['esp'].'?u='.env('APP_ASSET_TIMESTAMP');
					$img_expanded_url['esp'] = env('APP_URL').$img_expanded['esp'].'?u='.env('APP_ASSET_TIMESTAMP');
				}


				$data_attr = 'data-img_url_eng="'.$img_url['eng'].'" ';
				$data_attr.= 'data-img_url_esp="'.$img_url['esp'].'" ';
				$data_attr.= 'data-img_thumbnail_url_eng="'.$img_thumbnail_url['eng'].'" ';
				$data_attr.= 'data-img_thumbnail_url_esp="'.$img_thumbnail_url['esp'].'" ';
				$data_attr.= 'data-img_expanded_url_eng="'.$img_expanded_url['eng'].'" ';
				$data_attr.= 'data-img_expanded_url_esp="'.$img_expanded_url['esp'].'" ';

			  ?>

				<div class="column is-4 is-flex tile-container lang-container" {!! $data_attr !!}>
					<div class="tile has-lang">

						{{-- <a class="zoom"
						href="{{ $img_url['eng'] }}"
						data-title="Main - {{ $p['name'].' '.$p['name_code_group'].' '.$p['name_code'] }}"
						data-lightbox="{{ $p['name'] .'-'. $p['name_code_group'] .'-'. $p['name_code'] .'-'. $pi['id'] }}-zoom">
							<i class="fas fa-search-plus"></i>
						</a> --}}


						<div class="img-container">
						  <img src="{{ $img_thumbnail_url['eng'] }}" alt="Vcare Earth {{ $pi['title'] }}">
						  {{-- <a href="{{ $img_url['eng'] }}"
						  data-title="Package Details - {{ $p['name'].' '.$p['name_code_group'].' '.$p['name_code'] }}"
						  data-lightbox="{{ $p['name'] .'-'. $p['name_code_group'] .'-'. $p['name_code'] .'-'. $pi['id'] }}-images">
							<img src="{{ $img_thumbnail_url['eng'] }}" alt="Vcare Earth {{ $pi['title'] }}">
						  </a> --}}

						</div>
						<h2>{{ $pi['title2'] }}</h2>

						{{-- if no expanded image --}}
						@if($img_expanded_file)
						<hr>
						<a href="{{ $img_expanded_url['eng'] }}" class="has-expanded" target="_blank">
						  FULL VIEW <i class="fas fa-chevron-right ml-1" style="float: right;"></i>
						</a>
						@endif
					</div>


					<div class="tile has-no-lang"  style="display: none;">
						<div class="img-container">
							<img alt="Vcare Earth Logo" src="/files/logos/vcareplaceholder.svg?u={{ env('APP_ASSET_TIMESTAMP') }}"/>
							<h2 class="subtitle has-text-centered coming-soon-text">Coming Soon</h2>
						</div>
						<h2>{{ $pi['title2'] }}</h2>
						{{-- <hr>
						<a>
						  {{ $pi['title2'] }} <i class="fas fa-chevron-right ml-1" style="float: right;"></i>
						</a> --}}
					</div>

				</div>
			  @endforeach
			  @endif






















			  {{-- Datasheets --}}
				<?php

				$langs = ['eng'=>'','esp'=>''];
				$img = $langs;
				$img_url = $langs;

				$img['eng'] = public_path(). '/files/products/'.$p['public_id'].'/datasheets/datasheet';
				$img['esp'] = public_path(). '/files/products/'.$p['public_id'].'/lang/esp/datasheets/datasheet';

				if(File::exists($img['eng']))
				$img_url['eng'] = route('datasheet',['product_code'=>$p['code']]);
				if(File::exists($img['esp']))
				$img_url['esp'] = route('datasheetLanguage',['product_code'=>$p['code'],'language'=>'esp']);

				$data_attr = 'data-img_url_eng="'.$img_url['eng'].'" ';
				$data_attr.= 'data-img_url_esp="'.$img_url['esp'].'" ';

			  	?>

				@if (File::exists($img['eng']))
				<h2 class="subtitle is-6 tab-content-title"> DATASHEET </h2>
				<div class="column is-4 is-flex tile-container lang-container" {!! $data_attr !!}>
				  <div class="tile has-lang">
						<div class="img-container">

							<a href="{{ $img_url['eng'] }}" target="_blank">
							  <i class="far fa-file-alt"></i>
							</a>

						</div>
						<hr>
						<a href="{{ $img_url['eng'] }}" target="_blank">
						  View <i class="fas fa-chevron-right ml-1" style="float: right;"></i>
						</a>
						{{-- <h1>{{ $p['name'] }}</h1>
						<hr>
						<a href="{{ $img_url['eng'] }}" target="_blank">
						  Datasheet
						</a> --}}
					</div>


					<div class="tile has-no-lang"  style="display: none;">
						<div class="img-container">
							  {{-- <i class="far fa-times-circle"></i> --}}
							<img alt="Vcare Earth Logo" src="/files/logos/vcareplaceholder.svg?u={{ env('APP_ASSET_TIMESTAMP') }}"/>
							<h2 class="subtitle has-text-centered coming-soon-text">Coming Soon</h2>
						</div>
						{{-- <h1>{{ $p['name'] }}</h1>
						<hr>
						<a href="{{ $img_url['eng'] }}"target="_blank">
						  Datasheet
						</a> --}}
					</div>

				</div>
			  @endif




























			  {{-- Manuals --}}
			  @if ($cat['parent']['code']=='covid-19-testing')
			  <h2 class="subtitle is-6 tab-content-title"> MANUAL </h2>
				  <?php

				  $langs = ['eng'=>'','esp'=>''];
				  $img = $langs;
				  $img_url = $langs;

				  $img['eng'] = public_path(). '/files/products/'.$p['public_id'].'/manuals/manual';
				  $img['esp'] = public_path(). '/files/products/'.$p['public_id'].'/lang/esp/manuals/manual';

				  if(File::exists($img['eng']))
				  $img_url['eng'] = route('manual',['product_code'=>$p['code']]);
				  if(File::exists($img['esp']))
				  $img_url['esp'] = route('manualLanguage',['product_code'=>$p['code'],'language'=>'esp']);

				  $data_attr = 'data-img_url_eng="'.$img_url['eng'].'" ';
				  $data_attr.= 'data-img_url_esp="'.$img_url['esp'].'" ';

				?>
				<div class="column is-4 is-flex tile-container lang-container" {!! $data_attr !!}>

					<div class="tile has-lang" {!! ($img_url['eng'])?'':'style="display: none;"' !!}>
						<div class="img-container">
						  <a href="{{ $img_url['eng'] }}" target="_blank">
							<i class="far fa-file-alt"></i>
						  </a>
						</div>
						{{-- <h1>{{ $p['name'] }}</h1> --}}
						<hr>
						<a href="{{ $img_url['eng'] }}" target="_blank">
						  View <i class="fas fa-chevron-right ml-1" style="float: right;"></i>
						</a>
					</div>


					<div class="tile has-no-lang" {!! ($img_url['eng'])?'style="display: none;"':'' !!}>
						<div class="img-container">
							<img alt="Vcare Earth Logo" src="/files/logos/vcareplaceholder.svg?u={{ env('APP_ASSET_TIMESTAMP') }}"/>
							<h2 class="subtitle has-text-centered coming-soon-text">Coming Soon</h2>
						</div>
						{{-- <h1>{{ $p['name'] }}</h1>
						<hr>
						<a href="{{ $img_url['eng'] }}" target="_blank">
						  Manual
						</a> --}}
					</div>

				</div>
			  @endif

















			  {{-- if no images or anything then show below --}}
			  @if (!count($p['images-main']) && !count($p['images-3d-view']) && !count($p['images-package-details']) && !File::exists('files/products/'.$p['public_id'].'/datasheets/datasheet'))
			  <div class="name-group-title">
				<h1 class="title product-name cat-par">
				  @if (!$p['name'])
				  {{ strtoupper($cat_par['name']) }}
				  @endif
				</h1>
			  </div>
			  <h1 class="subtitle is-6 tab-content-title mt-0"> &nbsp; </h1>

			  <div class="column">
				<div class="coming-soon-content">
				  <div class="banner" style="background-image: url(/files/single-product/pricelist/pricelistbanner.jpg)">
					<img alt="VCare.Earth Respirators, Face Masks, Covid19 Rapid Test" 
					class="ico-image" src="/files/single-product/comingsoon-wgown.png{{ '?u='.env('APP_ASSET_TIMESTAMP') }}"/>
				  </div>
				  <img alt="Vcare Earth Logo" class="ico-image" src="/files/logos/vcareicon.svg{{ '?u='.env('APP_ASSET_TIMESTAMP') }}"/>

				  <h1 class="subtitle is-4 mb-1 pb-1 pt-6" style="text-align: center;">
					Product's resources are coming soon!
				  </h1>
				</div>
			  </div>
			  @endif
