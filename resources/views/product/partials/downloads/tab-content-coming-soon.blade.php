
		<div id="{{ $code }}" class="tab-content coming-soon">
				<div class="columns is-multiline">
			
					{{-- <div class="name-group-title">
						<h1 class="subtitle name-code-group">
								{{ $p['category']['name'] }} 
						</h1>
						<h1 class="subtitle name-type-code is-5" >{{ $p['name_type'] .' '. $p['name_code'] }}</h1>

						<h1 class="title product-name">{{ strtoupper($p['name']) }} </h1>
					</div> --}}

				{{-- <div class="name-group-title">
					<h1 class="title product-name" style="margin-top: -10px;">{{ strtoupper($name) }}</h1>
				</div> --}}

				<div class="name-group-title">
					<h2 class="subtitle name-code-group">
							{{ $name }} 
					</h2>
					<h2 class="subtitle name-type-code is-5" >{{ $name_type }}</h2>


					<h2 class="title product-name" style="">Coming Soon</h2>
				</div>


				<h2 class="subtitle is-6 tab-content-title mt-0"> &nbsp; </h2>
				<div class="column">
					<div class="coming-soon-content">
						<div class="banner" style="background-image: url(/files/single-product/pricelist/pricelistbanner.jpg)">
							<img alt="VCare.Earth Respirators, Face Masks, Covid19 Rapid Test" 
							src="/files/single-product/comingsoon-wgown.png{{ '?u='.env('APP_ASSET_TIMESTAMP') }}"/>
						</div>
						<img alt="VCare Earth Logo" class="ico-image" src="/files/logos/vcareicon.svg{{ '?u='.env('APP_ASSET_TIMESTAMP') }}"/>
						
						<h2 class="subtitle is-3 mb-1 pb-1 pt-6" style="text-align: center;">
							This product is coming soon!
						</h2>
						<h2 class="subtitle is-6 mt-3 has-text-centered let-you-know">
							Will let you know once product is availble.
						</h2>
					</div>
				</div>
	
		
				</div>
			</div>
	
	