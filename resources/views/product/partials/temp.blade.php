
{{-- 

<h1 class="title has-text-centered mt-6"> TEMP.BLADE </h1>
<h1 class="title has-text-centered"> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx </h1>
<h1 class="title has-text-centered"> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx </h1>
<h1 class="title has-text-centered mb-6"> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx </h1>



<section class="section mb-0 mt-6 pb-3">
    <div class="container">
        <div class="columns is-vcentered features">
            <div class="column has-text-centered">
                <h4 class="title is-4 mb-6">Intended Use</h4>
                <p>
                    Surgical Gowns are designed to protect the wearer from the spread of infection or illness if the wearer comes in contact with potentially infectious liquid and solid material. They also help prevent the gown wearer from transferring microorganisms that could harm patients.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section features_benefits">
    <div class="container">
        <div class="columns is-vcentered features reverse-flow-mobile">
            <div class="column">
                <h5 class="title is-5 mb-1">Directions For Use</h5>
                <P>Surgical Gowns are "book" folded for quick and easy sterile donning process. </P>
                <br>
                <ol>
                    <li>Pick up the gown grasping at the single fold below the neck. Place the fingers of each hand under the side flaps of the arm holes. Open gown laterally.</li>
                    <li>Allow the body of the gown to fall open. Extend arms into the sleeves.</li>
                    <li>Have assistant fasten inside rear waist ties and then fasten neck closure.</li>
                    <li>Grasp top portion of the transfer card with the right hand and remove the left side tie with the left hand.</li>
                    <li>Hand the bottom portion of the transfer card to assistant.</li>
                    <li>Turn to the left, pulling the waist tie from the transfer card held by assistant. Tie the waist ties at the left side of the gown.</li>
                </ol>
                
            </div>
            <div class="column">
                <div class="features-image" style="background-image: url(/files/single-product/gown/surgicalgown-reinforced.svg);"></div>
            </div>
        </div>
    </div>
</section>










<section class="section features_benefits">
    <div class="container">
        <div class="columns features">
            <div class="column">
                <h5 class="title is-5">Precaution</h5>
                <ol>
                    <li>Package must be inspected prior to use to assure there are no damage has occurred. The package is critical component to ensure sterility of the gown.</li>
                    <li>Do not use the gown if package is damaged.</li>
                    <li>Do not alter, repair, wash, and abuse the gown.</li>
                    <li>Do not reuse the gown. Dispose used gown in accordance with applicable regulations.</li>
                </ol>                
            </div>
            <div class="column">
                <h5 class="title is-5">Storage and Transporation</h5>
                <p>
                    Store within temperature range of -10°C to +30°C and at less than 80% relative humidity. Avoid direct sunlight exposure. When storing or transporting this product use original packaging provided.
                </p>
            </div>
        </div>
    </div>
</section>



<section class="section packaging gown">
    <div class="container">
        <h4 class="title is-4 mb-6">PACKAGING</h4>
        <div class="columns">
            <div class="column is-flex">
                <div class="package">
                    <a href="/files/products/74/images/3d-view/srgwn-dawn-pp_bag.png" data-lightbox="COVID19 Test Kit 2S DAWN PP Bag">
                        <img src="/files/products/74/images/thumbnails/3d-view/srgwn-dawn-pp_bag.png" alt="COVID19 Test Kit 2S DAWN PP Bag" />
                    </a>
                </div>
            </div>
            <div class="column is-flex">
                <div class="package">
                    <a href="/files/products/74/images/3d-view/srgwn-dawn-master_box.png" data-lightbox="COVID19 Test Kit 2S DAWN Middle Box">
                        <img src="/files/products/74/images/thumbnails/3d-view/srgwn-dawn-master_box.png" alt="COVID19 Test Kit 2S DAWN Middle Box" />
                    </a>
                </div>
            </div>
        </div>

        <div class="columns is-centered mt-6">
            <div class="column is-narrow has-text-centered-mobile">
                <a class="button  is-medium is-outlined vcare-btn" target="_blank" href="/downloads#srgwn-dawn">
                    <span>View Packaging Gallery</span>
                    <span class="icon">
                        <i class="fas fa-chevron-right" aria-hidden="true"></i>
                    </span>
                </a>
            </div>
        </div>
    </div>
</section>







<h1 class="title has-text-centered mt-6"> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx </h1>
<h1 class="title has-text-centered "> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx </h1>
<h1 class="title has-text-centered "> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx </h1>
<h1 class="title has-text-centered mb-6"> TEMP.BLADE </h1> --}}





{{-- 

<h1 class="title has-text-centered mt-6"> TEMP.BLADE </h1>
<h1 class="title has-text-centered"> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx </h1>
<h1 class="title has-text-centered"> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx </h1>
<h1 class="title has-text-centered mb-6"> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx </h1>



<section class="section respirator-comparison">
    <div class="container">
        <h4 class="title is-4 mb-6">RESPIRATOR COMPARISON</h4>
        <div class="columns">
            <div class="column">

                <table class="table is-fullwidth is-hoverable hide-on-mobile">
                    <thead>
                        <tr>
                            <th></th>
                            <th>FFP1</th>
                            <th>FFP2</th>
                            <th>FFP3</th>
                            <th>MASK</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>Protection</th>
                            <td><p>Respirator protects the person wearing it, but not the surrounding people.</p></td>
                            <td><p>Respirator protects the person wearing it and the surrounding people.</p></td>
                            <td><p>Heavy duty protection for everyone.</p></td>
                            <td><p>Basic mask, and people wear it when they are experiencing mild symptoms.</p></td>
                        </tr>
                        <tr>
                            <th>Dust Reduction</th>
                            <td><p>By 4</p></td>
                            <td><p>By 10</p></td>
                            <td><p>By 20</p></td>
                            <td><p>None</p></td>
                        </tr>
                        <tr>
                            <th>Prevents</th>
                            <td><p>It stops the aerosol and small water droplets which often contain most of the virus.</p></td>
                            <td><p>Protects against solid and liquid irritating aerosols.</p></td>
                            <td><p>Advisable for high levels of airborne particles and toxic conditions.</p></td>
                            <td><p>It does not protect you from others around you who might have symptoms or a viral infection.</p></td>
                        </tr>
                    </tbody>
                </table>

                <div class="hide-on-desktop ">
                    <table class="table is-fullwidth is-hoverable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>FFP1</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Protection</th>
                                <td><p>Respirator protects the person wearing it, but not the surrounding people.</p></td>
                            </tr>
                            <tr>
                                <th>Dust Reduction</th>
                                <td><p>By 4</p></td>
                            </tr>
                            <tr>
                                <th>Prevents</th>
                                <td><p>It stops the aerosol and small water droplets which often contain most of the virus.</p></td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table is-fullwidth is-hoverable mt-6">
                        <thead>
                            <tr>
                                <th></th>
                                <th>FFP2</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Protection</th>
                                <td><p>Respirator protects the person wearing it and the surrounding people.</p></td>
                            </tr>
                            <tr>
                                <th>Dust Reduction</th>
                                <td><p>By 10</p></td>
                            </tr>
                            <tr>
                                <th>Prevents</th>
                                <td><p>Protects against solid and liquid irritating aerosols.</p></td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table is-fullwidth is-hoverable mt-6">
                        <thead>
                            <tr>
                                <th></th>
                                <th>FFP3</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Protection</th>
                                <td><p>Heavy duty protection for everyone.</p></td>
                            </tr>
                            <tr>
                                <th>Dust Reduction</th>
                                <td><p>By 20</p></td>
                            </tr>
                            <tr>
                                <th>Prevents</th>
                                <td><p>Advisable for high levels of airborne particles and toxic conditions.</p></td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table is-fullwidth is-hoverable mt-6">
                        <thead>
                            <tr>
                                <th></th>
                                <th>MASK</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Protection</th>
                                <td><p>Basic mask, and people wear it when they are experiencing mild symptoms.</p></td>
                            </tr>
                            <tr>
                                <th>Dust Reduction</th>
                                <td><p>None</p></td>
                            </tr>
                            <tr>
                                <th>Prevents</th>
                                <td><p>It does not protect you from others around you who might have symptoms or a viral infection.</p></td>
                            </tr>
                        </tbody>
                    </table>
                </div>



            </div>
        </div>

        <div class="columns is-centered mt-6">
            <div class="column is-narrow has-text-centered-mobile">
                <a class="button is-outlined vcare-btn is-medium" target="_blank" href="/vcare-detailed-comparison-for-masks-pdf">
                    <span>Detailed Comparison</span>
                    <span class="icon">
                        <i class="fas fa-chevron-right" aria-hidden="true"></i>
                    </span>
                </a>
            </div>
        </div>
    </div>
</section>



<h1 class="title has-text-centered mt-6"> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx </h1>
<h1 class="title has-text-centered "> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx </h1>
<h1 class="title has-text-centered "> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx </h1>
<h1 class="title has-text-centered mb-6"> TEMP.BLADE </h1> --}}

















<h1 class="title has-text-centered mt-6"> TEMP.BLADE </h1> 
<h1 class="title has-text-centered mb-6"> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx </h1>





<h1 class="title has-text-centered mt-6"> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx </h1>
<h1 class="title has-text-centered mb-6"> TEMP.BLADE </h1> 