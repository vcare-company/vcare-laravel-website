
<section id="title" class="section">
    <div class="container">

        <div class="columns is-vcentered">
            <div class="column is-6 title-column">
                <div class="name-group-title">
                    <h1 class="">
                        <span class="subtitle name-code-group">
                            {{ $product['category']['name'] }} 
                        </span>
                        <span class="subtitle name-type-code is-5" >{{ $product['name_type'] .' '. $product['name_code'] }}</span><br>
                        <span class="title product-name">{{ strtoupper($product['name']) }} </span>
                    </h1>
                </div>
            </div>
            <div class="column is-7 column-files">
                <div class="buttons">
                
                
                @if ($product['category']['parent']['code']=='covid-19-testing' && File::exists('files/products/'.$product['public_id'].'/manuals/manual'))
                <a class="button is-vcare vcare-btn"
                target="_blank"
                href="{{ route('manual',['product_code'=>$product['code']]).'?u='.env('APP_ASSET_TIMESTAMP') }}">
                <span class="icon is-small">
                    <i class="fas fa-file-download"></i>
                </span>
                <span>MANUAL</span>
                </a>
                @endif


                {{-- Datasheets --}}
                {{-- /////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                
                @if (File::exists('files/products/'.$product['public_id'].'/lang/esp/datasheets/datasheet'))

                <span class="button is-vcare vcare-btn show-modal" data-target="#datasheet-languages">
                <span class="icon is-small">
                    <i class="fas fa-file-download"></i>
                </span>
                <span>DATASHEET</span>
                </span>

                <div class="modal modal-fx-fadeInScale" id="datasheet-languages">
                    <div class="modal-background"></div>
                    <div class="modal-content">

                        <span class="close-modal">
                            <i class="fas fa-times-circle"></i>
                        </span>

                        <div class="box has-text-centered px-6 py-6">
        
                            {{-- <img class="vcare-logo" style="width: 200px;"
                            src="/files/logos/vcare-earth-logo.svg?u=20200922" 
                            alt="VCare.Earth Logo"> --}}
                            
                            <h1 class="title mb-6 mt-0">Datasheets</h1>

                            <a class="button is-vcare vcare-btn mx-1"
                            target="_blank"
                            href="{{ route('datasheet',['product_code'=>$product['code']]).'?u='.env('APP_ASSET_TIMESTAMP')  }}">
                            <span class="icon is-small">
                                <i class="fas fa-file-download"></i>
                            </span>
                            <span>ENGLISH</span>
                            </a>
            
                            <a class="button is-vcare vcare-btn mx-1"
                            target="_blank"
                            href="{{ route('datasheetLanguage',['product_code'=>$product['code'],'language'=>'esp']).'?u='.env('APP_ASSET_TIMESTAMP')  }}">
                            <span class="icon is-small">
                                <i class="fas fa-file-download"></i>
                            </span>
                            <span>ESPAÑOL</span>
                            </a>
            
                        </div>
                    </div>
                    
                </div>                        

                @elseif (File::exists('files/products/'.$product['public_id'].'/datasheets/datasheet'))
                <a class="button is-vcare vcare-btn"
                target="_blank"
                href="{{ route('datasheet',['product_code'=>$product['code']]).'?u='.env('APP_ASSET_TIMESTAMP')  }}">
                <span class="icon is-small">
                    <i class="fas fa-file-download"></i>
                </span>
                <span>DATASHEET</span>
                </a>
                @endif
                
                {{-- /////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                {{-- Datasheets --}}
                

                <a class="button is-vcare vcare-btn"
                target="_blank"
                href="{{ route('single-product-certificates',['product_code'=>$product['code']]) }}">
                <span class="icon is-small">
                    <i class="fas fa-file-download"></i>
                </span>
                <span>CERTIFICATES</span>
                </a>            

                <a class="button is-vcare vcare-btn"
                target="_blank"
                href="{{ route('single-product-pricelist',['product_code'=>$product['code']]) }}">
                <span class="icon is-small">
                    <i class="fas fa-file-download"></i>
                </span>
                <span>PRICELIST</span>
                </a>            



                <a class="button is-vcare vcare-btn"
                target="_blank"
                href="{{ route('contact') .'?type=b2b&product='.$product['code'] }}">
                <span class="icon is-small">
                    <i class="far fa-envelope"></i>
                </span>
                <span>ENQUIRE</span>
                </a>            


                <a class="button is-vcare vcare-btn"
                target="_blank"
                href="{{ route('downloads') .'#'.$product['code'] }}">
                <span class="icon is-small">
                    <i class="far fa-images"></i>
                </span>
                <span>PACKAGE GALLERY</span>
                </a>            
                    
                
                </div>

            </div>
        </div>    
        

    </div>
</section>



<section class="section section-bg1">
    <div class="container">
        <div class="columns">
            <div class="column is-6">
                <div class="image-container">

                    @if ($product['main-image'])
                    <div class="main-image">
                        
                        <a href="{{ env('APP_URL').'/files/products/'.$product['public_id'].'/images/'.$product['main-image']['type'].'/'. $product['main-image']['filename'].'.'.$product['main-image']['ext'].'?u='.env('APP_ASSET_TIMESTAMP') }}" 
                        data-lightbox="{{ $product['main-image']['title'] }}">
                            <i class="fas fa-search-plus"></i>
                            <img src="{{ env('APP_URL').'/files/products/'.$product['public_id'].'/images/thumbnails/'.$product['main-image']['type'].'/'.$product['main-image']['filename'].'.'.$product['main-image']['ext'].'?u='.env('APP_ASSET_TIMESTAMP') }}" 
                            alt="Vcare Earth {{ $product['main-image']['title'] }}">
                        </a>
                    </div>
                    @else
                        {{-- <img class="img-placeholder" src="/files/logos/vcareicon.svg"/> --}}
                        <img class="img-placeholder" src="/files/logos/vcareplaceholder.svg"/>
                        <h1 class="subtitle has-text-centered coming-soon-text">Coming Soon</h1>
                    @endif


                    @if (count($product['images'])>1)
                    <div class="images">
                        @foreach ($product['images'] as $key => $img )
                        <div>
                            <img 
                            src="{{ env('APP_URL') }}/files/products/{{ $product['public_id'] }}/images/thumbnails/{{ $img['type'] }}/{{ $img['filename'].'.'.$img['ext'].'?u='.env('APP_ASSET_TIMESTAMP') }}" 
                            data-src="{{ env('APP_URL') }}/files/products/{{ $product['public_id'] }}/images/{{ $img['type'] }}/{{ $img['filename'].'.'.$img['ext'].'?u='.env('APP_ASSET_TIMESTAMP') }}"
                            alt="Vcare Earth {{ $img['title'] }}">
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
            <div class="column is-6">

                <div class="tabs overview-certification-tabs">
                    <ul>
                        @if($product['overview'])                      
                        <li class="is-active"><a href="#overview">Overview</a></li>
                        @endif

                        @if($product['certifications'])
                        <li><a href="#certifications">Certifications</a></li>
                        @endif
                    </ul>
                </div>

                <div class="overview-certification-content">
                    @if($product['overview'])
                    <div class="tab-content overview">
                        {{-- {!! $product['overview'] !!} --}}
                        {!! str_replace('%%APP_ASSET_TIMESTAMP%%','?u='.env('APP_ASSET_TIMESTAMP'),$product['overview']) !!}
                    </div>
                    @endif

                    @if($product['certifications'])
                    <div class="tab-content certifications">
                        {{-- {!! $product['certifications'] !!} --}}
                        {!! str_replace('%%APP_ASSET_TIMESTAMP%%','?u='.env('APP_ASSET_TIMESTAMP'),$product['certifications']) !!}
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</section>



{{-- @if(true) --}}
@if(env('APP_TEST_PRODUCT_CONTENT'))
@include('product.partials.temp')
@endif


@if ($product['other-text'])
<section class="section content-text strong-as-block">
    <div class="container">
        <div class="columns is-vcentered">
            <div class="column">
                {!! $product['other-text'] !!}
            </div>
        </div>
    </div>
</section>
@else
{{-- {!! str_replace('%%APP_ASSET_TIMESTAMP%%','?u='.env('APP_ASSET_TIMESTAMP'),$product['html_content']) !!} --}}
<?php
$html_content = str_replace('%%APP_ASSET_TIMESTAMP%%','?u='.env('APP_ASSET_TIMESTAMP'),$product['html_content']);
$html_content = str_replace('%%PUBLIC_ID%%',$product['public_id'],$html_content);
?>
{!! $html_content !!} 

@endif






