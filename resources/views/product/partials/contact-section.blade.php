

<!-- Contact -->
<section class="section inquiry-section">
  <div class="container">
      <div class="columns">
          <div class="column is-half is-offset-one-quarter has-text-centered">
              <h2 class="is-size-2 is-size-5-mobile has-vcare-text font-family-title"> Get in touch with us</h2>
              <p class="mt-4">
                  For inquiries and orders    
              </p>
              <button class="button vcare-button-outlined mt-4">
                  <a href="{{ route('contact').$url }}" target="_blank">
                    <span class="vcare-button-text is-size-7 pr-4">SEND US A MESSAGE</span><i class="fas fa-chevron-right"></i>
                  </a>
              </button>
          </div>
      </div>
  </div>
</section>
<!-- END Contact -->

