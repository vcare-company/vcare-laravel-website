@extends('layout.main')

@section('head-end')
<link rel="stylesheet" href="{{ mix('css/single-product-login-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/single-product-login-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection







@section('content')



<section class="section section-bg1">
    <div class="container">
        <div class="columns is-vcentered is-centered is-multiline height-50vh">
            


            <div class="column is-narrow">

            <div class="login-form">

                @if ($error_message)                
                <div class="notification is-danger is-light error-message" 
                style="text-align: center; padding-left: 20px; padding-right: 20px;">
                    {{ $error_message }}
                </div>    
                @endif    
                
                <form method="POST" action="{{ route('single-product-login-post',['product_code'=>$product_code,'login_type'=>$login_type,]) }}">
                    @csrf
                    <img src="{{ env('APP_URL') }}/files/logos/vcareicon.svg" style="margin: auto; display: block;">
                    
                    @if ($login_type=='pricelist')
                    <h1 class="form-title">LOGIN TO VIEW PRICELIST</h1>
                    @elseif ($login_type=='certificates')
                    <h1 class="form-title">LOGIN TO VIEW CERTIFICATES</h1>
                    @elseif ($login_type=='original_certificates')
                    <h1 class="form-title">LOGIN TO VIEW CERTIFICATES</h1>
                    @elseif ($login_type=='obm_certificates')
                    <h1 class="form-title">LOGIN TO VIEW CERTIFICATES</h1>
                    @else
                    <h1 class="form-title">LOGIN</h1>
                    @endif

                    @if ($login_type=='certificates' || $login_type=='original_certificates' || $login_type=='obm_certificates')
                    <div class="field" style="display: none !important;">
                        <p class="control">
                        <input class="input" value="blank@email.com" name="username" type="text" placeholder="Email">
                        </p>
                    </div>
                    @else
                    <div class="field">
                        <p class="control">
                        <input class="input" name="username" type="text" placeholder="Email">
                        </p>
                    </div>

                    @endif

                    <div class="field">
                        <p class="control">
                        <input class="input" name="password" type="password" placeholder="Password" >
                        </p>
                    </div>
                    <div class="field">
                        <p class="control" style="text-align: center;">
                        <button type="submit" class="button is-outlined submit-btn">
                            SUBMIT
                            <i class="fas fa-chevron-right ml-3" aria-hidden="true"></i>
                        </button>
                        </p>
                    </div>

                </form>
            
            </div>

            </div>            
        </div>
    </div>
</section>




@endsection