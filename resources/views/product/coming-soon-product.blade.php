@extends('layout.main')

@section('head-end')
<link rel="stylesheet" href="{{ mix('css/coming-soon-product-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/coming-soon-product-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection







@section('content')


<section class="section banner-section">
    <div class="banner-section-bg"
    style="background-image: url({{ env('APP_URL').'/files/single-product/pricelist/pricelistbanner.jpg' }})">
    </div>
    <div class="container">
        <div class="columns">
            <div class="column is-12">
                <div class="banner" 
                style="background-image: url({{ env('APP_URL').'/files/single-product/pricelist/pricelistbanner.jpg' }})">
                    <div class="columns is-vcentered">
                        


                        <div class="column" style="position: relative;">
                            <div class="main-image" style="background-image: url(/files/single-product/comingsoon-wgown.png{{ '?u='.env('APP_ASSET_TIMESTAMP') }})"></div>
                        </div>

                    </div>
                </div>                
            </div>
        </div>
    </div>
</section>


<section class="section resources">
    <div class="container">        
        <div class="columns is-multiline is-centered is-vcentered">
            
            <div class="column">
                <div style="display: flex; flex-flow: column; justify-content: center; align-items: center;">
                    <img alt="VCare Earth Logo" class="ico-image" src="/files/logos/vcareicon.svg{{ '?u='.env('APP_ASSET_TIMESTAMP') }}"/>
                    
                    <h1 class="subtitle is-3 mb-1 pb-1 pt-6" style="text-align: center;">
                        This product is coming soon!
                    </h1>
                    <a href="{{ route('all-products') }}">
                        <h1 class="subtitle is-5 mb-3 pt-6 pb-3 px-3 browse-products">
                            In the mean time please browse our other products.
                        </h1>
                    </a>
                </div>
            </div>
    
        </div>
    </div>
</section>


@include('product.partials.contact-section',['url'=>'?type=b2b&product='.$product['code']])


@endsection