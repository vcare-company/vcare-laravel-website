@extends('layout.main')



@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('refund') }}"
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/refund-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/refund-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection


@section('head-title')
<title>Refund Policy | Vcare.earth</title>
@endsection



@section('seo-meta-title')
<?php 
    $meta_title = "VCare.Earth Commitment to Care - Refund" ; 
    $meta_desc = "Our products and services are available to customers worldwide. For any questions on refund matters you can refer on this page.";
?>    
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 





@section('content')

<section class="section section-header">
    <div class="container">
        <div class="columns">
            <div class="column title-column">
                <h1 class="title is-1">Refund Policy </h1>
            </div>
        </div>        
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="columns">
            <div class="column">
                <p class="">
                    VCARE.EARTH is available to customers worldwide. 
                </p>
                <br>
                <p>
                You have the right to revoke this contract within fourteen days without giving reasons. The withdrawal period is fourteen days from the day on which you or a third party designated by you, other than the carrier, have taken possession of the goods. In order to exercise your right of revocation, you must inform us (VCARE HEALTH IBERICA S.L., Tel: +34 637 801 689, E-Mail: info@vcare.earth) by means of a clear statement (e.g. a letter, fax or e-mail sent by post) of your decision to revoke this contract. You may use the attached model revocation form, but this is not mandatory. In order to comply with the revocation period, it is sufficient to send the notification of the exercise of the right of revocation before the end of the revocation period.
                </p>
            </div>
        </div>        
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Consequences of revocation</b></p><br>
        <p>
        If you revoke this Agreement, we shall reimburse you for all payments we have received from you, including delivery charges (except for additional charges resulting from your choosing a different method of delivery from the cheapest standard delivery offered by us), immediately and no later than fourteen days from the date we receive notification of your revocation of this Agreement. For this refund, we will use the same means of payment that you used for the original transaction, unless expressly agreed otherwise with you; in no case will you be charged for this refund. We may refuse to make a refund until we have received the goods back or until you have provided evidence that you have returned the goods, whichever is the earlier. You must return or hand over the goods to us without delay and in any event within fourteen days at the latest from the date on which you notify us of the cancellation of this agreement. This period shall be deemed to have been observed if you send the goods before the expiry of the fourteen-day period. You shall bear the direct costs of returning the goods. You shall only be liable for any loss of value of the goods if this loss of value is due to handling of the goods that is not necessary for testing the condition, properties and functionality of the goods.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Sample cancellation form</b></p><br>
        <p>
        (If you want to cancel the contract, please fill out this form and send it back).
    <br><br>
        To: 
        <br><br>
        VCARE HEALTH IBERICA, S.L. <br>
        Madrid, Spain <br>
        <br>
        I/we (*) hereby revoke the contract concluded by me/us (*) for the purchase of the following <br> goods (*)/the provision of the following service (*) – Ordered on (*)/received on (*)
        <br><br>
        – Name of the consumer(s) <br>
        – Address of the consumer(s)<br> 
        – Signature of the consumer(s) (only in case of communication on paper)<br>
        – Date <br>
        <br>
        ___________ 
        <br>
        (*) Delete as applicable.
        </p>
    </div>
</section>

@endsection