<div id="privacy-policy">
    <div class="notification is-danger">
        <button class="delete close-privacy-policy"></button>
        <p class="mb-3">
            We use cookies to offer you a better browsing experience, analyze site traffic, personalize content. Read about how we use cookies and how you can control them on our Privacy Policy. If you continue to use this site, you consent to our use of cookies.
        </p>
        
        <button class="button vcare-button continue-privacy-policy mr-3 my-1">
            Continue
        </button>
        <a class="button vcare-button my-1" href="/privacy-policy" style="text-decoration: none;">
            <span class="vcare-button-text is-size-7 pr-4">PRIVACY POLICY</span>
            <i class="fas fa-chevron-right" aria-hidden="true"></i>
        </a>
      </div>
</div>