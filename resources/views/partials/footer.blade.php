




<!-- Footer -->

@if (\Request::is('medical-equipment-supplier'))
<footer id="footer" class="footer">
  <div class="content has-text-centered">

      <div class="columns">
          <div class="column is-12 med-sup-footer">

                <a href="{{ route('home') }}">
                    <img class="vcare-logo-med-sup" src="/files/logos/vcare-earth-logo.svg" alt="VCare.Earth Logo"/>
                </a>

                <div class="pt-5">
                    <a href="{{ route('home') }}">Home</a> |
                    <a href="{{ route('about') }}">About Us</a> |
                    <a href="{{ route('all-products') }}">Our Products</a> |
                    <a href="{{ route('downloads') }}">Downloads</a> |
                    <a href="{{ route('charity') }}">Charity</a> 
                </div>
                 <div>   
                    <a href="{{ route('sample') }}">Samples</a> |
                    <a href="{{ route('blog') }}">Blog</a> |
                    <a href="{{ route('faq') }}">FAQ</a> |
                    <a href="{{ route('careers') }}">Careers</a> |
                    <a href="{{ route('contact') }}">Contact Us</a> 
                </div>

                <div class="social-med-sup">

                  <h3>FOLLOW US</h3>
                  <a href="https://linkedin.com/company/vcare-earth"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a>
                  <a href="https://instagram.com/vcare_earth"><i class="fab fa-instagram" aria-hidden="true"></i></a>              
                  <a href="https://twitter.com/VCare_Earth"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                </div>
                <div class="email-address" style="background-image: url(/files/vcare-email.svg);"></div>

          </div>
          <!-- <div class="column is-10">

            <div class="page-links-container">
                <ul class="page-links static-pages">
                  <li><a href="{{ route('home') }}">Home</a></li>
                  <li><a href="{{ route('about') }}">About Us</a></li>
                  <li><a href="{{ route('all-products') }}">Our Products</a></li>
                  <li><a href="{{ route('downloads') }}">Downloads</a></li>
                  <li><a href="{{ route('covid19-statistics') }}">C19 UPDATES</a></li>
                  <li><a href="{{ route('charity') }}">Charity</a></li>
                  <li><a href="{{ route('sample') }}">Samples</a></li>
                  <li><a href="{{ route('blog') }}">Blog</a></li>
                  <li><a href="{{ route('faq') }}">FAQ</a></li>
                  <li><a href="{{ route('careers') }}">Careers</a></li>
                  <li><a href="{{ route('contact') }}">Contact Us</a></li>
</ul>
            </div>

          </div> -->
      </div>

      <div class="columns is-multiline  is-vcentered">
          <div class="column is-12"> <hr class="footer-separator"> </div>
          <div class="column is-9">
              <ul class="page-links-legals">
                  <li><a href="/privacy-policy">Privacy</a></li>
                  <li><a href="/legal">Legal Notice</a></li>
                  <li><a href="/terms">Terms</a></li>
                  <li><a href="/refund-policy">Refund Policy</a></li>
              </ul>
          </div>

          <div class="column is-3 brand-name">
              {{-- <h3>© VCARE.EARTH 2020</h3> --}}
              <h3>© VCARE HEALTH IBERICA, S.L. 2020</h3>
          </div>

      </div>

  </div>
</footer>

@else
<footer id="footer" class="footer">
  <div class="content has-text-centered">

      <div class="columns">
          <div class="column is-2">

                <a href="{{ route('home') }}">
                    <img class="vcare-logo" src="/files/logos/vcare-earth-logo.svg" alt="VCare.Earth Logo"/>
                </a>
                <div class="social">

                  <h3>FOLLOW US</h3>
                  <a href="https://linkedin.com/company/vcare-earth"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a>
                  <a href="https://instagram.com/vcare_earth"><i class="fab fa-instagram" aria-hidden="true"></i></a>              
                  <a href="https://twitter.com/VCare_Earth"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                </div>
                <div class="email-address" style="background-image: url(/files/vcare-email.svg);"></div>
                <div class="social pt-0">
                    <h3 class="mt-5">FEATURED IN</h3>
                    <div class="mt-3">
                        <a href="https://www.elconfidencial.com/espana/coronavirus/2021-01-31/donde-comprar-mascarillas-ffp2-precio-modelos_2926040/" target="_blank">
                            <img src="https://www.ecestaticos.com/file/0be042580f4f0f53813bd70f7984033c/1456834011.svg" alt="" width="50%">
                        </a>
                    </div>
                    <div class="mt-3">
                        <a href="https://www.hola.com/seleccion/20210205183804/mascarillas-ffp2-farmacia/" target="_blank">
                            <img src="https://www.himgs.com/imagenes/hola/comunes/logo.png" alt="" width="45%">
                        </a>
                    </div>

                </div>
          </div>
          <div class="column is-10">

            <div class="page-links-container">
                <ul class="page-links static-pages">
                  <li><a href="{{ route('home') }}">Home</a></li>
                  <li><a href="{{ route('about') }}">About Us</a></li>
                  <li><a href="{{ route('all-products') }}">Our Products</a></li>
                  <li><a href="{{ route('downloads') }}">Downloads</a></li>
                  <li><a href="{{ route('covid19-statistics') }}">C19 UPDATES</a></li>
                  <li><a href="{{ route('charity') }}">Charity</a></li>
                  <li><a href="{{ route('sample') }}">Samples</a></li>
                  <li><a href="{{ route('blog') }}">Blog</a></li>
                  <li><a href="{{ route('faq') }}">FAQ</a></li>
                  <li><a href="{{ route('careers') }}">Careers</a></li>
                  <li><a href="{{ route('contact') }}">Contact Us</a></li>
                </ul>


                <ul class="page-links product-pages">
                    {{-- @foreach ($categories as $cat)
                    @foreach($cat['products'] as $p) --}}

                    @foreach ($parent_categories as $per_cat)
                    <li>
                        <h2 class="label-title">
                            @if(Route::has('cat-'.$per_cat['code']))
                            <a href="{{ route('cat-'.$per_cat['code']) }}">{{ $per_cat['name'] }}</a>
                            @else
                            {{ $per_cat['name'] }}
                            @endif                                
                        </h2>
                        <ul class="cat_code_{{ $per_cat['code'] }}">
                        @foreach ($per_cat['children'] as $cat)            
                            <li>
                                <h2 class="label-only">
                                    @if(Route::has('cat-'.$cat['code']))
                                    <a href="{{ route('cat-'.$cat['code']) }}">{{ $cat['name'] }}</a>
                                    @else
                                    {{ $cat['name'] }}
                                    @endif
                                </h2>
                                
                                @if (count($cat['products']))
                                    @foreach($cat['products'] as $p)
                                        <a href="{{ route('single-product',['product_code'=>$p['code']]) }}">
                                            
                                            {{-- Because boss wants something different for surgical gown --}}
                                            @if ($cat['code']=='surgical-gown')
                                                {{ $p['name'].' '.$p['name_code'].' ('.$p['name_type'].')' }}
                                            @else
                                                {{ $p['name_type'].' '.$p['name'] }} 
                                                @if ($p['name_code'])
                                                    {{ $p['name_code'] }}
                                                @endif                                            
                                            @endif

                                            @if ($p['coming_soon'])
                                                *Coming Soon
                                            @endif
                                        </a>
                                        

                                        {{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
                                        {{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
                                        {{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
                                        {{-- @if(Helper::adminMode())
                                        <a href="{{ route('edit_product_content',['product_code'=>$p['code']]) }}" 
                                        class="ml-3"    
                                        target="_blank">
                                            Edit
                                        </a>
                                        @endif --}}
                                        {{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
                                        {{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}
                                        {{-- dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev dev  --}}


                                    @endforeach
                                @else
                                    <a href="{{ route('coming-soon-product-code',['product_code'=>$cat['code']]) }}" target="_blank">
                                        <span>*Coming Soon</span>
                                    </a>
                                @endif
                                
                            </li>
                        @endforeach
                        </ul>
                    </li>
                    @endforeach
                </ul>
            </div>

          </div>
      </div>

      <div class="columns is-multiline  is-vcentered">
          <div class="column is-12"> <hr class="footer-separator"> </div>
          <div class="column is-9">
              <ul class="page-links-legals">
                  <li><a href="/privacy-policy">Privacy</a></li>
                  <li><a href="/legal">Legal Notice</a></li>
                  <li><a href="/terms">Terms</a></li>
                  <li><a href="/refund-policy">Refund Policy</a></li>
              </ul>
          </div>
          <div class="column is-3 brand-name">
              {{-- <h3>© VCARE.EARTH 2020</h3> --}}
              <h3>© VCARE HEALTH IBERICA, S.L. 2020</h3>
          </div>

      </div>

  </div>
</footer>
@endif