
<nav id="navigation" class="navbar">
    <div class="container">
        <div class="navbar-brand">
            <a href="{{ env('APP_URL') }}" class="navbar-item">
                <img class="vcare-logo" 
                src="/files/logos/vcare-earth-logo.svg?u={{ env('APP_ASSET_TIMESTAMP') }}"
                alt="VCare.Earth Logo"/>
                {{-- <img class="vcare-logo" src="{{ env('APP_URL') }}/files/logos/vcarelogo_old.svg?u={{ env('APP_ASSET_TIMESTAMP') }}"/> --}}
            </a>
        </div>

        <div class="navbar-menu">

            <div class="navbar-end">
                <a href="{{ route('home') }}" class="a-menu is-size-7 navbar-item">HOME</a>
                <a href="{{ route('about') }}" class="a-menu is-size-7 navbar-item">ABOUT</a>
                
        @if (\Request::is('medical-equipment-supplier'))
        @else
                
                <div class="a-menu is-size-7 navbar-item has-submenu">
                    {{-- <span class="submenu-title">PRODUCTS <i class="fas fa-chevron-down ml-3"></i></span> --}}
                    <span class="submenu-title">
                        <a href="{{ route('all-products') }}" class="pb-6">
                            PRODUCTS
                            <i class="fas fa-chevron-down ml-1"></i>
                        </a>
                    </span>
                    <div class="submenu-container">

                    <ul class="submenu">             
                        @foreach ($parent_categories as $per_cat)
                        <li>
                            <h2 class="label-title">
                                @if(Route::has('cat-'.$per_cat['code']))
                                <a href="{{ route('cat-'.$per_cat['code']) }}">{{ $per_cat['name'] }}</a>
                                @else
                                {{ $per_cat['name'] }}
                                @endif                                
                            </h2>
                            <ul class="cat_code_{{ $per_cat['code'] }}">
                                @foreach ($per_cat['children'] as $cat)            
                                <li>
                                    <h2 class="label-only">
                                        @if(Route::has('cat-'.$cat['code']))
                                        <a href="{{ route('cat-'.$cat['code']) }}">{{ $cat['name'] }}</a>
                                        @else
                                        {{ $cat['name'] }}
                                        @endif
                                    </h2>
                                    
                                    @if (count($cat['products']))
                                        @foreach($cat['products'] as $p)
                                            <a href="{{ route('single-product',['product_code'=>$p['code']]) }}">

                                                {{-- {{ $p['name_type'].' '.$p['name'] }}
                                                @if ($p['name_code'])
                                                    <span class="ml-1">{{ $p['name_code'] }}</span> 
                                                @endif
                                                
                                                @if ($p['coming_soon'])
                                                <span class="coming_soon">*Coming Soon</span>
                                                @endif --}}


                                                {{-- Because boss wants something different for surgical gown --}}
                                                @if ($cat['code']=='surgical-gown')
                                                    {{ $p['name'].' '.$p['name_code'].' ('.$p['name_type'].')' }}
                                                @else
                                                    {{ $p['name_type'].' '.$p['name'] }} 
                                                    @if ($p['name_code'])
                                                        {{ $p['name_code'] }}
                                                    @endif                                            
                                                @endif

                                                @if ($p['coming_soon'])
                                                    *Coming Soon
                                                @endif


                                            </a>
                                        @endforeach
                                    @else
                                    <a href="{{ route('coming-soon-product-code',['product_code'=>$cat['code']]) }}">
                                        <span class="coming_soon">*Coming Soon</span>
                                    </a>
                                    @endif
                                    
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        @endforeach

                    </ul>
                    <a class="view-all-products" href="{{ route('all-products') }}">
                        <h2 class="subtitle is-6 label-title">VIEW ALL PRODUCTS</h2>
                    </a>                    
                    </div>
                </div>

                @endif


            
                <a href="{{ route('downloads') }}" class="a-menu is-size-7 navbar-item">DOWNLOADS</a>                  
                <!-- <a href="{{ route('covid19-statistics') }}" class="a-menu is-size-7 navbar-item">C19 UPDATES</a> -->
                <!-- <a href="{{ route('charity') }}" class="a-menu is-size-7 navbar-item">CHARITY</a> -->
                <!-- <a href="{{ route('sample') }}" class="a-menu is-size-7 navbar-item">SAMPLES</a>                   -->
                <a href="{{ route('blog') }}" class="a-menu is-size-7 navbar-item">BLOG</a>
                <!-- <a href="{{ route('faq') }}" class="a-menu is-size-7 navbar-item">FAQ</a> -->
                <!-- <a href="{{ route('careers') }}" class="a-menu is-size-7 navbar-item">CAREERS</a> -->
                <!-- <a href="{{ route('contact') }}" class="a-menu is-size-7 navbar-item">CONTACT US</a> -->
                <div class="a-menu is-size-7 navbar-item has-submenu-2">
                    {{-- <span class="submenu-title">PRODUCTS <i class="fas fa-chevron-down ml-3"></i></span> --}}
                    <span class="submenu-title">
                        <a href="{{ route('contact') }}" class="pb-6">
                            Contact Us
                            <i class="fas fa-chevron-down ml-1"></i>
                        </a>
                    </span>
                    <div class="submenu-container-2">

                    <ul class="submenu-2">        
                        <li>
                            <a class="is-size-7 navbar-item" href="{{ route('contact') }}">
                                Contact
                            </a>
                        </li> 
                        <li>
                            <a class="is-size-7 navbar-item" href="{{ route('sample') }}">
                                Samples
                            </a>
                        </li>     
                        <li>
                            <a class="is-size-7 navbar-item" href="{{ route('careers') }}">
                                Careers
                            </a>
                        </li>

                    </ul>
                                      
                    </div>
                </div>



<!-- 
                <div class="a-menu is-size-7 navbar-item has-dropdown is-hoverable">
                    <a class="" href="{{ route('contact') }}">
                        Contact Us
                        <i class="fas fa-chevron-down ml-1"></i>
                    </a>
                    <div class="navbar-dropdown is-boxed">
                     
                    </div>
                </div> -->
                <span class="a-menu is-size-7 navbar-item search-btn">
                    <i class="fas fa-search"></i>
                </span>
            </div>

            <div class="search-bar">
                <form action="{{ route('search-post') }}" method="POST">
                    @csrf
                    <div class="field has-addons has-addons-centered">
                        <div class="control is-expanded">
                            <input class="input is-small" type="text" placeholder="Search Product" name="search_string" value="">
                        </div>
                        <p class="control">
                          <button class="button vcare-btn search-submit is-small">
                            <i class="fas fa-search"></i>
                          </button>
                        </p>
                      </div>                    
                </form>
            </div>

        </div>
        <span class="navbar-burger burger" data-target="navbarMenuHeroA">
            <span></span>
            <span></span>
            <span></span>
        </span>
    </div>
</nav>

