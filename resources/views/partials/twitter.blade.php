{{-- twitter tiles --}}
<section id="feed-tiles" style="width: 400%; display: none;">
    <h1 style="height: 0 !important; width: 0 !important; display:none !important;">Social Feed</h1>
    <!-- Place <div> tag where you want the feed to appear -->
    <div id="curator-feed-default-feed-layout" style="margin-bottom: -25px; opacity: 0.7;"><a href="https://curator.io" target="_blank" class="crt-logo crt-tag" style="bottom: 30px; color: #aaaaaa;"></a></div>
</section>
<!-- The Javascript can be moved to the end of the html page before the </body> tag -->
<script>
/* curator-feed-default-feed-layout */
(function(){
var i, e, d = document, s = "script";i = d.createElement("script");i.async = 1;
i.src = "https://cdn.curator.io/published/5d83bf8e-fabb-4190-b76b-b71ccd1e05d7.js";
e = d.getElementsByTagName(s)[0];e.parentNode.insertBefore(i, e);

setTimeout(function(){
    $('#feed-tiles').attr('style', '')
}, 5000)

})();
</script>
{{--// twitter tiles --}}
