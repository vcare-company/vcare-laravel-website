
<nav id="navigation" class="navbar">
    <div class="container">
        <div class="navbar-brand">
            <a href="{{ env('APP_URL') }}" class="navbar-item">
                <img src="{{ env('APP_URL') }}/files/logos/vcarelogo.svg"/>
            </a>
        </div>

        <div class="navbar-menu">

            <div class="navbar-end">
                <a href="{{ route('home') }}" class="a-menu is-size-7 navbar-item">HOME</a>
                <div href="#" class="a-menu is-size-7 navbar-item has-submenu">
                    <span class="submenu-title">OUR PRODUCTS <i class="fas fa-chevron-down ml-3"></i></span>
                    <ul class="submenu">
                        <li>
                            <a href="{{ route('all-products') }}">VIEW ALL PRODUCTS</a>
                            <i class="fas fa-chevron-right ml-3"></i>
                        </li>
                        
                        {{-- <li class="label-only">PPE RESPORATORS</li>
                        <li><a href="{{ route('single-product',['product_code'=>'ffp2-nr-1a']) }}">FFP2 1A <i class="fas fa-chevron-right ml-3"></i></a></li>
                        <li><a href="#">LOU <i class="fas fa-chevron-right ml-3"></i></a></li>
                        <li><a href="#">GRANT <i class="fas fa-chevron-right ml-3"></i></a></li>
                        <li><a href="#">MAX <i class="fas fa-chevron-right ml-3"></i></a></li>
                        <li><a href="#">ROSS <i class="fas fa-chevron-right ml-3"></i></a></li>
                        <li><a href="#">DEE <i class="fas fa-chevron-right ml-3"></i></a></li>
                        <li><a href="#">FRETT <i class="fas fa-chevron-right ml-3"></i></a></li>

                        <li class="label-only">COVID-19 TEST</li>
                        <li><a href="#">FINN <i class="fas fa-chevron-right ml-3"></i></a></li>

                        <li class="label-only">HEAD SILICON CLIP</li>
                        <li><a href="#">GAIL <i class="fas fa-chevron-right ml-3"></i></a></li> --}}



                        @foreach ($categories as $cat)
                            @if (count($cat['products']))
                                <li class="label-only">
                                    {{ $cat['name'] }}
                                </li>
                                @foreach($cat['products'] as $p)
                                    <li>
                                        <a href="{{ route('single-product',['product_code'=>$p['code']]) }}">
                                            {{ $p['name'] }}
                                            @if ($p['name_code_group'] && $p['name_code'])
                                                <span class="ml-1 box-code">{{ $p['name_code_group'] }} - {{ $p['name_code'] }}</span> 
                                            @elseif ($p['name_code'])
                                                <span class="ml-1 box-code">{{ $p['name_code'] }}</span> 
                                            @elseif ($p['name_code_group'])
                                                <span class="ml-1 box-code">{{ $p['name_code_group'] }}</span> 
                                            @endif
                                        </a>
                                        <i class="fas fa-chevron-right ml-3"></i>
                                    </li>
                                @endforeach
                            @endif
                        @endforeach

                    </ul>
                </div>
            
                <a href="{{ route('charity') }}" class="a-menu is-size-7 navbar-item">CHARITY</a>
                <a href="{{ route('downloads') }}" class="a-menu is-size-7 navbar-item">DOWNLOADS</a>                  
                <a href="{{ route('faq') }}" class="a-menu is-size-7 navbar-item">FAQs</a>
                <a href="{{ route('about') }}" class="a-menu is-size-7 navbar-item">ABOUT</a>
                <a href="{{ route('contact') }}" class="a-menu is-size-7 navbar-item">CONTACT US</a>
            </div>
        </div>
        <span class="navbar-burger burger" data-target="navbarMenuHeroA">
            <span></span>
            <span></span>
            <span></span>
        </span>
    </div>
</nav>

