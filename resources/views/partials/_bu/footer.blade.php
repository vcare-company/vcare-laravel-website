

<!-- Footer -->
<footer id="footer" class="footer">
  <div class="content has-text-centered">

      <div class="columns">
          <div class="column is-4">

              <img src="{{ env('APP_URL') }}/files/logos/vcarelogo.svg"/>

          </div>
          <div class="column is-5">
              <ul class="page-links">
                  <li><a href="{{ route('about') }}">About Us</a></li>
                  <li><a href="{{ route('charity') }}">Charity</a></li>
                  <li><a href="{{ route('downloads') }}">Downloads</a></li>
                  <li><a href="{{ route('faq') }}">FAQs</a></li>
                  <li><a href="{{ route('contact') }}">Contact Us</a></li>
                  <li><a href="{{ route('all-products') }}">Our Products</a></li>
                  
                  {{-- <li><a href="#">GRANT</a></li>
                  <li><a href="#">MAX</a></li>
                  <li><a href="#">ROSS</a></li>
                  <li><a href="#">DEE</a></li>
                  <li><a href="#">GAIL</a></li>
                  <li><a href="#">LOU</a></li>
                  <li><a href="#">FINN</a></li>
                  <li><a href="#">GREY</a></li> --}}

                    @foreach ($categories as $cat)
                    @foreach($cat['products'] as $p)
                    <li>
                        <a href="{{ route('single-product',['product_code'=>$p['code']]) }}">
                            {{ $p['name'] }}
                            @if ($p['name_code_group'] && $p['name_code'])
                                <span class="ml-2">{{ $p['name_code_group'] }} - {{ $p['name_code'] }}</span> 
                            @elseif ($p['name_code'])
                                <span class="ml-2">{{ $p['name_code'] }}</span> 
                            @elseif ($p['name_code_group'])
                                <span class="ml-2">{{ $p['name_code_group'] }}</span> 
                            @endif    
                        </a>
                    </li>
                    @endforeach
                    @endforeach

              </ul>
          </div>
          <div class="column is-3 social">
              <h3>FOLLOW US</h3>
              <a href="https://linkedin.com/company/vcare-earth"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a>
              <a href="https://instagram.com/vcare_earth"><i class="fab fa-instagram" aria-hidden="true"></i></a>              
              <a href="https://twitter.com/VCare_Earth"><i class="fab fa-twitter" aria-hidden="true"></i></a>
          </div>
      </div>
      <div class="columns is-multiline">
          <div class="column is-5 is-offset-4">
              <ul class="page-links-legals">
                  <li><a href="/privacy-policy">Privacy</a></li>
                  <li><a href="/legal">Legal Notice</a></li>
                  <li><a href="/terms">Terms</a></li>
                  <li><a href="/refund-policy">Refund Policy</a></li>
              </ul>
          </div>
          <div class="column is-3 brand-name">
              <h3>© VCARE.EARTH 2020</h3>
          </div>

      </div>

  </div>
</footer>
