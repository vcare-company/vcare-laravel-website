@extends('layout.main') 

@section('head-end')
<link rel="stylesheet" href="{{ mix('css/respirator-comparison-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}" />
<script src="{{ mix('js/respirator-comparison-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection 


<?php 
    $meta_title = 'Respirator Comparison EN149 FFP1 | EN149 FFP1 | VCare.Earth' ; 
    $meta_desc = 'Compare different respirators at VCare.Earth. You can easily compare EN149 FFP1, FFP2, and FFP3 respirators and recommended usage to protect yourself against solid dust particles, fibres, and more. Visit us now!';
    $meta_url = Request::fullUrl();
?>    
    
@section('head-title')
<title>{{ $meta_title }}</title>
@endsection

{{-- SEO stuff --}}
@section('seo-meta')
<link rel="canonical" href="{{ $meta_url }}" />

<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />

<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />

<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>

<meta property="og:url" content="{{ $meta_url }}" />
<meta property="og:site_name" content="VCare.earth" />

<meta property="og:image" content="{{ '/files/logos/meta-image.png?u='.env('APP_ASSET_TIMESTAMP') }}" />
<meta property="og:image:secure_url" content="{{ '/files/logos/meta-image.png?u='.env('APP_ASSET_TIMESTAMP') }}" />


<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@VCare_Earth" />
<meta name="twitter:creator" content="@VCare_Earth" />
@endsection





@section('content')


@section('seo-meta-title')
<?php 
    $meta_title = "VCare.Earth Commitment to Care - Respirator Comparison" ; 
    $meta_desc = "Masks protect you against solid dust particles, fibres, microorganisms, mists and aerosols where work environments often conceal hazards such as viruses.";
?>    
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 


<section class="section pb-3 pt-3">
    <div class="container">
        <div class="columns is-flex-end">
            <div class="column is-12 title-column">
                <h1 class="title is-size-2 mb-4">Compare Different Respirators Easily at VCare.Earth</h1>
                <p class="is-size-5">
                    Masks protect you against solid dust particles, fibers, microorganisms, mists and aerosols (suspended droplets and dust). Work environments often conceal hazards such as viruses (Covid-19) that are not immediately recognized. Such a hazard can cause infection spread, permanent damage to your health, and in the worst case can even be life-threatening.
                </p>
            </div>
            {{-- <div class="column has-text-centered follow-cause"></div> --}}
        </div>
    </div>
</section>


<section class="section ">
    <div class="container">
        <div class="columns is-centered">
            <div class="column">
                {{-- <h1 class="subtitle color2">Why a wear a Protective Mask?</h1> --}}
                <h2 class="subtitle color2">Why Compare Respirators before Buying?</h2>
                {{-- <p>
                    It is important to get the maximum possible protection for your respiratory passages against the viral infections (Covid-19) to say the least. Protect yourself and protect people around you.
                    <br><br>
                    Masks marked with the code FFP stands for ‘filtering facepiece’. This shows what and how many particles of suspended dust, mist, fibres, bacteria, are filtered.
                </p> --}}
                <p>
                    When you shop fff1 respirators, a key decision making factor would be “choice”. Regardless of how proficient and well designed the en149 ffp1 standard product suppliers and equipment are, you will come across instances where the respirators cannot be used. 
                </p>
            </div>
            <div class="column">
                {{-- <h2 class="subtitle color2">What is FFP?</h2>
                <p>
                    Masks marked with the code FFP stands for ‘filtering facepiece’. This shows what and how many particles of suspended dust, mist, fibres, bacteria, are filtered.
                </p> --}}
                <h2 class="subtitle color2">&nbsp;</h2>
                <p>
                    It is important for you to choose a product that suits your need completely. And, you need to be aware of all the hazards in your workplace or environment, before you shop fff1 respirators. 
                </p>

            </div>
        </div>
        <div class="columns is-centered">
            <div class="column">
                <h2 class="subtitle color2 mt-4">This is a four step process:</h2>
                <ul>
                    <li><p>
                        One, you need to understand more about the kind of hazards that need protective equipment in your workplace or neighborhood. 
                    </p></li>

                    <li><p>
                        Two, you need to identify places where your employees will require respiratory protection.
                    </p></li>

                    <li><p>
                        Three, the amount of protection required by your employees has to be calibrated. This will differ from one place to another. Also, you need to know about the protection levels, before picking en149 ffp1 standard product suppliers. 
                    </p></li>

                    <li><p>
                        Finally, you have to handpick the type of respiratory products your employees need. This would be one of the most challenging things about en149 ffp1 standard product suppliers. Most of them have a range of protective products to choose from. Multiple factors have to be taken into consideration before you decide on the type and level of protection every employee in the workplace needs. 
                    </p></li>

                </ul>                
            </div>
        </div>
        <div class="columns is-centered">
            <div class="column is-6 has-text-centered-desktop">
                <p class="mt-6 mb-4 my-3-mobile">Three classes of FFP: FFP1, FFP2 and FFP3; higher the number, better the protection. Let’s COMPARE, FFP1, FFP2, FFP3 & the often term used as Mask.</p>
            </div>            
        </div>
    </div>
</section>


<section class="section respirator-comparison">
    <div class="container">
        {{-- <h4 class="title is-4 mb-6">RESPIRATOR COMPARISON</h4> --}}
        <div class="columns">
            <div class="column">
                <div class="card">
                    <div class="card-content">


                        <table class="table is-fullwidth is-hoverable hide-on-mobile">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>FFP1</th>
                                    <th>FFP2</th>
                                    <th>FFP3</th>
                                    <th>MASK</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($maskTypeTable as $row)
                                <tr>
                                    <th><h1 class="title is-6 whitespace-nowrap">{{ $row['title'] }}</h1></th>
                                    <td><p class="">{{ $row['ffp1'] }}</p></td>
                                    <td><p class="">{{ $row['ffp2'] }}</p></td>
                                    <td><p class="">{{ $row['ffp3'] }}</p></td>
                                    <td><p class="">{{ $row['mask'] }}</p></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>


                        <div class="hide-on-desktop">
                            <?php 
                                $data_mobile = ['ffp1','ffp2','ffp3','mask']    
                            ?>
                                        
                            @foreach ($data_mobile as $dm)

                            <table class="table is-fullwidth is-hoverable mb-6">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th> <h1 class="title is-4 is-size-4">{{ strtoupper($dm) }}</h1> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($maskTypeTable as $row)
                                    <tr>
                                        <th>{{ $row['title'] }}</th>
                                        <td><p class="">{{ $row[$dm] }}</p></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @endforeach
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

        {{-- <div class="columns is-centered mt-6">
            <div class="column is-narrow has-text-centered-mobile">
                <a class="button is-outlined vcare-btn is-medium" target="_blank" href="/vcare-detailed-comparison-for-masks-pdf">
                    <span>Detailed Comparison</span>
                    <span class="icon">
                        <i class="fas fa-chevron-right" aria-hidden="true"></i>
                    </span>
                </a>
            </div>
        </div> --}}
    </div>
</section>



<section class="section ">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-6 has-text-centered-desktop">
                <p class="mt-6 mb-6 my-3-mobile">
                    A technical difference between a “mask” and a “respirator”? Day to day we often say mask, when referring to what are technically called respirators.    
                </p>
            </div>            
        </div>
        <div class="columns is-centered">
            <div class="column">
                <h1 class="subtitle color2">Uses for Masks:</h1>
                <p>
                    Masks are loose fitting, covering the nose and mouth. Designed for one way protection, to capture bodily fluid leaving the wearer. I.e. Worn during surgery to prevent coughing, sneezing, etc on the vulnerable patient. 
                    <br><br>
                    Contrary to belief, masks are NOT designed to protect the wearer. Vast majority of masks do not have a safety rating assigned to them, i.e. NIOSH / EN.
                </p>
            </div>
            <div class="column">
                <h1 class="subtitle color2">Uses for Respirators:</h1>
                <p>
                    Respirators are tight fitting masks, designed to create a facial seal. Non-valved respirators provide good two way protection, filtering both inflow and outflow of air. 
                    <br><br>
                    Designed to protect the wearer (If worn properly), up to the safety rating of the mask Available as disposable, half face or full face.</p>
            </div>
        </div>
        <div class="columns is-centered">
            <div class="column is-6 has-text-centered-desktop">
                <p class="mt-6 mb-6 my-3-mobile">
                    Whilst surgical style masks are not redundant by any means, they aren’t designed to protect the wearer, whilst respirators are; educate the Buyer when necessary.
                </p>
            </div>            
        </div>                
    </div>
</section>










<section class="section ">
    <div class="container">
        <h1 class="title is-5 has-text-centered color2"
        style="line-height: 2rem;">
            VCARE FFP2/FFP3 <br>
            RESPIRATORS & RECOMMENDED USAGE
        </h1>
        <div class="columns is-centered">
            <div class="column">
            <div class="mask-table">
                
                <div class="hide-on-mobile">
                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth has-text-centered mt-4 ">
                    <thead>
                        <tr class="">
                            <th><h1 class="title my-5 is-6">MASK REFERENCE NO.</h1></th>
                            {{-- <th><h1 class="title my-5 is-6">STANDARD</h1></th> --}}
                            <th><h1 class="title my-5 is-6">PRODUCT TYPE</h1></th>
                            <th><h1 class="title my-5 is-6">WEARING STYLE</h1></th>
                            <th><h1 class="title my-5 is-6">USAGE</h1></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th class="py-6" colspan="4">
                                <h1 class="title is-size-4 color2 mt-0 px-6">FFP2</h1>                                
                            </th>
                            {{-- <td><p></p></td>
                            <td><p></p></td>
                            <td><p></p></td>
                            <td><p></p></td> --}}
                        </tr>
                        @foreach ($maskTable['ffp2'] as $row)
                        <tr>
                            <th>
                                <div class="mask-left-title">
                                    <a class="datasheet"
                                    target="_blank"
                                    href="{{ env('APP_URL').'/datasheet/'.$row['code'].'?u='.env('APP_ASSET_TIMESTAMP') }}"
                                    title="{{ $row['mask-reference-no'] }} Datasheets">
                                    <span class="icon">
                                        <i class="fas fa-file-download"></i>
                                    </span>
                                    </a> 

                                    <div class="mask-image">
                                        <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                        target="_blank">
                                            <img src="/files/respirator-comparison/table-images/{{ $row['mask-reference-no-image'] }}" 
                                            {{-- <img src="{{ $row['main-image'] }}"  --}}
                                            alt="VCare.Earth FFP2 Respirator {{ $row['mask-reference-no'] }}">
                                        </a>                                       
                                    </div>
                                    <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                    target="_blank">
                                        <h1 class="subtitle is-6">{{ $row['mask-reference-no'] }}</h1>
                                    </a>
                                </div>
                            </th>
                            {{-- <td><p>{{ $row['standard'] }}</p></td> --}}
                            <td><p>{{ $row['product-type'] }}</p></td>
                            <td><p>{{ $row['wearing-style'] }}</p></td>
                            <td><p>{{ $row['usage'] }}</p></td>
                        </tr>
                        @endforeach

                        <tr>
                            <th class="py-6" colspan="4">
                                <h1 class="title is-size-4 color2 mt-0 px-6">FFP3</h1>                                
                            </th>
                            {{-- <td><p></p></td>
                            <td><p></p></td>
                            <td><p></p></td>
                            <td><p></p></td> --}}
                        </tr>
                        @foreach ($maskTable['ffp3'] as $row)
                        <tr>
                            <th>
                                <div class="mask-left-title">
                                    <a class="datasheet"
                                    target="_blank"
                                    href="{{ env('APP_URL').'/datasheet/'.$row['code'].'?u='.env('APP_ASSET_TIMESTAMP') }}"
                                    title="{{ $row['mask-reference-no'] }} Datasheets">
                                    <span class="icon">
                                        <i class="fas fa-file-download"></i>
                                    </span>
                                    </a> 

                                    <div class="mask-image">
                                        <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                        target="_blank">
                                        <img src="/files/respirator-comparison/table-images/{{ $row['mask-reference-no-image'] }}" 
                                        {{-- <img src="{{ $row['main-image'] }}"  --}}
                                        alt="VCare.Earth FFP2 Respirator {{ $row['mask-reference-no'] }}">

                                        </a>
                                    </div>
                                    <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                    target="_blank">
                                    <h1 class="subtitle is-6">{{ $row['mask-reference-no'] }}</h1>
                                    </a>
                                </div>
                            </th>
                            {{-- <td><p>{{ $row['standard'] }}</p></td> --}}
                            <td><p>{{ $row['product-type'] }}</p></td>
                            <td><p>{{ $row['wearing-style'] }}</p></td>
                            <td><p>{{ $row['usage'] }}</p></td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                </div>

                <div class="hide-on-desktop">
                    <?php 
                        $data_mobile = ['product-type','wearing-style','usage'];
                    ?>
    
    
                    <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth has-text-centered mt-4 ">
                        <thead>
                            <tr class="">
                                <th><h1 class="title my-5 is-5">FFP2</h1></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($maskTable['ffp2'] as $row)
                            <tr>
                                <th>
                                    <div class="mask-left-title">
                                        <a class="datasheet"
                                        target="_blank"
                                        href="{{ env('APP_URL').'/datasheet/'.$row['code'].'?u='.env('APP_ASSET_TIMESTAMP') }}"
                                        title="{{ $row['mask-reference-no'] }} Datasheets">
                                        <span class="icon">
                                            <i class="fas fa-file-download"></i>
                                        </span>
                                        </a>

                                        <div class="mask-image">
                                            <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                            target="_blank">
                                            <img src="/files/respirator-comparison/table-images/{{ $row['mask-reference-no-image'] }}" 
                                            {{-- <img src="{{ $row['main-image'] }}"  --}}
                                            alt="VCare.Earth FFP2 Respirator {{ $row['mask-reference-no'] }}">
                                            </a>
                                        </div>
                                        <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                        target="_blank">
                                        <h1 class="subtitle is-5">FFP2 {{ $row['mask-reference-no'] }}</h1>
                                        </a>
                                    </div>
                                    <div class="mask-content">
                                        {{-- <span class="subtitle is-5 is-size-7">STANDARD:</span>
                                        <p class="mb-3">{{ $row['standard'] }}</p> --}}

                                        <span class="subtitle is-5 is-size-7">PRODUCT TYPE:</span>
                                        <p class="mb-3">{{ $row['product-type'] }}</p>

                                        <span class="subtitle is-5 is-size-7">WEARING STYLE:</span>
                                        <p class="mb-3">{{ $row['wearing-style'] }}</p>

                                        <span class="subtitle is-5 is-size-7">USAGE:</span>
                                        <p class="mb-3">{{ $row['usage'] }}</p>

                                        {{-- <span class="subtitle is-5 is-size-7">DATASHEET:</span> --}}
                                        {{-- <a class="datasheet"
                                        target="_blank"
                                        href="{{ env('APP_URL').'/datasheet/'.$row['code'].'?u='.env('APP_ASSET_TIMESTAMP') }}"
                                        title="{{ $row['mask-reference-no'] }} Datasheets">
                                        DATASHEET
                                        <span class="icon">
                                            <i class="fas fa-file-download"></i>
                                        </span>
                                        </a> --}}

                                    </div>
                                </th>
                            </tr>
                            @endforeach
    
                        </tbody>
                    </table>
    
    
                    <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth has-text-centered mt-6 ">
                        <thead>
                            <tr class="">
                                <th><h1 class="title my-5 is-5">FFP3</h1></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($maskTable['ffp3'] as $row)
                            <tr>
                                <th>
                                    <div class="mask-left-title">
                                        <a class="datasheet"
                                        target="_blank"
                                        href="{{ env('APP_URL').'/datasheet/'.$row['code'].'?u='.env('APP_ASSET_TIMESTAMP') }}"
                                        title="{{ $row['mask-reference-no'] }} Datasheets">
                                        <span class="icon">
                                            <i class="fas fa-file-download"></i>
                                        </span>
                                        </a>

                                        
                                        <div class="mask-image">
                                            <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                            target="_blank">
                                            <img src="/files/respirator-comparison/table-images/{{ $row['mask-reference-no-image'] }}" 
                                            {{-- <img src="{{ $row['main-image'] }}" --}}
                                            alt="VCare.Earth FFP2 Respirator {{ $row['mask-reference-no'] }}">
                                            </a>
                                        </div>
                                        <a href="{{ route('single-product',['product_code'=>$row['code']]) }}"
                                        target="_blank">
                                        <h1 class="subtitle is-5">FFP3 {{ $row['mask-reference-no'] }}</h1>
                                        </a>
                                    </div>
                                    <div class="mask-content">
                                        {{-- <span class="subtitle is-5 is-size-7">STANDARD:</span>
                                        <p class="mb-3">{{ $row['standard'] }}</p> --}}

                                        <span class="subtitle is-5 is-size-7">PRODUCT TYPE:</span>
                                        <p class="mb-3">{{ $row['product-type'] }}</p>

                                        <span class="subtitle is-5 is-size-7">WEARING STYLE:</span>
                                        <p class="mb-3">{{ $row['wearing-style'] }}</p>

                                        <span class="subtitle is-5 is-size-7">USAGE:</span>
                                        <p>{{ $row['usage'] }}</p>

                                    </div>
                                </th>
                            </tr>
                            @endforeach
    
                        </tbody>
                    </table>
    
                    </div>
    

            </div>
            </div>
        </div>
    </div>
</section>




<section class="section">
    <div class="container">
        </h1>

        <div class="columns is-centered is-multiline">
            <div class="column is-12">
                <h2 class="title is-5 color2">The Technics Followed by En149 Ffp1 Standard Product Suppliers</h2>
                <p class="mb-5">
                    By default, all respiratory protective equipment depend on the hazard it handles. And, it depends on the APF and substance it resists. Of course, these are not the only factors in categorizing respiratory protection systems. Instead, they can be identified as the most prominent factors in the list.
                    <br><br>
                    Another interesting type of safety equipment to focus on, when you shop fff1 respirators would be the disposable ones. These are also known as filtering face pieces. They are carefully designed to get rid of certain types of particulates. These respirators are extremely lightweight. And, they can be discarded after use. This means, you need zero maintenance for filtering face pieces. 
                    <br><br>
                    Meanwhile, you can also shop fff1 respirators that can be reused time after time. These products are designed with multiple cartridges, which can handle both vapor and gases. The cartridge has to be replaced periodically. Of course, you can choose to replace at fixed time intervals, or when required.                     
                </p>
            </div>            
            <div class="column is-12">
                <h2 class="title is-5 color2">The Verdict</h2>
                <p>
                    It is important to Compare Respirators en149 standards. The comparison helps you understand more about the environment, employee needs and the mechanism behind every respiratory protective system. The analysis plays an important role in identifying the right respiratory gear. 
                </p>
            </div>            
        </div>
    </div>
</section>


<section class="section mb-6" id="summary">
    <div class="container">
        <h1 class="title is-5 has-text-centered color2">SUMMARY</h1>

        <div class="columns is-centered">
            <div class="column is-6">
                <ul>
                    <li>Always protect yourself as well as possible. Choose the appropriate Protective Mask, to work not only work safely, but also better and more comfortably.</li>
                    <li>Dust mask with FFP1 score protects you against non-harmful particles.</li>
                    <li>Dust mask with FFP2 score protects you against irritating and harmful particles.</li>
                    <li>Dust mask with FFP3 score protects you against toxic and harmful particles.</li>
                    <li>Only use a dust mask once (NR), unless indicated otherwise.</li>
                    <li>Dust masks with the indications V and/or D give higher respiratory comfort.</li>
                    <li>Only choose dust masks that meet the EN 149 standard.</li>
                    <li>Take the possible risks into account and use maximum protection.</li>
                    <li>Always work comfortably and safely.</li>
                </ul>
            </div>            
        </div>
    </div>
</section>

@endsection
