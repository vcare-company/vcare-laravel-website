@extends('layout.main')



@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : Request::fullUrl() }}" 
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/single-category-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/single-category-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection


<?php 
    $meta_title = $category['meta_title'];
    $meta_desc = $category['meta_desc'];
    $meta_url = Request::fullUrl();
?>    
    
@section('head-title')
<title>{{ $meta_title }}</title>
@endsection

{{-- SEO stuff --}}
@section('seo-meta')
<link rel="canonical" href="{{ $meta_url }}" />

<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />

<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />

<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>

<meta property="og:url" content="{{ $meta_url }}" />
<meta property="og:site_name" content="VCare.earth" />

<meta property="og:image" content="{{ '/files/logos/meta-image.png?u='.env('APP_ASSET_TIMESTAMP') }}" />
<meta property="og:image:secure_url" content="{{ '/files/logos/meta-image.png?u='.env('APP_ASSET_TIMESTAMP') }}" />


<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@VCare_Earth" />
<meta name="twitter:creator" content="@VCare_Earth" />
@endsection



@section('content')

<section class="section">
    <div class="container">
        <div class="columns is-flex-end is-multiline">
            <div class="column is-12 title-column">
                <h2 class="subtitle">
                    {{ $category['h2'] }}
                </h2>
                <h1 class="title is-size-2 is-size-4-mobile mt-0 mb-4">{{ $category['h1'] }}</h1>
                {{-- <p> {{ $category['p'] }} </p> --}}
                {!! $category['p'] !!}
            </div>
            <div class="column has-text-right-desktop has-text-left-mobile">
                {{-- <a href="{{ route('respirator-comparison') }}">
                    <span class="vcare-button-text is-size-7 pr-4">RESPIRATOR COMPARISON</span>
                </a> --}}
                <a class="button vcare-button mt-3" href="{{ route('respirator-comparison') }}"
                target="_blank">
                    <span class="vcare-button-text is-size-7 pr-4">RESPIRATOR COMPARISON</span>
                    <i class="fas fa-chevron-right"></i>
                </a>
            </div>
        </div>
        <div class="columns">
            <div class="column">
                <hr>
            </div>
        </div>                
    </div>
</section>



<section class="section resources">
<div class="container">
<div class="columns">


    
    {{-- left section, list --}}
    <div class="column is-narrow">

          <div class="tabs tab-menu">
            <h3 class="title is-6"> {{ $category['name'] }} </h3>

            @if(count($category['products']))
                @include('category.partials.product-list',[
                    'cat'=>$category,
                ])            
            @endif
            
            @foreach($category['children'] as $cat)
                <h3 class="subtitle is-6"> {{ $cat['name'] }} </h3>
                @include('category.partials.product-list',[
                    'cat'=>$cat,
                ])
            @endforeach



            
          </div>
                
    </div>







    {{-- right section --}}
    <div class="column">



        @if (count($category['products']))
            <h2 class="subtitle main-group-name"> {!! $category['html'] !!}</h2>
            <div class="columns is-multiline">
                @include('category.partials.product-items',[
                    'cat'=>$category,
                ])
            </div>
        @endif


        @if (count($category['children']))
            <div class="columns is-multiline">
                @foreach ($category['children'] as $cat)
                    @include('category.partials.product-items',[
                        'cat'=>$cat,
                    ])
                @endforeach
            </div>
        @endif


        {{-- <hr class="mt-6 mb-6"> --}}

    </div>



</div>
</div>
</section>





@endsection
