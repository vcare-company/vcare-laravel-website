


@if (count($cat['products']))
    @foreach($cat['products'] as $k =>  $p)
    <div class="column is-4 is-flex">
        <div class="tile scrollto-{{ $p['code'] }}">
            <h3 class="name">
                <a href="{{ route('single-product',['product_code'=>$p['code']]) }}">
                {{ $cat['name'] }}
                </a>
            </h3>
            <h3 class="name_type">{{ $p['name_type'] }}&nbsp;</h3>
            <div class="img-container">
                @if ($p['main-image'])

                <a href="{{ route('single-product',['product_code'=>$p['code']]) }}">
                    <img 
                    src="{{ env('APP_URL').'/files/products/'.$p['public_id'].'/images/thumbnails/'.$p['main-image']['type'].'/'.$p['main-image']['filename'].'.'.$p['main-image']['ext_thumb'].'?u='.env('APP_ASSET_TIMESTAMP') }}" 
                    alt="Vcare Earth {{ $p['main-image']['title'] }}">
                </a>
                @else
                <a href="{{ route('single-product',['product_code'=>$p['code']]) }}">
                    <div class="img-placeholder-container">
                        <img class="img-placeholder" alt="Vcare Earth" src="/files/logos/vcareplaceholder.svg?u={{ env('APP_ASSET_TIMESTAMP') }}"/>
                        <h1 class="subtitle has-text-centered coming-soon-text">Coming Soon</h1>
                    </div>
                </a>
                @endif
            </div>


            <hr>
            <a href="{{ route('single-product',['product_code'=>$p['code']]) }}">
                {{ $p['name'] }} 
                @if ($p['name_code'])
                    <span class="ml-1"> - {{ $p['name_code'] }}</span>
                @endif

                @if ($p['coming_soon'])
                *Coming Soon
                @endif                

                <i class="fas fa-chevron-right ml-1" style="float: right;"></i>
            </a>
        </div>
    </div>

    @endforeach
@else
    <div class="column is-4 is-flex">
        <div class="tile scrollto-{{ $cat['code'] }}">
            <h3 class="name">{{ $cat['name'] }}</h3>
            <h3 class="name_type">&nbsp;</h3>
            <div class="img-container">
                <a href="{{ route('coming-soon-product-code',['product_code'=>$cat['code']]) }}">
                    <div class="img-placeholder-container">
                        <img class="img-placeholder" 
                        alt="Vcare Earth"
                        src="/files/logos/vcareplaceholder.svg?u={{ env('APP_ASSET_TIMESTAMP') }}"/>
                        <h1 class="subtitle has-text-centered coming-soon-text">Coming Soon</h1>
                    </div>
                </a>
            </div>
            <hr>
            <a href="{{ route('coming-soon-product-code',['product_code'=>$cat['code']]) }}">
                *Coming Soon
                <i class="fas fa-chevron-right ml-1" style="float: right;"></i>
            </a>
        </div>
    </div>        
@endif