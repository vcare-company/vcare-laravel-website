
    <ul>
    <?php if(count($cat['products'])) { ?>
        <?php foreach($cat['products'] as $p) { ?>
        <?php if ($p['coming_soon']) { ?>
            <li> 
            <a
            href="{{ route('single-product',['product_code'=>$p['code']]) }}" 
            class="scrollto_action"
            data-scrollto_id="{{ $p['code'] }}"> 
                {{ $p['name_type'].' '.$p['name'] }} 
                @if ($p['name_code'])
                    {{ $p['name_code'] }}
                @endif       
                *Coming Soon
            </a> 
            </li>
        <?php } else { ?>
            <li> 
            <a
            href="{{ route('single-product',['product_code'=>$p['code']]) }}"
            class="scrollto_action"
            data-scrollto_id="{{ $p['code'] }}"> 

                {{-- Because boss wants something different for surgical gown --}}
                @if ($cat['code']=='surgical-gown')
                    {{ $p['name'].' '.$p['name_code'].' ('.$p['name_type'].')' }}
                @else
                    {{ $p['name_type'].' '.$p['name'] }} 
                    @if ($p['name_code'])
                        {{ $p['name_code'] }}
                    @endif                                            
                @endif
            </a> 
            </li>
        <?php } ?>
        <?php } ?>
    
    <?php } else { ?>
        <li> 
        <a 
        href="{{ route('coming-soon-product-code',['product_code'=>$cat['code']]) }}"
        class="scrollto_action"
        data-scrollto_id="{{ $cat['code'] }}">
            *Coming Soon 
        </a>         
        </li>
    <?php } ?>



    </ul>