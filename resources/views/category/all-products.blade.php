@extends('layout.main')



@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('all-products') }}"
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/all-products-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/all-products-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>

@endsection


<?php 
    $meta_title = 'Personal Protective Products | Personal Protective Medical Equipment | VCare.Earth' ; 
    $meta_desc = 'Get high-quality medical personal protective and safety equipment provided by VCare.Earth. Our healthcare products are carefully designed to be durable for the local and global B2B clients. Visit us now!';
    $meta_url = Request::fullUrl();
?>    
    
@section('head-title')
<title>{{ $meta_title }}</title>
@endsection

{{-- SEO stuff --}}
@section('seo-meta')
<link rel="canonical" href="{{ $meta_url }}" />

<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />

<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />

<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>

<meta property="og:url" content="{{ $meta_url }}" />
<meta property="og:site_name" content="VCare.earth" />

<meta property="og:image" content="{{ '/files/logos/meta-image.png?u='.env('APP_ASSET_TIMESTAMP') }}" />
<meta property="og:image:secure_url" content="{{ '/files/logos/meta-image.png?u='.env('APP_ASSET_TIMESTAMP') }}" />


<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@VCare_Earth" />
<meta name="twitter:creator" content="@VCare_Earth" />
@endsection



@section('content')

<section class="section">
    <div class="container">
        <div class="columns is-flex-end is-multiline">
            <div class="column is-12 title-column ">
                <h2 class="subtitle">
                    OUR PRODUCTS
                </h2>
                <h1 class="title is-size-2 is-size-4-mobile mt-0 mb-4">The Best Healthcare & Medical Equipment</h1>
                <p class="mb-6">
                    VCare.Earth presents a distinctive array of personal safety equipment including PPE and respirators, carefully designed and created to a high-quality, with durability, and reliability built in. We are happy to serve our customers with all products and professional services guaranteed at an economical price.
                </p>

                <h2 class="title is-5">High-Quality Personal Safety Equipment </h2>
                <p class="">
                    Selecting the right personal equipment protection can be a challenge due to the vast array of products in the market, with many genres available from surgical face masks to surgical gowns and isolation gowns.   With multiple factors to consider, it’s important to know how to select the best products from a personal safety equipment manufacturer you can trust. 
                </p>


                                
                <div class="read-more-content">
                    <h2 class="title is-5 mt-6">
                        The Right Safety Product Manufacturer
                    </h2>
                    <p class="mb-6">
                        It is critical you choose your products from a company with years of experience in manufacturing for this industry. Why? The process of building personal safety equipment requires the right gears and strategies, which is a level of proficiency it takes most brands a long time to develop – an expertise you are completely assured of when dealing with world- leaders in personal safety products, VCare.Earth.
                    </p>
                    
                    <h2 class="title is-5">
                        State of the Art Safety Equipment
                    </h2>
                    <p class="mb-6">
                        Next, consider the equipment and machinery used by the personal safety equipment suppliers. Over time, the technology behind the creation of PPE safety products has evolved, as have the range in items available – such as head gadgets, which are now crucial within multiple industries including healthcare. At VCare.Earth, we focus on a state of the art infrastructure to ensure the fine quality and pristine nature of our products is never compromised. The technologies we use to design and develop your safety equipment are tested in extreme conditions, helping us to produce equipment with maximum strength, reliability and style.
                    </p>

                    <h2 class="title is-5">
                        An Extensive Catalogue of Personal Equipment Protection
                    </h2>
                    <p class="mb-6">
                        Until recently, personal safety equipment was limited to just a handful of products created to protect people who travelled through, worked or lived in extreme conditions. The scenario today is extremely different - and much more demanding. VCare.Earth offers its customers an inspiring catalogue of medical equipment and PPE safety products to choose from, including a range in respiratory protectors, filters and masks, each one ensuring the safe execution of your healthcare tasks.
                    </p>
                    
                    <h2 class="title is-5">
                        Warranty and Extensive Support

                    </h2>
                    <p class="mb-6">
                        A unique feature that distinguishes VCare.Earth from other manufacturers is our warranty and customer support service, with representatives available round the clock to answer your questions. Due to the exhaustive research behind each of our personal safety equipment, we’re confident in offering an extended warranty which means each item you receive from VCare.Earth is protected, giving you full peace of mind to use our personal protective products without a second thought. 
                    </p>
                    
                    <h2 class="title is-5">
                        Robust, Reliable Materials
                    </h2>
                    <p class="mb-3">
                        Regardless of the personal safety equipment supplier, it is important you are fully aware of the materials used in the design and manufacture of your products. The materials make a world of difference to the levels of protection offered, which is why VCare.Earth remains extremely cautious as to the ones we use in the manufacturing process. All of our personal safety equipment can be used in tough conditions including environments containing reactive chemicals, offering maximum protection and assurance to its user at all times.
                    </p>
                </div>
                <div class="read-more-action">
                    <button class="button is-ghost is-small btn-read-more">Read more...</button>
                    <button class="button is-ghost is-small btn-read-less">Read less...</button>
                </div>                
            </div>
            <div class="column is-12 has-text-right-desktop has-text-left-mobile">
                {{-- <a href="{{ route('respirator-comparison') }}">
                    <span class="vcare-button-text is-size-7 pr-4">RESPIRATOR COMPARISON</span>
                </a> --}}
                <a class="button vcare-button mt-3" href="{{ route('respirator-comparison') }}"
                target="_blank">
                    <span class="vcare-button-text is-size-7 pr-4">RESPIRATOR COMPARISON</span>
                    <i class="fas fa-chevron-right"></i>
                </a>
            </div>
        </div>
        <div class="columns">
            <div class="column">
                <hr>
            </div>
        </div>                
    </div>
</section>



<section class="section resources">
<div class="container">
<div class="columns">


    
    {{-- left section, list --}}
    <div class="column is-narrow">

          @foreach($categories as $cat_par)
          <div class="tabs tab-menu">
            <h3 class="title is-6"> {{ $cat_par['name'] }} </h3>
            @foreach($cat_par['children'] as $cat)
              <h3 class="subtitle is-6"> {{ $cat['name'] }} </h3>
              @include('category.partials.product-list',[
                    'cat'=>$cat,
                ])
            @endforeach
          </div>
          @endforeach
                
    </div>







    {{-- right section --}}
    <div class="column">
    @foreach ($categories as $i => $categ)
    @if (count($categ['children']))
    
    
        <h2 class="subtitle main-group-name"> {!! $categ['html'] !!}</h2>
        <div class="columns is-multiline">

            @foreach ($categ['children'] as $cat)
                @include('category.partials.product-items',[
                    'cat'=>$cat,
                ])
            @endforeach
            
        </div>


        @if($i < count($categories)-1)
        <hr class="mt-6 mb-6">
        @endif


    @endif
    @endforeach
    </div>



</div>
</div>
</section>





@endsection
