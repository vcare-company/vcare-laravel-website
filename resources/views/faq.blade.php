@extends('layout.main')



@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('faq') }}" 
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/faq-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/faq-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection




@section('head-title')
<title>FAQs | Vcare.earth</title>
@endsection


@section('seo-meta-title')
<?php 
    $meta_title = "VCare.Earth Commitment to Care - FAQs" ; 
    $meta_desc = "We provide premium medical and healthcare products to clients local and global. For any questions you can refer on this page.";
?>    
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 




@section('content')

<section class="section">
    <div class="container">
        <div class="columns is-flex-end">
            <div class="column title-column">
                <h2 class="subtitle">
                    FAQ
                </h2>
                <h1 class="title is-1">Help Center </h1>
            </div>
            <div class="column has-text-left-mobile has-text-right">
                <button class="button vcare-button">
                    <a href="{{ route('more-info') }}">
                        <span class="vcare-button-text is-size-7 pr-4">MORE PRODUCT INFORMATION</span>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </button>
            </div>
        </div>        
    </div>
</section>

<section class="section blue-vcare-bg" 
style="background-image: url({{ env('APP_URL') }}/files/faq/images/faq.png);">
  <div class="container">
    <div class="columns is-vcentered">
      <div class="column">
       
      </div>
    </div>
  </div>
</section>

<?php 

$resp_qa=[];    
$resp_qa[] = ['no'=>'1','question'=>'What is PPE (Personal Protective Equipment)?', 'answer'=>'PPE is equipment that will protect the user against health or safety risks at work. It can include items such as safety helmets, gloves, eye protection, high-visibility clothing, safety footwear and safety harnesses. It also includes respiratory protective equipment (RPE).'];    
$resp_qa[] = ['no'=>'2','question'=>'What is meant by “Medical” and “Non-Medical” use?', 'answer'=>'To make things clear: PPE, Personal Protective Equipment, (FFP2, KN95, N95) and currently better known as Respirator Protective Masks are all declared as “Non-Medical”. However, this does not mean that you cannot use those during the current COVID-19 pandemic. The opposite is true! Surgical Masks are communicated as “Medical” Masks but in fact, those masks are not protecting you against a COVID-19 infection.'];    
$resp_qa[] = ['no'=>'3','question'=>'Does the term “Non-Medical” mean I cannot use the mask as PPE or for the medical sector?', 'answer'=>'No. Different regions use different wording. The most important factor is the product is “Fit for Purpose” and meets the specification. In-fact, PPE by nature comes form the construction industry, to protect users from particles such as dust. PPE also is classified as “Non-Medical”. However Non-Medical is also used for PPE. In Europe, there is no distinction between PPE and Non-Medical and hence they use both. Medical use is usually referred to a product such as the “Surgical Mask”, but does not protect users from contracting a virus such as COVID-19 through aerosols and droplets.'];    
$resp_qa[] = ['no'=>'4','question'=>'What is a Protective Respirator?', 'answer'=>'A Protective Respirator (also known as a dust mask) is worn to efficiently protect wearers against solid dust particles, fibres, microorganisms, mists and aerosols (suspended droplets and dust). When people breath, such harmful particulates can damage your respiratory system. A proper seal between the wearers face and the respirator forces inhaled air to be pulled through the respirator’s filter material, thereby providing protection.'];    
$resp_qa[] = ['no'=>'5','question'=>'Why should I wear a Protective Respirator?', 'answer'=>'As the COVID-19 attacks the Respiratory System (airways, mouth, nose, lungs etc.), it is crucial for people to wear a Respirator Protective Mask in order to minimize risks of being infected by the COVID-19 disease. Work environments often conceal hazards viruses such as COVID-19 that are not immediately recognised. Such a hazard can cause infection spread (coughs, sneezes, droplets and aerosols), permanent damage to your health, in the worst case even be life-threatening and potentially leading to death.'];    
$resp_qa[] = ['no'=>'6','question'=>'How does a Protective Respirator protect people?', 'answer'=>'A Protective Respirator is produced as a tight face fit and made out of high-grade material which filters out large and small airborne particles. A growing body of research shows that the virus is spread by minuscule viral particles that can linger in the air as long as 16 hours. When used properly, the Respirator blocks 95% of tiny air particles (down to 0.3 micron in diameter). As the Respirator is of high-grade protection, it protects both the wearer and the people around the wearer; effectively a two-way protection.'];    
$resp_qa[] = ['no'=>'7','question'=>'What are the main classes of FFP (Filtering Facepiece) Protective Respirator?', 'answer'=>'There three main classes of FFP Protective Respirators:<br>FFP2 = EUROPE = CE = Compliant to EN 149:2001+A1:2009 and EU2016/425<br>KN95 = CHINA = GB2626-2006 = Compliant to GB2626-2006<br>N95 = USA = NIOSH = Compliant to NIOSH 42CFR84'];    
$resp_qa[] = ['no'=>'8','question'=>'Difference between valved and non-valved Protective Respirator?', 'answer'=>'We currently do believe that Valved Protective Respirator are the right option for protecting people from viruses such as the Covid-19. Valved masks makes breathing more easier for the wearer but we also know that this can reduce efficiency in blocking out viruses. A valved respirator is more comfortable for the wearer but in the same time when infected already with COVID-19 – because of the velve where the virus can easily exit – you will spread the virus further to people around you. Hence, the best way to stay safe for everyone are masks without velves, especially when your COVID-19 status is unknown.'];    
$resp_qa[] = ['no'=>'9','question'=>'What is FFP?', 'answer'=>'Stands for “Filtering Facepiece”, is also known as Protective Respirator is a type of protective mask certified by the European Union that serves to protect against particulates such as dust particles and various viruses in the air. FFR, Filtering Facepiece Respirator (sometimes called disposable respirators), are subject to various regulatory standards around the world. These standards specify certain required physical properties and performance characteristics in order for respirators to claim compliance with the particular standard. During pandemic or emergency situations, health authorities often reference these standards when making respirator recommendations, stating, for example, that certain populations should use: N95, FFP2, KN95, or an equivalent Respirator.'];    
$resp_qa[] = ['no'=>'10','question'=>'How many FFP (Filtering Facepiece) categories are available?', 'answer'=>'The EN149 standard defines three classes of filter efficiency for these respirator protection masks: FFP1, FFP2 and FFP3. These are half-masks, which means they protects the chin, nose and mouth. The mask must meet certain standards and effectiveness tests. Efficacy is assessed by the filtration rate (filter penetration, also referred to as efficiency), as well as the degree of leakage around the edges. The mask should be correctly adjusted to the face. FFP Masks, (unlike the surgical mask), protects the wearer from inhaling infectious agents or pollutants in the form of aerosols, droplets, or small solid particles.'];    
$resp_qa[] = ['no'=>'11','question'=>'How do the FFP (Filtering Facepiece) categories compare?', 'answer'=>'FFP1: Protects against large solid particles, irritating but not harmful substances.<br>Minimum filter efficiency 78%. Reduces amount of dust by 4. Breathe in, no protection. Breath out, protects people around you. FFP1 uses too wide filter for filtering the air, so for breathing in, it won’t stop the virus from penetrating the mask, but for breathing out, it stops the aerosol and small water droplets which often contain most of the virus. Protects surrounding people but not the wearer.<br><br>FFP1 is equivalent to a basic Surgical Mask.<br><br>FFP2: Protect against viruses; solid and liquid irritating aerosols.Pro-Advanced Filtration Protection. Minimum filter efficiency 92%. Reduces amount of dust by 10. Protects the wearer and surrounding people. FFP2 is equivalent to N95 and KN95<br><br>FFP3: Highest protection against high levels of airborne particles, solid and liquid toxic aerosols and toxic conditions. Minimum filter efficiency 98%. Reduces amount of dust by 20.'];    
$resp_qa[] = ['no'=>'12','question'=>'Are your products classed, standard certified and compliance tested?', 'answer'=>'Yes, our products are certified. Please be aware that certification varies based regions. See below:<br><br>Classification: Meaning type of class respirator such as: FFP2, KN95 and N95.<br><br>Region: Meaning which country the Respirator is related to: China, Europe, or USA.<br><br>Certification: Meaning product has been tested and recognized by the authorities.<br><br>Testing Standards: Is a numeric code referring to the product being tested.'];    
$resp_qa[] = ['no'=>'13','question'=>'Which is the most efficient Respirator Protective Mask, KN95, N95 or FFP2?', 'answer'=>'KN95, N95 and FFP2 are all high-grade and high-quality filtering facepiece respirator protection masks that protect you from viruses such as the COVID-19. VCare are familiar with all three classes so please feel free to ask us any questions in relation to the efficiency.<br><br>Please see below 3M technical bulletin comparing the respirator classes: <a href="">Mask Comparison</a>'];    
$resp_qa[] = ['no'=>'14','question'=>'I have found many masks being sold online, why should we buy your masks?', 'answer'=>'On the contrary, e-commerce is flourishing, and with current global lockdown measures in place. The best way to buy any product is online. More importantly, we want to make sure our clients verify who they are buying from, what they are buying and if the product is fit for purpose? If not, this can cause the spread of viral infection, risk to health, potentially cost not only a financial loss but also risk lives. Buying from VCare, we will always adhere to high quality standards and we go the further mile to verify our products.'];    
$resp_qa[] = ['no'=>'15','question'=>'How can I verify the difference between a fashion mask, a fake mask and an authentic Respirator Protective Mask?', 'answer'=>'To differentiate a fake mask vs. an authentic mask which protects you and others from spreading the COVID-19 virus, its almost impossible for the layman. Also, governments are facing continuously difficulties not to fall for counterfeit products and suppliers. If you are not sure if your product is authentic, please feel free to reach out to us and we will be happy to assist with a free consultation. Our main concern is to help and to support wherever we can in this pandemic situation.'];    
$resp_qa[] = ['no'=>'16','question'=>'What does “Fit For Purpose” mean in relation to Protective Respirator?', 'answer'=>'Yes, certifications and test reports are available upon request.'];    
$resp_qa[] = ['no'=>'17','question'=>'Can I sanitize and re-use my Protective Respirator?', 'answer'=>'The Protective Respirator we sell are NR (not reusable) rated, which means they are a one-time use for up to 8 hours. Although there have been reports of sanitizing and cleaning Protective Respirators, we believe the risk of spread and infection can be high and therefore we are against cleaning and re-using. First studies already show that cleaning of masks are not effective and when doing so, you are putting yourself and the people around you at high risk to get infected or spreading the virus further.'];    
$resp_qa[] = ['no'=>'18','question'=>'Why do Respirators cost more than a Surgical Mask?', 'answer'=>'This is simply because of the high value filtration materials used in the manufacturing process, the safety rating, and the purpose of Protective Respirators which is to protect people from deadly viruses, and therefore are higher in cost as opposed to a basic Surgical Mask.'];    
$resp_qa[] = ['no'=>'19','question'=>'Why are Protective Respirator both high in demand and short in supply globally?', 'answer'=>'Given the current global pandemic, there has been a huge surge in demand for  Protective Respirators (KN95, N95 and FFP2). These Respirators, filter and protect the wearer both inwards and outwards. As these are disposable respirators and used once, they are being consumed fairly quick and therefore causing a global shortage in supply globally.'];    
$resp_qa[] = ['no'=>'20','question'=>'Does Protective Respirators have indications?', 'answer'=>'Yes, Protective Respirators also known as Dust Masks have the following indications:<br><br>D – Dust masks with the ‘D’ indication have passed the dolomite test. They are more resistant against clogging over time. There is also less breathing resistance, which makes work more pleasant.<br><br>V – Dust masks with the ‘V’ indication have a valve for exhalation. This reduces breathing resistance and also ensures that the CO2 and moisture level in the dust mask stays as low as possible.<br><br>Dv – Dust masks that have passed the dolomite test and have a valve.<br><br>R – The R indication shows that it is reusable. Standard dust masks can only be used for one session.<br><br>NR – Can only be used for one session. To clarify this, the indication NR may be stated on the packaging. If nothing is stated, the dust mask should in any case only be used once.'];    
$resp_qa[] = ['no'=>'21','question'=>'What are the risks with using a Protective Respirator?', 'answer'=>'It is important to follow the right methods of wearing and taking of the mask.'];    
$resp_qa[] = ['no'=>'22','question'=>'What are the most reliable brands for Protective Respirator?', 'answer'=>'3M, UVEX, Honeywell, Moldex and GVS to name a few. VCare uses the highest quality of material to uphold the same standards as our competitors in our products.'];    
$resp_qa[] = ['no'=>'23','question'=>'Can I just avoid wearing a mask and practice social distancing?', 'answer'=>'We recommend to wear a mask when outside in at work, or in places you will be in close contact to people which is sometimes inevitable, but we also strongly advise all people follow the social distancing guidelines outlined by their government.'];    
$resp_qa[] = ['no'=>'24','question'=>'Can I buy a Surgical Mask to filter and protect me from the Novel Coronavirus (COVID-19)?', 'answer'=>'The Surgical Mask is a basic mask, and we are currently not aware of any Surgical Mask produced or available to protect the wearer and surrounding people from the Covid-19 virus.'];    
$resp_qa[] = ['no'=>'25','question'=>'What are the Protective Respirator protecting us from?', 'answer'=>'Droplets<br>A primary reason for wearing a respirator is to protect from droplets. For example if a sick person coughs or sneezes when in close proximity to us, the respirator forms a barrier to prevent their bodily fluids reaching our face. Droplets are large, and gravity drags them down to land on objects, rather than staying in the air. So they don’t travel very long distances.<br><br>Aerosols<br>What may remain in the air for some time are aerosolized virus particles. So for example, you could imagine someone creating two issues when sneezing, the first are ejected droplets, which travel a short distance, then second, aerosolized virus particles that stay in the air for longer.<br><br>Currently there is debate and uncertainty around how long COVID-19 can remain aerosolized, and how much of a risk that vector is compared to others. What we can do is be aware of what research currently says, and err on the side of caution until its been confirmed.<br><br>Scientists at the National Institute of Allergy and Infectious Diseases (NIAID) published a study in NEJM on what can happen under controlled lab conditions. They used a nebulizer, which creates an aerosol from liquids, and tested how long the virus remains measurable in the air whilst aerosolized. They also tested how long the virus was measurable on other surfaces. Their results showed the virus remained measurable for the full duration of the aerosolization experiment; 3 hours. Some other studies have suggested for up to 16 hours.'];    
$resp_qa[] = ['no'=>'26','question'=>'What can I do to reduce my risk of being infected from COVID-19?', 'answer'=>'Follow social distancing guidelines outlined by your government. Regular handwashing, the use of sanitizer at the right time, maintain a healthy immune system by eating, exercising, and sleeping well. Generally, stay aware of where bacteria can spread and practice good overall hygiene to clean and disinfect regularly. Wear a Fit For Purpose mask such as the Respirator Protective Mask (KN95, N95 or FFP2).<br><br>We are all at a risk of getting this virus and we are at a risk of bringing it home to our families, we are not the first to say this and we will not be the last. Please stay aware, protect yourself properly and stay safe.'];    
$resp_qa[] = ['no'=>'27','question'=>'Does VCare.Earth serve B2B And B2C?', 'answer'=>'Given the current global circumstances, VCare.Earth has prioritized its products and service towards B2B companies, frontline workers, key workers and towards the vulnerable who are in need of our products the most.<br><br>In the future, we may consider to gradually open our B2C platform, and we thank our clients for their patience and understanding during these times.'];    
  


$rapid_qa=[];    
$rapid_qa[] = ['no'=>'1','question'=>'What is a Covid-19 Antibody lgG/lgM Rapid Test Home Kit?', 'answer'=>'Blood test designed to tell whether people have already had the virus and are now immune. We know they work best around 10 – 14 days after someone has had the virus. These could maybe be done at home with a finger prick, and deliver results in as little as 20 minutes. Such a test could, if developed in such a way that they could be reliably used at home and be sufficiently accurate, be a game-changer.'];    
$rapid_qa[] = ['no'=>'2','question'=>'What is antibody rapid tests?', 'answer'=>'An antibody test detects antibodies that are produced by a patient’s immune system against parts of e.g. Pathogens (antigens) are formed. These are protein, sugar or fat molecules that sit on the outside of viruses or bacteria. These include those that occur in several related pathogens and others that can only be found on the surface of the germ in question, i.e. are highly specific.'];      
$rapid_qa[] = ['no'=>'4','question'=>'What are Antibodies?', 'answer'=>'Antibodies (immunoglobulins) are generally made against all foreign substances that our immune cells encounter. For an accurate, specific statement of an antibody test, one concentrates on the highly specific antibodies, which are directed against the pathogen-typical proteins.<br><br>Antibody tests offer people wanting to find out if they have had Covid-19, and are now immune and get back to work.'];    
$rapid_qa[] = ['no'=>'5','question'=>'What types of antibodies are there?', 'answer'=>'During the immune response to a pathogen, early antibodies are formed (e.g. immunoglobulin A, IgA for short, usually also immunoglobulin M, IgM for short) and later mature antibodies (IgG). The antibodies can sometimes be measured a few days after the pathogen has entered, sometimes only after weeks, sometimes only when the pathogen has already left the body. The IgG antibodies usually remain in the blood for a very long time, often for life, and are of concern then for a rapid immune response if the same pathogen should enter the body again. We call this state of readiness immunity, and we can identify the phase of the infection from the presence of IgA and IgG in the blood serum and the ratio of the IgA and IgG concentration to one another. For example, if only IgA is to be measured and no IgG, this indicates a recent infection. The reverse case – IgA negative, IgG high – is typical of a long-running infection.'];    
$rapid_qa[] = ['no'=>'6','question'=>'What is the difference between IgG and IgM antibodies?', 'answer'=>'Immunoglobulin G (IgG), the most abundant type of antibody, is found in all body fluids and protects against bacterial and viral infections. Therefore, this is the antibody that would be expected to found in the human body to help protect against repeat infection Covid-19.Immunoglobulin M (IgM), which is found mainly in the blood and lymph fluid, is the first antibody to be made by the body to fight a new infection. Therefore, this is the antibody that would be expected to found in the human body to help fight an initial infection of Covid-19.'];    
$rapid_qa[] = ['no'=>'7','question'=>'What is the difference between Primary SARS-CoV-2 infection and Secondary SARS-CoV-2 infection?', 'answer'=>'Primary SARS-CoV-2 infection is characterised by the presence of detectable IgM antibodies approximately 3-7 days after the onset of infection.Secondary SARS-CoV-2 infection is characterized by the elevation of SARS-CoV-2 specific IgG. In the majority of the cases, this is accompanied by elevated levels of IgM.'];    
$rapid_qa[] = ['no'=>'8','question'=>'What antibodies are searched for with the Corona Antibody Test?', 'answer'=>'In our case, we want to detect IgG and IgA antibodies against SARS-CoV-2 in order to find out whether and when the investigated person dealt with the Covid-19 pathogen immunologically. With SARS-CoV-2, and not with SARS-CoV-1, MERS or any other coronavirus, of which there are a number of harmless types that also elicit immune responses. We call this “cross reaction”. Widespread e.g. Antibodies against the corona virus OC-43.'];    
$rapid_qa[] = ['no'=>'9','question'=>'What are the requirements for a good antibody test?', 'answer'=>'We are essentially interested in two questions:<br><br>Does the test find all patients who have actually been infected with SARS-CoV-2 (sensitivity)?<br><br>Were the antibodies found specifically generated only against SARS-CoV-2 and not against related pathogens (specificity)?<br><br>Our goal as Bioscientia is to provide this data selectivity with our own data. For this purpose, we have been taking blood samples from suitable patients since the beginning of March, of which we e.g. also know the result of the corona PCR and examine the antibodies formed in various test systems.'];    
$rapid_qa[] = ['no'=>'10','question'=>'Can I take the Covid-19 Antibody lgG/lgM Rapid Test at home?', 'answer'=>'Yes, the test is a Home Kit, however we recommend professional assistance.'];    
$rapid_qa[] = ['no'=>'11','question'=>'How Does COVID-19 IgM IgG Rapid Test Kit Work?', 'answer'=>'Screening for COVID-19 IgM and IgG antibodies is a rapid and effective method for the diagnosis of COVID-19 infection. The IgM and IgG antibody test can also provide information on the stage of infection.<br><br>Both Immunoglobulin M (IgM) and Immunoglobulin G (IgG) antibodies are produced during the primary immune response. As the body’s largest antibody, IgM is the first antibody to appear in response to an initial exposure to antigens. IgM provides the first line of defence during viral infections, followed by the generation of adaptive, high affinity Immunoglobulin G (IgG) responses for long-term immunity and immunological memory. IgG is usually detectable about 7 days after the IgM appears.'];    
$rapid_qa[] = ['no'=>'12','question'=>'Difference between swab test (PCR) and blood test (antibody)?', 'answer'=>'The biggest difference is swab test can detect early and onset infection with most accuracy. Whereas, blood test is quicker to get results and not as sensitive as swab test for early infection.<br><br>Swab test detects the presence of the virus’ genetic material (RNA). Blood test indirectly detects by measuring our body’s immune (IgG, IgM) to the virus.<br><br>The sample test needed for swab test are Nasopharyngeal (Nose/Throat) and for blood test its a finger prickle of blood.<br><br>It takes the same amount of time both tests to get the results.'];    
$rapid_qa[] = ['no'=>'13','question'=>'How do I interpret the test result?', 'answer'=>'The tests usually detect two types of antibodies. One, called IgM, is typically produced about a week after infection and could identify patients who may still be infected.Levels of IgM begin to wane as the body makes another type of antibody called IgG, which can persist for longer periods of time.<br><br>Negative Result: If only the quality control line (C) appears and G and M are not visible, then the result is negative.<br><br>Positive Result, M only: If both C and M appear, then the result is positive for the IgM antibody<br><br>Positive Result, G only: If both C and G appear, then the result is positive for the IgG antibody.<br><br>Positive Result, G and M: If C and both G and M appear, then the result is positive for both the IgG and IgM antibodies'];    
$rapid_qa[] = ['no'=>'14','question'=>'How accurate are these test results?', 'answer'=>'In order to test the detection relative sensitivity and specificity of the COVID-19 IgG-IgM combined antibody test, serum samples were collected from COVID-19 patients from multiple hospitals and Chinese CDC laboratories. The tests were done separately at each site. A total of 1585 cases were tested: 421 (positive) clinically confirmed (including PCR test) COVID-19-infected patients and 1164 negative. The test results showed that the product has a relative sensitivity of 98.81% (95% CI: 97.25%, 99.61%) and a relative specificity of 98.02% (95% CI: 97.05%, 98.74%).'];    
$rapid_qa[] = ['no'=>'15','question'=>'Which statements does the antibody detection currently not allow?', 'answer'=>'In the current phase of the pandemic, the public is hoping for statements based on antibody detection that we are familiar with from other viral diseases, e.g. Mumps and measles.Specific examples:Can a geriatric nurse with positive antibody detection have contact with elderly people in need of care?Can an IgG-positive risk patient go back to his normal life without special protective measures?Does an IgG-positive nurse really pose no risk of infection for patients with previous illnesses or for residents of old people’s homes with the known increased risk?Are grandchildren with positive antibody detection allowed to visit their grandparents again without worry?At present, these questions cannot be answered conclusively with the available test systems and are the subject of studies that, among other things, also carries out the bioscientia. It will probably take a few months before the database becomes stable enough for reliable answers. Oxford Professor John Bell, for example, has plausibly presented this assessment.'];    
$rapid_qa[] = ['no'=>'16','question'=>'When will antibody tests be available in sufficient numbers?', 'answer'=>'We have been offering the first antibody tests since the beginning of April, with reference to the currently limited significance. Other laboratories in other countries have also started testing. The number of manufacturers of CE-certified and sufficiently validated test kits is limited worldwide. As with pathogen detection using PCR, delivery bottlenecks are already to be expected, which can only be eliminated slowly by increasing capacity.'];    
$rapid_qa[] = ['no'=>'17','question'=>'Who can be tested?', 'answer'=>'Anyone for whom there is a medical indication.'];    
$rapid_qa[] = ['no'=>'18','question'=>'Where can I get tested?', 'answer'=>'Your general practitioner or company doctor will take a blood sample from you, if indicated, which, together with the referral slip, is transported to our laboratories in the usual way and examined there.'];    
$rapid_qa[] = ['no'=>'19','question'=>'What does the test cost?', 'answer'=>'The examination is currently (April 20, 2020) not a cash benefit, but can only be provided as an IGeL or private benefit. The cost per antibody is € 20.11.'];    
$rapid_qa[] = ['no'=>'20','question'=>'Do the health insurance companies cover the costs for the antibody test?', 'answer'=>'No. The examination is currently (April 20, 2020) not a cash benefit, but can only be provided as an IGeL or private benefit. The cost per antibody is € 20.11.'];    
$rapid_qa[] = ['no'=>'21','question'=>'How does the antibody test work in the specialist laboratory?', 'answer'=>'We measure the antibodies using the so-called ELISA method, which can be easily automated and enables large sample quantities to be processed on the same day. How the test works:<br><br>A 96 well plastic plate is coated with antigen. An extract of virus components of SARS-CoV-2 is then on the walls of the wells.<br><br>A patient sample is pipetted into each well. If there are antibodies against the virus components on the wall in the sample, they bind to them – as in the body. After 20 minutes, everything that has not bound is removed from the well.<br><br>Now add a second antibody that shows two special features:<br><br>a) one end binds specifically to a specific antibody type (here IgA or IgG). The result: All patient antibodies that linked to the antigen in step 2 are now in turn bound.<br>b) the other end of the second antibody carries a marker molecule, which reacts in the next step and provides the measurement signal.<br><br>The patient antibody sought is now – if present in the sample – wrapped between<br>“its” antigen and the labelled second antibody. This is why this test method is also called the sandwich method. Anything that has not reacted is rinsed out of the wells again. A solution is now pipetted into the remaining “sandwich packets”, which reacts with the labelling molecules from step 3.b).<br><br>The result is a color, the intensity of which is measured. The stronger the colour, the more antibodies were in the patient’s serum.'];    
$rapid_qa[] = ['no'=>'22','question'=>'Does VCare.Earth serve B2B And B2C?', 'answer'=>'Given the current global circumstances, VCare.Earth has prioritized its products and service towards B2B companies, frontline workers, key workers and towards the vulnerable who are in need of our products the most.<br><br>In the future, we may consider to gradually open our B2C platform, and we thank our clients for their patience and understanding during these times.'];    

?>





<section class="section">
    <div class="container">
        <div class="tabs is-centered faq-tabs">
            <ul>
                <li class="is-active">
                    <a href="#respirator">
                        Particulate Disposable Respirator
                    </a>
                </li>
                <li>
                    <a href="#rapid-test">
                        IgG/IgM Antibody Rapid Test
                    </a>
                </li>
            </ul>
        </div>
        <div class="faq-content">
            <div class="tab-content respirator">
                <div class="columns">
                    <div class="column">
                        <div id="accordion_second">
                            @foreach ($resp_qa as $qa )  
                            <article class="message">
                                <div class="message-header">
                                    <p>{{ $qa['question'] }} </p>
                                    <a href="#collapsible-message-accordion-second-{{ $qa['no'] }}" data-action="collapse">
                                        <span class="icon">
                                            <i class="fas fa-angle-down" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                </div>
                                <div id="collapsible-message-accordion-second-{{ $qa['no'] }}" class="message-body is-collapsible" data-parent="accordion_second" data-allow-multiple="true">
                                    <div class="message-body-content">
                                        {!! $qa['answer'] !!}
                                    </div>
                                </div>
                            </article>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>                
            <div class="tab-content rapid-test">
                <div class="columns">
                    <div class="column">
                        <div id="accordion_second">
                            @foreach ($rapid_qa as $qa )  
                            <article class="message">
                                <div class="message-header">
                                    <p>{{ $qa['question'] }} </p>
                                    <a href="#collapsible-message-accordion-second-{{ $qa['no'] }}" data-action="collapse">
                                        <span class="icon">
                                            <i class="fas fa-angle-down" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                </div>
                                <div id="collapsible-message-accordion-second-{{ $qa['no'] }}" class="message-body is-collapsible" data-parent="accordion_second" data-allow-multiple="true">
                                    <div class="message-body-content">
                                        {!! $qa['answer'] !!}
                                    </div>
                                </div>
                            </article>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



@endsection