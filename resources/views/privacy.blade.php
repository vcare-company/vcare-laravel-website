@extends('layout.main')



@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('privacy') }}"
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/terms-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/terms-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection


@section('head-title')
<title>Privacy Policy | Vcare.earth</title>
@endsection



@section('seo-meta-title')
<?php 
    $meta_title = "VCare.Earth Commitment to Care - Privacy" ; 
    $meta_desc = "Our products and services are available to customers worldwide. For any questions on privcy matters you can refer on this page.";
?>    
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 





@section('content')


<section class="section section-header">
    <div class="container">
        <div class="columns">
            <div class="column title-column">
                <h1 class="title is-1">Privacy Policy</h1>
            </div>
        </div>        
    </div>
</section>



<section class="section">
    <div class="container">
        <p><b>Introduction</b></p><br>
        <p>
            By accessing or using www.vcare.earth (the “Website”), a website owned and maintained by VCARE HEALTH IBERICA, S.L. (“Company”, “we,” “us” or “our”), you consent to the information collection, disclosure and use practices described in this Privacy Policy. This Privacy Policy applies to all services provided by us and sets out how we may collect, use and disclose information in relation to users of the Website. 
        </p>
        <br>
        <p>
            The Website provides a platform and e-commerce store to acquire medical equipment and devices such as but not limited to; respiratory and surgical masks, respirators, antibody rapid test kits (Covid-19), thermometers, sanitizers, safety glasses, protective eye masks, gloves or protective gowns and lab coats. 
        </p>
        <br>
        <p>    
            Use of Website, including all materials presented herein and all Services provided by us, is subject to separate Terms and Conditions and the following Privacy Policy. 
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Information We Collect</b></p><br>
        <p>
            Your privacy is important to us and we have taken steps to ensure that we do not collect more information from you than is necessary for us to provide you with our services and to protect your account.     
        </p>
        <br>
        <p>
            We may process the following categories of personal data about you:
        </p>
        <br>
        <p>    
            Personal Data means any information relating to an identified or identifiable natural person (“You”, or “User”). This information may include, in particular by reference to an identifier, such as your name, address, identification number, location data, business name, business address, or any other document as we may deem necessary for provision of Services. When you contact us through any of the communication modes as mentioned below, your email address may be added to our mailing list from which you can unsubscribe at any time using the unsubscribe link in each email or by contacting us at info@vcare.earth.   
        </p>
        <br>
        <p>
            Communication Data may include any communication that you send to us whether that be through the contact form on our Website, through call, email, text, social media messaging, social media posting or any other communication that you send us. We process this data for the purposes of communicating with you, for record keeping and for the establishment, pursuance or defense of legal claims. 
        </p>
        <br>
        <p>
            Our lawful ground for this processing is our legitimate interests which in this case are to reply to communications sent to us, to keep records and to establish, pursue or defend legal claims. 
        </p>
        <br>
        <p>
            Technical Data may include data about your use of our Website and online services such as your IP address, your login data, details about your browser, length of visit to pages on our Website, page views and navigation paths, details about the number of times you use our Website, time zone settings and other technology on the devices you use to access our Website. The source of this data is from our analytics tracking system. We process this data to analyze your use of our Website and other online services, to administer and protect our business and Website, to deliver relevant website content and advertisements to you and to understand the effectiveness of our advertising. Our lawful ground for this processing is our legitimate interests which in this case are to enable us to properly administer our Website and our business and to grow our business and to decide our marketing strategy.
        </p>
        <br>
        <p>
            Marketing Data may include data about your use of our Website and online services such as your IP address, your login data, details about your browser, length of visit to pages on our Website, page views and navigation paths, details about the number of times you use our Website, time zone settings and other technology on the devices you use to access our Website. The source of this data is from our analytics tracking system. We process this data to analyze your use of our Website and other online services, to administer and protect our business and Website, to deliver relevant website content and advertisements to you and to understand the effectiveness of our advertising. Our lawful ground for this processing is our legitimate interests which in this case are to enable us to properly administer our Website and our business and to grow our business and to decide our marketing strategy.  
        </p>
        <br>
        <p>
            We may use Personal Data, Communication Data, Technical Data and Marketing Data (the “Data”) to provide you with Services and to deliver relevant Website content and advertisements to you and to measure or understand the effectiveness of the advertising we serve you. We may also use such data to send other marketing communications to you.
        </p>
    </div>
</section>


<section class="section">
    <div class="container">
        <p><b>Name and Address of the Data Controller</b></p><br>
        <p>
            Data Controller for the purposes of the General Data Protection Regulation (GDPR), other data protection laws applicable in Member states of the European Union and other provisions related to data protection is:
        </p>
        <br>
        <p>
            Name: VCARE HEALTH IBERICA, S.L. <br>
            Address: Madrid, Spain <br>
            Email: info@vcare.earth <br>
            Website: www.vcare.earth <br>
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Consent and its Withdrawal</b></p><br>
        <p>
            When you visit our Website you provide us consent to use your Data as per this Privacy Policy. In order to provide you with the Services, it is necessary for us to collect all relevant and necessary Data about you from you. 
        </p>
        <br>
        <p>
            If you change your mind, you may withdraw your consent for us to contact you, for the continued collection, use or disclosure of your information, at any time, by contacting us at info@vcare.earth.
        </p>
    </div>
</section>


<section class="section">
    <div class="container">
        <p><b>How We Use and Process the Data</b></p><br>
        <p>
            The Data collected by us from you may be used to provide you with Services and better understand your needs related services and programs, to correspond with you and reply to your questions with about our services. 
        </p>
        <br>
        <p>
            We will not rent or sell your Data to others. We may store the Data in locations outside the direct control of the Company (for instance, on servers or databases co-located with hosting providers). If you provide any Data to us, you are deemed to have authorized us to collect, retain and use that data for the following purposes:
        </p>
        <br>
        <p>
            Verifying your identity;<br> 
            Providing you with customer service and responding to your queries, feedback, or disputes;<br> 
            Making such disclosures as may be required for any of the above purposes or as required by law, regulations and guidelines or in respect of any investigations, claims or potential claims brought on or against us; <br>
            Provide and maintain the Services; <br>
            Notify you about changes to our Services; <br>
            We shall ensure that: The Data collected and processed for and on our behalf by any party is collected and processed fairly and lawfully; <br>
            You are always made fully aware of the reasons for the collection of Data and are given details of the purpose(s) for which the data will be used; <br>
        </p>
        <br>
        <p>
            The Data is only collected to the extent that is necessary to fulfil the purpose(s) for which it is required; <br>
            No Data is held for any longer than necessary in light of the purpose(s) for which it is required. Whenever cookies or similar technologies are used online by us, they shall be used strictly in accordance with the law; <br>
            You are informed if any data submitted by you online cannot be fully deleted at your request under normal circumstances and how to request that the we delete any other copies of that data, where it is within your right to do so;<br>
            All Data is held in a safe and secure manner taking all appropriate technical and organizational measures to protect the data; <br>
            All data is transferred securely, whether it is transmitted electronically or in hard copy. You can fully exercise your rights with ease and without hindrance
        </p>   
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Disclosure of Data</b></p><br>
        <p>
            We shall not be able to keep your Data private in response to legal process i.e., a court order or a subpoena, a law enforcement agency’s request. If, in our view, it is deemed appropriate to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of our terms of use, or as otherwise required by law, we may be compelled to disclose the Data and Personal Data. Moreover, in case of takeover, merger or acquisition, we reserve a right to transfer your data to new platform. 
        </p>
        <br>
        <p>
            We may disclose the Data in the good faith belief that such action is necessary to:
        </p>
        <br>
        <p>
            comply with a legal obligation <br>
            protect and defend our rights or property <br> 
            prevent or investigate possible wrongdoing <br>
            protect the personal safety of users of the Service or the public <br> 
            protect against legal liability
        </p>
        <br>
        <p>
            When necessary, we may also disclose and transfer your Data to our professional advisers, law enforcement agencies, insurers, government and regulatory and other organizations.
        </p>   
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Data Storage</b></p><br>
        <p>
            Your Data may be stored and processed at the servers in the United States, Europe, or any other country in which the Website or its subsidiaries, affiliates or service providers maintain facilities. 
        </p>
        <br>
        <p>
            The Website may transfer Data to affiliated entities, or to other third parties across borders and from your country or jurisdiction to other countries or jurisdictions around the world. Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer. 
        </p>
        <br>
        <p>
            We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.
        </p>
        <br>
        <p>
            We will only retain your Data preferably for as long as necessary to fulfil the purposes we collected it for, including for the purposes of satisfying any legal, see section regarding insurance requirement and reporting requirements. When deciding what the correct time is to keep the Data for we look at its amount, nature and sensitivity, potential risk of harm from unauthorised use or disclosure, the processing purposes, if these can be achieved by other means and legal requirements.
        </p>   
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Who we share your data with</b></p><br>
        <p>
            We store all the Data submitted by you through Website at a secure database. We employ commercially reasonable security methods to prevent unauthorized access to the Website, to maintain data accuracy and to ensure the correct use of the information we hold.         
        </p>
        <br>
        <p>
            For registered users of the Website, your Personal Information (if any) may be viewed through your account, which is protected by a password. We recommend that you do not divulge your password to anyone. Our personnel will never ask you for your password in an unsolicited phone call or in an unsolicited email. If you share a computer with others, you should not choose to save your log-in information (e.g., user ID and password) on that shared computer. Remember to sign out of your account and close your browser window when you have finished your session.
        </p>
        <br>
        <p>
            We are concerned with protecting your privacy and data, but we cannot ensure or warrant the security of any data you transmit to or guarantee that your Data may not be accessed, disclosed, altered or destroyed by breach of any of our industry standard physical, technical or managerial safeguards.
        </p>
        <br>
        <p>
            Any Data supplied by you will be retained by us and will be accessible by our employees, any service providers engaged by us and third parties.
        </p>   
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>The rights of Users</b></p><br>
        <p>
            You may exercise certain rights regarding your Data processed by us. In particular, you have the right to do the following:     
        </p>
        <br>
        <p>
            For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.       
        </p>
        <br>
        <p>
            Right of confirmation <br>
            You shall have the right granted by the European legislator to obtain from the Company the confirmation as to whether or not personal data concerning you are being processed. If you wish to avail of this right of confirmation, you may, at any time, contact us at info@vcare.earth<br> 
            Right of Access <br>
            You shall have the right granted by the European legislator to obtain from the Company free information about your personal data stored at any time and a copy of this information. Furthermore, the European directives and regulations grant you access to the following information:<br> 
            the purposes of the processing; <br>
            the categories of personal data concerned;<br>
            the recipients or categories of recipients to whom the personal data have been or will be disclosed, in particular recipients in third countries or international organisations; the existence of the right to lodge a complaint with a supervisory authority; <br>
            where the personal data are not collected from you, any available information as to its source; <br>
            the existence of automated decision-making, including profiling, referred to in Article 22(1) and (4) of the GDPR and, at least in those cases, meaningful information about the logic involved, as well as the significance and envisaged consequences of such processing for you. <br>
            Right to rectification <br>
            You shall have the right granted by the European legislator to obtain from the Company without undue delay the rectification of inaccurate personal data concerning you. Taking into account the purposes of the processing, you shall have the right to have incomplete personal data completed, including by means of providing a supplementary statement <br>
            </p>
        <br>
        <p> 
            If you wish to avail yourself this right of access, you may, at any time, contact us at info@vcare.earth. <br>
            Right to erasure (Right to be forgotten) <br>
            You shall have the right granted by the European legislator to obtain from the Company the erasure of personal data concerning you without undue delay, and the Company shall have the obligation to erase personal data without undue delay where one of the following grounds applies, as long as the processing is not necessary: <br>
            The personal data are no longer necessary in relation to the purposes for which they were collected or otherwise processed. <br>
            You withdraw consent to which the processing is based according to point (a) of Article 6(1) of the GDPR, or point (a) of Article 9(2) of the GDPR, and where there is no other legal ground for the processing. <br>
            </p>
        <br>
        <p>    
            You object to the processing pursuant to Article 21(1) of the GDPR and there are no overriding legitimate grounds for the processing, or you object to the processing pursuant to Article 21(2) of the GDPR. <br>
            The personal data have been unlawfully processed.<br> 
            The personal data must be erased for compliance with a legal obligation in Union or Member State law to which the Company is subject. <br>
            The personal data have been collected in relation to the offer of information society services referred to in Article 8(1) of the GDPR. <br>
            </p>
        <br>
        <p>
            If one of the aforementioned reasons applies, and you wish to request the erasure of personal data stored by the Company, you may, at any time, contact us at info@vcare.earth.
            </p>
        <br>
        <p>    
            Where the Company has made personal data public and is obliged pursuant to Article 17(1) to erase the personal data, it, taking account of available technology and the cost of implementation, shall take reasonable steps, including technical measures, to inform other controllers processing the personal data that you have requested erasure by such controllers of any links to, or copy or replication of, those personal data, as far as processing is not required. All employees of the Company will arrange the necessary measures in individual cases. <br>
            Right of restriction of processing <br>
            You shall have the right granted by the European legislator to obtain from the Company restriction of processing where one of the following applies: The accuracy of the personal data is contested by the data subject, for a period enabling the Company to verify the accuracy of the personal data. The processing is unlawful, and the data subject opposes the erasure of the personal data and requests instead the restriction of their use instead. The Company no longer needs the personal data for the purposes of the processing, but they are required by the data subject for the establishment, exercise or defense of legal claims. <br>
            You have objected to processing pursuant to Article 21(1) of the GDPR pending the verification whether the legitimate grounds of the Company override those of yours. If one of the aforementioned conditions is met, and you wish to request the restriction of the processing of personal data stored by the Company, you may at any time contact the Company. 
            </p>
        <br>
        <p>    
            Right to data portability <br>
            You shall have the right granted by the European legislator, to receive the personal data concerning you, which was provided to the Company, in a structured, commonly used and machine-readable format. You shall have the right to transmit those data to another data controller without hindrance from the Company to which the personal data have been provided, as long as the processing is based on consent pursuant to point (a) of Article 6(1) of the GDPR or point (a) of Article 9(2) of the GDPR, or on a contract pursuant to point (b) of Article 6(1) of the GDPR, and the processing is carried out by automated means, as long as the processing is not necessary for the performance of a task carried out in the public interest or in the exercise of official authority vested in the Company. 
            </p>
        <br>
        <p>
            In order to assert the right to data portability, you may at any time contact us. <br>
            Right to object <br>
            You shall have the right granted by the European legislator to object, on grounds relating to your particular situation, at any time, to processing of personal data concerning you, which is based on point (e) or (f) of Article 6(1) of the GDPR. This also applies to profiling based on these provisions. 
            </p>
        <br>
        <p>
            The Company shall no longer process the personal data in the event of the objection, unless we can demonstrate compelling legitimate grounds for the processing which override the interests, rights and freedoms of the data subject, or for the establishment, exercise or defence of legal claims. 
            </p>
        <br>
        <p>
            If the Company processes personal data for direct marketing purposes, you shall have the right to object at any time to processing of personal data concerning you for such marketing. This applies to profiling to the extent that it is related to such direct marketing. If you object to the Company to the processing for direct marketing purposes, the Company will no longer process the personal data for these purposes. 
            </p>
        <br>
        <p>     
            In addition, you have the right, on grounds relating to your particular situation, to object to processing of personal data concerning you by the Company for scientific or historical research purposes, or for statistical purposes pursuant to Article 89(1) of the GDPR, unless the processing is necessary for the performance of a task carried out for reasons of public interest. In order to exercise the right to object, you may contact us.
            </p>
        <br>
        <p>    
            Automated individual decision-making, including profiling <br>
            You shall have the right granted by the European legislator not to be subject to a decision based solely on automated processing, including profiling, which produces legal effects concerning you, or similarly significantly affects you, as long as the decision (1) is not is necessary for entering into, or the performance of, a contract between you and the Company, or (2) is not authorized by Union or Member State law to which the Company is subject and which also lays down suitable measures to safeguard your rights and freedoms and legitimate interests, or (3) is not based on your explicit consent. 
            </p>
        <br>
        <p>
            If the decision (1) is necessary for entering into, or the performance of, a contract between you and the Company, or (2) it is based on your explicit consent, the Company shall implement suitable measures to safeguard your rights and freedoms and legitimate interests, at least the right to obtain human intervention on the part of the Company, to express your point of view and contest the decision. <br>
            Right to withdraw data protection consent <br>
            You shall have the right granted by the European legislator to withdraw your consent to processing of your personal data at any time. If you wish to exercise the right to withdraw the consent, you may, at any time, contact us. You may initiate request with us at info@vcare.earth to exercise any of the above mentioned rights. We shall review your request and, in our own discretion, honor your request, if deemed necessary by us, within reasonable time.
        </p>
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Compliance with the GDPR</b></p><br>
        <p>
            For users based in the European Union (EU), the Website shall make all reasonable efforts to ensure that it complies with The General Data Protection Regulation (GDPR) (EU) 2016/679 as set forth by the European Union regarding the collection, use, and retention of Data from European Union member countries. Website shall make all reasonable efforts to adhere to the requirements of notice, choice, onward transfer, security, data integrity, access and enforcement.       
        </p>   
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Cookies</b></p><br>
        <p>
            We use technologies, such as cookies, to make better user experience, customise content and advertising, to provide social media features and to analyse traffic to the Website. Where applicable the Website uses a cookie control system allowing the user on their first visit to the Website to allow or disallow the use of cookies on their computer / device. 
            </p>
        <br>
        <p>     
            We also share information about your use of our site with our trusted social media, advertising and analytics partners. Third parties, including Facebook and Google, may use cookies, web beacons, and other storage technologies to collect or receive your information from the Websites and use that information to provide measurement services and target ads. 
            </p>
        <br>
        <p>   
            Cookies are small files saved to the user’s computers’ or mobile devices’ hard drive or memory that track, save and store information about the user’s interactions and usage of the Website. This allows the Website, through its server to provide the users with a tailored experience within this Website. 
            </p>
        <br>
        <p>   
            Users are advised that if they wish to deny the use and saving of cookies from this Website on to their computers hard drive they should take necessary steps within their web browsers security settings to block all cookies from this Website and its external serving vendors. 
            </p>
        <br>
        <p>    
            We may gather certain information automatically and store it in log files. This information includes Internet protocol (IP) addresses, browser type, Internet service provider (ISP), referring/exit pages, operating system, date/time stamp, and click stream data. We may use this information, which does not identify individual users, to analyze trends, to administer the Website, to track users’ movements around the Website and to gather demographic information about our user base as a whole. 
            </p>
        <br>
        <p>    
            We may track the referring URL (the web page you left before coming to the Website) and the pages, links, and graphics of the Website you visited. We do so because it allows us to evaluate the reputation and responsiveness of specific web pages and any promotional programs we may be running. 
            </p>
        <br>
        <p>    
            Managing Cookies: Many web browsers allow you to manage your preferences. You can set your browser to refuse cookies or delete certain cookies. You may be able to manage other technologies in the same way that you manage cookies using your browser’s preferences.        
        </p>  
    </div>
</section>

<section class="section">
    <div class="container">
        <p><b>Third Party Links</b></p><br>
        <p>
            The Website may contain links to third-party websites, plug-ins and applications. Except as otherwise discussed in this Privacy Policy, this document only addresses the use and disclosure of information we collect from you on our Website. Other websites accessible through our site via links or otherwise have their own policies in regard to privacy. We are not responsible for the privacy policies or practices of third parties. When you leave our Website, we encourage you to read the privacy notice of every website you visit.    
        </p>   
    </div>
</section>


<section class="section">
    <div class="container">
        <p><b>Changes to this Privacy Statement</b></p><br>
        <p>
            We may modify these this Privacy Policy from time to time, and any such change shall be reflected on the Website with the updated version of the Privacy Policy and you agree to be bound to any changes to the updated version of Privacy Policy when you use the Website or its services. 
        </p>
        <br>
        <p>     
            You acknowledge and agree that it is your responsibility to review this Website and this Policy periodically and to be aware of any modifications. Updates to this Policy will be posted on this page. 
        </p>
        <br>
        <p>     
            Also, occasionally there may be information on the Website that contains typographical errors, inaccuracies or omissions that may relate to service descriptions, pricing, availability, and various other information, and the Website reserves the right to correct any errors, inaccuracies or omissions and to change or update the information at any time, without prior notice   
        </p>   
    </div>
</section>


<section class="section">
    <div class="container">
        <p><b>Contact Us</b></p><br>
        <p>
            If you have questions about our Privacy Policy, please contact us via email: info@vcare.earth    
        </p>   
    </div>
</section>

@endsection