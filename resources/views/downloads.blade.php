@extends('layout.main')



@section('head-end')
<script type="application/ld+json">
{   
    "@context": "https://schema.org/", 
    "@type": "BreadcrumbList", 
    "itemListElement": 
    [
        @foreach($breadcrumbs as $brkey => $br)
        {
            "@type": "ListItem", 
            "position": {{ $brkey + 1 }}, 
            "name": "{{ $br['title']}}",
            "item": "{{ $br['route'] ? route($br['route']) : route('downloads') }}" 
        },
        @endforeach
    ]

}
</script>
<link rel="stylesheet" href="{{ mix('css/downloads-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/downloads-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection


@section('head-title')
<title>Downloads | Vcare.earth</title>
@endsection



@section('seo-meta-title')
<?php 
		$meta_title = "VCare.Earth Commitment to Care - Downloads" ; 
		$meta_desc = "Everything about VCare.Earth and our products is right here. We provide premium medical and healthcare products to clients local and global.";
?>    
<meta property="og:title" content="{{ $meta_title }}" />
<meta name="twitter:title" content="{{ $meta_title }}" />
@endsection 

@section('seo-meta-description')
<meta property="og:description" content="{{ $meta_desc }}" />
<meta name="twitter:description" content="{{ $meta_desc }}" />
<meta name="description" content="{{ $meta_desc }}"/>
@endsection 





@section('content')

<section class="section">
	<div class="container">

			<div class="columns is-flex-end">
					<div class="column title-column">
							<h2 class="subtitle">
									DOWNLOADS
							</h2>
							<h1 class="title is-1">Resource Center </h1>
					</div>
			</div>        

	</div>
</section>

<section class="section blue-vcare-bg" 
style="background-image: url({{ env('APP_URL') }}/files/charity/charity.png);">
	<div class="container">
		<div class="columns is-vcentered is-centered">
			<div class="column is-8">
				<h2 class="subtitle is-2 has-text-centered has-text-white">
					{{-- Everything you neeed to know<br> about VCare Products --}}
						Everything you need to know about VCare.Earth and our products, right here. 
				</h2>
			</div>
		</div>
	</div>
</section>



<section class="section resources">
<div class="container">


<section class="section resources">
<div class="container">
<div class="columns">
	


























	
	{{-- list menu --}}
	{{-- ////////////////////////////////////////////////////////////////////// --}}
	{{-- ////////////////////////////////////////////////////////////////////// --}}
	<div class="column is-narrow">


	<div class="tabs tab-menu company-resources">
		<h2 class="title is-6"> COMPANY RESOURCES </h2>
		<ul>
			@foreach($company_resources as $cr)
					<li> 
						<a href="{{ route('downloads') }}#{{ $cr['code'] }}"
						data-tab_id="{{ $cr['code'] }}"> 
							{{ $cr['name'] }}
						</a> 
					</li>
			@endforeach
		</ul>
	</div>

	@foreach($categories_parent as $cat_par)
	<div class="tabs tab-menu">
		<h2 class="title is-6"> {{ $cat_par['name'] }} </h2>
		@foreach($cat_par['children'] as $cat)
			<h2 class="subtitle is-6"> {{ $cat['name'] }} </h2>
			<ul>
				@if(count($cat['products']))
					<?php foreach($cat['products'] as $p) { ?>
						<?php if ($p['coming_soon']) { ?>
							<li> 
								<a href="{{ route('downloads') }}#{{ $p['code'] }}"
								data-tab_id="{{ $p['code'] }}"> 
										{{ $p['name_type'].' '.$p['name'] }} 
										@if ($p['name_code'])
												{{ $p['name_code'] }}
										@endif       
										*Coming Soon
								</a> 
							</li>
						<?php } else { ?>
							<li> 
								<a href="{{ route('downloads') }}#{{ $p['code'] }}"
								data-tab_id="{{ $p['code'] }}"> 

									{{-- Because boss wants something different for surgical gown --}}
									@if ($cat['code']=='surgical-gown')
											{{ $p['name'].' '.$p['name_code'].' ('.$p['name_type'].')' }}
									@else
											{{ $p['name_type'].' '.$p['name'] }} 
											@if ($p['name_code'])
													{{ $p['name_code'] }}
											@endif                                            
									@endif
								</a> 
							</li>
						<?php } ?>
					<?php } ?>
				
				@else
					<li> 
						<a href="{{ route('downloads') }}#{{ $cat['code'] }}"
						data-tab_id="{{ $cat['code'] }}"> 
							*Coming Soon 
						</a>         
					</li>
				@endif



			</ul>
		@endforeach
	</div>
	@endforeach

	</div>
	{{-- ////////////////////////////////////////////////////////////////////// --}}
	{{-- ////////////////////////////////////////////////////////////////////// --}}
	{{-- list menu --}}


























	{{-- list content --}}
	<div id="tab-contents" class="column tab-contents ">
	{{-- /////////////////////////////////// --}}


















		{{-- downloads page landing tab --}}
		{{-- /////////////////////////////////////////////////////////// --}}
		<div id="downloads-page" class="tab-content" style="display: block;">
			<div class="columns is-multiline">


			<h2 class="subtitle is-5 tab-content-title mt-1"> COMPANY RESOURCES </h2>
			@foreach($company_resources as $cr)  
			<div class="column is-4 is-flex">
					<div class="tile">
							<div class="img-container">
								<a href="{{ route($cr['route']) }}"
								target="_blank">
									<img src="{{ env('APP_URL').'/files/downloads/'.$cr['image'].'?u='.env('APP_ASSET_TIMESTAMP') }}" 
									alt="Vcare Earth {{ $cr['name'] }}"/>
								</a>
							</div>
							{{-- <h1>{{ $cr['group'] }}</h1> --}}
							<br>
							<hr>
							<a href="{{ route($cr['route']) }}"
							target="_blank"
							style="{{ $cr['style'] }}">
								<span>{!! $cr['name'] !!}</span> 
								<i class="fas fa-chevron-right ml-1" style="float: right;"></i>
							</a>
					</div>
			</div>
			@endforeach
			<h2 class="subtitle tab-content-title mb-6">&nbsp;</h2>





			@foreach ($categories_parent as $i => $categ)
				@if (count($categ['children']))
					<h2 class="subtitle is-5 tab-content-title mt-1"> {!! $categ['html'] !!} </h2>
					@foreach ($categ['children'] as $cat) 

						<?php if (count($cat['products'])) { ?>
								@foreach($cat['products'] as $k =>  $p)
								<div class="column is-4 is-flex">
										<div class="tile">

												<h2 class="name"> {{ $cat['name'] }} </h2>
												<h2 class="name_type">{{ $p['name_type'] }}&nbsp;</h2>

												<div class="img-container">
														<a class="gototab" href="#{{ $p['code'] }}"
														data-tab_id="{{ $p['code'] }}">                          
																@if ($p['main-image'])
																		<img 
																		src="{{ env('APP_URL').'/files/products/'.$p['public_id'].'/images/thumbnails/'.$p['main-image']['type'].'/'.$p['main-image']['filename'].'.'.$p['main-image']['ext_thumb'].'?u='.env('APP_ASSET_TIMESTAMP') }}" 
																		alt="Vcare Earth {{ $p['main-image']['title'] }}">
																@else
																<div class="img-placeholder-container">
																		<img class="img-placeholder" alt="VCare Earth Logo" src="/files/logos/vcareplaceholder.svg?u={{ env('APP_ASSET_TIMESTAMP') }}"/>
																		<h2 class="subtitle has-text-centered coming-soon-text">Coming Soon</h2>
																</div>
																@endif
														</a>
												</div>

												<br>
												<hr>

												<a class="gototab" href="#{{ $p['code'] }}"
												data-tab_id="{{ $p['code'] }}">
														<span>
														{{ $p['name'] }} 
														@if ($p['name_code'])
																<span class="ml-1"> - {{ $p['name_code'] }}</span>
														@endif
														
														@if ($p['coming_soon'])
														*Coming Soon
														@endif      
														</span>

														<i class="fas fa-chevron-right ml-1" style="float: right;"></i>
												</a>

										</div>
								</div>
								@endforeach
						<?php } else {?>
								<div class="column is-4 is-flex">
										<div class="tile">

												<h2 class="name"> {{ $cat['name'] }} </h2>
												<h2 class="name_type">&nbsp;</h2>
												<div class="img-container">
														<a class="gototab" href="#{{ $cat['code'] }}"
														data-tab_id="{{ $cat['code'] }}">
																<div class="img-placeholder-container">
																		<img class="img-placeholder" 
																		alt="VCare Earth Logo"
																		src="/files/logos/vcareplaceholder.svg?u={{ env('APP_ASSET_TIMESTAMP') }}"/>
																		<h2 class="subtitle has-text-centered coming-soon-text">Coming Soon</h2>
																</div>
														</a>
												</div>

												<br>
												<hr>

												<a class="gototab" href="#{{ $cat['code'] }}"
												data-tab_id="{{ $cat['code'] }}">
														*Coming Soon      
														<i class="fas fa-chevron-right ml-1" style="float: right;"></i>
												</a>

										</div>
								</div>                
						<?php } ?>
					
					@endforeach

					@if($i < count($categories_parent)-1)
					<hr class="mt-6 mb-6" style="width: 100%;"/>
					@endif

				@endif
			
			@endforeach

			
			</div>          
			</div>    
		{{-- /////////////////////////////////////////////////////////// --}}
		{{-- downloads page landing tab --}}




















		{{-- Company Resources --}}
		@foreach($company_resources as $cr)
		<div id="{{ $cr['code'] }}" class="tab-content">
		<div class="columns is-multiline">
		{{-- /////////////////////////////////////////////////////////// --}}
		<h2 class="title is-1 tab-content-title"> {{ $cr['name'] }} </h2>

			<div class="column is-4 is-flex">
					<div class="tile">
							<div class="img-container">
								<a href="{{ route($cr['route']) }}"
								target="_blank">
									<img src="{{ env('APP_URL').'/files/downloads/'.$cr['image'].'?u='.env('APP_ASSET_TIMESTAMP') }}" 
									alt="Vcare Earth {{ $cr['name'] }}">
								</a>
							</div>
							{{-- <h1>{{ $cr['group'] }}</h1> --}}
							<br>
							<hr>
							<a href="{{ route($cr['route']) }}"
							target="_blank">
								{{ $cr['name'] }} 
								<i class="fas fa-chevron-right ml-1" style="float: right;"></i>
							</a>
					</div>
			</div>
		{{-- /////////////////////////////////////////////////////////// --}}
		</div>          
		</div>
		@endforeach
		{{-- Company Resources --}}



		{{-- Product Resources --}}
		@foreach($categories_parent as $cat_par)
		@foreach($cat_par['children'] as $cat)






		<?php if(count($cat['products'])) { ?>

				<?php foreach($cat['products'] as $p) { ?>
					
					<?php if($p['coming_soon']) { ?>

						{{-- if sub cat has products but product coming soon--}}
						@include('product.partials.downloads.tab-content-coming-soon',[
							'code'=>$p['code'],
							'name'=>$cat['name'],
							'name_type'=>$p['name_type'],
						])
					
					<?php } else { ?>

						{{-- /////////////////////////////////////////////////////////// --}}
						<div id="{{ $p['code'] }}" class="tab-content">
								<div class="columns is-multiline">

	
								<div class="name-group-title">
										<h2 class="subtitle name-code-group"> {{ $p['category']['name'] }} </h2>
										<h2 class="subtitle name-type-code is-5" >{{ $p['name_type'] .' '. $p['name_code'] }}</h2>
										<h2 class="title product-name">{{ strtoupper($p['name']) }} </h2>
								</div>
				
				
														
										@include('product.partials.downloads.tab-content',[
										'cat'=>$cat,
										'p'=>$p,
										])
								</div>
						</div>
						{{-- /////////////////////////////////////////////////////////// --}}
										
					<?php } ?>


				<?php } ?>

		<?php } else { ?>
		
			{{-- if sub cat has no products --}}
			@include('product.partials.downloads.tab-content-coming-soon',[
				'code'=>$cat['code'],
				'name'=>$cat['name'],
				'name_type'=>'',
			])

		<?php } ?>



		@endforeach
		@endforeach
		{{-- Product Resources --}}


	{{-- /////////////////////////////////// --}}
	</div>
	{{-- list content --}}

</div>




</div>
</section>

</div>
</section>



<section class="section resources">
<div class="container">
	{{-- <h4 class="title is-4 mb-6 has-text-centered">PACKAGING DETAILS</h4> --}}
	{{-- <h2 class="subtitle"> {{ $p['name'] }} </h2> --}}

</div>
</section>




@endsection
