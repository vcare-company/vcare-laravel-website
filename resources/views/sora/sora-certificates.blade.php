@extends('layout.main')



@section('head-end')
<link rel="stylesheet" href="{{ mix('css/sora-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/sora-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection

@section('head-title')
<title>Vcare.earth SORA</title>
@endsection


@section('content')


<section class="section">
    <div class="container">
  
        <div class="columns is-flex-end">
            <div class="column title-column">
                {{-- <h2 class="subtitle">
                    Certificates
                </h2> --}}
                <h1 class="title is-1">Certificates </h1>
            </div>
        </div>        
  
    </div>
  </section>
  
  <section class="section blue-vcare-bg" 
  style="background-image: url({{ env('APP_URL').'/files/home/images/homepage-prideinquality.jpg?u='.env('APP_ASSET_TIMESTAMP') }}); padding: 15vh 10px;">
    <div class="container">
      <div class="columns is-vcentered">
        <div class="column">
          <h2 class="subtitle is-2 has-text-centered has-text-white">
            {{-- Certificate Passwords and Files --}}
          </h2>
        </div>
      </div>
    </div>
  </section>
  
  {{-- background-image: url(https://vcare.ddns.net/files/home/images/homepage-prideinquality.jpg?u=20200823); --}}

<section class="section resources">
<div class="container">
<div class="columns">
    
    
        
















  
  {{-- list menu --}}
  {{-- ////////////////////////////////////////////////////////////////////// --}}
  {{-- ////////////////////////////////////////////////////////////////////// --}}
  <div class="column is-narrow">
    <div class="tabs tab-menu mb-1 is-active">
        <a class="title is-5 password-menu" 
        href="{{ route('sora-certificates-page') }}#password-table"
        data-tab_id="password-table"
        style=""> 
            PASSWORDS
        </a>
    </div>

  
    @foreach($categories_parent as $cat_par)
    <div class="tabs tab-menu">
      <h1 class="title is-6"> {{ $cat_par['name'] }} </h1>
      @foreach($cat_par['children'] as $cat)
        <h1 class="subtitle is-6"> {{ $cat['name'] }} </h1>
        <ul>
          @if(count($cat['products']))
            <?php foreach($cat['products'] as $p) { ?>
              <?php if ($p['coming_soon']) { ?>
                <li> 
                  <a href="{{ route('sora-certificates-page') }}#{{ $p['code'] }}"
                  data-tab_id="{{ $p['code'] }}"> 
                      {{ $p['name_type'].' '.$p['name'] }} 
                      @if ($p['name_code'])
                          {{ $p['name_code'] }}
                      @endif       
                      *Coming Soon
                  </a> 
                </li>
              <?php } else { ?>
                <li> 
                  <a href="{{ route('sora-certificates-page') }}#{{ $p['code'] }}"
                  data-tab_id="{{ $p['code'] }}"> 
  
                    {{-- Because boss wants something different for surgical gown --}}
                    @if ($cat['code']=='surgical-gown')
                        {{ $p['name'].' '.$p['name_code'].' ('.$p['name_type'].')' }}
                    @else
                        {{ $p['name_type'].' '.$p['name'] }} 
                        @if ($p['name_code'])
                            {{ $p['name_code'] }}
                        @endif                                            
                    @endif
                  </a> 
                </li>
              <?php } ?>
            <?php } ?>
          
          @else
            <li> 
              <a href="{{ route('sora-certificates-page') }}#{{ $p['code'] }}"
              data-tab_id="{{ $cat['code'] }}"> 
                *Coming Soon 
              </a>         
            </li>
          @endif
  
  
  
        </ul>
      @endforeach
    </div>
    @endforeach
  
    </div>
    {{-- ////////////////////////////////////////////////////////////////////// --}}
    {{-- ////////////////////////////////////////////////////////////////////// --}}
    {{-- list menu --}}
  
  
  
  
  
  











    {{-- list content --}}
    {{-- ////////////////////////////////////////////////////////////////////// --}}
    {{-- ////////////////////////////////////////////////////////////////////// --}}
    <div id="tab-contents" class="column tab-contents ">
  
  
        
        

            {{-- /////////////////////////////////////////////////////////// --}}
            <div id="password-table" class="tab-content" style="display: block;">
            <div class="columns is-multiline">
            {{-- /////////////////////////////////////////////////////////// --}}
                    


            <div class="name-group-title">
                <h1 class="subtitle name-code-group">
                    Passwords
                </h1>
                {{-- <h1 class="subtitle name-type-code is-5" >Passwords</h1>
                <h1 class="title product-name">Passwords</h1> --}}
            </div>

            <section class="section resources">
                <div class="container">        
                    <div class="columns is-multiline is-centered is-vcentered">
                        
                        <div class="column">
           
                            <table class="table is-fullwidth is-hoverable  is-bordered">
                                <thead>
                                    <tr>
                                        <th>Product Name</th>
                                        <th>Public Access (Link for your client!)</th>
                                        <th>Password</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($passwords as $pw)
                                    <tr>
                                        <td>{{ $pw['0'] }}</td>
                                        <td>
                                            <div style="display: flex;
                                            justify-content: space-between;
                                            align-items: center;">
                                                <a id="{{ $pw['0'] }}-link" href="{{ $pw['1'] }}" target="_blank" 
                                                style="font-size: 12px;">{{ $pw['1'] }}</a>
                                                <button class="button is-success is-small ml-2" onclick="copyToClipboard('{{ $pw['0'] }}-link')">Copy</button>
                                            </div>
                                            @if(isset($pw['3']))
                                                <hr style="margin:0px;">
                                                <div style="display: flex;
                                                justify-content: space-between;
                                                align-items: center;">
                                                    <a id="{{ $pw['0'] }}-link-two" href="{{ $pw['3'] }}" target="_blank" 
                                                    style="font-size: 12px;">{{ $pw['3'] }}</a>
                                                    <button class="button is-success is-small ml-2" onclick="copyToClipboard('{{ $pw['0'] }}-link-two')">Copy</button>
                                                </div>
                                            @endif
                                        </td>
                                        <td>
                                            <div style="display: flex;
                                            justify-content: space-between;
                                            align-items: center;">                                            
                                                <span id="{{ $pw['0'] }}-password">{{ $pw['2'] }}</span>
                                                <button class="button is-success is-small ml-2" onclick="copyToClipboard('{{ $pw['0'] }}-password')">Copy</button>
                                            </div>
                                            @if(isset($pw['4']))
                                                <hr style="margin:0px;">
                                                <div style="display: flex;
                                                justify-content: space-between;
                                                align-items: center;">                                            
                                                    <span id="{{ $pw['0'] }}-password-two">{{ $pw['4'] }}</span>
                                                    <button class="button is-success is-small ml-2" onclick="copyToClipboard('{{ $pw['0'] }}-password-two')">Copy</button>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
            
                                        
                        </div>
                
                    </div>
                </div>
            </section>

                    
            {{-- /////////////////////////////////////////////////////////// --}}
            </div>
            </div>
            {{-- /////////////////////////////////////////////////////////// --}}
  
  



            {{-- Product Resources --}}
            @foreach($categories_parent as $cat_par)
            @foreach($cat_par['children'] as $cat)
            <?php foreach($cat['products'] as $p) { ?>


        

            {{-- /////////////////////////////////////////////////////////// --}}
            <div id="{{ $p['code'] }}" class="tab-content">
            <div class="columns is-multiline">
            {{-- /////////////////////////////////////////////////////////// --}}


                <div class="name-group-title">
                    <h1 class="subtitle name-code-group">
                        {{ $p['category']['name'] }} 
                    </h1>
                    <h1 class="subtitle name-type-code is-5" >{{ $p['name_type'] .' '. $p['name_code'] }}</h1>
                    <h1 class="title product-name">{{ strtoupper($p['name']) }} </h1>
                </div>


                <section class="section resources pt-1">
                <div class="container">
                        
                    {{-- Access --}}
                    @if (array_key_exists($p['code'], $passwords))
                    <div class="columns is-multiline is-vcentered">
                    <h1 class="subtitle is-6 tab-content-title"> Certificate Access </h1>
                    
                    <h1 class="subtitle is-6 mb-0 ml-3">Public Acces (Link for your client!): </h1>
                    <div class="column is-12 is-flex py-1" style="align-items: center;">
                        <a id="{{ $passwords[$p['code']]['0'] }}-link" href="{{ $passwords[$p['code']]['1'] }}" target="_blank" 
                        style="">{{ $passwords[$p['code']]['1'] }}</a>
                        <button class="button is-success is-small ml-5" onclick="copyToClipboard('{{ $passwords[$p['code']]['0'] }}-link')">Copy</button>
                    </div>
                    
                    <h1 class="subtitle is-6 mt-4 mb-0 ml-3">Password: </h1>
                    <div class="column is-12 is-flex py-1" style="align-items: center;">
                        <span id="{{ $passwords[$p['code']]['0'] }}-password">{{ $passwords[$p['code']]['2'] }}</span>
                        <button class="button is-success is-small ml-5" onclick="copyToClipboard('{{ $passwords[$p['code']]['0'] }}-password')">Copy</button>
                    </div>
                    </div>
                    @endif
                    {{-- Access-------------------------------- --}}

                        
                    {{-- certificates --}}
                    <div class="columns is-multiline">
                    <h1 class="subtitle is-6 tab-content-title"> Certificates </h1>

                        @if (count($p['certificates']))
                        @foreach($p['certificates'] as $pc)
                        @if(strpos($pc['title'], 'UNFILTERED') == false)
                        @endif
                        <div class="column is-12 is-flex py-1">
                            <a href="{{ route('certificate',['file_code'=>$pc['filename'],'product_code'=>$p['code']]).'?u='.env('APP_ASSET_TIMESTAMP') }}"
                                target="_blank">
                                {{ $pc['title'] }}
                            </a>                            
                        </div>
                        @endforeach
                        @else
                        <div class="column">
                            <h1 class="subtitle my-3 py-3" style="text-align: left; font-size: 3em; font-weight: 100;">Coming soon</h1>
                        </div>
                        @endif
                        <hr>
                    </div>
                    {{-- certificates-------------------------------- --}}

                        
                    {{-- Datasheets --}}
                    <?php

                    // $langs = ['eng'=>'','esp'=>''];
                    // $img = $langs;
                    // $img_url = $langs;
    
                    // $img['eng'] = public_path(). '/files/products/'.$p['public_id'].'/datasheets/datasheet';
                    // $img['esp'] = public_path(). '/files/products/'.$p['public_id'].'/lang/esp/datasheets/datasheet';
    
                    // if(File::exists($img['eng'])) 
                    // $img_url['eng'] = route('datasheet',['product_code'=>$p['code']]);
                    // if(File::exists($img['esp'])) 
                    // $img_url['esp'] = route('datasheetLanguage',['product_code'=>$p['code'],'language'=>'esp']);
    
                    // $data_attr = 'data-img_url_eng="'.$img_url['eng'].'" ';
                    // $data_attr.= 'data-img_url_esp="'.$img_url['esp'].'" ';
    
                    ?>
    
                    {{-- <div class="columns is-multiline">
                    <h1 class="subtitle is-6 tab-content-title"> Datasheets </h1>

                        @if (File::exists($img['eng']) || File::exists($img['esp']))
                        
                            @if (File::exists($img['eng']))
                            <div class="column is-12 is-flex py-1">
                                <a href="{{ $img_url['eng'] }}"
                                    target="_blank">
                                    English Datasheet
                                </a>                            
                            </div>
                            @endif

                            @if (File::exists($img['esp']))
                            <div class="column is-12 is-flex py-1">
                                <a href="{{ $img_url['esp'] }}"
                                    target="_blank">
                                    Español Datasheet
                                </a>                            
                            </div>
                            @endif
                        
                        @else
                        <div class="column">
                            <h1 class="subtitle my-3 py-3" style="text-align: left; font-size: 3em; font-weight: 100;">Coming soon</h1>
                        </div>
                        @endif
                    </div> --}}
                    {{-- Datasheets-------------------------------------- --}}
                    
                    
                    
                    
                    {{-- Downloads --}}
                    <div class="columns is-multiline">
                        @include('product.partials.downloads.tab-content',[
                            'cat'=>$cat,
                            'p'=>$p,
                        ])





                        {{-- Packaging --}}
                        {{-- ////////////////////////////////////////////////////////////////////////////////// --}}
                        <?php

                        $langs = ['eng'=>'','esp'=>''];
                        $img = $langs;
                        $img_url = $langs;

                        $img['eng'] = public_path(). '/files/products/'.$p['public_id'].'/datasheets/packaging';
                        $img['esp'] = public_path(). '/files/products/'.$p['public_id'].'/lang/esp/datasheets/packaging';

                        if(File::exists($img['eng']))
                        $img_url['eng'] = route('packaging',['product_code'=>$p['code']]);
                        else
                        $img_url['eng'] = false;

                        if(File::exists($img['esp']))
                        $img_url['esp'] = route('packagingLanguage',['product_code'=>$p['code'],'language'=>'esp']);
                        else
                        $img_url['esp'] = false;

                        ?>

                        @if ($img_url['eng'] || $img_url['esp'])
                        <h2 class="subtitle is-6 tab-content-title"> PACKAGING </h2>
                        @endif

                        
                        @if ($img_url['eng'])
                        <div class="column is-4 is-flex tile-container lang-container">
                            <div class="tile has-lang">
                                <div class="img-container">

                                    <a href="{{ $img_url['eng'] }}" target="_blank">
                                        <i class="far fa-file-alt"></i>
                                    </a>

                                </div>
                                <hr>
                                <a href="{{ $img_url['eng'] }}" target="_blank">
                                    View <i class="fas fa-chevron-right ml-1" style="float: right;"></i>
                                </a>
                            </div>


                            <div class="tile has-no-lang"  style="display: none;">
                                <div class="img-container">
                                        {{-- <i class="far fa-times-circle"></i> --}}
                                    <img alt="Vcare Earth Logo" src="/files/logos/vcareplaceholder.svg?u={{ env('APP_ASSET_TIMESTAMP') }}"/>
                                    <h2 class="subtitle has-text-centered coming-soon-text">Coming Soon</h2>
                                </div>
                            </div>
                        </div>
                        @endif
                        
                        @if ($img_url['esp'])
                        <div class="column is-4 is-flex tile-container lang-container">
                            <div class="tile has-lang">
                                <div class="img-container">

                                    <a href="{{ $img_url['esp'] }}" target="_blank">
                                        <i class="far fa-file-alt"></i>
                                    </a>

                                </div>
                                <hr>
                                <a href="{{ $img_url['esp'] }}" target="_blank">
                                    View (ESP) <i class="fas fa-chevron-right ml-1" style="float: right;"></i>
                                </a>
                            </div>


                            <div class="tile has-no-lang"  style="display: none;">
                                <div class="img-container">
                                        {{-- <i class="far fa-times-circle"></i> --}}
                                    <img alt="Vcare Earth Logo" src="/files/logos/vcareplaceholder.svg?u={{ env('APP_ASSET_TIMESTAMP') }}"/>
                                    <h2 class="subtitle has-text-centered coming-soon-text">Coming Soon</h2>
                                </div>
                            </div>
                        </div>
                        @endif

                        {{-- ////////////////////////////////////////////////////////////////////////////////// --}}
                        {{-- Packaging --}}







                        {{-- files with markings --}}
                        {{-- ////////////////////////////////////////////////////////////////////////////////// --}}
                        {{-- ////////////////////////////////////////////////////////////////////////////////// --}}
                        <h1 class="title my-6" style="width: 100%;"> <hr style="height:10px;"> </h1>
                        
                        @foreach($p['files_with_markings'] as $lang => $fwm)

                        @if($fwm['image-main'])
                        <h1 class="subtitle is-6 tab-content-title"> MAIN IMAGES w/ MARKINGS ({{ strtoupper($lang) }})</h1>
                            <div class="column is-4 is-flex">
                                <div class="tile">
                    
                                    <div class="img-container">
                                        <a href="/{{ $fwm['image-main']['path'].'?u='.env('APP_ASSET_TIMESTAMP') }}" 
                                        data-title="Images with Markings"
                                        data-lightbox="{{ $fwm['image-main']['filename'] }}-images">
                                            <img src="/{{ $fwm['image-main']['path'].'?u='.env('APP_ASSET_TIMESTAMP') }}" 
                                            alt="Vcare Earth {{ $fwm['image-main']['filename'] }}">
                                        </a>
                                    </div>
            
            
                                </div>
                            </div>
                        @endif
                        
                        @if(count($fwm['images']))
                        <h1 class="subtitle is-6 tab-content-title"> 3D PACKAGING w/ MARKINGS ({{ strtoupper($lang) }})</h1>
                        @foreach($fwm['images'] as $img)
                            <div class="column is-4 is-flex">
                                <div class="tile">
                    
                                    <div class="img-container">
                                        <a href="/{{ $img['path'].'?u='.env('APP_ASSET_TIMESTAMP') }}" 
                                        data-title="Images with Markings"
                                        data-lightbox="{{ $img['filename'] }}-images">
                                            <img src="/{{ $img['path'].'?u='.env('APP_ASSET_TIMESTAMP') }}" 
                                            alt="Vcare Earth {{ $img['filename'] }}">
                                        </a>
                                    </div>
            
            
                                </div>
                            </div>
                        @endforeach
                        @endif

                        @if($fwm['datasheet'])
                        <h1 class="subtitle is-6 tab-content-title"> DATASHEET w/ MARKINGS ({{ strtoupper($lang) }})</h1>
                        <div class="column is-4 is-flex">
                            <div class="tile">
                    
                                <div class="img-container">
                                    <a href="{{ $fwm['datasheet'].'?u='.env('APP_ASSET_TIMESTAMP') }}"
                                    target="_blank">
                                    <i class="far fa-file-alt"></i>
                                    </a>
                                </div>
                                <hr>
                                <a href="/{{ $fwm['datasheet'].'?u='.env('APP_ASSET_TIMESTAMP') }}"
                                    target="_blank">
                                    View <i class="fas fa-chevron-right ml-1" style="float: right;"></i>
                                </a>
                    
                    
                            </div>
                        </div>
                        @endif


                        @endforeach
                        {{-- ////////////////////////////////////////////////////////////////////////////////// --}}
                        {{-- ////////////////////////////////////////////////////////////////////////////////// --}}
                        {{-- files with markings --}}












                    </div>


                    {{-- Downloads -------------------------------------- --}}

                </div>
                </section>

            {{-- /////////////////////////////////////////////////////////// --}}
            </div>
            </div>
            {{-- /////////////////////////////////////////////////////////// --}}

  
  
            <?php } ?>
            @endforeach
            @endforeach
            {{-- Product Resources --}}
  
      








    
    </div>
    {{-- ////////////////////////////////////////////////////////////////////// --}}
    {{-- ////////////////////////////////////////////////////////////////////// --}}
    {{-- list content --}}






















    
    
</div>
</div>
</section>
    
    
    


@endsection