@extends('layout.main')



@section('head-end')
<link rel="stylesheet" href="{{ mix('css/sora-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/sora-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection

@section('head-title')
<title>Vcare.earth SORA</title>
@endsection


@section('content')


<section class="section">
    <div class="container">
  
        <div class="columns is-flex-end">
            <div class="column title-column">
                {{-- <h2 class="subtitle">
                    Certificates
                </h2> --}}
                <h1 class="title is-1">Welcome to Sora </h1>
            </div>
        </div>        
  
    </div>
  </section>
  
  <section class="section blue-vcare-bg" 
  style="background-image: url({{ env('APP_URL').'/files/home/images/homepage-prideinquality.jpg?u='.env('APP_ASSET_TIMESTAMP') }}); padding: 15vh 10px;">
    <div class="container">
      <div class="columns is-vcentered">
        <div class="column">
          <h2 class="subtitle is-2 has-text-centered has-text-white">
            {{-- Certificate Passwords and Files --}}
          </h2>
        </div>
      </div>
    </div>
  </section>
  


<section class="section resources">
    <div class="container">
        <div class="columns">
            <div class="column">
                <div class="card">
                    <div class="card-content">
                        <a href="{{ route('sora-certificates-page') }}">
                            <p class="title has-text-centered">
                                Certificates
                                <i class="fas fa-chevron-right"></i>
                            </p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="card">
                    <div class="card-content">
                        <a href="{{ route('sora-inventory-page') }}">
                            <p class="title has-text-centered">
                                Inventory
                                <i class="fas fa-chevron-right"></i>
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    
    
    


@endsection