@extends('layout.main')



@section('head-end')
<link rel="stylesheet" href="{{ mix('css/sora-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/sora-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection

@section('head-title')
<title>Vcare.earth SORA</title>
@endsection


@section('content')


<section class="section">
    <div class="container">
  
        <div class="columns is-flex-end">
            <div class="column title-column">
                {{-- <h2 class="subtitle">
                    Certificates
                </h2> --}}
                <h1 class="title is-1">Inventory </h1>
            </div>
        </div>       
    </div>
  </section>
  
  <section class="section blue-vcare-bg" 
  style="background-image: url({{ env('APP_URL').'/files/home/images/homepage-prideinquality.jpg?u='.env('APP_ASSET_TIMESTAMP') }}); padding: 15vh 10px;">
    <div class="container">
      <div class="columns is-vcentered">
        <div class="column">
          <h2 class="subtitle is-2 has-text-centered has-text-white">
            {{-- Certificate Passwords and Files --}}
          </h2>
        </div>
      </div>
    </div>
  </section>
  
  {{-- background-image: url(https://vcare.ddns.net/files/home/images/homepage-prideinquality.jpg?u=20200823); --}}

<section class="section resources">
<div class="container">
<div class="columns">
    
    
        
















  
  {{-- list menu --}}
  {{-- ////////////////////////////////////////////////////////////////////// --}}
  {{-- ////////////////////////////////////////////////////////////////////// --}}
  <div class="column is-narrow">
    <div class="tabs tab-menu mb-1 is-active">
        <a class="title is-5 password-menu" 
        href="{{ route('sora-inventory-page') }}#inventory-table"
        data-tab_id="inventory-table"
        style=""> 
            INVENTORY
        </a>
    </div>

  
    @foreach($categories_parent as $cat_par)
    <div class="tabs tab-menu">
      <h1 class="title is-6"> {{ $cat_par['name'] }} </h1>
      @foreach($cat_par['children'] as $cat)
        <h1 class="subtitle is-6"> {{ $cat['name'] }} </h1>
        <ul>
          @if(count($cat['products']))
            <?php foreach($cat['products'] as $p) { ?>
              <?php if ($p['coming_soon']) { ?>
                <li> 
                  <a href="{{ route('sora-inventory-page') }}#{{ $p['code'] }}"
                  data-tab_id="{{ $p['code'] }}"> 
                      {{ $p['name_type'].' '.$p['name'] }} 
                      @if ($p['name_code'])
                          {{ $p['name_code'] }}
                      @endif       
                      *Coming Soon
                  </a> 
                </li>
              <?php } else { ?>
                <li> 
                  <a href="{{ route('sora-inventory-page') }}#{{ $p['code'] }}"
                  data-tab_id="{{ $p['code'] }}"> 
  
                    {{-- Because boss wants something different for surgical gown --}}
                    @if ($cat['code']=='surgical-gown')
                        {{ $p['name'].' '.$p['name_code'].' ('.$p['name_type'].')' }}
                    @else
                        {{ $p['name_type'].' '.$p['name'] }} 
                        @if ($p['name_code'])
                            {{ $p['name_code'] }}
                        @endif                                            
                    @endif
                  </a> 
                </li>
              <?php } ?>
            <?php } ?>
          
          @else
            <li> 
              <a href="{{ route('sora-inventory-page') }}#{{ $p['code'] }}"
              data-tab_id="{{ $cat['code'] }}"> 
                *Coming Soon 
              </a>         
            </li>
          @endif
  
  
  
        </ul>
      @endforeach
    </div>
    @endforeach
  
    </div>
    {{-- ////////////////////////////////////////////////////////////////////// --}}
    {{-- ////////////////////////////////////////////////////////////////////// --}}
    {{-- list menu --}}
  
  
  
  
  
  











    {{-- list content --}}
    {{-- ////////////////////////////////////////////////////////////////////// --}}
    {{-- ////////////////////////////////////////////////////////////////////// --}}
    <div id="tab-contents" class="column tab-contents ">
  
  
        
        

            {{-- /////////////////////////////////////////////////////////// --}}
            <div id="inventory-table" class="tab-content" style="display: block;">
            <div class="columns is-multiline">
            {{-- /////////////////////////////////////////////////////////// --}}
                    


            <div class="name-group-title">
                <h1 class="subtitle name-code-group">
                    Inventory
                </h1>
            </div>

            <section class="section resources" style="width: 100%;">
                <div class="container" style="width: 100%;">        
                    <div class="columns is-multiline is-centered is-vcentered">
                        <div class="column">
                           
                            <p><i class="fas fa-arrow-left"></i>&nbsp;Pick a product to start!</p>
                            @foreach($categories_parent as $cat_par)
                            <hr>
    <div class="inv-cards">
      <h1 class="title is-6 pt-5"> {{ $cat_par['name'] }} </h1>
      @foreach($cat_par['children'] as $cat)
        <h1 class="subtitle is-6"> {{ $cat['name'] }} </h1>
        <div class="columns">
            @if(count($cat['products']))
                <?php $i = 1 ?>
              <?php foreach($cat['products'] as $p) { ?>
                    <div class="column is-4 is-flex">
                        <div class="tile">
                            <h3 class="name">
                                <a href="{{ route('sora-inventory-page') }}#{{ $p['code'] }}"
                                data-tab_id="{{ $p['code'] }}"> 
                                {{ $cat['name'] }}
                                </a>
                            </h3>
                            <h3 class="name_type">{{ $p['name_type'] }}&nbsp;{{ $p['name'] }}  @if ($p['name_code']){{ $p['name_code'] }}   @endif </h3>
                            <div class="img-container">
                                @if ($p['main-image'])
                
                                <a href="{{ route('sora-inventory-page') }}#{{ $p['code'] }}"
            data-tab_id="{{ $p['code'] }}"> 
                                    <img 
                                    src="{{ env('APP_URL').'/files/products/'.$p['public_id'].'/images/thumbnails/'.$p['main-image']['type'].'/'.$p['main-image']['filename'].'.'.$p['main-image']['ext_thumb'].'?u='.env('APP_ASSET_TIMESTAMP') }}" 
                                    alt="Vcare Earth {{ $p['main-image']['title'] }}">
                                </a>
                                @else
                                <a href="{{ route('sora-inventory-page') }}#{{ $p['code'] }}"
                                data-tab_id="{{ $p['code'] }}"> 
                                    <div class="img-placeholder-container">
                                        <img class="img-placeholder" alt="Vcare Earth" src="/files/logos/vcareplaceholder.svg?u={{ env('APP_ASSET_TIMESTAMP') }}"/>
                                        <h1 class="subtitle has-text-centered coming-soon-text">Coming Soon</h1>
                                    </div>
                                </a>
                                @endif
                            </div>
                
                
                            <hr>
                            <a href="{{ route('sora-inventory-page') }}#{{ $p['code'] }}"
                            data-tab_id="{{ $p['code'] }}"> 
                            @if(isset($q_data[$p['id']]))   

                            @foreach($q_data as $qdkey => $qd)
                            @if($qdkey == $p['id'])
                            @foreach($qd as $qkey => $q)
                         
                                    <p class="">
                                        {{ $qkey }} -
                                        {{ $q ? number_format($q) : 0 }}
                                    </p>
                           
                            @endforeach
                                @endif
                            @endforeach

                            @else
                            <div class="has-text-danger">
                                No Stock
                            </div>
                            @endif        
                   
                             
                
                                @if ($p['coming_soon'])
                                *Coming Soon
                                @endif                
            
                            </a>
                        </div>
                    </div>
                
               
                @if($i % 3 == 0)
            </div>
            <div class="columns">
               
            @endif
                <?php $i++ ?>
              <?php } ?>

            
            @else
            <div class="column is-4">
                <div class="card">
                    <div class="card-content">
                        <a href="{{ route('sora-inventory-page') }}#{{ $p['code'] }}"
                        data-tab_id="{{ $cat['code'] }}"> 
                          *Coming Soon 
                        </a>      

                    </div>
                </div>
            </div>

            @endif

        </div>
      @endforeach
    </div>
    @endforeach

                        </div>
                    </div>
                </div>
            </section>

                    
            {{-- /////////////////////////////////////////////////////////// --}}
            </div>
            </div>
            {{-- /////////////////////////////////////////////////////////// --}}
  
  



            {{-- Product Resources --}}
            @foreach($categories_parent as $cat_par)
            @foreach($cat_par['children'] as $cat)
           
            <?php foreach($cat['products'] as $p) { ?>


        

            {{-- /////////////////////////////////////////////////////////// --}}
            <div id="{{ $p['code'] }}" class="tab-content">
            <div class="columns is-multiline">
            {{-- /////////////////////////////////////////////////////////// --}}


                <div class="name-group-title">
                    <h1 class="subtitle name-code-group">
                        {{ $p['category']['name'] }} 
                    </h1>
                    <h1 class="subtitle name-type-code is-5" >{{ $p['name_type'] .' '. $p['name_code'] }}</h1>
                    <h1 class="title product-name">{{ strtoupper($p['name']) }} </h1>
                </div>


                <section class="section resources pt-1" style="width: 100%;">
                <div class="container" style="width: 100%;">
                   
                    <div class="columns">
                        @foreach($q_data as $qdkey => $qd)
                        @if($qdkey == $p['id'])
                        @foreach($qd as $qkey => $q)
                        <div class="column">
                            <div class="card">
                                <div class="card-content has-text-centered">
                                    {{ $qkey }} 
                                    <p>{{ $q ? number_format($q) : 0 }} pcs</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        @endforeach
                    </div>
                   
                    <table class="table is-fullwidth has-text-centered" >
                        <thead>
                            <tr>
                                <th>Id #</th>
                                <th>Date</th>
                                <th>Quantity</th>
                                <th>Status</th>
                                <th>Client</th>
                                <th>Added by</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($inventory_data as $idkey => $id)
                            
                            
                            @if($id->pin_p_id == $p['id'])
                            <tr>
                                <td>{{ $idkey + 1 }}</td>
                                <td>{{ date('d-m-Y h:i A', strtotime($id->created_at))  }}</td>
                                <td>{{ number_format($id->pin_quantity) }}</td>
                                <td>{{ $id->pin_status }}</td>
                                <td>{{ $id->pin_client }}</td>
                                <td>{{ $id->pin_added_by }}</td>
                                <td>
                                    <button class="button is-small editModal" data-modal="{{$p['code']}}{{$idkey}}">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <button class="button is-small logModal" data-modal="{{$p['code']}}{{$idkey}}">
                                        <i class="fas fa-file-alt"></i>
                                    </button>
                                </td>
                            </tr>

                            <div class="modal modal{{$p['code']}}{{$idkey}}" id="modal{{$p['code']}}" data-modal="{{$p['code']}}{{$idkey}}">
                                <div class="modal-background"></div>
                                <div class="modal-card">
                                    <header class="modal-card-head">
                                        <p class="modal-card-title">Edit Inventory #{{$idkey + 1}}</p>
                                        <button class="delete" aria-label="close"></button>
                                    </header>
                                    <form action="{{ route('sora-inventory-page-post')}}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <section class="modal-card-body">
                                            <div class="field">
                                                <label class="label">Product Name</label>
                                                <div class="control">
                                                    <input class="input" type="text"  value="{{$p['name']}} - {{$p['name_type']}} {{$p['name_code']}} " disabled>
                                                    <input class="input" type="hidden" name="inventory_product_id" value="{{$p['id']}}">
                                                </div>
                                            </div>
                                            <div class="field">
                                                <label class="label">Quantity</label>
                                                <div class="control">
                                                    <input class="input" type="number" name="inventory_quantity" value="{{ $id->pin_quantity }}" required  title="Add new inventory">
                                                </div>
                                            </div>
                                            <div class="field">
                                                <label class="label">Status</label>
                                                <div class="control">    
                                                    <div class="select is-fullwidth">
                                                        <select name="inventory_status" required title="Add new inventory">
                                                            <option value="In-stock" {{ $id->pin_status == 'In-stock' ? 'selected' : '' }}>In Stock</option>
                                                            <option value="Production" {{ $id->pin_status == 'Production' ? 'selected' : '' }}>Production</option>
                                                            <option value="Shipping" {{ $id->pin_status == 'Shipping' ? 'selected' : '' }}>Shipping</option>
                                                            <option value="Sold" {{ $id->pin_status == 'Sold' ? 'selected' : '' }}>Sold</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="field">
                                                <label class="label">Client</label>
                                                <div class="control">
                                                    <input class="input" type="text" name="inventory_client" value="{{ $id->pin_client }}" required>
                                                </div>
                                            </div>
                                            <div class="field">
                                                <label class="label">Added by</label>
                                                <div class="control">
                                                    <div class="select is-fullwidth">
                                                        <select name="inventory_added_by" required title="Add new inventory">
                                                            <option value="Marc" {{ $id->pin_added_by == 'Marc' ? 'selected' : '' }}>Marc</option>
                                                            <option value="Johannes" {{ $id->pin_added_by == 'Johannes' ? 'selected' : '' }}>Johannes</option>
                                                            <option value="Wei" {{ $id->pin_added_by == 'Wei' ? 'selected' : '' }}>Wei</option>
                                                            <option value="Alejandro" {{ $id->pin_added_by == 'Alejandro' ? 'selected' : '' }}>Alejandro</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <footer class="modal-card-foot">
                                            <input class="input" type="hidden"  name="inventory_id" value="{{ $id->pin_id }}">
                                            <input class="input" type="hidden"  name="inventory_added_by_ip" value="{{ $_SERVER['REMOTE_ADDR'] }}">
                                            <button class="button is-success">Save changes</button>
                                        </footer>
                                    </form>
                                </div>
                            </div>


                            <div class="modal logmodal{{$p['code']}}{{$idkey}}" id="modal{{$p['code']}}" data-modal="{{$p['code']}}{{$idkey}}">
                                <div class="modal-background"></div>
                                <div class="modal-card">
                                    <header class="modal-card-head">
                                        <p class="modal-card-title">Inventory Log #{{$idkey + 1}}</p>
                                        <button class="delete" aria-label="close"></button>
                                    </header>
                                    <section class="modal-card-body">
                                        <!-- Content ... -->
                                        <div class="columns "> 
                                            <div class="column"><b>Date</b></div>
                                            <div class="column"><b>Quantity</b></div>
                                            <div class="column"><b>Status</b></div>
                                            <div class="column"><b>Client</b></div>
                                            <div class="column"><b>Added By</b></div>
                                        </div>


                                        @foreach($inventory_log_data as $ildkey => $ild)
                                            <div class="columns">
                                            @if($ild->pil_pin_id == $id['pin_id'])
                                                <div class="column">{{ date('d-m-Y h:i A', strtotime($ild->created_at)) }}</div>
                                                <div class="column">{{ number_format($ild->pil_quantity) }}</div>
                                                <div class="column">{{ $ild->pil_status }}</div>
                                                <div class="column">{{ $ild->pil_client }}</div>
                                                <div class="column">{{ $ild->pil_added_by}}</div>
                                            @endif
                                            
                                        </div>
                                            @endforeach

                                      </section>
                                
                                </div>
                            </div>


                            @endif
                          
                            @endforeach
                        </tbody>
                    </table>
                    <button class="button is-info showModal" id="" data-modal="{{$p['code']}}"><i class="fas fa-plus"></i>&nbsp;Add Inventory</button>
                    <div class="modal modal{{$p['code']}}" id="modal{{$p['code']}}" data-modal="{{$p['code']}}">
                        <div class="modal-background"></div>
                        <div class="modal-card">
                            <header class="modal-card-head">
                                <p class="modal-card-title">Add Inventory</p>
                                <button class="delete" aria-label="close"></button>
                            </header>
                            <form action="{{ route('sora-inventory-page-post')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <section class="modal-card-body">
                                    <div class="field">
                                        <label class="label">Product Name</label>
                                        <div class="control">
                                            <input class="input" type="text"  value="{{$p['name']}} - {{$p['name_type']}} {{$p['name_code']}} " disabled>
                                            <input class="input" type="hidden" name="inventory_product_id" value="{{$p['id']}}">
                                        </div>
                                    </div>
                                    <div class="field">
                                        <label class="label">Quantity</label>
                                        <div class="control">
                                            <input class="input" type="number" name="inventory_quantity" value="" required>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <label class="label">Status</label>
                                        <div class="control">    
                                            <div class="select is-fullwidth">
                                                <select name="inventory_status" required>
                                                    <option value="" disabled selected>Select an option</option>
                                                    <option value="In-stock">In Stock</option>
                                                    <option value="Production">Production</option>
                                                    <option value="Shipping">Shipping</option>
                                                    <option value="Sold">Sold</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <label class="label">Client</label>
                                        <div class="control">
                                            <input class="input" type="text" value="" name="inventory_client" required>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <label class="label">Added by</label>
                                        <div class="control">
                                            <div class="select is-fullwidth">
                                                <select name="inventory_added_by" required>
                                                    <option value="" selected disabled>Select a Person</option>
                                                    <option value="Marc">Marc</option>
                                                    <option value="Johannes">Johannes</option>
                                                    <option value="Wei">Wei</option>
                                                    <option value="Alejandro">Alejandro</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <footer class="modal-card-foot">
                                    <input class="input" type="hidden"  name="inventory_added_by_ip" value="{{ $_SERVER['REMOTE_ADDR'] }}">
                                    <button class="button is-success">Save changes</button>
                                </footer>
                            </form>
                        </div>
                    </div>
                    <hr>
                    <div class="comments-section">
                        <form action="{{ route('sora-inventory-comment-post')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card">
                                <div class="card-header">
                                    <p>Comments Section</p>
                                </div>
                                <div class="card-content">
                                    <textarea class="textarea" name="inventory_comment" placeholder="Enter your comment" required></textarea>
                                    <div class="field">
                                        <label class="label">Added by</label>
                                        <div class="control">
                                            <div class="select is-fullwidth">
                                                <select name="inventory_comment_added_by" required>
                                                    <option value="" selected disabled>Select a Person</option>
                                                    <option value="Marc">Marc</option>
                                                    <option value="Johannes">Johannes</option>
                                                    <option value="Wei">Wei</option>
                                                    <option value="Alejandro">Alejandro</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <input class="input" type="hidden" name="inventory_comment_product_id" value="{{$p['id']}}">
                                    <button class="button is-success">Submit</button>
                                    <hr>
                                    @foreach($inventory_comment_data as $icdkey => $icd)
                                    @if($icd->pic_p_id == $p['id'])
                                    <div class="card">
                                        <div class="card-content">
                                           <p style="font-size:20px;">
                                               {{ $icd->pic_comment }}
                                            </p> 
                                            <p>
                                                <small>By&nbsp;{{ $icd->pic_added_by }} </small>
                                            </p>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                </section>

            {{-- /////////////////////////////////////////////////////////// --}}
            </div>
            </div>
            {{-- /////////////////////////////////////////////////////////// --}}

            
            <?php } ?>
            @endforeach
            @endforeach
            {{-- Product Resources --}}
  
      








    
    </div>
    {{-- ////////////////////////////////////////////////////////////////////// --}}
    {{-- ////////////////////////////////////////////////////////////////////// --}}
    {{-- list content --}}






















    
    
</div>
</div>
</section>
    
    
    


@endsection