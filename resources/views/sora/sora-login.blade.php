@extends('layout.main')

@section('head-end')
<link rel="stylesheet" href="{{ mix('css/single-product-login-page.css') }}?u={{ env('APP_ASSET_TIMESTAMP') }}">
<script src="{{ mix('js/single-product-login-page.js') }}?u={{ env('APP_ASSET_TIMESTAMP') }}"></script>
@endsection







@section('content')



<section class="section">
    <div class="container">
        <div class="columns is-vcentered is-centered is-multiline height-50vh">
            


            <div class="column is-narrow">

            <div class="login-form">

                @if ($error_message)                
                <div class="notification is-danger is-light error-message" 
                style="text-align: center; padding-left: 20px; padding-right: 20px;">
                    {{ $error_message }}
                </div>    
                @endif    
                
                <form method="POST" action="{{ route('sora-login-post') }}">
                    @csrf
                    <img src="{{ env('APP_URL') }}/files/logos/vcareicon.svg" style="margin: auto; display: block;">
                    
                    <h1 class="form-title">LOGIN TO SORA</h1>
                    {{-- <div class="field">
                        <p class="control">
                        <input class="input" name="username" type="text" placeholder="Email">
                        </p>
                    </div> --}}
                    <div class="field">
                        <p class="control">
                        <input class="input" name="password" type="password" placeholder="Password" >
                        </p>
                    </div>
                    <div class="field">
                        <p class="control" style="text-align: center;">
                        <button type="submit" class="button is-outlined submit-btn">
                            SUBMIT
                            <i class="fas fa-chevron-right ml-3" aria-hidden="true"></i>
                        </button>
                        </p>
                    </div>

                </form>
            
            </div>

            </div>            
        </div>
    </div>
</section>




@endsection