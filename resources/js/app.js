// require('./bootstrap');
$(document).ready(function(){

    // console.log('app.js');

    $('#scrollToTop').click(function(e){

        $('html, body').animate({
            scrollTop: 0
        }, 800);  

    });


    $('#mylivechat-button').click(function(){
        $('.mylivechat_buttonround').click();
    });





    $('.read-more-action button').click(function(e){
        $('.read-more-content').stop().slideToggle();
        $('.read-more-action button').toggle();
    });



});

function addCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {   
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}



function getViewPortWidth() {
    return Math.max(
      document.body.scrollWidth,
      document.documentElement.scrollWidth,
      document.body.offsetWidth,
      document.documentElement.offsetWidth,
      document.documentElement.clientWidth
    );
}
  
function getViewPortHeight() {
    return Math.max(
      document.body.scrollHeight,
      document.documentElement.scrollHeight,
      document.body.offsetHeight,
      document.documentElement.offsetHeight,
      document.documentElement.clientHeight
    );
}


function isMobile() {
    if (window.matchMedia('screen and (max-width: 768px)').matches)    
    return true;
    else
    return false;
}