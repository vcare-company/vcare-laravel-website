$(document).ready(function(){

	// jQuery methods go here...
	$('#companies .images').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		prevArrow: $('.prev'),
		nextArrow: $('.next'),  
		autoplay: true,
		autoplaySpeed: 3500,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		]
	});


	if(message) {
		console.log('message',message);
		$('#message').addClass('is-active');
		$('.close-message').click(function(){
			$('#message').fadeOut(700, function(){
				$('#message').removeClass('is-active');			
			});			
		});
	}

	$('#subscribe-newsletter').on('submit',function(e){

		
		let name = $('.subscriber-name').val();
		let email = $('.subscriber-email').val();

		console.log('name',name);
		console.log('email',email);
		
		$('.subscriber-name-field .subscriber-name').removeClass('is-danger');
		$('.subscriber-name-field .help').fadeOut();
		$('.subscriber-email-field .subscriber-email').removeClass('is-danger');
		$('.subscriber-email-field .help').fadeOut();
		
		
		if(name && email) {
			return;
		} else {

			if(!name) {
				$('.subscriber-name-field .subscriber-name').addClass('is-danger');
				$('.subscriber-name-field .help').fadeIn();
			}
			if(!email) {
				$('.subscriber-email-field .subscriber-email').addClass('is-danger');
				$('.subscriber-email-field .help').fadeIn();
			}
			e.preventDefault();			
		}
	});


});
	
	
	