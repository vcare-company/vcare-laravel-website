$(document).ready(function(){

    
    // fileInput.onchange = () => {
    //   if (fileInput.files.length > 0) {
    //     const fileName = document.querySelector('#file-js-example .file-name');
    //     fileName.textContent = fileInput.files[0].name;
    //   }
    // }

    $('input.file-input').change(function(e){
        let files = e.target.files;
        if (files.length > 0) {
            let filename = files[0].name;
            // console.log(files[0]);
            $(this).parents('label').find('.file-name').html(filename);
            $(this).parents('.field-container').find('input.input').val(filename.split('.')[0]);
        } else {
            $(this).parents('label').find('.file-name').html('---');
            $(this).parents('.field-container').find('input.input').val('');

        }
    });
  
});





