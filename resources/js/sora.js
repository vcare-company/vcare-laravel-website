$(document).ready(function(){

	
		
		$('.tabs.tab-menu a').click(function(e){
				// e.stopPropagation();
				// e.preventDefault();
				
				$('.tabs.tab-menu').removeClass('is-active');
				$('.tabs.tab-menu ul li').removeClass('is-active');
				$(this).parent().addClass('is-active');
	
				let id = $(this).attr('data-tab_id');
				$('.tab-content').stop().slideUp();
				$('#'+id).stop().slideDown( "fast", function() {
					if(id) {
						// console.log("$(.tab-contents) offset "+ $(".tab-contents").offset().top);
						$('html, body').animate({
							scrollTop: $(".tab-contents").offset().top
						}, 500);        
					}      
		
				});
	
	
		});

				$('.inv-cards .tile a').click(function(e){
				// e.stopPropagation();
				// e.preventDefault();
				
				
	
				let id = $(this).attr('data-tab_id');
				$('.tab-content').stop().slideUp();
				$('#'+id).stop().slideDown( "fast", function() {
					if(id) {
						// console.log("$(.tab-contents) offset "+ $(".tab-contents").offset().top);
						$('html, body').animate({
							scrollTop: $(".tab-contents").offset().top
						}, 500);        
					}      
		
				});
	
	
		});


		$('a.gototab').click(function(e){
				e.stopPropagation();
				e.preventDefault();
	
				let tab_id = $(this).attr('data-tab_id');
	
				if(tab_id) {
					$('.tabs.tab-menu a[data-tab_id="'+tab_id+'"]').click();
				}
		
		});    



    // To scroll to when accessed url with #
    let link = window.location.href;
    let tab_id = link.split("#")[1];
	console.log('tab_id = ',tab_id);
    if (tab_id) {
		$('.tabs.tab-menu a[data-tab_id="'+tab_id+'"]').click();
    }

	
	$('.showModal').click(function() {
		let mid = $(this).attr('data-modal');
		// console.log(mid);

		$('.modal'+mid).addClass("is-active");
	});


	$('.editModal').click(function() {
		let mid1 = $(this).attr('data-modal');
		// console.log(mid1);

		$('.modal'+mid1).addClass("is-active");
	});


	$('.logModal').click(function() {
		let mid2 = $(this).attr('data-modal');
		console.log(mid2);

		$('.logmodal'+mid2).addClass("is-active");
	});



	$(".delete").click(function() {
		$(".modal").removeClass("is-active");
	 });
});


	
function copyToClipboard(elementId) {

	// Create a "hidden" input
	var aux = document.createElement("input");

	// Assign it the value of the specified element
	aux.setAttribute("value", document.getElementById(elementId).innerHTML);

	// Append it to the body
	document.body.appendChild(aux);

	// Highlight its content
	aux.select();

	// Copy the highlighted text
	document.execCommand("copy");

	// Remove it from the body
	document.body.removeChild(aux);

}