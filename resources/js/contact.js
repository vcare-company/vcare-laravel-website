
$(document).ready(function(){


    


    if (window.location.href.indexOf("?type=b2b") > -1) {
        $(".b2b").addClass("contact-click");
        $(".topic-selector-text").hide()
        $(".all-contact-form").show()
        $(".b2b-contact").show();
        $(".interested-text").val("b2b");
        var s_prd = $(".b2b_select").children("option:selected").data("value");
        $(".b2b_select").closest("#clone-div").find("#b2b_quantity").find("input").attr("name", s_prd + "_qty")
    }


    $(".contact-card").click(function(){

        var className = $(this).attr('class').split(' ')[2];
        if(className == "chat") {
            $('.mylivechat_buttonround').click();     
        } else {
            $(".contact-card").removeClass("contact-click");
            // $(".not-chat-with-us").hide();
            $(".not-chat-with-us").show();

            if(!$(this).hasClass("contact-click")){
                $(this).addClass("contact-click");
                $(".interested-text").val(className)
            } else {
                $(this).removeClass("contact-click");
            }

            if(className == "b2b") {
                $(".b2b-contact").show();
            } else {
                $(".b2b-contact").hide();
            }

            // if(className != "chat") {
            //     $(".not-chat-with-us").show();
            // } else {
            //     $(".not-chat-with-us").hide();
            // }

            if(className == "supplier") {
                $("select[name='who']").val("Supplier");
                $(".supplier-contact").show()
                $("input[name='products_supply']").prop("required", true);
                $(".not-supplier-contact").hide()
            } else if(className == "partner"){
                $(".not-partner-contact").hide()
                $(".supplier-contact").hide()
                $(".partner-contact").show()
            } else {        
                $("select[name='who']").val("");
                $(".supplier-contact").hide()
                $(".not-supplier-contact").show()
                $("input[name='products_supply']").removeAttr("required");

            }

        



            $(".all-contact-form").show()
            $(".topic-selector-text").hide()


            // scroll to .all-contact-form
            $("html, body").animate(
                {
                    scrollTop: $(".all-contact-form").offset().top,
                },
                500
            );            


        }

    })





    $(".columns").on("change", ".b2b_select", function(){

        var select_number = $(this).data("select");
        var quantity_number = $(this).closest("#clone-div").find("#b2b_quantity").data("quantity");
        var selected_product = $(this).children("option:selected").data("value");
        var selected_product_val = $(this).children("option:selected").val();
        
        console.log(select_number + "," + quantity_number+ "," +selected_product);

        console.log($(this).closest("#clone-div").find("#b2b_quantity").attr("class"))


        if (select_number == quantity_number) {
            $(this).closest("#clone-div").find("#b2b_quantity").find("input").attr("name", selected_product + "_qty");
            $(this).attr("name", selected_product_val)
        }



    })
    
    var slt_number = 1;
    var qty_number = 1;

    $("#add-product").click(function(e){
        e.preventDefault();

        var html = $("#clone-div").clone();
        $("#cloned-div").append(html)

        slt_number++;

        console.log(slt_number);

        $(".b2b_select").last().attr("data-select", slt_number);
        $(".b2b_quantity").last().attr("data-quantity", slt_number);

    })

    $("select[name='number_type']").change(function(){
        if($(this).val() == 'Mobile') {
            $(".whatsapp-available").show()
        } else {
            $(".whatsapp-available").hide()
        }
    })

    $("select[name='who']").change(function(){
       
        if($(this).val() == 'Other') {
            $(".who-other-option").show()
            $("input[name='who_other_option']").prop("required", true)
        } else {
            $(".who-other-option").hide()
            $("input[name='who_other_option']").removeAttr("required")
        }
    })
    
    $("select[name='intention']").change(function(){
       
        if($(this).val() == 'Other') {
            $(".intention-other-option").show()
            $("input[name='intention_other_option']").prop("required", true)
        } else {
            $(".intention-other-option").hide()
            $("input[name='intention_other_option']").removeAttr("required")
        }
    })



    $("select[name='how']").change(function(){
        if($(this).val() == 'Referral') {
            $(".referral-option").show()
            $("input[name='referral_option']").prop("required", true)
        } else {
            $(".referral-option").hide()
            $("input[name='referral_option']").removeAttr("required")
        }
        
        if($(this).val() == 'Other') {
            $(".how-other-option").show()
            $("input[name='how_other_option']").prop("required", true)
        } else {
            $(".how-other-option").hide()
            $("input[name='how_other_option']").removeAttr("required")
        }
    })

    $("select[name='ppe']").change(function(){
        if($(this).val() == 'None') {
            $(".ppe-other-option").show()
            $("input[name='ppe_other_option']").prop("required", true)
            
        } else {
            $(".ppe-other-option").hide()
            $("input[name='ppe_other_option']").removeAttr("required")
        }

        if($(this).val() == 'PPE') {
            $(".ppe_option").show()
            $(".medical_option").hide()
        } else if($(this).val() == 'Medical Equipment Industry'){
            $(".medical_option").show()
            $(".ppe_option").hide()
        } else {
            $(".ppe_option").hide()
            $(".medical_option").hide()
        }

    

    })













    // after # in url
    let link = window.location.href;
    let poundString = link.split("#")[1];

    if (poundString=='chatwithus') {
        $('.card.contact-card.chat').click();
        // $('.card.contact-card.chat').addClass('contact-click');
        // $(".all-contact-form").show()
    }


})