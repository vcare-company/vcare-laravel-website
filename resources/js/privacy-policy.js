


$(document).ready(function(){

    let pp = getCookie("privacy-policy");

    console.log(pp);

    if(pp=='consent-accepted') {
        $('#privacy-policy').hide();
    } else {
        $('#privacy-policy').show();
    }

    $('#privacy-policy .close-privacy-policy').click(function(){
        $('#privacy-policy').fadeOut('slow');
    });

    $('#privacy-policy .continue-privacy-policy').click(function(){
        setCookie('privacy-policy','consent-accepted',30);
        $('#privacy-policy').fadeOut('slow');
    });

});