$(document).ready(function () {

    if(isMobile()) {
        $('#covid19-table-global').addClass('nowrap');
        $('#covid19-table-global-wrap').css('padding-right', '5px');
        $('#covid19-table-global-wrap').css('max-height', '500px');
        $('#covid19-table-global-wrap').css('overflow', 'hidden');
        
        $('#covid19-statistics-graph').attr('height','200');
    }

    $('#covid19-table-global').DataTable({
        // "pageLength": 25,
        "responsive": {
            details: true,
        },
        "order": [[ 4, "desc" ]],
        "paging": false,
    });

    $('#show-more-covid').on('click', function() {
        $('#covid19-table-global-wrap').css('max-height', '');
        $('#covid19-table-global-wrap').css('overflow', '');
    })

    $('#covid19-table-global').on("click",'tbody tr', function(){
        if($(this).hasClass('is-selected')) {
            $('#covid19-table-global tbody tr').removeClass('is-selected');
        }
        else {
            $('#covid19-table-global tbody tr').removeClass('is-selected');
            $(this).addClass('is-selected'); 
        }
    });












    var colors = {
        red: '#d3001c',
        redLight: 'rgb(211 0 28 / 50%)',
        blue: '#15284c',
        blueLight: 'rgb(21 40 76 / 0.5)',
        green: '#008000',
        greenLight: 'rgb(0 128 0 / 0.5)',
    };


    // var MONTHS = ['Jan', 'Feb', 'Mar', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    var MONTHS = covid19Data.cases.dates;
    var config = {
        type: 'line',
        data: {
            labels: MONTHS,
            datasets: [
                {
                    label: 'Cases',
                    backgroundColor: colors.blueLight,
                    borderColor: colors.blue,
                    data: covid19Data.cases.amount,
                    fill: false,
                },
                {
                    label: 'Recovered',
                    backgroundColor: colors.greenLight,
                    borderColor: colors.green,
                    data: covid19Data.recovered.amount,
                    fill: false,
                },
                {
                    label: 'Deaths',
                    backgroundColor: colors.redLight,
                    borderColor: colors.red,
                    data: covid19Data.deaths.amount,
                    fill: false,
                },
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: true,
            title: {
                display: false,
                text: 'Confirmed Cases and Deaths'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: function(tooltipItems, data) {
                        let ti = tooltipItems;
                        let dt = data;
                        return dt.datasets[ti.datasetIndex].label + ': ' + dt.datasets[ti.datasetIndex].data[ti.index].toLocaleString();
                    }
                }                
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                // x: {
                //     display: true,
                //     scaleLabel: {
                //         display: true,
                //         labelString: 'Date'
                //     }
                // },
                // y: {
                //     display: true,
                //     scaleLabel: {
                //         display: true,
                //         labelString: 'Cases'
                //     }
                // }
                xAxes: [{
                    ticks: {
                      beginAtZero: true,
                      callback: function(value, index, values) {
                        return value;
                      }
                    }
                }],
                yAxes: [{
                    ticks: {
                      beginAtZero: true,
                      callback: function(value, index, values) {
                        // return value.toLocaleString()+' xxxx';
                        return value/1000000+' M';
                      }
                    }
                }]
            }
        }
    };
    
    

    var ctx = $('#covid19-statistics-graph');
    var myLineChart = new Chart(ctx, config);
    
    

});


