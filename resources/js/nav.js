$(document).ready(function () {
    let isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;

    if (!isMobile) {
        // UNCOMMENT AFTER
        $(".a-menu.navbar-item.has-submenu")
        .mouseenter(function (e) {
            e.stopPropagation();
            e.preventDefault();
            $(".has-submenu .submenu-container").stop().slideDown();
        })
        .mouseleave(function (e) {
            e.stopPropagation();
            e.preventDefault();
            $(".has-submenu .submenu-container").stop().slideUp();
        });

        $(".a-menu.navbar-item.has-submenu-2")
        .mouseenter(function (e) {
            e.stopPropagation();
            e.preventDefault();
            $(".has-submenu-2 .submenu-container-2").stop().slideDown();
        })
        .mouseleave(function (e) {
            e.stopPropagation();
            e.preventDefault();
            $(".has-submenu-2 .submenu-container-2").stop().slideUp();
        });
    } else {
        // Mobile
        $(".has-submenu .submenu-title a").click(function (e) {
            e.preventDefault(); 
        });
        $(".a-menu.navbar-item.has-submenu").click(function () {
            $(".has-submenu .submenu-container").stop().slideToggle();
        });

        $(".has-submenu-2 .submenu-title a").click(function (e) {
            e.preventDefault(); 
        });
        $(".a-menu.navbar-item.has-submenu-2").click(function () {
            $(".has-submenu-2 .submenu-container-2").stop().slideToggle();
        });

        $(".navbar-burger").click(function () {
            $(".navbar-menu").stop().slideToggle();
        });
    }



    $('.search-btn').on('click',function(){
        $('.search-bar').stop().fadeIn(function(){
            $('.search-bar input').trigger('focus');
        });
    });


});


$(document).mouseup(function(e) {
    var container = $(".search-bar");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.fadeOut();
    }
});