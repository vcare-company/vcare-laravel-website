$(document).ready(function(){


    $('.search-bar-page .search-submit').on('click',function(e){
        searchString();
    });




    $('.search-bar-page .search-string').on('keyup',function(e){
        if(e.which == 13) {
            searchString();
        }
    });

    if(search_string_post){
        console.log('search_string_post = ',search_string_post);

        $('.search-bar-page .search-string').val(search_string_post);
        searchString();
    }



    function searchString(){
        let toSearch = $('.search-bar-page .search-string').val();
        // console.log('toSearch = ',toSearch);

        $('.product-col').addClass('hide');

        let found = false;
        if(toSearch) {
            let toSearchArr = toSearch.split(',');
            $('.product-col').each(function(i){
                let overview = $(this).find('.product_search_data').val();

                let tsFound = true;
                $.each( toSearchArr, function( key, val ) {
                    let re = new RegExp(val.trim(),'i');
                    // console.log(re);
                    if(overview.search(re)<0) {
                        tsFound = false;
                        return false;
                    }
                });            
                
                if(tsFound) {
                    $(this).removeClass('hide');
                    found = true;
                }

            });
        }

        if(found) {
            console.log('FOUND!');
            $('.noresults').hide();
        }
        else {
            console.log('NOT FOUND!');
            $('.noresults').show();
        }

    }    

});