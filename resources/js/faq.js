$(document).ready(function(){

    bulmaCollapsible.attach();
    
    $('.faq-tabs ul li a').click(function(){
        $('.faq-tabs ul li').removeClass('is-active');
    
        $(this).parent('li').addClass('is-active');
        let tab_content = $(this).attr('href').substr(1);
        console.log(tab_content);
    
        $('.faq-content .tab-content').hide();
        $('.faq-content .tab-content.'+tab_content).show();
      });
});


