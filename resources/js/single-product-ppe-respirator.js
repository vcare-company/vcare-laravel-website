$(document).ready(function(){

  // jQuery methods go here...
  $('.image-container .images').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });


  $('.image-container .images img').click(function(){
    let src = $(this).attr('data-src');
    $('.image-container .main-image a').attr('href',src);
    $('.image-container .main-image img').attr('src',src);
  });



  $('.overview-certification-tabs ul li a').click(function(){
    $('.overview-certification-tabs ul li').removeClass('is-active');

    $(this).parent('li').addClass('is-active');
    let tab_content = $(this).attr('href').substr(1);
    console.log(tab_content);

    $('.overview-certification-content .tab-content').hide();
    $('.overview-certification-content .tab-content.'+tab_content).show();
  });

});





