
$(document).ready(function(){



    $(".columns").on("change", ".b2b_select", function(){

        var select_number = $(this).data("select");
        var quantity_number = $(this).closest("#clone-div").find("#b2b_quantity").data("quantity");
        var selected_product = $(this).children("option:selected").data("value");
        var selected_product_val = $(this).children("option:selected").val();
        
        console.log(select_number + "," + quantity_number+ "," +selected_product);

        console.log($(this).closest("#clone-div").find("#b2b_quantity").attr("class"))


        if (select_number == quantity_number) {
            $(this).closest("#clone-div").find("#b2b_quantity").find("input").attr("name", selected_product + "_qty");
            $(this).attr("name", selected_product_val)
        }

    })
    
    var slt_number = 1;
    var qty_number = 1;

    $("#add-product").click(function(e){
        e.preventDefault();

        var html = $("#clone-div").clone();
        $("#cloned-div").append(html)

        slt_number++;

        console.log(slt_number);

        $(".b2b_select").last().attr("data-select", slt_number);
        $(".b2b_quantity").last().attr("data-quantity", slt_number);

    })

    $("select[name='number_type']").change(function(){
        if($(this).val() == 'Mobile') {
            $(".whatsapp-available").show()
        } else {
            $(".whatsapp-available").hide()
        }
    })

    $("select[name='who']").change(function(){
       
        if($(this).val() == 'Other') {
            $(".who-other-option").show()
        } else {
            $(".who-other-option").hide()
        }
    })
    
    $("select[name='how']").change(function(){
        if($(this).val() == 'Referral') {
            $(".referral-option").show()
        } else {
            $(".referral-option").hide()
        }

        if($(this).val() == 'Other') {
            $(".how-other-option").show()
        } else {
            $(".how-other-option").hide()
        }
    })


    $("input[name='same_courier_number']").click(function(){
       
        if($(this).val() == 'No') {
            $(".courier-number").show()
        } else {
            $(".courier-number").hide()
        }
    })


})