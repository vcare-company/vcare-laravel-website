// require('./bootstrap');
$(document).ready(function(){


    // To scroll to when accessed url with #
    let link = window.location.href;
    let el_id = link.split("#")[1];

    if (el_id=="in-the-course-of-incorporation") {
        $("html, body").animate(
            {
                scrollTop: $("#"+el_id).offset().top,
            },
            500,
            function(){
                $('#'+el_id).css('font-weight','bold');
            }
        );
    }

});
