$(document).ready(function () {
    lightbox.option({
        resizeDuration: 200,
        wrapAround: true,
    });

    $(".tabs.tab-menu a").click(function (e) {
        // e.stopPropagation();
        // e.preventDefault();

        // console.log('xxxxxxxxxxxxxxx');

        $(".tabs.tab-menu ul li").removeClass("is-active");
        $(this).parent().addClass("is-active");

        let id = $(this).attr("data-tab_id");
        $(".tab-content")
        // .stop()
        .slideUp();
        $("#" + id)
            // .stop()
            .slideDown("fast", function () {
                if (id) {
                    // console.log("$(.tab-contents) offset "+ $(".tab-contents").offset().top);
                    $("html, body").animate(
                        {
                            scrollTop: $(".tab-contents").offset().top,
                        },
                        500
                    );
                }
            });
    });

    $("a.gototab").click(function (e) {
        e.stopPropagation();
        e.preventDefault();

        let tab_id = $(this).attr("data-tab_id");

        if (tab_id) {
            $('.tabs.tab-menu a[data-tab_id="' + tab_id + '"]').click();
        }
    });

    // let isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;
    // if(isMobile) {
    //   $('.tabs.tab-menu a').click(function(e){
    //     let id = $(this).attr('data-tab_id');
    //     console.log(id);
    //   });
    // }

    // To scroll to when accessed url with #
    let link = window.location.href;
    let tab_id = link.split("#")[1];

    if (tab_id) {
        $('.tabs.tab-menu a[data-tab_id="' + tab_id + '"]').click();
    }

    $(".lang-btn").click(function (e) {
        let lang = $(this).attr("data-lang");

        $(".lang-btn").toggle();
        // let ph_url = '/files/placeholder-image.svg';
        let ph_url = "/files/logos/vcareicon.svg";
        // let ph_url = '/files/logos/vcareicon.svg?u='+env['APP_ASSET_TIMESTAMP'];
        // let ph_url = '/files/logos/vcareplaceholder.svg?u='+env['APP_ASSET_TIMESTAMP'];

        // let $a_els = $(this).parents('.tab-content').find('.tile a');
        // $('.tab-content .lang-container').find('.tile.has-lang');
        // let $has_lang_els = $(this).find('.tile.has-lang');

        $(".tab-content .tile-container.lang-container").each(function (i) {
            let $has_lang_els = $(this).find(".tile.has-lang");

            let a_url = $(this).attr("data-img_url_" + lang);
            let a_thumbnail_url = $(this).attr("data-img_thumbnail_url_" + lang);

            let a_expanded_url = $(this).attr("data-img_expanded_url_" + lang);

            // console.log('a_url',a_url);
            // console.log('a_thumbnail_url',a_thumbnail_url);

            // Hide or show if file/img exist
            let $tc = $(this);
            if (a_url) {
                $tc.find(".has-lang").show();
                $tc.find(".has-no-lang").hide();
            } else {
                $tc.find(".has-lang").hide();
                $tc.find(".has-no-lang").show();
            }

            let $a_els = $(this).find("a");
            $a_els.each(function (i2) {
                let url = a_url;
                // for package details expanded
                if ($(this).hasClass("has-expanded")) {
                    url = a_expanded_url;
                }

                if (url) $(this).attr("href", url);
                else $(this).removeAttr("href");

                let $a_thumbnail_el = $(this).find("img");

                if (a_thumbnail_url) {
                    $a_thumbnail_el.attr("src", a_thumbnail_url);
                } 

                // disabled because it is not needed
                // else {
                //     $a_thumbnail_el.attr("src", ph_url);
                // }
            });
        });
    });
});
