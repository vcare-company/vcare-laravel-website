<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog', function (Blueprint $table) {
            $table->increments('b_id');
            $table->string('b_public_id',100)->unique();
            $table->text('b_title');
            $table->text('b_short_content');
            $table->text('b_html_content')->nullable();
            $table->text('b_meta_title');
            $table->text('b_meta_desc');
            $table->text('b_url')->nullable();
            $table->integer('active')->length(1)->nullable()->default(1);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('blog');
    }
}
