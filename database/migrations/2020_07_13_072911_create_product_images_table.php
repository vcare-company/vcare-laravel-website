<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_images', function (Blueprint $table) {
            $table->bigIncrements('pi_id');
            $table->string('pi_title')->length(100)->nullable();;
            $table->string('pi_filename')->length(100)->nullable();;
            $table->string('pi_ext')->length(100)->nullable();;
            $table->string('pi_ext_thumb')->length(100)->nullable();;
            $table->string('pi_type')->length(50)->nullable();;
            $table->integer('pi_order')->length(2)->nullable();;
            $table->integer('pi_p_id')->length(11)->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_images');
    }
}
