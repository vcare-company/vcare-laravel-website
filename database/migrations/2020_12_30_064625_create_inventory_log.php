<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoryLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_inventory_log', function (Blueprint $table) {
            $table->increments('pil_id');
            $table->integer('pil_pin_id')->length(11)->nullable();   
            $table->integer('pil_quantity');
            $table->string('pil_status');  
            $table->string('pil_client')->nullable();  
            $table->string('pil_added_by');  
            $table->string('pil_added_by_ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_inventory_log');
    }
}
