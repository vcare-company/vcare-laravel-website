<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductImagesProductDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_images_product_details', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->integer('product_details_image_id');
            $table->integer('product_details_expanded_image_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_images_product_details', function (Blueprint $table) {
            //
        });
    }
}
