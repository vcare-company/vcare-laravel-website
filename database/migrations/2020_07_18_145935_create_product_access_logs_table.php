<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductAccessLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_access_logs', function (Blueprint $table) {
            $table->increments('pal_id');
            $table->string('pal_username',100)->nullable();
            $table->string('pal_product_code',100)->nullable();    
            $table->string('pal_access_type',100)->nullable();    
            $table->text('pal_note')->nullable();    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_access_logs');
    }
}
