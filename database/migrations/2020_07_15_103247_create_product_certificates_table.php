<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_certificates', function (Blueprint $table) {
            $table->increments('pc_id');
            $table->string('pc_title')->length(100)->nullable();
            $table->string('pc_filename')->length(100)->nullable();
            $table->string('pc_ext')->length(100)->nullable();
            $table->string('pc_ext_thumb')->length(100)->nullable();
            $table->string('pc_type')->length(50)->nullable();
            $table->integer('pc_order')->length(2)->nullable();
            $table->integer('pc_p_id')->length(11)->nullable();            
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_certificates');
    }
}
