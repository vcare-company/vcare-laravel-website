<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('p_id');
            $table->string('p_name')->length(100)->nullable();
            $table->string('p_code')->length(100)->nullable();
            $table->text('p_overview')->nullable();
            $table->text('p_certifications')->nullable();
            $table->text('p_features')->nullable();
            $table->text('p_benefits')->nullable();
            $table->text('p_text')->nullable();
            $table->integer('p_c_id')->nullable();
            $table->integer('p_order')->length(2)->default(0);
            $table->integer('active')->length(1)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
