<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('c_id');
            $table->string('c_code', 100);
            $table->string('c_name', 100);
            $table->text('c_desc');
            $table->text('c_meta_title')->nullable();
            $table->text('c_meta_desc')->nullable();
            $table->text('c_h1')->nullable();
            $table->text('c_h2')->nullable();
            $table->text('c_p')->nullable();
            $table->string('c_image_ext', 100);
            $table->string('c_image_alt', 100);
            $table->string('c_image_filename', 100);
            $table->string('active', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
