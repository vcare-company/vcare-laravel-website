<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_inventory_comments', function (Blueprint $table) {
            $table->increments('pic_id');
            $table->integer('pic_p_id')->length(11)->nullable();  
            $table->string('pic_comment');  
            $table->string('pic_added_by');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_inventory_comments');
    }
}
