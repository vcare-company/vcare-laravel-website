<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPackagingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_packaging', function (Blueprint $table) {
            $table->bigIncrements('pp_id');
            $table->string('pp_title')->length(100)->nullable();
            $table->integer('pp_pi_id')->length(11)->nullable();;
            $table->integer('pp_p_id')->length(11)->nullable();;
            $table->text('pp_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_packaging');
    }
}
