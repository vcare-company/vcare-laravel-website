<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPricelistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_pricelists', function (Blueprint $table) {
            $table->increments('pp_id');
            $table->text('pp_content')->nullable();
            $table->json('pp_certifications')->nullable();
            $table->json('pp_table')->nullable();
            $table->integer('pp_p_id')->length(11)->nullable();    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_pricelists');
    }
}
