<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateProductsTableUpdateDefaultValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('products', function (Blueprint $table) {
            $table->string('p_name')->nullable()->default(NULL)->change();
            $table->string('p_code')->nullable()->default(NULL)->change();
            $table->text('p_overview')->nullable()->default(NULL)->change();
            $table->text('p_certifications')->nullable()->default(NULL)->change();
            $table->text('p_features')->nullable()->default(NULL)->change();
            $table->text('p_benefits')->nullable()->default(NULL)->change();
            $table->text('p_text')->nullable()->default(NULL)->change();
            $table->text('p_datasheet')->nullable()->default(NULL)->change();
            $table->string('p_certificates_password',100)->nullable()->default(NULL)->change();
            $table->string('p_pricelist_password',100)->nullable()->default(NULL)->change();
            $table->integer('p_c_id')->nullable()->default(NULL)->change();
        });         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
