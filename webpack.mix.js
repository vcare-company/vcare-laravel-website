const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


// Main css
mix
.sass('./resources/sass/nav.scss', '../resources/css/nav.css')
.sass('./resources/sass/footer.scss', '../resources/css/footer.css')
.sass('./resources/sass/privacy-policy.scss', '../resources/css/privacy-policy.css')
.sass('./resources/sass/app.scss', '../resources/css/app.css')
.combine([
    './resources/css/nav.css',
    './resources/css/footer.css',
    './resources/css/privacy-policy.css',
    './resources/css/app.css',
], 
'./resources/css/main.css');


// Main js
mix.scripts([
    './resources/js/nav.js',
    './resources/js/footer.js',
    './resources/js/privacy-policy.js',
    './resources/js/app.js',
], 
'./resources/js/main.js');



// lightbox
mix.postCss('./node_modules/lightbox2/dist/css/lightbox.css', '../resources/css/lightbox/lightbox.css');
mix.scripts(['./node_modules/lightbox2/dist/js/lightbox.min.map'],'./public/js/lightbox.min.map')


// bulma-modal-fx
mix.postCss('./node_modules/bulma-modal-fx/dist/css/modal-fx.css', '../resources/css/bulma-modal-fx/bulma-modal-fx.css');


// slick
mix.sass('./node_modules/slick-carousel/slick/slick.scss', '../resources/css/slick/slick.css');
mix.sass('./node_modules/slick-carousel/slick/slick-theme.scss', '../resources/css/slick/slick-theme.css');

// datatables
mix.postCss('./node_modules/datatables.net-dt/css/jquery.dataTables.min.css', '../resources/css/datatables/datatables.css');

// chartist
// mix.postCss('./node_modules/chartist/dist/chartist.min.css', '../resources/css/chartist/chartist.css');
// mix.scripts(['./node_modules/chartist/dist/chartist.min.map'],'./public/js/chartist.min.map')


// chart.js
mix.postCss('./node_modules/chart.js/dist/Chart.min.css', '../resources/css/chart.js/chart.css');



////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////







// Home page
mix.sass('./resources/sass/home.scss', '../resources/css/home.css');

mix.combine([
    './resources/css/slick/slick.css',
    './resources/css/slick/slick-theme.css',    
    './resources/css/main.css',
    './resources/css/home.css',
], 
'./public/css/home-page.css');

mix.scripts([
    './node_modules/slick-carousel/slick/slick.min.js',
    './resources/js/main.js',
    './resources/js/home.js',
], 
'./public/js/home-page.js');






// Single Product page
mix.sass('./resources/sass/single-product.scss', '../resources/css/single-product.css');

mix.combine([
    './resources/css/lightbox/lightbox.css',
    './resources/css/bulma-modal-fx/bulma-modal-fx.css',
    './resources/css/slick/slick.css',
    './resources/css/slick/slick-theme.css',
    './resources/css/main.css',
    './resources/css/single-product.css',
], 
'./public/css/single-product-page.css');

mix.scripts([
    './node_modules/slick-carousel/slick/slick.min.js',
    './node_modules/lightbox2/dist/js/lightbox.min.js',
    './resources/js/main.js',
    './resources/js/single-product.js',
], 
'./public/js/single-product-page.js');




// // Single Product PPE Respirator page
// mix.sass('./resources/sass/single-product-ppe-respirator.scss', '../resources/css/single-product-ppe-respirator.css');

// mix.combine([
//     './resources/css/lightbox/lightbox.css',
//     './resources/css/slick/slick.css',
//     './resources/css/slick/slick-theme.css',
//     './resources/css/main.css',
//     './resources/css/single-product-ppe-respirator.css',
// ], 
// './public/css/single-product-ppe-respirator-page.css');

// mix.scripts([
//     './node_modules/slick-carousel/slick/slick.min.js',
//     './node_modules/lightbox2/dist/js/lightbox.min.js',
//     './resources/js/main.js',
//     './resources/js/single-product-ppe-respirator.js',
// ], 
// './public/js/single-product-ppe-respirator-page.js');



// Single Product Pricelist
mix
.sass('./resources/sass/single-product-pricelist.scss', '../resources/css/single-product-pricelist.css')
.combine([
    './resources/css/lightbox/lightbox.css',
    './resources/css/bulma-modal-fx/bulma-modal-fx.css',
    './resources/css/slick/slick.css',
    './resources/css/slick/slick-theme.css',    
    './resources/css/main.css',
    './resources/css/single-product.css',
    './resources/css/single-product-pricelist.css',
], 
'./public/css/single-product-pricelist-page.css');


mix.scripts([
    './node_modules/slick-carousel/slick/slick.min.js',
    './node_modules/lightbox2/dist/js/lightbox.min.js',    
    './resources/js/main.js',
    './resources/js/single-product.js',
    './resources/js/single-product-pricelist.js',
], 
'./public/js/single-product-pricelist-page.js');



// Single Product Certificates
mix
.sass('./resources/sass/single-product-certificates.scss', '../resources/css/single-product-certificates.css')
.combine([
    './resources/css/lightbox/lightbox.css',
    './resources/css/main.css',
    './resources/css/single-product-certificates.css',
], 
'./public/css/single-product-certificates-page.css');


mix.scripts([
    './node_modules/lightbox2/dist/js/lightbox.min.js',
    './resources/js/main.js',
    './resources/js/single-product-certificates.js',
], 
'./public/js/single-product-certificates-page.js');




// Coming Soon Product
mix
.sass('./resources/sass/coming-soon-product.scss', '../resources/css/coming-soon-product.css')
.combine([
    './resources/css/main.css',
    './resources/css/coming-soon-product.css',
], 
'./public/css/coming-soon-product-page.css');


mix.scripts([
    './resources/js/main.js',
    './resources/js/coming-soon-product.js',
], 
'./public/js/coming-soon-product-page.js');


// Single Product Login
mix
.sass('./resources/sass/single-product-login.scss', '../resources/css/single-product-login.css')
.combine([
    './resources/css/main.css',
    './resources/css/single-product-login.css',
], 
'./public/css/single-product-login-page.css');


mix.scripts([
    './resources/js/main.js',
    './resources/js/single-product-login.js',
], 
'./public/js/single-product-login-page.js');



// All Products page
mix.sass('./resources/sass/all-products.scss', '../resources/css/all-products.css');

mix.combine([
    // './resources/css/lightbox/lightbox.css',
    './resources/css/main.css',
    './resources/css/all-products.css',
], 
'./public/css/all-products-page.css');

mix.scripts([
    // './node_modules/lightbox2/dist/js/lightbox.min.js',
    './resources/js/main.js',
    './resources/js/all-products.js',
], 
'./public/js/all-products-page.js');



// Single Category page
mix.sass('./resources/sass/single-category.scss', '../resources/css/single-category.css');

mix.combine([
    // './resources/css/lightbox/lightbox.css',
    './resources/css/main.css',
    './resources/css/single-category.css',
], 
'./public/css/single-category-page.css');

mix.scripts([
    // './node_modules/lightbox2/dist/js/lightbox.min.js',
    './resources/js/main.js',
    './resources/js/single-category.js',
], 
'./public/js/single-category-page.js');



// Charity page
mix.sass('./resources/sass/charity.scss', '../resources/css/charity.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/charity.css',
], 
'./public/css/charity-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/charity.js',
], 
'./public/js/charity-page.js');




// Respirator Comparison page
mix.sass('./resources/sass/respirator-comparison.scss', '../resources/css/respirator-comparison.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/respirator-comparison.css',
], 
'./public/css/respirator-comparison-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/respirator-comparison.js',
], 
'./public/js/respirator-comparison-page.js');



// Respirator Promotion page
mix.sass('./resources/sass/respirator-promotion.scss', '../resources/css/respirator-promotion.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/respirator-promotion.css',
], 
'./public/css/respirator-promotion-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/respirator-promotion.js',
], 
'./public/js/respirator-promotion-page.js');



// Message from the CEO page
mix.sass('./resources/sass/message-ceo.scss', '../resources/css/message-ceo.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/message-ceo.css',
], 
'./public/css/message-ceo-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/message-ceo.js',
], 
'./public/js/message-ceo-page.js');


// Downloads page
mix.sass('./resources/sass/downloads.scss', '../resources/css/downloads.css');

mix.combine([
    './resources/css/lightbox/lightbox.css',
    './resources/css/main.css',
    './resources/css/downloads.css',
], 
'./public/css/downloads-page.css');

mix.scripts([
    './node_modules/lightbox2/dist/js/lightbox.min.js',
    './resources/js/main.js',
    './resources/js/downloads.js',
], 
'./public/js/downloads-page.js');


// About page
mix.sass('./resources/sass/about.scss', '../resources/css/about.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/about.css',
], 
'./public/css/about-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/about.js',
], 
'./public/js/about-page.js');


// Contact page
mix.sass('./resources/sass/contact.scss', '../resources/css/contact.css');

mix.combine([
    './node_modules/sweetalert2/dist/sweetalert2.min.css',
    './resources/css/main.css',
    './resources/css/contact.css',

], 
'./public/css/contact-page.css');

mix.scripts([
    './node_modules/sweetalert2/dist/sweetalert2.min.js',
    './resources/js/main.js',
    './resources/js/contact.js',
], 
'./public/js/contact-page.js');



// Sample page
mix.sass('./resources/sass/sample.scss', '../resources/css/sample.css');

mix.combine([
    './node_modules/sweetalert2/dist/sweetalert2.min.css',
    './resources/css/main.css',
    './resources/css/sample.css',

], 
'./public/css/sample-page.css');

mix.scripts([
    './node_modules/sweetalert2/dist/sweetalert2.min.js',
    './resources/js/main.js',
    './resources/js/sample.js',
], 
'./public/js/sample-page.js');


// FAQ page
mix.sass('./resources/sass/faq.scss', '../resources/css/faq.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/faq.css',
    './node_modules/@creativebulma/bulma-collapsible/dist/css/bulma-collapsible.min.css',
], 
'./public/css/faq-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/faq.js',
    './node_modules/@creativebulma/bulma-collapsible/dist/js/bulma-collapsible.min.js',
], 
'./public/js/faq-page.js');

// Legal page
mix.sass('./resources/sass/legal.scss', '../resources/css/legal.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/legal.css',
], 
'./public/css/legal-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/legal.js',
], 
'./public/js/legal-page.js');

// Privacy page
mix.sass('./resources/sass/privacy.scss', '../resources/css/privacy.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/privacy.css',
], 
'./public/css/privacy-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/privacy.js',
], 
'./public/js/privacy-page.js');

// Terms page
mix.sass('./resources/sass/terms.scss', '../resources/css/terms.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/terms.css',
], 
'./public/css/terms-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/terms.js',
], 
'./public/js/terms-page.js');

// Refund page
mix.sass('./resources/sass/refund.scss', '../resources/css/refund.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/refund.css',
], 
'./public/css/refund-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/refund.js',
], 
'./public/js/refund-page.js');


// Blog page
mix.sass('./resources/sass/blog.scss', '../resources/css/blog.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/blog.css',
], 
'./public/css/blog-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/blog.js',
], 
'./public/js/blog-page.js');


// Blog Single page
mix.sass('./resources/sass/blog-single.scss', '../resources/css/blog-single.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/blog-single.css',
], 
'./public/css/blog-single-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/blog-single.js',
], 
'./public/js/blog-single-page.js');

// Info page
mix.sass('./resources/sass/info.scss', '../resources/css/info.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/info.css',
], 
'./public/css/info-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/info.js',
], 
'./public/js/info-page.js');


//Info Single EN 149 page
mix.sass('./resources/sass/info-single.scss', '../resources/css/info-single.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/info-single.css',
], 
'./public/css/info-single-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/info-single.js',
], 
'./public/js/info-single-page.js');




// COVID19 Statistics page
mix.sass('./resources/sass/covid19-statistics.scss', '../resources/css/covid19-statistics.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/datatables/datatables.css',
    // './resources/css/chartist/chartist.css',
    './resources/css/chart.js/chart.css',
    './resources/css/covid19-statistics.css',
], 
'./public/css/covid19-statistics-page.css');

mix.scripts([
    './resources/js/main.js',
    './node_modules/datatables.net/js/jquery.dataTables.min.js',
    './node_modules/datatables.net-responsive/js/dataTables.responsive.min.js',
    // './node_modules/chartist/dist/chartist.min.js',
    './node_modules/chart.js/dist/Chart.min.js',
    './resources/js/covid19-statistics.js',
], 
'./public/js/covid19-statistics-page.js');










// Sora page
mix.sass('./resources/sass/sora.scss', '../resources/css/sora.css');

mix.combine([
    './resources/css/lightbox/lightbox.css',
    './resources/css/main.css',
    './resources/css/sora.css',
], 
'./public/css/sora-page.css');

mix.scripts([
    './node_modules/lightbox2/dist/js/lightbox.min.js',
    './resources/js/main.js',
    './resources/js/sora.js',
], 
'./public/js/sora-page.js');




// Search page
mix.sass('./resources/sass/search.scss', '../resources/css/search.css');

mix.combine([
    // './resources/css/lightbox/lightbox.css',
    './resources/css/main.css',
    './resources/css/search.css',
], 
'./public/css/search-page.css');

mix.scripts([
    // './node_modules/lightbox2/dist/js/lightbox.min.js',
    './resources/js/main.js',
    './resources/js/search.js',
], 
'./public/js/search-page.js');










// Careers page
mix.sass('./resources/sass/careers.scss', '../resources/css/careers.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/careers.css',
], 
'./public/css/careers-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/careers.js',
], 
'./public/js/careers-page.js');




// Sitemap HTML page
mix.sass('./resources/sass/sitemap-html.scss', '../resources/css/sitemap-html.css');

mix.combine([
    './resources/css/main.css',
    './resources/css/sitemap-html.css',
], 
'./public/css/sitemap-html-page.css');

mix.scripts([
    './resources/js/main.js',
    './resources/js/sitemap-html.js',
], 
'./public/js/sitemap-html-page.js');
































// Admin Stuff
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




// Main css
mix
.sass('./resources/sass/admin/app.scss', '../resources/css/admin/app.css')
.combine([
    './resources/css/admin/app.css',
], 
'./resources/css/admin/main.css');


// Main js
mix.scripts([
    './resources/js/admin/app.js',
], 
'./resources/js/admin/main.js');






// Add Product page
mix.sass('./resources/sass/admin/add-product.scss', '../resources/css/admin/add-product.css');

mix.combine([
    './resources/css/admin/main.css',
    './resources/css/admin/add-product.css',
], 
'./public/css/admin/add-product-page.css');

mix.scripts([
    './resources/js/admin/main.js',
    './resources/js/admin/add-product.js',
], 
'./public/js/admin/add-product-page.js');



