<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;


use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






// Route::get('/', function () {
//     return view('home');
// })->name('home');
Route::get('/','PageController@home')->name('home');

Route::post('/subscribe-newsletter','SubscribeController@subscribeNewsletter')->name('subscribeNewsletter');

Route::get('/charity','PageController@charity')->name('charity');

Route::get('/careers','PageController@careers')->name('careers');

Route::get('/medical-equipment-supplier','PageController@promotions')->name('medical-equipment-supplier');


Route::get('/covid19-statistics','StatisticsController@covid19Statistics')->name('covid19-statistics');

Route::get('/covid19-statistics/update-json-files','StatisticsController@updateFiles')->name('update_files');
	

Route::get('/about','PageController@about')->name('about');


Route::get('/respirator-comparison','RespiratorController@respiratorComparison')->name('respirator-comparison');
Route::get('/respirator-promotion','RespiratorController@respiratorPromotion')->name('respirator-promotion');


Route::get('/message-from-the-ceo','PageController@messageFromTheCeo')->name('message_from_the_ceo');


Route::get('/downloads','DownloadsController@index')->name('downloads');


Route::get('/faq', 'PageController@faq')->name('faq');

Route::get('/faqs', 'PageController@faqs')->name('faqs');

Route::get('/contact', 'ContactUsController@showContact')->name('contact');
Route::get('/sample-request', 'ContactUsController@showSample')->name('sample');


// Route::get('/coming-soon', 'PageController@comingSoonProduct')->name('coming-soon-product');
 


Route::post('/contact-us', 'ContactUsController@saveData')->name('contact-save');

Route::get('/contact/thank-you/{topic}', 'ContactUsController@thankYou')->name('thank-you');


// Certificates and Pricelist Logout
Route::get('/products/logout', "ProductController@singleProductLogout")->name('single-product-logout');


// SEO Category Products
//////////////////////////////////////////////////////////////////////////

$cats = [
	[ 'code'=>'ppe', 'url'=>'personal-protective-equipment-ppe'],
	[ 'code'=>'acf', 'url'=>'activated-carbon-filters'],
	[ 'code'=>'medical-equipment', 'url'=>'medical-equipment'],
	[ 'code'=>'surgical-gown', 'url'=>'medical-equipment/personal-protection-clothing'],
	[ 'code'=>'ffp2', 'url'=>'personal-protective-equipment-ppe/ffp2'],
	[ 'code'=>'ffp3', 'url'=>'personal-protective-equipment-ppe/ffp3'],
	[ 'code'=>'n95', 'url'=>'respirators/n95'],
];
foreach ($cats as $cat) {
	Route::get('/products/'.$cat['url'], function() use ($cat) {
        return App::call('App\Http\Controllers\CategoryController@categoryPage', ['cat_code'=>$cat['code']]);		
	})->name('cat-'.$cat['code']);
}


Route::get('/products/respirators', "CategoryController@respirators")->name('cat-respirators');

//////////////////////////////////////////////////////////////////////////
// SEO Category Products



Route::get('/products/{product_code}', "ProductController@singleProduct")->name('single-product');


Route::get('/products/{product_code}/pricelist', "ProductController@singleProductPricelist")->name('single-product-pricelist');
Route::get('/products/{product_code}/certificates', "ProductController@singleProductCertificates")->name('single-product-certificates');
Route::get('/products/{product_code}/original_certificates', "ProductController@singleProductCertificatesTwo")->name('single-product-original_certificates');
Route::get('/products/{product_code}/obm_certificates', "ProductController@singleProductCertificatesThree")->name('single-product-obm_certificates');


Route::get('/products', "CategoryController@allProducts")->name('all-products');


Route::post('/products/{product_code}/{login_type}/login', "ProductController@singleProductLoginPost")->name('single-product-login-post');
Route::get('/products/{product_code}/{login_type}/login', "ProductController@singleProductLogin")->name('single-product-login');


Route::get('/coming-soon', 'PageController@comingSoonProduct')->name('coming-soon-product');
Route::get('/coming-soon/{product_code}', 'PageController@comingSoonProductCode')->name('coming-soon-product-code');



// Datasheet Files
Route::get('/datasheet/{language}/{product_code}','RedirectController@datasheetLanguage')->name('datasheetLanguage');
Route::get('/datasheet/{product_code}','RedirectController@datasheet')->name('datasheet');

// Packaging Files
Route::get('/packaging/{language}/{product_code}','RedirectController@packagingLanguage')->name('packagingLanguage');
Route::get('/packaging/{product_code}','RedirectController@packaging')->name('packaging');


// had to add s to /manual/... I dont know why but it displays 404 error if you set it as /manual/...
Route::get('/manuals/{language}/{product_code}','RedirectController@manualLanguage')->name('manualLanguage');
Route::get('/manuals/{product_code}','RedirectController@manual')->name('manual');



// Certificates Download
Route::get('/certificate/{product_code}/{file_code}','RedirectController@certificate')->name('certificate');
// Route::get('/certificate/{product_code}/{file_code}','RedirectController@certificateTwo')->name('original-certificate');
// Route::get('/certificate/{product_code}/{file_code}','RedirectController@certificateThree')->name('obm-certificate');


Route::get('/vcare-company-profile-pdf','RedirectController@companyProfile')->name('companyProfile');
Route::get('/vcare-detailed-comparison-for-masks-pdf','RedirectController@detailedComparisonMasks')->name('detailedComparisonMasks');
Route::get('/vcare-respirator-comparison-pdf','RedirectController@respiratorComparison')->name('respiratorComparison');

Route::get('/class-standards-comparison-pdf','RedirectController@classStandardsComparison')->name('classStandardsComparison');






Route::get('/sitemap','SitemapController@sitemap')->name('sitemap');
Route::get('/sitemap.html','SitemapController@sitemapHTML')->name('sitemap_html');
Route::get('/sitemap.xml','SitemapController@sitemap')->name('sitemap_xml');
Route::get('/sitemap_index.xml','SitemapController@sitemap')->name('sitemap_index_xml');


// SORA
Route::get('/sora', 'SoraController@sora')->name('sora-page');
Route::get('/sora/certificates', 'SoraController@soraCertificates')->name('sora-certificates-page');
Route::get('/sora/inventory', 'SoraController@soraInventory')->name('sora-inventory-page');
Route::post('/sora/inventory/post', 'SoraController@soraInventoryPost')->name('sora-inventory-page-post');
Route::post('/sora/inventory/comment', 'SoraController@soraInventoryCommentPost')->name('sora-inventory-comment-post');
Route::get('/sora/login', "SoraController@soraLogin")->name('sora-login');
Route::post('/sora/login', "SoraController@soraLoginPost")->name('sora-login-post');




// Legal pages

Route::get('/privacy-policy','PageController@privacy')->name('privacy');

Route::get('/terms', 'PageController@terms')->name('terms');

Route::get('/legal', 'PageController@legal')->name('legal');

Route::get('/refund-policy', 'PageController@refund')->name('refund');




// Blog
Route::get('/blog','BlogController@blog')->name('blog');
Route::get('/blog/{blog_public_id}/{blog_title}','BlogController@blogSinglePage')->name('blog_single_page');


// Info
Route::get('/more-info','InfoController@info')->name('more-info');
Route::get('/en149-ffp1','InfoController@en49ffp1')->name('en149-ffp1');
Route::get('/reusable-respirator','InfoController@reusablerespirator')->name('reusable-respirator');
Route::get('/disposable-respirator','InfoController@disposablerespirator')->name('disposable-respirator');

// Search
Route::get('/search','SearchController@search')->name('search');
Route::post('/search','SearchController@searchPost')->name('search-post');



// Redirects from old links



// /packaging-gallery
// /product/corona-protection-mask
// /product/corona-protection-mask-ffp2-nr-1a
// /product/corona-protection-mask-ffp2-nr-1b
// /product/corona-protection-mask-ffp2-nr-2b
// /product/corona-protection-mask-ffp2-nr-3b
// /product/corona-protection-mask-ffp2-nr-4b
// /product/protection-respirator-ffp2-nr-4b
// /product/disposable-surgical-medical-face-mask
// /product/corona-rapid-test-kit


Route::get('/packaging-gallery', function () {
	return redirect()->route('downloads');
});
Route::get('/product/corona-protection-mask-ffp2-nr-1a', function () {
	return redirect()->route('all-products');
});
Route::get('/product/corona-protection-mask-ffp2-nr-1b', function () {
	return redirect()->route('all-products');
});
Route::get('/product/corona-protection-mask-ffp2-nr-2b', function () {
	return redirect()->route('single-product',['product_code'=>'ffp2-ross']);
});
Route::get('/product/corona-protection-mask-ffp2-nr-3b', function () {
	return redirect()->route('all-products');
});
Route::get('/product/corona-protection-mask-ffp2-nr-4b', function () {
	return redirect()->route('single-product',['product_code'=>'ffp2-max']);
});
Route::get('/product/protection-respirator-ffp2-nr-4b', function () {
	return redirect()->route('single-product',['product_code'=>'ffp2-max']);
});
Route::get('/product/disposable-surgical-medical-face-mask', function () {
	return redirect()->route('single-product',['product_code'=>'mfmask-gem']);
});
Route::get('/product/corona-rapid-test-kit', function () {
	return redirect()->route('single-product',['product_code'=>'rtest-finn']);
});



























Route::get('/jsonencode', function () {

		// dd(json_encode(
		// 	[
		// 		['title'=>'Certification','value'=>'GB2626-2006*'],
		// 		['title'=>'CNAS Authorized Test Report','value'=>'GB2626-2006'],
		// 		['title'=>'Classification','value'=>'KN95 NR'],
		// 		['title'=>'Regions Accepted','value'=>'China & Rest of World'],
		// 	]
		// ));

		// dd(json_encode(
		// 	[
		// 		['quantity'=>'500 - 2.500','price'=>2.95],
		// 		['quantity'=>'2.500 - 5.000','price'=>2.85],
		// 		['quantity'=>'5.000 - 25.000','price'=>2.75],
		// 		['quantity'=>'25.000 - 50.000','price'=>2.65],
		// 		['quantity'=>'50.000 - 75.000','price'=>2.55],
		// 		['quantity'=>'75.000 - 100.000','price'=>2.45],
		// 		['quantity'=>'100.000 - 250.000','price'=>2.35],
		// 		['quantity'=>'250.000 - 500.000','price'=>2.25],
		// 		['quantity'=>'500.000 - 1.000.000','price'=>2.15],
		// 		['quantity'=>'1.000.000 and more','price'=>2.05],
		// 	]
		// ));


		// dd($product['certification']);
		// dd($product['table']);

		// dd(json_encode([
		// 	'Our KN95 protection mask is approved by one of the Chinese Government authorized CNAS laboratories (China National Accreditation Service for Conformity Assessment). The applied GB2626 testing standard is equivalent to the European EN149:2001+A1:2009 procedure used to certify FFP2 protection
		// 	masks.',
		// 	'We reserve the right to change our prices at any time without further notice. However, if you have ordered but not yet paid for a product, we guarantee
		// 	the price for one month from when the order was placed.',
		// 	'NOTE: Import rules and regulations vary from country to country. Together with our partners we check, manufacture, design and distribute medical
		// 	products to our best knowledge to comply with most countries. The importer bears the sole and final responsibility',

		// ]));

})->name('jsonencode');





Route::get('/createpassword/many/', function () {

	$prod = [
		['FFP2 ROSS',env('APP_URL').'/products/ffp2-ross/original_certificates','36oyypfrpo'],
		['FFP2 MAX',env('APP_URL').'/products/ffp2-max/original_certificates','inzqn2wlf7'],
		['FFP2 ANN',env('APP_URL').'/products/ffp2-ann/original_certificates','027jik73wm'],
		['FFP2 jane',env('APP_URL').'/products/ffp2-jane/original_certificates','lciir6p1il'],
		['FFP2 kent',env('APP_URL').'/products/ffp2-kent/original_certificates','qjynppzq71'],
		['FFP2 KAI',env('APP_URL').'/products/ffp2-kai/original_certificates','2gyptgn0ti'],
		['FFP2 DAX',env('APP_URL').'/products/ffp2-dax/original_certificates','m0517ed45y'],
		['FFP2 STAR',env('APP_URL').'/products/ffp2-star/original_certificates','cmrq18l456'],
		['FFP3 TANI',env('APP_URL').'/products/ffp3-tani/original_certificates','or1b3xvw6p'],
		['FFP3 JOJO',env('APP_URL').'/products/ffp3-jojo/original_certificates','r2es097km1'],
		['FFP3 LUNA',env('APP_URL').'/products/ffp3-luna/original_certificates','96t291514j'],
		['FFP3 SKYE',env('APP_URL').'/products/ffp3-skye/original_certificates','qz4uq65kj5'],
		['FFP3 TIA',env('APP_URL').'/products/ffp3-tia/original_certificates','guni5guhao'],
		['FFP3 WEI',env('APP_URL').'/products/ffp3-wei/original_certificates','hsf9awn4ca'],
		['FFP3 SUE',env('APP_URL').'/products/ffp3-sue/original_certificates','1v9nvh3hsi'],
		['FFP3 KIM',env('APP_URL').'/products/ffp3-kim/original_certificates','sob3b01v6d'],
		['FFP3 JET',env('APP_URL').'/products/ffp3-jet/original_certificates','try9s2urcg'],
		['FFP3 LEI',env('APP_URL').'/products/ffp3-lei/original_certificates','bk5hgkxbyd'],
		['FFP3 LOU',env('APP_URL').'/products/ffp3-lei/original_certificates','qqhsfztkfy'],
		['FFP3 NAS',env('APP_URL').'/products/ffp3-lei/original_certificates','jbqi11m25d'],
		['FFP3 FINN',env('APP_URL').'/products/ffp3-lei/original_certificates','sc6dpcnolg'],
		['FFP3 MARC',env('APP_URL').'/products/ffp3-lei/original_certificates','5u904ag90y'],
		['FFP3 REID',env('APP_URL').'/products/ffp3-lei/original_certificates','qpnjgulhzy'],
		['FFP3 GEM',env('APP_URL').'/products/ffp3-lei/original_certificates','nscea3hy8e'],
		['FFP3 RIA',env('APP_URL').'/products/ffp3-lei/original_certificates','hzwuph3rs5'],
		['FFP3 DAWN',env('APP_URL').'/products/ffp3-lei/original_certificates','snwinnmeqf'],
		['FFP3 LEE',env('APP_URL').'/products/ffp3-lei/original_certificates','m7suxps7s1'],
		['FFP3 KAT',env('APP_URL').'/products/ffp3-lei/original_certificates','m5bqd2cc67'],
	];

	foreach ($prod as $key => $val) {
		$prod = $val[0];
		$cert = Hash::make($val[2]);

		echo $prod.' = <input type="" value="'.$cert.'"> <br><br>';

	}



})->name('createpassword-many');



Route::get('/createpassword/{password}', function ($password) {

    return Hash::make($password);

})->name('createpassword');













// ADMIN
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if(env('APP_ADMIN')) {


Route::get('/admin/add-product', 'AdminController@addProduct')->name('create_product');
Route::post('/admin/add-product', 'AdminController@addProductPost')->name('create_product_post');

Route::get('/admin/edit-product', 'AdminController@editProduct')->name('edit_product');

Route::get('/admin/edit-product/certificates/{product_code}', 'AdminController@editProductCertificates')->name('edit_product_certificates');
Route::post('/admin/edit-product/certificates', 'AdminController@editProductCertificatesPost')->name('edit_product_certificates_post');
Route::post('/admin/edit-product/certificate-names', 'AdminController@editProductCertificateNamesPost')->name('edit_product_certificate_names_post');

Route::get('/admin/edit-product/main-image/{product_code}', 'AdminController@addProductMainImage')->name('add_product_main_image');
Route::post('/admin/edit-product/main-image', 'AdminController@addProductMainImagePost')->name('add_product_main_image_post');

Route::get('/admin/edit-product/images/{product_code}', 'AdminController@editProductImages')->name('edit_product_images');
Route::post('/admin/edit-product/images', 'AdminController@editProductImagesPost')->name('edit_product_images_post');
Route::post('/admin/add-product/images', 'AdminController@addProductImagesPost')->name('add_product_images_post');

Route::get('/admin/edit-product/content/{product_code}', 'AdminController@editProductContent')->name('edit_product_content');
Route::post('/admin/edit-product/content', 'AdminController@editProductContentPost')->name('edit_product_content_post');

Route::get('/admin/edit-product/datasheets/{product_code}', 'AdminController@editProductDatasheets')->name('edit_product_datasheets');
Route::post('/admin/edit-product/datasheets', 'AdminController@editProductDatasheetsPost')->name('edit_product_datasheets_post');


// Route::get('/admin/test/{product_code}', 'TestController@test');
// Route::get('/admin/test2', 'TestController@test2');
// Route::get('/covid19/test', 'TestController@statsTest');

Route::get('/admin/add-article', 'AdminController@addArticle')->name('add_article');
Route::post('/admin/add-article', 'AdminController@addArticlePost')->name('add_article_post');
Route::get('/admin/edit-article/content/{public_id}', 'AdminController@editArticleContent')->name('edit_article_content');
Route::post('/admin/edit-article/content', 'AdminController@editArticleContentPost')->name('edit_article_content_post');


Route::get('/admin/rename-images/all', 'AdminController@renameAllProductImages')->name('rename_all_product_images');
Route::get('/admin/rename-images/{product_code}', 'AdminController@renameProductImages')->name('rename_product_images');

Route::get('/admin/login', function(Request $rqst){
	$rqst->session()->put('loggedin-status-admin', '1');
	echo "<h1>Admin Login Successful!</h1>";	
});

Route::get('/admin/logout', function(Request $rqst){
	$rqst->session()->forget('loggedin-status-admin');
	echo "<h1>Admin Logged Out!!</h1>";	
});






}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ADMIN




Route::get('/logout', function(Request $rqst){
	$rqst->session()->flush();
});

// Auth::routes();

// Auth::routes();

// Route::get('/home', 'PageController@index')->name('home');



Route::get('errors', function()
{
   echo 'XXXXXXXXXXXXXXXXXXXX';
})->name('errors');




// sadsad